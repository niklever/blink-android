package com.blinktrainingsystem.blink.common.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;

/**
 * Created by TALE on 9/15/2014.
 */
public class SimpleAlertDialogFragment extends DialogFragment {
    private static final String KEY_MSG = "msg";

    public SimpleAlertDialogFragment() {
    }

    public static SimpleAlertDialogFragment newInstance(String msg) {
        final SimpleAlertDialogFragment dialogFragment = new SimpleAlertDialogFragment();
        final Bundle args = new Bundle();
        if (!TextUtils.isEmpty(msg)) {
            args.putString(KEY_MSG, msg);
        }
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final Bundle args = getArguments();
        final String msg = args == null ? null : args.getString(KEY_MSG);
        TextView textView = new TextView(getActivity());
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
        final int padding = getResources().getDimensionPixelSize(R.dimen.double_padding);
        textView.setPadding(padding, padding, padding, padding);
        textView.setGravity(Gravity.CENTER);
        builder.setView(textView);
        if (msg != null) {
            textView.setText(msg);
        }
        builder.setPositiveButton(android.R.string.ok, null);
        return builder.create();
    }
}
