package com.blinktrainingsystem.blink.common.view.anytextview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class AnyEditTextView extends EditText {

    public AnyEditTextView(Context context) {
        this(context, null, 0);

    }

    public AnyEditTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnyEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode()) {
            Util.setTypeface(attrs, this);
        }
    }
}
