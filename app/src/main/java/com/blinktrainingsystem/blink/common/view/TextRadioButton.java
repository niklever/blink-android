package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;

/**
 * Created by TALE on 9/10/2014.
 */
public class TextRadioButton extends TextView implements IRadioButton {

    private final int unCheckedColor;
    private final int checkedColor;
    private int checkedBg;
    private int unCheckedBg;

    public TextRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        checkedColor = getResources().getColor(android.R.color.white);
        unCheckedColor = getResources().getColor(R.color.blue);
        setTextColor(unCheckedColor);
    }

    @Override
    public void setCheck(boolean checked) {
        setTextColor(checked ? checkedColor : unCheckedColor);
        setBackgroundResource(checked ? checkedBg : unCheckedBg);
    }

    public void setCheckedBg(int checkedBg) {
        this.checkedBg = checkedBg;
    }

    public void setUnCheckedBg(int unCheckedBg) {
        this.unCheckedBg = unCheckedBg;
        setBackgroundResource(unCheckedBg);
    }
}
