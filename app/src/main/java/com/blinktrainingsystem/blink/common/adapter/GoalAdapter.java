package com.blinktrainingsystem.blink.common.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.data.model.pojo.Goal;

import javax.inject.Inject;

/**
 * Created by TALE on 9/23/2014.
 */
public class GoalAdapter extends CustomAdapter<Goal> {

    final LayoutInflater layoutInflater;
    private final int textViewLayoutId;
    private final int itemWidth;
    private final int itemHeight;

    @Inject
    public GoalAdapter(LayoutInflater layoutInflater, int textViewLayoutId, int itemWidth, int itemHeight) {
        this.layoutInflater = layoutInflater;
        this.textViewLayoutId = textViewLayoutId;
        this.itemWidth = itemWidth;
        this.itemHeight = itemHeight;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(textViewLayoutId, parent, false);
            convertView.setLayoutParams(new AbsListView.LayoutParams(itemWidth, itemHeight));
        }
        Goal item = getItem(position);
        ((TextView) convertView).setText(item.text);
        return convertView;
    }
}
