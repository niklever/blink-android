package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.blinktrainingsystem.blink.R;

/**
 * Created by TALE on 12/5/2014.
 */
public class WorkHeatLayout extends FrameLayout {

    public WorkHeatLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_wordheat, this);
    }

    @Override protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
}
