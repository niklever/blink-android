package com.blinktrainingsystem.blink.common.activity;

import android.support.v7.app.ActionBarActivity;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by TALE on 7/23/2014.
 */
public class CallbackActionBarActivity extends ActionBarActivity {

    private Callback callback;
    private List<Runnable> runOnResumeTask;

    protected void runAfterResume(Runnable runnable) {
        if (runOnResumeTask == null) {
            runOnResumeTask = new ArrayList<Runnable>();
        }
        runOnResumeTask.add(runnable);
    }

    public void registerCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (callback != null) {
            callback.onResume();
        }
        if (runOnResumeTask != null && runOnResumeTask.size() > 0) {
            for (int i = 0; i < runOnResumeTask.size(); i++) {
                runOnUiThread(runOnResumeTask.get(i));
            }
            runOnResumeTask.clear();
            runOnResumeTask = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (callback != null) {
            callback.onStop();
        }
    }

    @Override
    public void onBackPressed() {
        if (callback != null && callback.onBackPressed()) {
            return;
        }
        Timber.d("Activity's onBackPressed");
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (callback != null) {
            callback.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            callback.onDestroy();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (callback != null) {
            callback.onStart();
        }
    }

    public static class Callback {
        public void onStart() {
        }

        ;

        public void onResume() {
        }

        ;

        public void onPause() {
        }

        ;

        public void onStop() {
        }

        ;

        public void onDestroy() {
        }

        ;

        public boolean onBackPressed() {
            return false;
        }

        ;

    }
}
