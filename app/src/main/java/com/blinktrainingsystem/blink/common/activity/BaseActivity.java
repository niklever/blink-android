/*
 * Copyright (C) 2014 Pedro Vicente Gómez Sánchez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blinktrainingsystem.blink.common.activity;

import android.os.Bundle;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.di.ActivityModule;
import com.google.common.collect.Lists;

import java.util.List;

import butterknife.ButterKnife;
import dagger.ObjectGraph;

/**
 * Base activity created to be extended by every activity in this_ application. This class provides
 * dependency injection configuration, ButterKnife Android library configuration and some methods
 * common to every activity.
 */
public abstract class BaseActivity extends CallbackActionBarActivity {

    private ObjectGraph activityScopeGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayoutId());

        injectDependencies();
        injectViews();
    }

    protected abstract int getContentLayoutId();

    /**
     * Method used to resolve dependencies provided by Dagger modules. Inject an object to provide
     * every @Inject annotation contained.
     *
     * @param object to inject.
     */
    public void inject(Object object) {
        activityScopeGraph.inject(object);
    }

    /**
     * Get a list of Dagger modules with Activity scope needed to this_ Activity.
     *
     * @return modules with new dependencies to provide.
     */
    protected abstract List<Object> getModules();

    /**
     * Create a new Dagger ObjectGraph to add new dependencies using a plus operation and inject the
     * declared one in the activity. This new graph will be destroyed once the activity lifecycle
     * finish.
     * <p/>
     * This is the key of how to use Activity scope dependency injection.
     */
    private void injectDependencies() {
        BlinkApplication tvShowsApplication = (BlinkApplication) getApplication();
        List<Object> activityScopeModules = getModules();
        if (activityScopeModules == null) {
            activityScopeModules = Lists.newArrayList();
        }
        activityScopeModules.add(new ActivityModule(this));
        activityScopeGraph = tvShowsApplication.plus(activityScopeModules);
        inject(this);
    }

    public ObjectGraph getActivityScopeGraph() {
        return activityScopeGraph;
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     */
    private void injectViews() {
        ButterKnife.inject(this);
    }
}
