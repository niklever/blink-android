package com.blinktrainingsystem.blink.common.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.SoundManager;
import com.blinktrainingsystem.blink.util.ViewAnimator;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class HelpView extends RelativeLayout {

    @InjectView(R.id.tvMessage)
    TextView tvMessage;
    @InjectView(R.id.rlMessage)
    RelativeLayout rlMessage;
    @InjectView(R.id.tvCloseNotify)
    TextView _TvCloseNotify;
    boolean isOpened;

    ViewAnimator viewAnimator;

    private EventBus bus;
    private SoundManager soundManager;

    public HelpView(Context context) {
        this(context, null);
    }

    public HelpView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.help, this);
        ButterKnife.inject(this, this);
        viewAnimator = new ViewAnimator();
    }

    public void setSoundManager(SoundManager soundManager) {
        this.soundManager = soundManager;
    }

    public void registerBus(EventBus bus) {
        this.bus = bus;
    }

    public void unregisterBus() {
        if (this.bus != null) {
            this.bus.unregister(this);
            this.bus = null;
        }
    }

    @OnClick(R.id.tvCloseNotify)
    public void closeHelper() {
        _TvCloseNotify.setEnabled(false);
        if (isOpened) {
            soundManager.loadAndPlay(R.raw.window_out);
        }
        viewAnimator.exitBottom(
                rlMessage, Constant.ANIMATION_DURATION, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        viewAnimator.exitFadeOut(
                                HelpView.this, Constant.ANIMATION_DURATION, new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        if (bus != null) {
                                            bus.post(Event.HelpClosed);
                                        }
                                        isOpened = false;
                                    }
                                }
                        );
                    }
                }
        );

    }

    public void showHelp(String message) {
        if (bus == null) {
            return;
        }
        bus.post(Event.HelpOpened);
        if (!isOpened) {
            soundManager.loadAndPlay(R.raw.window_in);
        }
        Timber.d("show help with message %s", message);
        final String displayMsg = message == null ? "" : message;
        tvMessage.setText(Helper.getStringWithReplacements(displayMsg));
        viewAnimator.enterFadeIn(
                this, Constant.ANIMATION_DURATION, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        Timber.d("enter message layout");
                        viewAnimator.enterBottom(
                                rlMessage, Constant.ANIMATION_DURATION, new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        _TvCloseNotify.setEnabled(true);
                                        isOpened = true;
                                    }
                                }
                        );
                    }
                }
        );
    }
}
