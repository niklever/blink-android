package com.blinktrainingsystem.blink.common.adapter;

import android.app.Application;
import android.view.View;
import android.view.ViewGroup;

import com.blinktrainingsystem.blink.common.view.CourseGridItemView;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;


/**
 * Created by TALE on 9/10/2014.
 */
public class CourseAdapter extends CustomAdapter<Course> {

    private final Application application;
    private final EventBus bus;
    private final Picasso picasso;
    private final LocalFileManager localFileManager;
    private final LocalSharedPreferences localSharedPreferences;

    public CourseAdapter(Application application, EventBus bus, Picasso picasso, LocalFileManager localFileManager, LocalSharedPreferences localSharedPreferences) {
        this.application = application;
        this.bus = bus;
        this.picasso = picasso;
        this.localFileManager = localFileManager;
        this.localSharedPreferences = localSharedPreferences;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CourseGridItemView courseGridItemView;
        if (convertView == null) {
            courseGridItemView = new CourseGridItemView(application, bus, picasso, localFileManager, localSharedPreferences);
        } else {
            courseGridItemView = (CourseGridItemView) convertView;
            bus.unregister(courseGridItemView);
        }
        Course course = getItem(position);
        courseGridItemView.bind(course);
        bus.register(courseGridItemView);
        return courseGridItemView;
    }

//    /**
//     * This class contains all butterknife-injected Views & Layouts from layout file 'grid_item_course.xml'
//     * for easy to all layout elements.
//     *
//     * @author ButterKnifeZelezny, plugin for Android Studio by Inmite Developers (http://inmite.github.io)
//     */
//    static class ViewHolder {
//        @InjectView(R.id.ivIcon)
//        ImageView ivIcon;
//        @InjectView(R.id.tvTitle)
//        TextView ivTitle;
//
//        ViewHolder(View view) {
//            ButterKnife.inject(this_, view);
//        }
//
//    }
}
