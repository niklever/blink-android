package com.blinktrainingsystem.blink.common.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.os.Build;
import android.os.Build.VERSION;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Category;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.DownloadProgress;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.google.common.base.Objects;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Created by TALE on 9/15/2014.
 */
public class CourseGridItemView extends RelativeLayout {

    private final LocalSharedPreferences localSharedPreferences;
    @InjectView(R.id.ivIcon)
    ImageView ivIcon;
    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.pbDownloadProgress)
    ProgressBar pbDownloadProgress;
    @InjectView(R.id.ivCloud)
    ImageView ivCloud;
    private final EventBus bus;
    private final Picasso picasso;
    private final LocalFileManager localFileManager;
    @InjectView(R.id.ivLiteStrip)
    ImageView mIvLiteStrip;
    @InjectView(R.id.ivTick)
    ImageView mIvTick;
    @InjectView(R.id.tvDownloading)
    TextView _TvDownloading;
    @InjectView(R.id.llMain)
    RelativeLayout _LlMain;

    private Course course;

    public CourseGridItemView(Context context, EventBus bus, Picasso picasso, LocalFileManager localFileManager, LocalSharedPreferences localSharedPreferences) {
        super(context);
        this.bus = bus;
        this.picasso = picasso;
        this.localFileManager = localFileManager;
        inflate(context, R.layout.grid_item_course, this);
        ButterKnife.inject(this);
        picasso.cancelRequest(ivIcon);
        pbDownloadProgress.setMax(100);
        setLayoutParams(new AbsListView.LayoutParams(getResources().getDimensionPixelSize(R.dimen.course_icon_w), ViewGroup.LayoutParams.WRAP_CONTENT));
        this.localSharedPreferences = localSharedPreferences;
    }

    public void bind(Course course) {
        this.course = course;
        showCloud(course.cloud);
        boolean showLite = Objects.equal(course.folderName, LocalFileManager.Folder.lite.toString());
        showLite(showLite); // Only show lite if the folderName is Lite
        Category category = Objects.equal(course.category, Category.user.toString()) ? Category.user : Category.all;
        boolean showTick = category == Category.all && localFileManager.exists(course.id, LocalFileManager.Folder.full);
        showTick(showTick); // Only show tick if category is not user and the file is already exists in disk.
        File file = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.full);
        boolean downloading = Objects.equal(course.folderName, LocalFileManager.Folder.downloading.toString());
        boolean showDownloading = !showLite && downloading;
        showDownloading(showDownloading); // Only show downloading if the course is not lite and category is user and file is not exists in disk.
        tvTitle.setText(course.title);
        if (!showDownloading) {
            bindIcon();
        }
    }

    private void showCloud(boolean show) {
        if (show){
            _LlMain.setAlpha(0.5f);
            ivCloud.setVisibility(VISIBLE);
        }else{
            _LlMain.setAlpha(1.0f);
            ivCloud.setVisibility(GONE);
        }
    }

    private void showDownloading(boolean show) {
        pbDownloadProgress.setVisibility(show ? VISIBLE : GONE);
        _TvDownloading.setVisibility(show ? VISIBLE : GONE);
        tvTitle.setVisibility(show ? INVISIBLE : VISIBLE);
        ivIcon.setVisibility(show ? INVISIBLE : VISIBLE);

        _LlMain.setBackgroundColor(show ? getResources().getColor(R.color.bg_gray) : getResources().getColor(android.R.color.transparent));
        if (show) {
            File file = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.downloading);
            if (file.exists()) {
                Timber.d("Start download course for " + course.id);
                WorkerService.intent(getContext()).downloadCourse(course.id, LocalFileManager.Folder.full).start();
            }
        }
    }

    private void showTick(boolean show) {
        mIvTick.setVisibility(show ? VISIBLE : GONE);
    }

    private void showLite(boolean show) {
        mIvLiteStrip.setVisibility(show ? VISIBLE : GONE);
    }

    private void bindIcon() {
        String imageUrl = String.format(WebServices.IMAGE_PATH, course.iconURL);
        Timber.d("imageUrl: " + imageUrl);
        picasso.load(imageUrl).into(ivIcon);
    }

    public void onEvent(DownloadProgress downloadProgress) {
        if (course.id == downloadProgress.courseId) {
            Timber.d("onDownloadUpdate: " + downloadProgress.progress);
            if (downloadProgress.progress == 100) {
                postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                bindIcon();
                                showDownloading(false);
                                bus.post(Event.CourseRefresh);
                            }
                        }, 1000
                );
            } else {
                pbDownloadProgress.setProgress(downloadProgress.progress);
            }
        }

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        bus.unregister(this);
    }
}
