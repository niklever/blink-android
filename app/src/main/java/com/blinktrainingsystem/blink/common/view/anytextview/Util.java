package com.blinktrainingsystem.blink.common.view.anytextview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class Util {
    public static Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();

    public static void setTypeface(AttributeSet attrs, TextView textView) {
        Context context = textView.getContext();

        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.AnyTextView);
        String typefaceName = values.getString(R.styleable.AnyTextView_customFont);

        values.recycle();
    }

    public static void setTypeface(TextView textView, String typefaceName) {
        final Typeface textViewTypeface = textView.getTypeface();
        if (typefaceName != null && typefaceName.contains("MyriadPro")) {
            if (textViewTypeface != null) {
                if (textViewTypeface.isBold()) {
                    typefaceName = typefaceName.contains("Regular") ? typefaceName.replace("Regular", "Bold") : typefaceName + "Bold.ttf";
                } else if (textViewTypeface.isItalic()) {
                    typefaceName = typefaceName.contains("Regular") ? typefaceName.replace("Regular", "Italic") : typefaceName + "Italic.ttf";
                }
            }
        }

        if (typefaceCache.containsKey(typefaceName)) {
            textView.setTypeface(typefaceCache.get(typefaceName));
            Timber.d("Found fond %s", typefaceName);
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + typefaceName);
                Timber.d("Found fond %s", typefaceName);
            } catch (Exception e) {
                Timber.e(textView.getContext().getString(R.string.typeface_not_found, typefaceName));
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            textView.setTypeface(typeface);
        }
    }
}