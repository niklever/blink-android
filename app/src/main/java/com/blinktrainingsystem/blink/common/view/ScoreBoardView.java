package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ViewAnimator;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import timber.log.Timber;

/**
 * Created by TALE on 9/22/2014.
 */
public class ScoreBoardView extends RelativeLayout {

    @InjectView(R.id.ivGem1)
    ImageView ivGem1;
    @InjectView(R.id.ivGem2)
    ImageView ivGem2;
    @InjectView(R.id.ivGem3)
    ImageView ivGem3;
    @InjectView(R.id.ivGem4)
    ImageView ivGem4;
    @InjectView(R.id.ivGem5)
    ImageView ivGem5;
    @InjectView(R.id.ivGem6)
    ImageView ivGem6;
    @InjectView(R.id.ivGem7)
    ImageView ivGem7;
    @InjectView(R.id.ivGem8)
    ImageView ivGem8;
    @InjectView(R.id.tvMyScoreForThisBurst)
    TextView tvMyScoreForThisBurst;
    @InjectView(R.id.tvPointsLeftInBurst)
    TextView tvPointsLeftInBurst;
    @InjectView(R.id.tvTargetToUnLockNextBurst)
    TextView tvTargetToUnLockNextBurst;
    @InjectView(R.id.tvTargetToUnLockNextBurstLabel)
    TextView tvTargetToUnLockNextBurstLabel;
    @InjectView(R.id.llText)
    LinearLayout llText;
    EventBus bus;
    private int gemsInThisBurst;

    public ScoreBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.score_board, this);
        ButterKnife.inject(this);
    }


    public void setMaxGems(int maxGems) {
        initGemsView(maxGems);
    }


    public void setScore(long scoreThisBurst, int targetToUnlock, int gems) {
        tvMyScoreForThisBurst.setText(String.valueOf(scoreThisBurst));
        if (targetToUnlock > 0 && scoreThisBurst >= targetToUnlock) {
            tvTargetToUnLockNextBurstLabel.setText("Next Burst unlocked");
            tvTargetToUnLockNextBurst.setVisibility(GONE);
        } else {
            tvTargetToUnLockNextBurstLabel.setText(R.string.txt_target_to_unlock_next_burst);
            tvTargetToUnLockNextBurst.setText(String.valueOf(targetToUnlock));
            tvTargetToUnLockNextBurst.setVisibility(VISIBLE);
        }
        updateGemView(gems);
    }

    private int updated = 0;

    private void updateGemView(int gems) {
        if (gems > 8 || gems == updated) {
            return;
        }

        if (gems < updated) {
            for (int i = gems; i < updated; i++) {
                ImageView gemView = getGemViewByIndex(i);
                gemView.setImageResource(R.drawable.gem_btn);
            }
        } else {
            for (int i = gems - 1; i >= updated; i--) {
                ImageView gemView = getGemViewByIndex(i);
                int gemId = Helper.randomGemId();
                gemView.setImageResource(gemId);
                Timber.d("updateGemView => gemsInThisBurst: %d, gems: %d", gemsInThisBurst, gems);
                if (gemsInThisBurst < gems) {
                    ViewAnimator.blink(gemView, 6, 700, null);
                    bus.post(Event.GainGem);
                }

            }
        }
        updated = gems;
    }

    public void setBus(EventBus bus) {
        this.bus = bus;
    }

    private ImageView getGemViewByIndex(int index) {
        switch (index) {
            case 7:
                return ivGem8;
            case 6:
                return ivGem7;
            case 5:
                return ivGem6;
            case 4:
                return ivGem5;
            case 3:
                return ivGem4;
            case 2:
                return ivGem3;
            case 1:
                return ivGem2;
            case 0:
                return ivGem1;
        }
        return null;
    }

    private void initGemsView(int maxGems) {
        ivGem8.setVisibility(maxGems > 7 ? VISIBLE : GONE);
        ivGem7.setVisibility(maxGems > 6 ? VISIBLE : GONE);
        ivGem6.setVisibility(maxGems > 5 ? VISIBLE : GONE);
        ivGem5.setVisibility(maxGems > 4 ? VISIBLE : GONE);
        ivGem4.setVisibility(maxGems > 3 ? VISIBLE : GONE);
        ivGem3.setVisibility(maxGems > 2 ? VISIBLE : GONE);
        ivGem2.setVisibility(maxGems > 1 ? VISIBLE : GONE);
        ivGem1.setVisibility(maxGems > 0 ? VISIBLE : GONE);
    }

    public void updateGem(int gems) {

    }

    public void setGemsInThisBurst(int gemsInThisBurst) {
        Timber.d("setGemsInThisBurst => gems: %d", gemsInThisBurst);
        this.gemsInThisBurst = gemsInThisBurst;
    }
}
