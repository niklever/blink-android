package com.blinktrainingsystem.blink.common.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.dialog.AlertDialogFragment;
import com.blinktrainingsystem.blink.common.dialog.ProgressDialogFragment;
import com.blinktrainingsystem.blink.util.ViewAnimator;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Base fragment created to be extended by every fragment in this_ application. This class provides
 * dependency injection configuration, ButterKnife Android library configuration and some methods
 * common to every fragment.
 */
public abstract class StructureFragment extends DaggerFragment {

    // We just can call injectView after all views is inflated. So, there fields are used before
    // child views are inflated need to be inject manual.
    RelativeLayout rlHeader;
    RelativeLayout rlContent;
    RelativeLayout rlFooter;


    @Inject
    ViewAnimator viewAnimator;
    private boolean isStopped;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View localView = inflater.inflate(R.layout.fragment_base, container, false);

        rlHeader = (RelativeLayout) localView.findViewById(R.id.header);
        rlContent = (RelativeLayout) localView.findViewById(R.id.content);
        rlFooter = (RelativeLayout) localView.findViewById(R.id.footer);
        View divider = localView.findViewById(R.id.topDivider);

//        llMenuContainer = (LinearLayout) localView.findViewById(R.id.llMenuContainer);

        int headerLayoutId = getHeaderLayoutId();
        if (headerLayoutId != 0) {
            inflater.inflate(headerLayoutId, rlHeader, true);
        } else {
            rlHeader.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
        }

        int fragmentLayoutId = getFragmentLayoutId();
        if (fragmentLayoutId != 0) {
            inflater.inflate(fragmentLayoutId, rlContent, true);
        } else {
            rlContent.setVisibility(View.GONE);
        }


        int footerLayoutId = getFooterLayoutId();
        if (footerLayoutId != 0) {
            inflater.inflate(footerLayoutId, rlFooter, true);
        } else {
            rlFooter.setVisibility(View.GONE);
        }

        return localView;
    }

    @Override
    public void onStart() {
        super.onStart();
        isStopped = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isStopped = true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
    }

    protected abstract int getMenuLayoutId();

    protected abstract int getHeaderLayoutId();

    protected abstract int getFooterLayoutId();

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this_ method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this_ method when extends BaseFragment.
     */
    protected abstract int getFragmentLayoutId();

    protected boolean isShowStopWatch() {
        return false;
    }

    protected int getMenuBackgroundColor() {
        return getResources().getColor(R.color.menu_bg_blue);
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     *
     * @param view to extract each widget injected in the fragment.
     */
    private void injectViews(final View view) {
        ButterKnife.inject(this, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    int progressCount = 0;

    public void dismissProgress() {
        progressCount--;
        if (progressCount < 0) {
            progressCount = 0;
        }
        Timber.d(getClass().getSimpleName() + " ==> Dismiss: " + progressCount);
        if (progressCount > 0 || isStopped) {
            return;
        }
        DialogFragment progressDialog = (DialogFragment) getFragmentManager().findFragmentByTag("progress");
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showProgress(String loadingMsg) {
        Timber.d(getClass().getSimpleName() + " ==> showProgress: " + loadingMsg + " progressCount: " + progressCount + " isStoppted: " + isStopped);
        if (progressCount == 0 && !isStopped) {
            DialogFragment progressDialog = (DialogFragment) getFragmentManager().findFragmentByTag("progress");
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            progressDialog = ProgressDialogFragment.newInstance(loadingMsg);
            progressDialog.show(getFragmentManager(), "progress");
        }
        progressCount++;
    }


    public void showAlert(String title, String message) {
        if (isStopped) {
            return;
        }
        DialogFragment progressDialog = (DialogFragment) getFragmentManager().findFragmentByTag("alert");
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = AlertDialogFragment.newInstance(title, message);
        progressDialog.show(getFragmentManager(), "alert");
    }
}
