package com.blinktrainingsystem.blink.common.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blinktrainingsystem.blink.util.Helper;

import javax.inject.Inject;

/**
 * Created by TALE on 9/23/2014.
 */
public class StringGridAdapter extends CustomAdapter<String> {

    final LayoutInflater layoutInflater;
    private final int textViewLayoutId;

    @Inject
    public StringGridAdapter(LayoutInflater layoutInflater, int textViewLayoutId) {
        this.layoutInflater = layoutInflater;
        this.textViewLayoutId = textViewLayoutId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(textViewLayoutId, parent, false);
        }
        String item = getItem(position);
        item = item == null ? "" : Helper.getStringWithReplacements(item);
        ((TextView) convertView).setText(item);
        return convertView;
    }
}
