package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by TALE on 10/1/2014.
 */
public class ScoreView extends LinearLayout {

    @InjectView(R.id.tvScoreLabel)
    TextView _TvScoreLabel;
    @InjectView(R.id.tvScore)
    TextView _TvScore;
    @InjectView(R.id.pbScore)
    ProgressBar _PbScore;

    public ScoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.score, this);
        ButterKnife.inject(this);
    }

}
