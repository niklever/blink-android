package com.blinktrainingsystem.blink.common.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.PurchaseItem;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;

import java.util.ArrayList;

import rx.functions.Action1;

/**
 * Created by TALE on 9/16/2014.
 */
public class PurchasePackagesDialog extends DialogFragment {

    private static final String KEY_TITLE = "key_title";
    private static final String KEY_ITEMS = "key_items";
    private LayoutInflater layoutInflater;

    public static interface OnItemSelectedListener {
        void onItemSelected(PurchaseItem purchaseItem);
    }

    private OnItemSelectedListener onItemSelectedListener;

    public static PurchasePackagesDialog newInstance(String message, ArrayList<PurchaseItem> purchaseItems) {
        PurchasePackagesDialog dialogFragment = new PurchasePackagesDialog();
        final Bundle args = new Bundle();
        args.putString(KEY_TITLE, message);
        args.putParcelableArrayList(KEY_ITEMS, purchaseItems);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        layoutInflater = LayoutInflater.from(getActivity());

        final int padding = getResources().getDimensionPixelSize(R.dimen.double_padding);
        View view = layoutInflater.inflate(R.layout.dialog_purchase_package, null);
        final int width = getResources().getDisplayMetrics().widthPixels - 2 * padding;

        final int itemWidth = (width - 6 * padding) / 5;

        ViewObservableHelper.globalLayoutFrom(view).subscribe(
                new Action1<View>() {
                    @Override public void call(View view) {
                        view.getLayoutParams().width = width;
                        view.requestLayout();
                    }
                }
        );

        String message = args.getString(KEY_TITLE) == null ? null : args.getString(KEY_TITLE);
        if (message != null) {
            TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
        }
        final LinearLayout llPackageList = (LinearLayout) view.findViewById(R.id.llPackageList);
        final ArrayList<PurchaseItem> purchaseItems = args.getParcelableArrayList(KEY_ITEMS);
        for (int i = 0, count = purchaseItems.size(); i < count; i++) {
            final PurchaseItem purchaseItem = purchaseItems.get(i);
            View purchaseItemView = newPurchaseItemView(purchaseItem, llPackageList, itemWidth);
            llPackageList.addView(purchaseItemView);
            if (i > 0) {
                ((LinearLayout.LayoutParams) purchaseItemView.getLayoutParams()).leftMargin = padding;
            }
        }

        // Close click.
        view.findViewById(R.id.btClose).setOnClickListener(
                new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        dismiss();
                    }
                }
        );
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setContentView(view);
        return dialog;
    }

    private View newPurchaseItemView(final PurchaseItem purchaseItem, ViewGroup parent, final int width) {
        final View view = layoutInflater.inflate(R.layout.item_purchase, parent, false);
//        ViewObservableHelper.globalLayoutFrom(view).subscribe(
//                new Action1<View>() {
//                    @Override public void call(View view) {
//                        final int viewWidth = view.getWidth();
//                        float scale = (float) width / viewWidth;
//                        view.setScaleX(scale);
//                        view.setScaleY(scale);
//                    }
//                }
//        );
//        int padding = getResources().getDimensionPixelSize(R.dimen.base_padding);
//        view.setLayoutParams(new ViewGroup.LayoutParams(width, 2 * width));
//        view.findViewById(R.id.bg).getLayoutParams().width = width - 2 * padding;
//        view.findViewById(R.id.bg).getLayoutParams().height = width - 2 * padding;
        final TextView tvCredit = (TextView) view.findViewById(R.id.tvCredit);
        tvCredit.setText(String.valueOf(purchaseItem.credits));
        final TextView tvPrice = (TextView) view.findViewById(R.id.btPrice);
        final int color = Color.parseColor("#256ED7");
        final Drawable roundedDrawable = UiHelper.createRoundedDrawable(color, 5f, tvPrice.getContext().getResources().getDisplayMetrics().density);
        CompatibleHelper.setBackgroundDrawable(tvPrice, roundedDrawable);
        tvPrice.setText("$" + purchaseItem.price);
        view.setOnClickListener(
                new View.OnClickListener() {
                    @Override public void onClick(View v) {
                        dismiss();
                        if (onItemSelectedListener != null) {
                            onItemSelectedListener.onItemSelected(purchaseItem);
                        }
                    }
                }
        );
        return view;
    }
}
