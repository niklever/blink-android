package com.blinktrainingsystem.blink.common.adapter;

import android.app.Application;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.util.Helper;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by talenguyen on 22/09/2014.
 */
public class BurstGridAdapter extends CustomAdapter<Blink> {

    final LayoutInflater layoutInflater;
    final Application application;
    public int unlockIndex = 0;
    public BlinkHandler blinkHandler;
    public long courseId;
    public boolean allowSkip;

    @Inject
    public BurstGridAdapter(LayoutInflater layoutInflater, Application application) {
        this.layoutInflater = layoutInflater;
        this.application = application;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_burst_grid, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        Blink item = getItem(i);
        viewHolder._TvText.setText(item.summary);
        if (i > unlockIndex && !allowSkip) {
            view.setAlpha(0.5f);
            viewHolder._IvLock.setVisibility(View.VISIBLE);
            viewHolder.llGems.setVisibility(View.INVISIBLE);
        } else {
            view.setAlpha(1);
            viewHolder._IvLock.setVisibility(View.GONE);
            Timber.d("Item{id=%d, summary=%s}", item.id, item.summary);
            if (courseId > 0 && blinkHandler != null) {
                final int burstIndex = blinkHandler.getBlinkIndex(item);
                // For debugging purpose.
                final Blink blink = blinkHandler.get(burstIndex);

                final int maxGemsInThisBurst = blinkHandler.getMaxGemsInThisBurst(burstIndex);
                final int userGems = blinkHandler.getUserGemsInBurst(burstIndex, courseId);
                Timber.d("Blink at index %d has title is %s and have %d gems", burstIndex, blink == null ? "" : blink.summary, maxGemsInThisBurst);
                if (maxGemsInThisBurst > 0) {
                    viewHolder.llGems.removeAllViews();
                    for (int j = 0; j < maxGemsInThisBurst; j++) {
                        final ImageView imageView = newGemView();
                        imageView.setImageResource(R.drawable.gem_btn);
                        if (j < userGems) {
                            imageView.setImageResource(Helper.randomGemId());
                        }
                        viewHolder.llGems.addView(imageView);
                        if (j < maxGemsInThisBurst - 1) {
                            ((LinearLayout.LayoutParams) imageView.getLayoutParams()).rightMargin = application.getResources().getDimensionPixelSize(R.dimen.padding_small);
                        }
                    }
                }

            }
        }

        return view;
    }

    public ImageView newGemView() {
        final int size = application.getResources().getDimensionPixelSize(R.dimen.grid_gem_size);
        ImageView imageView = new ImageView(application);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(size, size));
        return imageView;
    }

    static class ViewHolder {
        @InjectView(R.id.tvText)
        TextView _TvText;
        @InjectView(R.id.ivLock)
        ImageView _IvLock;
        @InjectView(R.id.llGems)
        LinearLayout llGems;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            final int lockSize = view.getResources().getDimensionPixelSize(R.dimen.grid_gem_size);
            _IvLock.getLayoutParams().width = lockSize;
            _IvLock.getLayoutParams().height = lockSize;
        }


    }
}
