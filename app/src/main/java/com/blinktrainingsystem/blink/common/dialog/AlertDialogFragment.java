package com.blinktrainingsystem.blink.common.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

/**
 * Created by TALE on 9/15/2014.
 */
public class AlertDialogFragment extends DialogFragment {
    private static final String KEY_TITLE = "title";
    private static final String KEY_MSG = "msg";

    public AlertDialogFragment() {
    }

    public static AlertDialogFragment newInstance(String title, String msg) {
        final AlertDialogFragment dialogFragment = new AlertDialogFragment();
        final Bundle args = new Bundle();
        if (!TextUtils.isEmpty(msg)) {
            args.putString(KEY_MSG, msg);
        }
        if (!TextUtils.isEmpty(title)) {
            args.putString(KEY_TITLE, title);
        }
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final Bundle args = getArguments();
        final String msg = args == null ? null : args.getString(KEY_MSG);
        if (msg != null) {
            builder.setMessage(msg);
        }
        final String title = args == null ? null : args.getString(KEY_TITLE);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setPositiveButton(android.R.string.ok, null);
        return builder.create();
    }
}
