package com.blinktrainingsystem.blink.common.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

/**
 * Created by TALE on 8/29/2014.
 */
public class ProgressDialogFragment extends DialogFragment {

    private static final String KEY_MSG = "msg";

    public ProgressDialogFragment() {
    }

    public static ProgressDialogFragment newInstance(String msg) {
        final ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        final Bundle args = new Bundle();
        if (!TextUtils.isEmpty(msg)) {
            args.putString(KEY_MSG, msg);
        }
        progressDialogFragment.setArguments(args);
        return progressDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        final Bundle args = getArguments();
        final String msg = args == null ? null : args.getString(KEY_MSG);
        if (msg != null) {
            dialog.setMessage(msg);
        }
        return dialog;
    }

}
