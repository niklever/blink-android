package com.blinktrainingsystem.blink.common.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import java.io.Serializable;

/**
 * Created by talenguyen on 19/09/2014.
 */
public class ConfirmDialogFragment extends DialogFragment {

    private static final String KEY_MESSAGE = "key_message";
    private static final String KEY_NEGATIVE_TEXT = "key_negative_text";
    private static final String KEY_POSITIVE_TEXT = "key_positive_text";
    private static final String KEY_LISTENER = "key_listener";

    public static interface ConfirmDialogListener extends Serializable {

        public void onPositiveClick();

        public void onNegativeClick();

    }

    public static class Builder {
        public final Bundle args;

        public Builder() {
            args = new Bundle();
        }

        public Builder setMessage(String message) {
            args.putString(KEY_MESSAGE, message);
            return this;
        }

        public Builder setPositiveText(String text) {
            args.putString(KEY_POSITIVE_TEXT, text);
            return this;
        }

        public Builder setNegativeText(String text) {
            args.putString(KEY_NEGATIVE_TEXT, text);
            return this;
        }

        public Builder setListener(ConfirmDialogListener listener) {
            args.putSerializable(KEY_LISTENER, listener);
            return this;
        }

        public ConfirmDialogFragment build() {
            ConfirmDialogFragment confirmDialogFragment = new ConfirmDialogFragment();
            confirmDialogFragment.setArguments(args);
            return confirmDialogFragment;
        }

        public ConfirmDialogFragment show(FragmentManager fragmentManager, String tag) throws NullPointerException {
            if (fragmentManager == null) {
                throw new NullPointerException("FragmentManager can not be null");
            }
            if (tag == null) {
                throw new NullPointerException("tag can not be null");
            }

            ConfirmDialogFragment confirmDialogFragment = (ConfirmDialogFragment) fragmentManager.findFragmentByTag(tag);
            if (confirmDialogFragment != null) {
                confirmDialogFragment.dismiss();
                confirmDialogFragment = null;
            }
            confirmDialogFragment = build();
            confirmDialogFragment.show(fragmentManager, tag);
            return confirmDialogFragment;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        final String message = arguments == null ? null : arguments.getString(KEY_MESSAGE);
        final String positiveText = arguments == null ? null : arguments.getString(KEY_POSITIVE_TEXT);
        final String negativeText = arguments == null ? null : arguments.getString(KEY_NEGATIVE_TEXT);
        final ConfirmDialogListener listener = arguments == null ? null : (ConfirmDialogListener) arguments.getSerializable(KEY_LISTENER);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (message != null) {
            builder.setMessage(message);
        }
        if (positiveText != null) {
            DialogInterface.OnClickListener positiveOnClickListener = listener == null ? null : new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    listener.onPositiveClick();
                }
            };
            builder.setPositiveButton(positiveText, positiveOnClickListener);
        }
        if (negativeText != null) {
            DialogInterface.OnClickListener negativeOnClickListener = listener == null ? null : new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    listener.onNegativeClick();
                }
            };
            builder.setNegativeButton(negativeText, negativeOnClickListener);
        }
        return builder.create();
    }
}
