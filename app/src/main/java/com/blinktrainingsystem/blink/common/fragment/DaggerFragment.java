package com.blinktrainingsystem.blink.common.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.blinktrainingsystem.blink.common.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

/**
 * Base fragment created to be extended by every fragment in this_ application. This class provides
 * dependency injection configuration, ButterKnife Android library configuration and some methods
 * common to every fragment.
 */
public abstract class DaggerFragment extends Fragment {

    protected Handler mHandler = new Handler(Looper.getMainLooper());

    private CompositeSubscription compositeSubscription;
    public ObjectGraph fragmentObjectGraph;
    private List<Runnable> pendingRunnables;

    @Override
    public void onPause() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
            compositeSubscription.clear();
            compositeSubscription = null;
        }
        if (pendingRunnables != null && pendingRunnables.size() > 0) {
            for (Runnable pendingRunnable : pendingRunnables) {
                mHandler.removeCallbacks(pendingRunnable);
            }
        }
        super.onPause();
        Timber.d("onPause");
    }

    protected void takeCareSubscription(Subscription subscription) {
        if (compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Timber.d("onSaveInstanceState");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectDependencies();
        Timber.d("onViewCreated");
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.d("onResume()");
    }

    @Override public void onStart() {
        super.onStart();
        Timber.d("onStart");
    }

    @Override public void onStop() {
        super.onStop();
        Timber.d("onStop");
    }

    @Override public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Timber.d("onViewStateRestored");
    }

    /**
     * Replace every field annotated using @Inject annotation with the provided dependency specified
     * inside a Dagger module value.
     */
    private void injectDependencies() {
        List<Object> modules = getModules();
        if (modules == null) {
            fragmentObjectGraph = ((BaseActivity) getActivity()).getActivityScopeGraph();
        } else {
            fragmentObjectGraph = ((BaseActivity) getActivity()).getActivityScopeGraph().plus(modules.toArray());
        }
        fragmentObjectGraph.inject(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragmentObjectGraph = null;
    }

    protected abstract List<Object> getModules();

    protected void postMainThread(Runnable runnable) {
        mHandler.post(runnable);
    }

    protected void postMainThread(final Runnable runnable, long delayMillis) {
        if (pendingRunnables == null) {
            pendingRunnables = new ArrayList<Runnable>();
        }
        Runnable future = new Runnable() {
            @Override
            public void run() {
                pendingRunnables.remove(this);
                runnable.run();
            }
        };
        pendingRunnables.add(future);
        mHandler.postDelayed(future, delayMillis);
    }
}
