package com.blinktrainingsystem.blink.common.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;

import java.io.Serializable;

/**
 * Created by TALE on 9/16/2014.
 */
public class BuyCourseConfirmationDialog extends DialogFragment {

    private static final String KEY_LISTENER = "listener";
    private static final String KEY_TITLE = "key_title";

    public static interface ConfirmationButtonOnClickListener extends Serializable {
        void onCancel();

        void onOK();
    }

    private ConfirmationButtonOnClickListener confirmationButtonOnClickListener;

    public static BuyCourseConfirmationDialog newInstance(String message, ConfirmationButtonOnClickListener confirmationButtonOnClickListener) {
        BuyCourseConfirmationDialog dialogFragment = new BuyCourseConfirmationDialog();
        final Bundle args = new Bundle();
        args.putSerializable(KEY_LISTENER, confirmationButtonOnClickListener);
        args.putString(KEY_TITLE, message);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        confirmationButtonOnClickListener = args == null ? null : (ConfirmationButtonOnClickListener) args.getSerializable(KEY_LISTENER);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_buy_course_confirmation, null);
        String message = args.getString(KEY_TITLE) == null ? null : args.getString(KEY_TITLE);
        if (message != null) {
            TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
        }
        view.findViewById(R.id.btCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (confirmationButtonOnClickListener != null) {
                    confirmationButtonOnClickListener.onCancel();
                }
            }
        });
        view.findViewById(R.id.btOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (confirmationButtonOnClickListener != null) {
                    confirmationButtonOnClickListener.onOK();
                }
            }
        });
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setContentView(view);
        return dialog;
    }
}
