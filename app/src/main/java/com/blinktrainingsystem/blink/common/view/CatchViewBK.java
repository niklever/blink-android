package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.util.Helper;

import java.io.File;
import java.util.Random;

import timber.log.Timber;

/**
 * Created by TALE on 10/10/2014.
 */
public class CatchViewBK extends FrameLayout {
    public boolean correct;
    public int slot;
    public double prop;
    public boolean inSlot;
    public int bonus;
    public double duration;
    public long createTime;
    public int targetX;
    public int origin_x;
    public int origin_y;
    public Random random;
    public ImageView imageView;

    TextView lbl;
    private int parentWidth;
    private int parentHeight;

    public static interface Delegate {
        public void removeCatchView(CatchViewBK catchView);

        public void catchViewPressed(CatchViewBK catchView);
    }

    Delegate delegate;

    public CatchViewBK(Context context) {
        super(context);
    }

    public CatchViewBK(Context context, String filePath, int parentWidth, int parentHeight, boolean correct, double speed, int slot, int bonus, Delegate delegate)

    {
        super(context);
        this.parentWidth = parentWidth;
        this.parentHeight = parentHeight;
        this.bonus = bonus;
        ImageView tmpImg = new ImageView(context);
        tmpImg.setImageURI(Uri.fromFile(new File(filePath))); // TODO: Check if image can not load.

        this.addView(tmpImg);
        this.correct = correct;
        this.duration = (speed * 1000 / 10) * 25.0;
        this.slot = slot;
        this.createTime = System.currentTimeMillis();
        this.targetX = (int) (System.currentTimeMillis() % parentWidth);
        this.inSlot = false;
        this.delegate = delegate;
        setTranslationX(parentWidth / 2);
        setTranslationY(parentHeight / 2);
        update();
        setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        catchView();
                    }
                }
        );
    }


    public CatchViewBK(Context context, String text, int parentWidth, int parentHeight, boolean correct, double speed, int slot, int bonus, Delegate delegate, boolean isTextOnly) {
        super(context);
        this.parentWidth = parentWidth;
        this.parentHeight = parentHeight;
        this.bonus = bonus;

        ImageView tmpImg = new ImageView(context);
        int drawableResourceId = this.getResources().getIdentifier("catch" + (bonus + 1) + ".png", "drawable", "uk.co.catalystpics.blink");
        tmpImg.setImageResource(drawableResourceId);

        int padding = 25;

        this.addView(tmpImg);

        lbl = new TextView(context);
        lbl.setTextColor(Color.WHITE);
        lbl.setGravity(Gravity.CENTER);
        lbl.setText(Helper.getStringWithReplacements(text));
        this.addView(lbl);
        this.correct = correct;
        duration = (speed * 1000 / 10.0) * padding;
        this.slot = slot;
        this.inSlot = false;
        this.createTime = System.currentTimeMillis();
        this.targetX = (int) (System.currentTimeMillis() % parentWidth);
        this.delegate = delegate;
        setTranslationX(parentWidth / 2);
        setTranslationY(parentHeight / 2);

        update();
    }

    public void update() {
        if (this.inSlot) return;
        double elapsedTime = (System.currentTimeMillis() - this.createTime);
        elapsedTime = elapsedTime / 3;
        Timber.d("elapsedTime: %f, createTime:%d, duration, %f", elapsedTime, createTime, duration);
        prop = elapsedTime / duration;
        if (prop > 1.0) {
            double alpha = 1.0 - ((elapsedTime - duration) / 0.2);
            if (alpha < 0.0) {
                Timber.d("prop:%f, alpha:%f", prop, alpha);
                delegate.removeCatchView(this);
                return;
            } else {
                this.setAlpha((float) alpha);
            }
        }

        int firstTy = 50;
        int secondTy = 300;

        double tx = prop * targetX;
        double t = prop * 6.0;
        double ty = firstTy * t * t - secondTy * t + parentHeight;

        Timber.d("targetX: %d, prop: %f, t: %f, tx: %f, ty: %f", targetX, prop, t, tx, ty);
        setScaleX((float) prop);
        setScaleY((float) prop);
        setTranslationX((float) tx);
        setTranslationY((float) ty);

    }

    private double calculateX(double tx) {
        final int width = parentWidth / 2;
        final int a1 = width - targetX;
        final int a2 = width - a1;
        return width - (tx * a2) / a1;
    }

    private void catchView() {
        delegate.catchViewPressed(this);
    }

}
