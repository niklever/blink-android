package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observable;
import rx.android.observables.ViewObservable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by TALE on 10/22/2014.
 */
public class MenuView extends LinearLayout {

    @InjectView(R.id.tvUnits)
    public TextView _TvUnits;
    @InjectView(R.id.tvActivities)
    public TextView _TvActivities;
    @InjectView(R.id.tvDossiers)
    public TextView _TvDossiers;
    @InjectView(R.id.tvMyCourse)
    public TextView _TvMyCourse;
    @InjectView(R.id.tvReturnToCourse)
    public TextView _TvReturnToCourse;
    @InjectView(R.id.llReturnToCourse)
    public LinearLayout _LlReturnToCourse;

    public Observable<TextView> unitClickObservable;
    public Observable<TextView> activitiesClickObservable;
    public Observable<TextView> dossiersClickObservable;
    public Observable<TextView> myCoursesClickObservable;
    public Observable<TextView> returnToCourseClickObservable;
    public Observable<TextView> quickLinkClickObservable;
    @InjectView(R.id.llActivities)
    LinearLayout _LlActivities;
    @InjectView(R.id.tvQuickLink)
    TextView _TvQuickLink;
    @InjectView(R.id.llQuickLink)
    LinearLayout _LlQuickLink;

    public MenuView(Context context) {
        this(context, null);
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUi();
        initEvents();
    }

    private void initEvents() {
        unitClickObservable = ViewObservable.clicks(_TvUnits, false).observeOn(AndroidSchedulers.mainThread());
        activitiesClickObservable = ViewObservable.clicks(_TvActivities, false).observeOn(AndroidSchedulers.mainThread());
        dossiersClickObservable = ViewObservable.clicks(_TvDossiers, false).observeOn(AndroidSchedulers.mainThread());
        myCoursesClickObservable = ViewObservable.clicks(_TvMyCourse, false).observeOn(AndroidSchedulers.mainThread());
        returnToCourseClickObservable = ViewObservable.clicks(_TvReturnToCourse, false).observeOn(AndroidSchedulers.mainThread());
        quickLinkClickObservable = ViewObservable.clicks(_TvQuickLink, false).observeOn(AndroidSchedulers.mainThread());
    }

    private void initUi() {
        inflate(getContext(), R.layout.menu_1, this);
        ButterKnife.inject(this);
        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(VERTICAL);
        final int padding = getResources().getDimensionPixelSize(R.dimen.base_padding);
        setPadding(padding, padding, padding, padding);
    }

    public void setUnlockActivity(boolean unlocked) {
        _LlActivities.setVisibility(unlocked ? VISIBLE : GONE);
    }

    public void setEnableQuickLink(boolean enable) {
        _LlQuickLink.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    public void setQuickLink(String text) {
        _TvQuickLink.setText(text);
    }
}
