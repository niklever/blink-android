package com.blinktrainingsystem.blink.common.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.util.SoundManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;

/**
 * Created by TALE on 10/1/2014.
 */
public class StopWatchView extends RelativeLayout {
    @InjectView(R.id.ccStopWatchProgress)
    CircularCounter _CcStopWatchProgress;
    @InjectView(R.id.ivStopWatch)
    ImageView _IvStopWatch;
    @InjectView(R.id.tvProgress)
    public TextView _TvProgress;

    SoundManager soundManager;

    //    private int progress;
    private EventBus bus;
    private int timer = -1;
    private Runnable updateTimer = new Runnable() {
        @Override
        public void run() {
            timer--;
            update(timer);
            if (timer > 0) {
                postDelayed(this, 1000);
            }
        }
    };

    public void setSoundManager(SoundManager soundManager) {
        this.soundManager = soundManager;
    }

    public StopWatchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.stopwatch, this);
        ButterKnife.inject(this);
        TypedValue typedValue = new TypedValue();
        getResources().getValue(R.dimen.w_draw_cricular, typedValue, true);
        float myFloatValue = typedValue.getFloat();
        _CcStopWatchProgress.setFirstWidth(myFloatValue)
                .setFirstColor(Color.BLUE)
                .setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    public void setBus(EventBus bus) {
        this.bus = bus;
    }


    public void configure(int timer) {
        _CcStopWatchProgress.setRange(timer);
        this.timer = timer;
        update(timer);
    }


    public void pause() {
        soundManager.loadAndPlay(R.raw.timer_stop);
        removeCallbacks(updateTimer);
    }


    public void start() {
        soundManager.loadAndPlay(R.raw.timer_start);
        update(timer);
        removeCallbacks(updateTimer);
        postDelayed(updateTimer, 1000);
    }


    public void addTime(int addTime) {
        this.timer += addTime;
        if (timer > _CcStopWatchProgress.getRange()) {
            _CcStopWatchProgress.setRange(timer);
        }
    }


    public void update(final int progress) {
        if (timer < 0) {
            return;
        }
        final String text = String.valueOf(progress);
        _TvProgress.setText(text);
        _CcStopWatchProgress.setValues(progress, progress, progress);
        if (timer == 0) {
            soundManager.loadAndPlay(R.raw.timer_stop);
            if (bus != null) {
                bus.post(Event.TimeOut);
            }
            timer = -1;
        }
    }
}
