package com.blinktrainingsystem.blink.common.view.anytextview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.Answer;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.UiHelper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by TALE on 10/7/2014.
 */
public class ItemMultipleView extends LinearLayout {

    @InjectView(R.id.ivIcon)
    ImageView _IvIcon;
    @InjectView(R.id.tvText)
    TextView _TvText;

    private UiHelper uiHelper;

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
        if (answer != null) {
            setText(answer.text);
        }
    }

    private Answer answer;

    public boolean getSelected() {
        return selected;
    }

    private boolean selected;
    private Drawable roundedSolidNormal;
    private Drawable roundedStrokeNormal;
    private Drawable roundedStrokeWrong;
    private Drawable roundedStrokeRight;
    private Drawable roundedStrokeBg;

    public ItemMultipleView(Context context) {
        this(context, null);
    }

    public ItemMultipleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.item_multiple, this);
        ButterKnife.inject(this);
        Util.setTypeface(_TvText, getResources().getString(R.string.font_myriad_pro_regular));
    }

    public void setUiHelper(UiHelper uiHelper) {
        this.uiHelper = uiHelper;
        roundedSolidNormal = uiHelper.createRoundedDrawable(Color.parseColor("#B0B6C2"), 5);
        roundedStrokeBg = uiHelper.createRoundedWithStrokeDrawable(3, Color.TRANSPARENT, Color.parseColor("#B0B6C2"), 5);
        roundedStrokeNormal = uiHelper.createRoundedWithStrokeDrawable(3, Color.parseColor("#B0B6C2"), Color.parseColor("#5d6470"), 5);
        roundedStrokeRight = uiHelper.createRoundedWithStrokeDrawable(3, Color.parseColor("#00cc00"), Color.parseColor("#5d6470"), 5);
        roundedStrokeWrong = uiHelper.createRoundedWithStrokeDrawable(3, Color.parseColor("#ff3333"), Color.parseColor("#5d6470"), 5);
    }

    public void setText(CharSequence text) {
        _TvText.setText(Helper.getStringWithReplacements(text.toString()));
        CompatibleHelper.setBackgroundDrawable(_TvText, roundedSolidNormal);
    }

    public void setIcon(int icon) {
        _IvIcon.setImageResource(icon);
    }

    public void toggle() {
        setSelected(!selected);
    }

    public void showAnswer() {
        if (answer.correct) {
            if (selected) {
                CompatibleHelper.setBackgroundDrawable(_TvText, roundedStrokeRight);
            }
            _IvIcon.setImageResource(selected ? R.drawable.multiple_correct1 : R.drawable.multiple_correct2);
        } else {
            if (selected) {
                CompatibleHelper.setBackgroundDrawable(_TvText, roundedStrokeWrong);
            }
            _IvIcon.setImageResource(selected ? R.drawable.multiple_wrong : R.drawable.multiple_wrong1);
        }
    }

    public void setSelectBg(boolean selected) {
        CompatibleHelper.setBackgroundDrawable(this, selected ? roundedStrokeBg : null);
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            CompatibleHelper.setBackgroundDrawable(_TvText, roundedStrokeNormal);
        } else {
            CompatibleHelper.setBackgroundDrawable(_TvText, roundedSolidNormal);
        }
    }

}
