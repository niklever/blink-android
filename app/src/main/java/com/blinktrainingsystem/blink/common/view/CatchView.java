package com.blinktrainingsystem.blink.common.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.animation.AnimatorPath;
import com.blinktrainingsystem.blink.animation.PathEvaluator;
import com.blinktrainingsystem.blink.animation.PathPoint;
import com.blinktrainingsystem.blink.util.Helper;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Random;

import de.greenrobot.event.EventBus;

/**
 * Created by TALE on 10/17/2014.
 */
public class CatchView extends FrameLayout {

    private static final DecelerateInterpolator sDecelerateInterpolator =
            new DecelerateInterpolator();
    private static final String TAG = CatchView.class.getSimpleName();
    private final long duration;
    private final int parentHeight;
    private final Picasso picasso;
    private boolean addingText;

    private int targetX;
    public final boolean correct;
    public final int slot;
    public final int bonus;
    public boolean inSlot;
    private TextView mTextView;

    public TextView getTextView() {
        return mTextView;
    }

    public CatchView(Context context, Picasso picasso, int parentWidth, int parentHeight, boolean correct, double speed, int slot, int bonus) {
        super(context);
        this.correct = correct;
        this.picasso = picasso;
        this.duration = (long) ((speed * 1000 / 10) * 25.0);
        this.slot = slot;
        this.bonus = bonus;
        this.parentHeight = parentHeight;
        final int deltaX = parentWidth / 2;
        final Random random = new Random();
        targetX = random.nextInt(parentWidth) - deltaX;
        setVisibility(INVISIBLE);
    }

    public CatchView(Context context, Picasso picasso, int parentWidth, int parentHeight, File imageFile, boolean correct, double speed, int slot, int bonus) {
        this(context, picasso, parentWidth, parentHeight, correct, speed, slot, bonus);
        if (imageFile != null) {
            addView(newImageView(context, imageFile));
        }
    }

    public CatchView(final Context context, Picasso picasso, int parentWidth, int parentHeight, final String text, boolean correct, double speed, int slot, int bonus) {
        this(context, picasso, parentWidth, parentHeight, correct, speed, slot, bonus);
        int drawableResourceId = this.getResources().getIdentifier("catch" + (bonus + 1), "drawable", "uk.co.catalystpics.blink");
        setBackgroundResource(drawableResourceId);
        if (!TextUtils.isEmpty(text)) {
            addingText = true;
            getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    if (getHeight() > 0) {
                        getViewTreeObserver().removeOnPreDrawListener(this);
                        mTextView = newTextView(getContext(), text);
                        addView(mTextView);
                        addingText = false;
                        if (isStaring) {
                            start();
                        }
                        return true;
                    }
                    return true;
                }
            });
        }
    }

    EventBus eventBus;

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    private ImageView newImageView(Context context, File imageFile) {
        final ImageView imageView = new ImageView(context);
        picasso.load(imageFile).into(imageView);
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
//        if (!isTablet) {
//            imageView.setScaleX(0.7f);
//            imageView.setScaleY(0.7f);
//        }
        return imageView;
    }

    private ImageView newImageView(Context context, int bonus) {
        final ImageView imageView = newImageView(context);
        int drawableResourceId = this.getResources().getIdentifier("catch" + (bonus + 1), "drawable", "uk.co.catalystpics.blink");
        imageView.setImageResource(drawableResourceId);
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        if (!isTablet) {
            imageView.setScaleX(0.5f);
            imageView.setScaleY(0.5f);
        }

        return imageView;
    }

    private ImageView newImageView(Context context) {
        final LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        final ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParams);
//        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
//        if (!isTablet) {
//            imageView.setScaleX(0.5f);
//            imageView.setScaleY(0.5f);
//        }
        return imageView;
    }

    private TextView newTextView(Context context, String text) {
//        Log.d(TAG, String.format("Text: %s, width: %d, height: %d", text, getWidth(), getHeight()));
        LayoutParams layoutParams = new LayoutParams(getWidth(), getHeight());
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        if (isTablet) {
            layoutParams = new LayoutParams(getWidth(), getHeight());
        } else {
            layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        final int padding = getResources().getDimensionPixelSize(R.dimen.catch_view_padding);
        layoutParams.gravity = Gravity.CENTER;
        TextView textView = new TextView(context);
        textView.setTextColor(Color.WHITE);
        textView.setGravity(Gravity.CENTER);
        textView.setMaxLines(2);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setLayoutParams(layoutParams);
        textView.setPadding(padding, padding / 2, padding, padding / 2);
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.textsize_cathview));
        textView.setText(Helper.getStringWithReplacements(text));

        return textView;
    }

    public ObjectAnimator anim;
    private boolean isStaring;

    public void start() {
        isStaring = true;
        if (addingText) {
            return;
        }
        if (anim != null) {
            anim.cancel();
        }
        getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                getViewTreeObserver().removeOnPreDrawListener(this);
                setVisibility(VISIBLE);
                AnimatorPath path = new AnimatorPath();
                int height = getHeight();
//                Log.d("CatchView", String.format("height: %d, parentHeight: %d", height, parentHeight));
                boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
                createPath(path, 0, targetX, 0, (parentHeight / 2) - height / 2);
//                if (isTablet) {
//                } else {
//                    createPath(path, 0, targetX, 0, getResources().getDimensionPixelOffset(R.dimen.h_maxmove_catview));
//                }

                // Set up the animation
                anim = ObjectAnimator.ofObject(
                        CatchView.this, "location",
                        new PathEvaluator(), path.getPoints().toArray());
                anim.setInterpolator(sDecelerateInterpolator);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        eventBus.post(CatchView.this);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        eventBus.post(CatchView.this);
                    }
                });
                anim.setDuration(duration);
                anim.start();
                return false;
            }
        });
    }

    public void setLocation(PathPoint newLoc) {
//        Log.d("setButtonLoc", String.format("newX: %f, newY: %f", newLoc.mX, newLoc.mY));
        float scale = newLoc.mX / targetX;
        setScaleX(scale);
        setScaleY(scale);
        setTranslationX(newLoc.mX);
        final float newLocY;
        final int topEdge = -(parentHeight) / 2;
        if (newLoc.mY < topEdge) {
            newLocY = topEdge;
        } else {
            newLocY = newLoc.mY;
        }
        setTranslationY(newLocY);
    }

    private void createPath(AnimatorPath path, int startX, int endX, int startY, int endY) {
//        Log.d("createPath", String.format("startX: %d, endX: %d, startY:%d, endY:%d", startX, endX, startY, endY));
        path.moveTo(startX, startY);
        final int deltaX = endX - startX;
        final int deltaY = endY - startY;
        path.curveTo(startX, -deltaY, deltaX / 3, -2f * deltaY, endX, endY);
    }
}
