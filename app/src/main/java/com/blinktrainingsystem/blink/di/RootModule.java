package com.blinktrainingsystem.blink.di;

import android.app.Application;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.DataModule;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.blinktrainingsystem.blink.task.TaskModule;
import com.blinktrainingsystem.blink.util.UtilModule;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * Dagger module created to work as junction of every module with an application scope.
 * <p/>
 * This module provides every application scope dependencies related with the AndroidSDK.
 */

@Module(
        includes = {
                DataModule.class,
                UtilModule.class,
                TaskModule.class,
        },
        injects = {
                BlinkApplication.class,
                WorkerService.class
        },
        library = true,
        complete = false
)
public final class RootModule {

    private final Application application;

    public RootModule(Application application) {
        this.application = application;
    }

    @Provides Application provideApplicationContext() {
        return application;
    }

    @Provides BlinkApplication provideApplication() {
        return (BlinkApplication) application;
    }

    @Provides LayoutInflater provideLayoutInflater() {
        return LayoutInflater.from(application);
    }

    @Provides
    @Singleton Handler provideMainHandler() {
        return new Handler(Looper.getMainLooper());
    }

    @Provides
    @Singleton EventBus provideBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton AssetManager provideAssetManager() {
        return application.getAssets();
    }

    @Provides
    @Singleton
    public Tracker provideTracker() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(application);
        return analytics.newTracker(R.xml.ga_tracker);
    }
}
