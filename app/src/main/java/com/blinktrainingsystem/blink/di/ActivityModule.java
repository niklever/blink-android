package com.blinktrainingsystem.blink.di;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.activity.CallbackActionBarActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module created to provide some common activity scope dependencies as @ActivityContext. This
 * module is going to be added to the graph generated for every activity while the activity creation
 * lifecycle.
 */
@Module(library = true)
public final class ActivityModule {

    private final CallbackActionBarActivity activity;

    public ActivityModule(CallbackActionBarActivity activity) {
        this.activity = activity;
    }

    @ActivityContext
    @Provides CallbackActionBarActivity provideActivityContext() {
        return activity;
    }

    @FragmentContainerId
    @Provides int provideFragmentContainerId() {
        return R.id.fragment_container;
    }

}
