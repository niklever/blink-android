package com.blinktrainingsystem.blink.di;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by TALE on 9/8/2014.
 */
@Qualifier
@Retention(RUNTIME)
public @interface FragmentContainerId {
}
