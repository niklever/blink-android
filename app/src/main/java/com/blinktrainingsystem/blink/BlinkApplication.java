package com.blinktrainingsystem.blink;

//import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.model.pojo.UserCourse;
import com.blinktrainingsystem.blink.di.RootModule;
import com.google.common.collect.Lists;
import com.talenguyen.androidframework.module.database.DBContract;
import com.talenguyen.androidframework.module.database.ITable;
import com.talenguyen.androidframework.module.database.SQLiteUtil;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class BlinkApplication extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "rCZ6KEWp54vbBo7bihpwOnURP";
    private static final String TWITTER_SECRET = "17Ipi6NG0SMLPUaPIL0JNNgD2fW1hFFmUo4aIbD85fQMeXpy4U";

    private static final String TAG = BlinkApplication.class.getSimpleName();
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new TweetComposer());
        initializeDependencyInjector();
        initializeDebugLog();
        initializeLocalDatabase();
    }

    private void initializeLocalDatabase() {
        SQLiteUtil.init(
                this, new DBContract() {
                    @Override
                    public List<Class<? extends ITable>> getTableClasses() {
                        List<Class<? extends ITable>> tables = Lists.newArrayList();
                        tables.add(Course.class);
                        tables.add(Credit.class);
                        tables.add(UserCourse.class);
                        return tables;
                    }

                    @Override
                    public int getDBVersion() {
                        return 2;
                    }

                    @Override
                    public String getDBName() {
                        return TAG;
                    }
                }
        );
    }

    private void initializeDebugLog() {
        if (BuildConfig.DEBUG) {
            Timber.DebugTree debugTree = new Timber.DebugTree();
            debugTree.tag(TAG);
            Timber.plant(debugTree);
        }
    }

    /*
     * We could use this_ code to enable or disable night mode or eve use a auto night mode.
     * But to use this_ feature we have to enable car mode and the UX is not the expected :S
     * If you enable car mode the application is going to show a persistent notification!
     *
     * Use this_ method inside the onCreate and create a new "values-night" directory with some
     * color changes to show how it works.
     *
     */
    private void initializeUiManager() {
    /*
     * UiModeManager uiModeManager = (UiModeManager) getSystemService(UI_MODE_SERVICE);
     * uiModeManager.enableCarMode(0);
     * uiModeManager.setNightMode(UiModeManager.MODE_NIGHT_AUTO);
    */
    }

    /**
     * Inject every dependency declared in the object with the @Inject annotation if the dependency
     * has been already declared in a module and already initialized by Dagger.
     *
     * @param object to inject.
     */
    public void inject(Object object) {
        objectGraph.inject(object);
    }

    /**
     * Extend the dependency container graph will new dependencies provided by the modules passed as
     * arguments.
     *
     * @param modules used to populate the dependency container.
     */
    public ObjectGraph plus(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException(
                    "You can't plus a null module, review your getModules() implementation"
            );
        }
        return objectGraph.plus(modules.toArray());
    }

    private void initializeDependencyInjector() {
        List<Object> modules = getModules();
        if (modules != null) {
            objectGraph = ObjectGraph.create(modules.toArray());
        }
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }

    protected List<Object> getModules() {
        List<Object> modules = new ArrayList<Object>();
        modules.add(new RootModule(this));
        return modules;
    }
}