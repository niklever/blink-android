package com.blinktrainingsystem.blink.task;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        library = true
)
public class TaskModule {

    @Provides
    @Singleton TaskManager provideTaskManager() {
        return new TaskManager();
    }
}
