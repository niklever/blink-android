package com.blinktrainingsystem.blink.task;

import android.os.AsyncTask;

import timber.log.Timber;

public abstract class Task<Result> extends AsyncTask<Object, Object, Result> {

    private final TaskManager mTaskManager;

    public Task(TaskManager taskManager) {
        this.mTaskManager = taskManager;
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        Timber.d("onPostExecute");
        onFinished();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Timber.d("onCancelled");
        onFinished();
    }

    /**
     * Called when task is completed or cancelled.
     */
    protected void onFinished() {
        Timber.d("onFinished");
        boolean removed = mTaskManager.remove(this);
        Timber.d("Task is removed: " + removed);
    }
}