package com.blinktrainingsystem.blink.data.net;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.util.LongSparseArray;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.pojo.DownloadProgress;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.task.Task;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.FileUtils;
import com.google.common.base.Objects;
import com.google.common.io.Files;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import timber.log.Timber;

/**
 * Created by TALE on 9/15/2014.
 */
public class WorkerService extends Service {

    private static final String ACTION_DOWNLOAD_COURSE = "action_download_course";
    private static final String ACTION_UPDATE_COURSES = "action_update_courses";
    private static final String ACTION_UPDATE_USER_DATA = "action_update_user_data";
    private static final String EXTRA_COURSE_ID = "course_id";
    private static final String EXTRA_GUID = "guid";
    private static final String EXTRA_DATA = "data";
    private static final String EXTRA_FOLDER = "folder";
    public static final String DOWNLOAD_URL_PATTERN = WebServices.MEDIA_URL + "/%s/course%d.zip?%d";
    private static final String DOWNLOAD_TAG = "download";
    private static final int BUFFER_SIZE = 4 * 1024; // 4 Kbs for buffer.

    @Inject
    EventBus bus;
    @Inject
    LocalFileManager localFileManager;
    @Inject
    OkHttpClient httpClient;
    @Inject
    TaskManager taskManager;
    @Inject
    LocalSharedPreferences localSharedPreferences;
    @Inject
    CoursesRepo coursesRepo;

    @Inject
    WebServices webServices;

    LongSparseArray<Long> downloadTasks = new LongSparseArray<Long>();
    private File downloadingFileZip;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((BlinkApplication) getApplication()).inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
        Timber.d("onDestroy");
        taskManager.cancelAll();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        Timber.d("onStartCommand");
        String action = intent.getAction();
        if (Objects.equal(action, ACTION_DOWNLOAD_COURSE)) {
            final long courseId = intent.getLongExtra(EXTRA_COURSE_ID, 0);
            final String folderName = intent.getStringExtra(EXTRA_FOLDER);
            if (courseId != 0) {
                final LocalFileManager.Folder folder;
                if (Objects.equal(folderName, LocalFileManager.Folder.lite.toString())) {
                    folder = LocalFileManager.Folder.lite;
                } else if (Objects.equal(folderName, LocalFileManager.Folder.downloading)) {
                    folder = LocalFileManager.Folder.downloading;
                } else {
                    folder = LocalFileManager.Folder.full;
                }
                download(courseId, folder);
            }
        } else if (Objects.equal(action, ACTION_UPDATE_COURSES)) {
            updateCourses();
        } else {
            final long courseId = intent.getLongExtra(EXTRA_COURSE_ID, 0);
            final String guid = intent.getStringExtra(EXTRA_GUID);
            final String data = intent.getStringExtra(EXTRA_DATA);
            updateUserData(data, courseId, guid);
        }
        return START_STICKY;
    }

    private void updateUserData(String data, long courseId, String guid) {
        webServices.saveUserData(
                data, courseId, localSharedPreferences.userId().getOr(0l), guid, new Callback<BaseParser>() {
                    @Override
                    public void success(BaseParser baseParser, retrofit.client.Response response) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                }
        );
    }

    private void updateCourses() {
        Task<Boolean> task = new Task<Boolean>(taskManager) {
            @Override
            protected Boolean doInBackground(Object... params) {
                Long userId = localSharedPreferences.userId().getOr(-1L);
                if (userId == -1L) {
                    return false;
                }
                return coursesRepo.loadCourses(userId);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                Timber.d("Update courses result: " + result);
                if (result) {
                    bus.post(Event.CourseUpdate);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                if (taskManager.isQueueEmpty()) {
                    stopSelf();
                }
            }
        };
        taskManager.enqueue(task);
    }

    private void download(final long courseId, final LocalFileManager.Folder folder) {
//        Create a file name is courseId in download folder.
        final File downloadingEmptyFile = localFileManager.getCourseFile(courseId, LocalFileManager.Folder.downloading);
        boolean result = FileUtils.newEmptyFile(downloadingEmptyFile);
        if (!result) {
            return;
        }

        if (downloadTasks.get(courseId, -1l) != -1l) {
            Timber.d("There is a downloading");
            return;
        }
//        Start download service to download.
//        Call downloadCourse API
        Task<Boolean> task = new Task<Boolean>(taskManager) {
            int progress = 0;

            @Override
            protected Boolean doInBackground(Object... params) {
                SystemClock.sleep(500); // Delay for .5s. Purpose is wait for the Grid is refreshed or missing items.
                final String downloadUrl = getDownloadUrl(courseId, folder, System.currentTimeMillis());
                Timber.d("Download url for course %d is %s", courseId, downloadUrl);
                downloadingFileZip = localFileManager.getZipFile(courseId, LocalFileManager.Folder.downloading);
                Timber.d("Local file for course %d is %s", courseId, downloadingFileZip.getAbsolutePath());

                InputStream inputStream = null;
                OutputStream outputStream = null;
                long total = -1;
                try {
                    Request request = new Request.Builder().tag(DOWNLOAD_TAG).url(downloadUrl).build();
                    Response response = httpClient.newCall(request).execute();
                    if (response.isSuccessful()) {
                        total = response.body().contentLength();
                        inputStream = response.body().byteStream();
                        outputStream = new FileOutputStream(downloadingFileZip);
                    }
                    if (inputStream == null || outputStream == null) {
                        return false;
                    }
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int bytesRead;
                    long length = 0;
                    //        Convert read and save stream to file.
                    while ((bytesRead = inputStream.read(buffer, 0, BUFFER_SIZE)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        length += bytesRead;
                        int progress = (int) (length * 100 / total);
                        if (progress > 100) {
                            progress = 100;
                        }
                        Timber.d("Course %d is downloaded %d percent", courseId, progress);
                        publishProgress(progress);
                    }
                    outputStream.flush();
                    boolean result = length >= total;
                    Timber.d("Stream write result: " + result + " size: " + length);
                    //                DONE
                    return result;
                } catch (IOException e) {
                    Timber.e(e, "Write stream error.");
                } finally {
                    closeQuite(inputStream);
                    closeQuite(outputStream);
                }
                return false;
            }

            @Override
            protected void onProgressUpdate(Object... values) {
                super.onProgressUpdate(values);
                int progress = (Integer) values[0];
                if (this.progress != progress) {
                    bus.post(new DownloadProgress(courseId, progress));
                    this.progress = progress;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if (result) {

                    try {
                        //        Delete file name is courseId in download folder.
                        FileUtils.rm(downloadingEmptyFile);
                        //        Move the downloaded zip to the correct folder.
                        File downloadFileZip = localFileManager.getZipFile(courseId, folder);
                        Files.move(downloadingFileZip, downloadFileZip);
                        //        Unzip the zip file.
                        //        Delete the zip file.
                        boolean unZip = false;
                        int attempt = 5;
                        while (!unZip && attempt > 0) {
                            unZip = localFileManager.unzip(courseId, folder);
                        }
                        //        Notify complete.
                        localSharedPreferences.removeLong(courseId);
                        bus.post(new DownloadProgress(courseId, 100));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else {
                    bus.post(new DownloadProgress(courseId, -1));
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                downloadTasks.remove(courseId);
                if (taskManager.isQueueEmpty()) {
                    stopSelf();
                }
            }
        };
        taskManager.enqueueSerial(task);
        localSharedPreferences.putLong(courseId, courseId); // Save to
        downloadTasks.put(courseId, courseId);
    }
//private void download(final long courseId, final LocalFileManager.Folder folder) {
//        boolean result = coursesRepo.addToUser(courseId, folder);
//        if (!result) {
//            return;
//        }
//
//        File file = localFileManager.getCourseFile(courseId, folder);
//        if (downloadTasks.get(courseId, -1l) != -1l || file.isDirectory()) {
//            return;
//        }
//
//        Task<Boolean> task = new Task<Boolean>(taskManager) {
//            int progress = 0;
//
//            @Override
//            protected Boolean doInBackground(Object... params) {
//                final String downloadUrl = getDownloadUrl(courseId, folder, System.currentTimeMillis());
//                Timber.d("Download url for course %d is %s", courseId, downloadUrl);
//                final File localFile = localFileManager.getZipFile(courseId, folder);
//                Timber.d("Local file for course %d is %s", courseId, localFile.getAbsolutePath());
//
//                InputStream inputStream = null;
//                OutputStream outputStream = null;
//                long total = -1;
//                try {
//                    Request request = new Request.Builder().tag(DOWNLOAD_TAG).url(downloadUrl).build();
//                    Response response = httpClient.newCall(request).execute();
//                    if (response.isSuccessful()) {
//                        total = response.body().contentLength();
//                        inputStream = response.body().byteStream();
//                        outputStream = new FileOutputStream(localFile);
//                    }
//                    if (inputStream == null || outputStream == null) {
//                        return false;
//                    }
//                    byte[] buffer = new byte[BUFFER_SIZE];
//                    int bytesRead;
//                    long length = 0;
//                    while ((bytesRead = inputStream.read(buffer, 0, BUFFER_SIZE)) != -1) {
//                        outputStream.write(buffer, 0, bytesRead);
//                        length += bytesRead;
//                        int progress = (int) (length * 100 / total);
//                        if (progress > 100) {
//                            progress = 100;
//                        }
//                        Timber.d("Course %d is downloaded %d percent", courseId, progress);
//                        publishProgress(progress);
//                    }
//                    outputStream.flush();
//                    boolean result = length >= total;
//                    Timber.d("Stream write result: " + result + " size: " + length);
//                    if (result) {
//                        result = localFileManager.unzip(courseId, folder);
//                    }
//                    return result;
//                } catch (IOException e) {
//                    Timber.e(e, "Write stream error.");
//                } finally {
//                    closeQuite(inputStream);
//                    closeQuite(outputStream);
//                }
//                return false;
//            }
//
//            @Override
//            protected void onProgressUpdate(Object... values) {
//                super.onProgressUpdate(values);
//                int progress = (Integer) values[0];
//                if (this.progress != progress) {
//                    bus.post(new DownloadProgress(courseId, progress));
//                    this.progress = progress;
//                }
//            }
//
//            @Override
//            protected void onPostExecute(Boolean result) {
//                super.onPostExecute(result);
//                if (result) {
//                    localSharedPreferences.removeLong(courseId);
//                    bus.post(new DownloadProgress(courseId, 100));
//                } else {
//                    bus.post(new DownloadProgress(courseId, -1));
//                }
//            }
//
//            @Override
//            protected void onFinished() {
//                super.onFinished();
//                downloadTasks.remove(courseId);
//                if (taskManager.isQueueEmpty()) {
//                    stopSelf();
//                }
//            }
//        };
//        taskManager.enqueue(task);
//        localSharedPreferences.putLong(courseId, courseId); // Save to
//        downloadTasks.put(courseId, courseId);
//    }

    private void closeQuite(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException e) {
            Timber.e(e, "Close stream error");
        }
    }

    public String getDownloadUrl(long courseId, LocalFileManager.Folder folder, long timeStamp) {
        return String.format(DOWNLOAD_URL_PATTERN, folder.toString(), courseId, timeStamp);
    }

    public static IntentBuilder intent(Context context) {
        return new IntentBuilder(context);
    }

    public static class IntentBuilder {
        private final Intent intent;
        private final Context context;

        public IntentBuilder(Context context) {
            this.context = context;
            this.intent = new Intent(this.context, WorkerService.class);
        }

        public void start() {
            context.startService(intent);
        }

        public void stop() {
            context.stopService(intent);
        }

        public IntentBuilder updateCourses() {
            intent.setAction(ACTION_UPDATE_COURSES);
            return this;
        }

        public IntentBuilder updateUserData(String data, long courseId, String guid) {
            intent.setAction(ACTION_UPDATE_USER_DATA);
            intent.putExtra(EXTRA_COURSE_ID, courseId);
            intent.putExtra(EXTRA_GUID, guid);
            intent.putExtra(EXTRA_DATA, data);
            return this;
        }

        public IntentBuilder downloadCourse(long courseId, LocalFileManager.Folder folder) {
            intent.setAction(ACTION_DOWNLOAD_COURSE);
            courseId(courseId);
            folder(folder);
            return this;
        }

        public IntentBuilder courseId(long courseId) {
            intent.putExtra(EXTRA_COURSE_ID, courseId);
            return this;
        }

        public IntentBuilder folder(LocalFileManager.Folder folder) {
            intent.putExtra(EXTRA_FOLDER, folder.toString());
            return this;
        }

    }
}
