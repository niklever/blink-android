package com.blinktrainingsystem.blink.data.model.pojo;

import java.util.List;

/**
 * Created by TALE on 10/2/2014.
 */
public class WordFill {
    public String text;//": "Fewer misunderstandings preempts many ##2## and can significantly increase the efficiency and ##5## of your organization. Customers and suppliers will sense a cooperative ##1## and will be more likely to want to continue ##3## with your organization.",
    public String prompt;//": "Finally, think on this: when you speak with confidence, it leads to fewer misunderstandings with colleagues, customers, and other parties.nnAnd what does that lead to?",
    public String sound;//": "",
    public List<String> words;//": [        "attitude",        "problems",        "doing business",        "helpful",        "effectiveness",        "positive"    ],
    public int timer;//": 60
}
