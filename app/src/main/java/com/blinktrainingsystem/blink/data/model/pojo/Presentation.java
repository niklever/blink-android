package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 10/2/2014.
 */
public class Presentation {
    public String text;//": "Conversation Background",
    public int in;//": 1,
    public int time;//": 1,
    public String sound;//": "",
    public int font;//": 0,
    public int size;//": 2,
    public String color;//": 1,
    public int alignment;//": 0
}
