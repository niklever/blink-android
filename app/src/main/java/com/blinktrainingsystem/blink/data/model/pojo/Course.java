package com.blinktrainingsystem.blink.data.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.blinktrainingsystem.blink.data.model.Category;
import com.talenguyen.androidframework.module.database.ITable;

/**
 * Created by TALE on 9/5/2014.
 */
public class Course implements ITable, Parcelable {
    public long id = -1;//": "159",
    public String credits;//": "2",
    public String dollars;//": "9.99",
    public String summary;//": "Learn to communicate with confidence in your business and personal life.",
    public String description;//": "Learn to use the essential elements of trust, credibility, and confident behavior to communicate effectively and confidently.",
    public String date;//": "2014-08-05 07:38:08",
    public String iconURL;//": "icon159.jpg",
    public String imageURL;//": "image159.jpg",
    public String vimeo;//": "",
    public String title;//": "The Confident Communicator",
    public String editDate;//": "2014-09-01 05:00:16",
    public String buildAvailable;//": "1",
    public String published;//": "1",
    public String publicationDate;//": null
    public String category;
    public String folderName;
    public boolean lite;
    public boolean cloud;
    private long _id;

    @Override
    public long get_id() {
        return _id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.credits);
        dest.writeString(this.dollars);
        dest.writeString(this.summary);
        dest.writeString(this.description);
        dest.writeString(this.date);
        dest.writeString(this.iconURL);
        dest.writeString(this.imageURL);
        dest.writeString(this.vimeo);
        dest.writeString(this.title);
        dest.writeString(this.editDate);
        dest.writeString(this.buildAvailable);
        dest.writeString(this.published);
        dest.writeString(this.publicationDate);
        dest.writeString(this.category);
        dest.writeString(this.folderName);
        dest.writeByte((byte) (this.lite ? 1 : 0));
        dest.writeByte((byte) (this.cloud ? 1 : 0));
    }

    public Course() {
        this.id = -1;
        this.credits = "";
        this.dollars = "";
        this.summary = "";
        this.description = "";
        this.date = "";
        this.iconURL = "";
        this.imageURL = "";
        this.vimeo = "";
        this.title = "";
        this.editDate = "";
        this.buildAvailable = "";
        this.published = "";
        this.publicationDate = "";
        this.category = "user";
        this.folderName = "";
        this.lite = false;
        this.cloud = false;
    }

    public Course(UserCourse course) {
        this.id = course.id;
        this.credits = "";
        this.dollars = "";
        this.summary = "";
        this.description = "";
        this.date = "";
        this.iconURL = course.iconURL;
        this.imageURL = "";
        this.vimeo = "";
        this.title = course.title;
        this.editDate = "";
        this.buildAvailable = "";
        this.published = "";
        this.publicationDate = "";
        this.category = "user";
        this.folderName = "";
        this.lite = course.lite;
        this.cloud = course.cloud;
    }

    private Course(Parcel in) {
        this.id = in.readLong();
        this.credits = in.readString();
        this.dollars = in.readString();
        this.summary = in.readString();
        this.description = in.readString();
        this.date = in.readString();
        this.iconURL = in.readString();
        this.imageURL = in.readString();
        this.vimeo = in.readString();
        this.title = in.readString();
        this.editDate = in.readString();
        this.buildAvailable = in.readString();
        this.published = in.readString();
        this.publicationDate = in.readString();
        this.category = in.readString();
        this.folderName = in.readString();
        this.lite = in.readByte() != 0;
        this.cloud = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
