package com.blinktrainingsystem.blink.data.model.pojo;

import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.util.JsonParser;

import timber.log.Timber;

/**
 * Created by TALE on 9/17/2014.
 */
public class Blink {
    public long id;//": "55602",
    public long courseId;//": "174",
    public int type;//": "1",
    public String summary;//": "The Basics of Confident Communication",
    public int index;//": "0",
    public String json;//": "{\\\"type\\\":14,\\\"question\\\":{\\\"timer\\\":60,\\\"multiplechoice\\\":true,\\\"answers\\\":[{\\\"correct\\\":false,\\\"text\\\":\\\"Patience\\\",\\\"feedback\\\":\\\"\\\"},{\\\"correct\\\":true,\\\"text\\\":\\\"Trust\\\",\\\"feedback\\\":\\\"\\\"},{\\\"correct\\\":false,\\\"text\\\":\\\"Good looks\\\",\\\"feedback\\\":\\\"\\\"}],\\\"sound\\\":\\\"\\\",\\\"text\\\":\\\"What is the cornerstone of confident communication?\\\"},\\\"feedback\\\":[\\\"\\\",\\\"\\\"],\\\"score\\\":\\\"10\\\",\\\"gems\\\":0,\\\"intro\\\":{\\\"title\\\":\\\"The Basics of Confident Communication\\\",\\\"description\\\":\\\"In this_ Blink Training course, you&#39;ll learn how confident communication can improve your life in many ways.&#10;&#10;Blink Training courses serve up content in small units (called Blinks), add just enough gaming to keep it fun and arrange those Blinks into short sequences (called Bursts). Each Burst takes just a few minutes to complete so you can do them while waiting on line for a cappuccino, waiting for a phone call at work, or whenever you have some free time. Blink Training can turn your downtime into learning time.&#10;&#10;Touch Next to start the first Burst.\\\",\\\"image\\\":\\\"\\\"},\\\"dossier\\\":true}",
    public int lite;//": "0",
    public String guid;//": "2b576205-31e3-45f3-9125-4f8e5a446cac",
    public String growth_activity;//": "0"

    public <T extends Json> T loadJsonToObject() {
        final String jsonToParse = cleanJson(json);
        Timber.d("jsonToParse: " + jsonToParse);
        if (type == BlinkType.KBlinkConversation) {
            return (T) JsonParser.parseObject(jsonToParse, JsonConversation.class);
        }
        return (T) JsonParser.parseObject(jsonToParse, JsonEx.class);
    }

    private String cleanJson(String json) {
        final String result = json.replace("\\n", "&#10;");
        return result.replace("\\", "");
    }
}
