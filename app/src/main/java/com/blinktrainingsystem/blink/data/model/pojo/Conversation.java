package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 10/2/2014.
 */
public class Conversation {
    public String text;//": "Hi, Mario. Thank you for the report yesterday. I can always rely on your reports to be accurate. But I&#39;m worrying that the Paint Department is often under-stocked. What do you think?",
    public String image;//": "CD23C54C-2BE6-258A-3513-DCF09AE446AC.png",
    public String sound;//": "C86625D6-1CF2-8BC2-EA57-2A500BC2F28B.mp3"
}
