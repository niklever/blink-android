package com.blinktrainingsystem.blink.data.model;

/**
 * Created by TALE on 9/10/2014.
 */
public class Constant {
    public static final String ERROR_FAIL_TO_CONNECT_SERVER = "Failed to connect to server";
    public static final long ERROR_DURATION = 2000; // 2 seconds
    public static final long ANIMATION_DURATION = 500;
    public static final long ANIMATION_BLINK_DURATION = 1500;
}
