package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 9/15/2014.
 */
public class DownloadProgress {

    public final long courseId;
    public final int progress;

    public DownloadProgress(long courseId, int progress) {
        this.courseId = courseId;
        this.progress = progress;
    }

}
