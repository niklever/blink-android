package com.blinktrainingsystem.blink.data.model.pojo;

import java.util.List;

/**
 * Created by TALE on 10/2/2014.
 */
public class OrderItems {
    public List<String> slots;//[6]
    public List<Item> items;//[6]
    public String ordered;//:	true
    public int timer;//:	60
    public String sound;//:
    public String prompt;//:	Mary was listening to a speaker who fidgeted and closed her eyes when she talked. Mary had heard good things about her, but she had trouble believing the woman's words. Build the correct statement below.
    public List<String> feedback;//[2]
}
