package com.blinktrainingsystem.blink.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.blinktrainingsystem.blink.data.database.CourseDAO;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.net.NetModule;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        library = true,
        includes = NetModule.class
)
public class DataModule {
    private static final String PREF_NAME = "Blink";

    @Provides
    @Singleton CoursesRepo provideCourseRepo(WebServices webServices, DeviceInfo deviceInfo, LocalFileManager localFileManager, CourseDAO courseDAO, LocalSharedPreferences localSharedPreferences) {
        return new CoursesRepo(webServices, deviceInfo, localFileManager, courseDAO, localSharedPreferences);
    }

    @Provides
    @Singleton LocalSharedPreferences provideLocalSharedPreferences(Application application) {
        final SharedPreferences sharedPreferences = application.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return new LocalSharedPreferences(sharedPreferences);
    }

    @Provides
    @Singleton Gson provideGson() {
        return new GsonBuilder().create();
    }
}
