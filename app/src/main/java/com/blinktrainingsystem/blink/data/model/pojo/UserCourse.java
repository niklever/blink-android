package com.blinktrainingsystem.blink.data.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.talenguyen.androidframework.module.database.ITable;

/**
 * Created by NikLever on 20/05/16.
 */
public class UserCourse implements ITable, Parcelable {
    public long id = -1;//": "159",
    public String title;//": "2",
    public String iconURL;//": "9.99",
    public boolean lite;
    public boolean cloud;//": "Learn to communicate with confidence in your business and personal life.",

    private long _id;

    @Override
    public long get_id() {
        return _id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.iconURL);
        dest.writeByte((byte)(this.lite ? 1 : 0));
        dest.writeByte((byte)(this.cloud ? 1 : 0));
    }

    public UserCourse() {
    }

    private UserCourse(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.iconURL = in.readString();
        this.lite = (in.readByte()!=0);
        this.cloud = (in.readByte()!=0);
    }

    public static final Parcelable.Creator<UserCourse> CREATOR = new Parcelable.Creator<UserCourse>() {
        public UserCourse createFromParcel(Parcel source) {
            return new UserCourse(source);
        }

        public UserCourse[] newArray(int size) {
            return new UserCourse[size];
        }
    };
}