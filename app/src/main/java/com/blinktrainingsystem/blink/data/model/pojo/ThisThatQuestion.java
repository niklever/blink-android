package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 9/26/2014.
 */
public class ThisThatQuestion {
    public String text;//:	With enough confidence you can go just about anywhere. Do you believe this_?
    public String correct;//:	I do
    public String wrong;//:	Not really
    public boolean value;//:	true
    public String[] feedback;
}
