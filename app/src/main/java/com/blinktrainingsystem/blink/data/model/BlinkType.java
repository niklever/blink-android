package com.blinktrainingsystem.blink.data.model;

/**
 * Created by talenguyen on 22/09/2014.
 */
public class BlinkType {
    public static final int KBlinkLearningUnitStart = 1;//(1), -> OK
    public static final int KBlinkTypeSpecificContentLearningUnitEnd = 2;//,//Learning unit end
    public static final int KBlinkBurstStart = 3;//, -> OK
    public static final int KBlinkTypeSpecificContentBurstEnd = 4;//,//burst end -> OK
    public static final int KBlinkEBook = 5;//,  -> OK
    public static final int KBlinkPresentation = 6;//,  -> OK
    public static final int KBlinkInput = 7;//, -> OK
    public static final int KBlinkBuildAStatement = 8;//,-> OK
    public static final int KBlinkWordFill = 9;//, -> OK
    public static final int KBlinkDragAndDrop = 10;//, -> OK
    public static final int KBlinkSimon = 11;//, -> OK
    public static final int KBlinkOrderItems = 12;//, -> OK
    public static final int KBlinkThisOrThat = 13;//, -> OK
    public static final int KBlinkQuestion = 14;//, -> OK
    public static final int KBlinkStatementRotator = 15;//, --> OK
    public static final int KBlinkWordheat = 16;//,
    public static final int KBlinkVideo = 17;//, --> OK
    public static final int KBlinkCatchGame = 18;//,
    public static final int KBlinkConversation = 19;//,
    public static final int KBlinkMultiple = 20;// -> OK
}
