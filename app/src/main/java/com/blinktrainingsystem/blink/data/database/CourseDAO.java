package com.blinktrainingsystem.blink.data.database;

import com.blinktrainingsystem.blink.data.model.Category;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.UserCourse;
import com.talenguyen.androidframework.module.database.SQLiteUtil;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by TALE on 9/16/2014.
 */
public class CourseDAO {

    @Inject
    public CourseDAO() {
    }

    public int delete(long courseId) {
        return SQLiteUtil.delete(Course.class, "id = ?", buildSelectArgs(courseId));
    }

    public int deleteAll() {
        int result1 = SQLiteUtil.delete(Course.class, null, null);
        int result2 = SQLiteUtil.delete(UserCourse.class, null, null);
        return result1 & result2;
    }

    public int insertOrUpdate(List<Course> courses) {
        if (courses == null && courses.size() == 0) {
            return 0;
        }
        int count = 0;
        for (Course course : courses) {
            long insertResult = insertOrUpdate(course);
            if (insertResult > 0) {
                count++;
            }
        }
        return count;
    }

    public long insertOrUpdate(Course course) {
        if (course == null) {
            return 0;
        }
        delete(course.id);
        return insert(course);
    }

    public int insertOrUpdateUserCourse(List<UserCourse> courses) {
        if (courses == null && courses.size() == 0) {
            return 0;
        }
        int count = 0;
        for (UserCourse course : courses) {
            long insertResult = insertOrUpdateUserCourse(course);
            if (insertResult > 0) {
                count++;
            }
        }
        return count;
    }

    public long insertOrUpdateUserCourse(UserCourse course) {
        if (course == null) {
            return 0;
        }
        delete(course.id);
        return insert(course);
    }

    public long insert(Course course) {
        if (course == null) {
            return 0;
        }
        return SQLiteUtil.insert(course);
    }

    public long insert(UserCourse course) {
        if (course == null) {
            return 0;
        }
        return SQLiteUtil.insert(course);
    }

    public List<Course> getAll() {
        return (List<Course>) SQLiteUtil.quickQueryObjects(Course.class, "category = ?", new String[]{Category.all.toString()});
    }

    public List<Course> getUnits() {
        return (List<Course>) SQLiteUtil.quickQueryObjects(Course.class, "category = ?", new String[]{Category.unit.toString()});
    }

    public List<UserCourse> getUserCourses() {
        return (List<UserCourse>) SQLiteUtil.quickQueryObjects(UserCourse.class, "id > ?", buildSelectArgs(0));
    }

    public Course getById(long id) {
        List<Course> courses = (List<Course>) SQLiteUtil.quickQueryObjects(Course.class, "id = ?", buildSelectArgs(id));
        if (courses != null && courses.size() > 0) {
            return courses.get(0);
        }
        return null;
    }

    private String[] buildSelectArgs(Object... args) {
        if (args == null || args.length == 0) {
            return null;
        }
        String[] selectArgs = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            selectArgs[i] = String.valueOf(args[i]);
        }
        return selectArgs;
    }

}
