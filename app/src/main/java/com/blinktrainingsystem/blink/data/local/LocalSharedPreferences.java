package com.blinktrainingsystem.blink.data.local;

import android.content.SharedPreferences;

import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.tale.sharepref.BooleanEditor;
import com.tale.sharepref.IntEditor;
import com.tale.sharepref.LongEditor;
import com.tale.sharepref.PrettySharedPreferences;
import com.tale.sharepref.StringEditor;

import java.util.Map;

/**
 * Created by TALE on 9/10/2014.
 */
public class LocalSharedPreferences extends PrettySharedPreferences {

    private static final String KEY_USER_ID = "userId";
    private static final String KEY_CREDIT = "credits";
    private static final String KEY_COURSE_INIT = "course_init";
    private static final String KEY_USR = "username";
    private static final String KEY_PWD = "password";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_LOCALE = "locale";
    private static final String KEY_BLINK_INDEX = "%d_blink_index_%d_%s";
    private final SharedPreferences sharedPreferences;

    public LocalSharedPreferences(SharedPreferences sharedPreferences) {
        super(sharedPreferences);
        this.sharedPreferences = sharedPreferences;
    }

    public BooleanEditor allowSkip() {
        return getBooleanEditor("allowSkip", true);
    }

    public BooleanEditor admin() {
        return getBooleanEditor("admin", true);
    }

    public LongEditor userId() {
        return getLongEditor(KEY_USER_ID, true);
    }

    public StringEditor credits() {
        return getStringEditor(KEY_CREDIT, true);
    }

    public StringEditor usr() {
        return getStringEditor(KEY_USR, true);
    }

    public StringEditor gender() {
        return getStringEditor(KEY_GENDER, true);
    }

    public StringEditor locale() {
        return getStringEditor(KEY_LOCALE, true);
    }

    public StringEditor pwd() {
        return getStringEditor(KEY_PWD, true);
    }

    public StringEditor payload(long userId, long courseId, long itemId) {
        return getStringEditor(String.format("user_%d_course_%d_package_%d", userId, courseId, itemId), true);
    }

    public BooleanEditor courseInit() {
        return getBooleanEditor(KEY_COURSE_INIT, true);
    }

    public BooleanEditor isHelpShown(long courseId, int type) {
        return getBooleanEditor(String.format("%d_isHelpShown_%d", courseId, type), true);
    }

    public IntEditor unitUnlock(long courseId) {
        return getIntEditor(String.format("%d_unitUnlock_%d", userId().getOr(0l), courseId), true);
    }

    public IntEditor quickLinkIndex(long courseId) {
        return getIntEditor(String.format("%d_quickLinkIndex_%d", userId().getOr(0l), courseId), true);
    }

    public IntEditor burstUnlock(long courseId, int unit) {
        return getIntEditor(String.format("%d_burstUnlock_%d_%d", userId().getOr(0l), courseId, unit), true);
    }

    public IntEditor blinkIndex(long userId, long courseId, String type) {
        return getIntEditor(String.format(KEY_BLINK_INDEX, userId, courseId, type), true);
    }

    public StringEditor unlockActivityIds(long userId, Course course) {
        return getStringEditor(String.format("UNLOCKED_ACTIVITIES_%d_%d_%s", userId, course.id, course.category), true);
    }

    public StringEditor completedActivityIds(long userId, Course course) {
        return getStringEditor(String.format("IS_ALREADY_COMPLETE_ACTIVITIES_%d_%d_%s", userId, course.id, course.category), true);
    }

    public BooleanEditor unlockedActivity(long userId, Course course, String guid) {
        return getBooleanEditor(String.format("IS_ALREADY_COMPLETE_ACTIVITIES_%d_%d_%s_%s", userId, course.id, course.category, guid), true);
    }

    public void putLong(long key, long value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(String.valueOf(key), value);
        edit.apply();
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(key, value);
        edit.apply();
    }

    public long getLongOr(long key, long def) {
        return sharedPreferences.getLong(String.valueOf(key), def);
    }

    public long getLongOr(String key, long def) {
        return sharedPreferences.getLong(key, def);
    }

    public void removeLong(long key) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove(String.valueOf(key));
        edit.apply();
    }

    public void removeKey(String key) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove(key);
        edit.apply();
    }

    public Map<String, ?> getAllKeys() {
        return sharedPreferences.getAll();
    }

    public IntEditor gemForThisBurst(long courseId, int burstIndex) {
        return getIntEditor(String.format("GEM_FOR_THIS_BURST_%d_%d", courseId, burstIndex), true);
    }

    public IntEditor blinkScore(Long userId, long courseId, String category, String guid) {
        return getIntEditor(String.format("QUESTION_SCORE_BLINKDID_%d_%d_%s_%s", userId, courseId, category, guid), true);
    }

    public StringEditor quickLinkTitle(long courseId) {
        return getStringEditor(String.format("%d_quickLinkTitle_%d", userId().getOr(0l), courseId), true);
    }

    public IntEditor highestIndex(long courseId) {
        return getIntEditor(String.format("%d_highestIndex_%d", userId().getOr(0l), courseId), true);
    }

    public StringEditor pendingPurchase(long userId, long courseId) {
        return getStringEditor(String.format("user_%d_course_%d", userId, courseId), true);
    }
}
