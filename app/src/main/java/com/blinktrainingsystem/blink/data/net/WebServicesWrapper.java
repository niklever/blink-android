package com.blinktrainingsystem.blink.data.net;

import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.parser.DossierParser;
import com.blinktrainingsystem.blink.data.model.parser.GetCoursesParser;
import com.blinktrainingsystem.blink.data.model.parser.PurchaseParser;
import com.blinktrainingsystem.blink.data.model.parser.SpentCreditParser;
import com.blinktrainingsystem.blink.data.model.parser.UpdatePurchaseParser;
import com.blinktrainingsystem.blink.data.model.parser.UserDataParser;
import com.blinktrainingsystem.blink.data.model.pojo.Community;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.model.pojo.PurchaseTransaction;
import com.blinktrainingsystem.blink.data.model.request.RegisterRequest;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by TALE on 9/5/2014.
 */
public class WebServicesWrapper implements WebServices {

    private final WebServices webServices;
    public final Gson gson;

    public WebServicesWrapper(WebServices webServices, Gson gson) {
        this.webServices = webServices;
        this.gson = gson;
    }

    @Override
    public BaseParser fbLogin(@Query("id") String id, @Query("email") String email, @Query("name") String name, @Query("locale") String locale, @Query("gender") String gender, @Query("fb") boolean fb ) {
        try {
            return webServices.fbLogin(id, email, name, locale, gender, true);
        } catch (Exception e) {
            Timber.e(e, "fblogin error");
        }
        return null;
    }

    @Override
    public BaseParser login(@Query("email") String email, @Query("password") String password) {
        try {
            return webServices.login(email, password);
        } catch (Exception e) {
            Timber.e(e, "login error");
        }
        return null;
    }

    @Override
    public BaseParser register(@Query("email") String email, @Query("firstname") String firstname, @Query("lastname") String lastname, @Query("password") String password) {
        try {
            return webServices.register(email, firstname, lastname, password);
        } catch (Exception e) {
            Timber.e(e, "requestRegister error");
        }
        return null;
    }

    @Override
    public BaseParser register(@Body RegisterRequest registerRequest) {
        try {
            return webServices.register(registerRequest);
        } catch (Exception e) {
            Timber.e(e, "requestRegister error");
        }
        return null;
    }

    public Observable<BaseParser> registerAsync(RegisterRequest registerRequest) {
        return threadPoolForComputation(Observable.from(webServices.register(registerRequest)));
    }

    @Override
    public GetCoursesParser getCourses(@Query("userId") long userId) {
        try {
            return webServices.getCourses(userId);
        } catch (Exception e) {
            Timber.e(e, "getCourses error");
        }
        return null;
    }

    @Override
    public Observable<GetCoursesParser> getCoursesObservable(@Query("userId") long userId) {
        return webServices.getCoursesObservable(userId);
    }

    @Override
    public Credit getCredits(@Query("userId") long userId) {
        try {
            return webServices.getCredits(userId);
        } catch (Exception e) {
            Timber.e(e, "getCredits error");
        }
        return null;
    }

    public Observable<Credit> getCreditsObservable(@Query("userId") final long userId) {
        return Observable.create(
                new Observable.OnSubscribe<Credit>() {
                    @Override public void call(Subscriber<? super Credit> subscriber) {
                        try {
                            final Credit credits = webServices.getCredits(userId);
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onNext(credits);
                            }
                        } catch (Exception e) {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onError(e);
                            }
                        } finally {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                            }
                        }
                    }
                }
        );
    }

    @Override
    public SpentCreditParser spendCredits(@Query("userId") long userId, @Query("courseId") long courseId, @Query("credits") String credits) {
        try {
            return webServices.spendCredits(userId, courseId, credits);
        } catch (Exception e) {
            Timber.e(e, "spendCredits error");
        }
        return null;
    }

    @Override
    public BaseParser clearUserData(@Query("courseId") long courseId, @Query("userId") long userId) {
        try {
            return webServices.clearUserData(courseId, userId);
        } catch (Exception e) {
            Timber.e(e, "clearUserData error");
        }
        return null;
    }

    @Override
    public void saveUserData(@Query("data") String data, @Query("courseId") long courseId, @Query("userId") long userId, @Query("guid") String guid, Callback<BaseParser> callback) {
        try {
            webServices.saveUserData(data, courseId, userId, guid, callback);
        } catch (Exception e) {
            Timber.e(e, "saveUserData error");
        }
    }

    @Override
    public Observable<UserDataParser> getUserData(@Query("courseId") long courseId, @Query("userId") long userId, @Query("guid") String guid) {
        try {
            return webServices.getUserData(courseId, userId, guid);
        } catch (Exception e) {
            Timber.e(e, "getUserData error");
        }
        return null;
    }

    public Observable<BaseParser> saveDossier(List<Map<String, String>> data, @Query("courseId") long courseId, @Query("userId") long userId) {
        return saveDossier(gson.toJson(data), courseId, userId);
    }

    @Override
    public Observable<BaseParser> saveDossier(@Query("data") String data, @Query("courseId") long courseId, @Query("userId") long userId) {
        try {
            return webServices.saveDossier(data, courseId, userId);
        } catch (Exception e) {
            Timber.e(e, "saveDossier error");
        }
        return null;
    }

    @Override
    public Observable<DossierParser> getDossier(@Query("courseId") long courseId, @Query("userId") long userId, @Query("guids") String guids) {
        try {
            Timber.d("getDossier => guids: %s", guids);
            return webServices.getDossier(courseId, userId, guids);
        } catch (Exception e) {
            Timber.e(e, "getDossier error");
        }
        return null;
    }

    public Observable<DossierParser> getDossier(@Query("courseId") long courseId, @Query("userId") long userId, @Query("guids") List<String> guids) {
        return getDossier(courseId, userId, gson.toJson(guids));
    }

    @Override
    public BaseParser forgottenPassword(@Query("email") String email) {
        try {
            return webServices.forgottenPassword(email);
        } catch (Exception e) {
            Timber.e(e, "forgottenPassword error");
        }
        return null;
    }

    @Override public Observable<Community> updateCommunity(@Query("guid") String guid, @Query("userId") long userId, @Query("gems") int gems, @Query("points") double points, @Query("traction") int traction) {
        try {
            return webServices.updateCommunity(guid, userId, gems, points, traction);
        } catch (Exception e) {
            Timber.e(e, "updateCommunity error");
        }
        return null;
    }

    @Override public PurchaseParser getIapPackages(@Query("userId") long userId, @Query("platform") String platform) {
        try {
            return webServices.getIapPackages(userId, platform);
        } catch (Exception e) {
            Timber.e(e, "getIapPackages error");
        }
        return null;
    }

    @Override public UpdatePurchaseParser updatePurchase(@Query("userId") long userId, @Query("platform") String platform, @Body PurchaseTransaction purchaseTransaction) {
        try {
            return webServices.updatePurchase(userId, platform, purchaseTransaction);
        } catch (Exception e) {
            Timber.e(e, "updatePurchase error");
        }
        return null;
    }

    @Override public UpdatePurchaseParser updatePurchaseGet(@Query("userId") long userId, @Query("platform") String platform, @Query("itemId") String itemId, @Query("transactionId") String transactionId) {
        try {
            return webServices.updatePurchaseGet(userId, platform, itemId, transactionId);
        } catch (Exception e) {
            Timber.e(e, "updatePurchase error");
        }
        return null;
    }

    public Observable<UpdatePurchaseParser> updatePurchase(final long userId, final PurchaseTransaction purchaseTransaction) {
        return Observable.create(
                new Observable.OnSubscribe<UpdatePurchaseParser>() {
                    @Override public void call(Subscriber<? super UpdatePurchaseParser> subscriber) {
                        try {
                            final UpdatePurchaseParser purchaseParser = webServices.updatePurchase(userId, "android", purchaseTransaction);
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onNext(purchaseParser);
                            }
                        } catch (Exception e) {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onError(e);
                            }
                        } finally {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                            }
                        }
                    }
                }
        );
    }

    public Observable<PurchaseParser> getIapPackagesObservable(@Query("userId") final long userId) {
        return Observable.create(
                new Observable.OnSubscribe<PurchaseParser>() {
                    @Override public void call(Subscriber<? super PurchaseParser> subscriber) {
                        try {
                            final PurchaseParser purchaseParser = webServices.getIapPackages(userId, "android");
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onNext(purchaseParser);
                            }
                        } catch (Exception e) {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onError(e);
                            }
                        } finally {
                            if (!subscriber.isUnsubscribed()) {
                                subscriber.onCompleted();
                            }
                        }
                    }
                }
        );
    }

    public static <T> Observable<T> threadPoolForIO(Observable<T> transformToAsync) {
        return transformToAsync.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public static <T> Observable<T> threadPoolForComputation(Observable<T> transformToAsync) {
        return transformToAsync.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.computation());
    }
}
