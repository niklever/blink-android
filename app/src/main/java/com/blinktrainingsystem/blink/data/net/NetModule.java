package com.blinktrainingsystem.blink.data.net;

import android.app.Application;
import android.net.Uri;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import timber.log.Timber;

/**
 * Created by TALE on 9/5/2014.
 */
@Module(
        library = true,
        complete = false
)
public class NetModule {

    private static final long MAX_CACHE_SIZE = 10 * 1024 * 1024; // 10 Mb for cache.

    @Provides
    @Singleton OkHttpClient provideOkHttpClient(Application application, DeviceInfo deviceInfo) {
        Cache cache = null;
        try {
            cache = new Cache(deviceInfo.getAvailableCacheDirectory(), MAX_CACHE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        OkHttpClient httpClient = new OkHttpClient();
        if (cache != null) {
            httpClient.setCache(cache);
        }
        return httpClient;
    }

    @Provides
    @Singleton Picasso providePicasso(Application app, OkHttpClient client) {
        return new Picasso.Builder(app)
                .listener(
                        new Picasso.Listener() {
                            @Override
                            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {
                                Timber.e(e, "Failed to load image: %s", uri);
                            }
                        }
                )
                .downloader(new OkHttpDownloader(client))
                .build();
    }

    @Provides
    @Singleton WebServices provideApi(Gson gson) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Content-Type", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WebServices.BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        WebServices webServices = restAdapter.create(WebServices.class);
        return new WebServicesWrapper(webServices, gson);
    }

    @Provides
    @Singleton @Named("custom") WebServices provideApiCustom(LocalSharedPreferences localSharedPreferences, Gson gson) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Content-Type", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(String.format("%s/update_purchase.php?userId=%d&platform=android", WebServices.BASE_URL, localSharedPreferences.userId().getOr(0l)))
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        WebServices webServices = restAdapter.create(WebServices.class);
        return new WebServicesWrapper(webServices, gson);
    }
}
