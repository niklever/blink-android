package com.blinktrainingsystem.blink.data.local;

import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by TALE on 9/15/2014.
 */
public class LocalFileManager {

    public static enum Folder {
        full, lite, downloading;

        public static Folder get(String name) {
            if (lite.toString().equals(name)) {
                return lite;
            } else if (full.toString().equals(name)) {
                return full;
            } else if (downloading.equals(name)) {
                return downloading;
            }
            return null;
        }
    }

    final DeviceInfo deviceInfo;

    private final File COURSES_DIR;
    private final File FULL_DIR;
    private final File LITE_DIR;
    private final File DOWNLOADING_DIR;

    @Inject
    public LocalFileManager(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
        COURSES_DIR = new File(deviceInfo.getAvailableFileDirectory(), "courses");
        FULL_DIR = new File(COURSES_DIR, Folder.full.toString());
        FileUtils.mkdirs(FULL_DIR.toString());
        LITE_DIR = new File(COURSES_DIR, Folder.lite.toString());
        FileUtils.mkdirs(LITE_DIR.toString());
        DOWNLOADING_DIR = new File(COURSES_DIR, Folder.downloading.toString());
        FileUtils.mkdirs(DOWNLOADING_DIR.toString());
    }

    public File getCourseFile(long courseId, Folder folder) {
        switch (folder) {
            case full:
                return new File(FULL_DIR, String.valueOf(courseId));
            case lite:
                return new File(LITE_DIR, String.valueOf(courseId));
            case downloading:
                return new File(DOWNLOADING_DIR, String.valueOf(courseId));
            default:
                return new File(FULL_DIR, String.valueOf(courseId));
        }
    }

    public File getZipFile(long courseId, Folder folder) {
        switch (folder) {
            case full:
                return new File(FULL_DIR, String.valueOf(courseId) + ".zip");
            case lite:
                return new File(LITE_DIR, String.valueOf(courseId) + ".zip");
            case downloading:
                return new File(DOWNLOADING_DIR, String.valueOf(courseId) + ".zip");
            default:
                return new File(FULL_DIR, String.valueOf(courseId) + ".zip");
        }
    }

    public boolean exists(long id, Folder folder) {
        return getCourseFile(id, folder).exists();
    }

    public String[] list(Folder folder) {
        switch (folder) {
            case downloading:
                return DOWNLOADING_DIR.list();
            case full:
                return FULL_DIR.list();
            case lite:
                return LITE_DIR.list();
        }
        return FULL_DIR.list();
    }

    public File[] listFile(Folder folder) {
        switch (folder) {
            case downloading:
                return DOWNLOADING_DIR.listFiles();
            case full:
                return FULL_DIR.listFiles();
            case lite:
                return LITE_DIR.listFiles();
        }
        return FULL_DIR.listFiles();
    }

    public boolean unzip(long courseId, Folder folder) {
        File file = getZipFile(courseId, folder);
        try {
            boolean unzip = unzip(file.getAbsolutePath(), null, false);
            if (unzip) {
                // Delete zip if unzip succeed.
                final boolean delete = file.delete();
                Timber.d("Delete the zip file after unzip: %s", delete);
                return delete;
            }
        } catch (IOException e) {
            Timber.e(e, "Unzip failure.");
        }
        return false;
    }

    /**
     * Unzip the zip file.
     *
     * @param zipFile    The file to unzip.
     * @param outputPath the output path or <code>null</code> for current path
     * @throws ZipException
     * @throws IOException
     */
    public static boolean unzip(
            String zipFile, String outputPath,
            boolean deletedFileSource
    ) throws ZipException, IOException {
        int BUFFER = 2048; // 2 Kbs
        File file = new File(zipFile);

        if (outputPath == null) {
            final int index = file.getAbsolutePath().lastIndexOf(".");
            outputPath = file.getPath().substring(0, index);
        }
        FileUtils.rm(new File(outputPath));
        FileUtils.mkdirs(outputPath);

        ZipFile zip = null;

        try {
            zip = new ZipFile(file);

            Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();

            // Process each entry
            while (zipFileEntries.hasMoreElements()) {
                // grab a zip file entry
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
                File destFile = new File(outputPath, entry.getName());

                if (entry.isDirectory()) {
                    FileUtils.mkdirs(destFile.getAbsolutePath());
                } else {
                    FileUtils.mkdirs(destFile.getParent());

                    BufferedInputStream is = new BufferedInputStream(
                            zip.getInputStream(entry)
                    );
                    int currentByte;
                    // establish buffer for writing file
                    byte data[] = new byte[BUFFER];

                    // write the current file to disk
                    FileOutputStream fos = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(
                            fos,
                            BUFFER
                    );

                    // read and write until last byte is encountered
                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (zip != null) {
                zip.close();
            }
        }
        if (deletedFileSource) {
            FileUtils.rm(new File(zipFile));
        }
        return true;
    }

}
