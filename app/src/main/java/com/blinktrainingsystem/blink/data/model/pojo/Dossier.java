package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 9/5/2014.
 */
public class Dossier {
    public String guid; //" : "95b8f4a6-3847-424e-9302-394f32153e2d",
    public String text; //" : "43.90,Something"
    public String data; //" : "43.90,Something"
    public int type; //" : "43.90,Something"
    public String label; //" : "43.90,Something"
    public int count; //" : "43.90,Something"
    public int min;
    public int max;
}