package com.blinktrainingsystem.blink.data.model.event;

/**
 * Created by TALE on 9/16/2014.
 */
public enum Event {
    CourseUpdate,
    ShowHelp,
    BlinkCompleted,
    HelpClosed,
    TimeOut,
    SetTimer,
    StartTimer,
    PauseTimer,
    ShowTimer,
    DoneClick,
    HeaderMenuStepClick,
    CourseRefresh,
    NextClick,
    HelpOpened, AddTimer, HelpRequest, ViewCourseDetail, Back, AllowSkipChange, GainGem, UnlockActivity, CloseHelp, OpenDossier, ShareFb, ShareTw;

    private Object extra;

    public Object getExtra() {
        return extra;
    }

    public Event setExtra(Object extra) {
        this.extra = extra;
        return this;
    }
}
