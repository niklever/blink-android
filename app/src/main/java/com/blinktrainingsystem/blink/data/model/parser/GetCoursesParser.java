package com.blinktrainingsystem.blink.data.model.parser;

import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.UserCourse;

import java.util.List;

/**
 * Created by TALE on 9/5/2014.
 */
public class GetCoursesParser {
    public List<Course> all;
    //    public List<Course> user;
    public List<Course> units;
    public List<UserCourse> user;
}
