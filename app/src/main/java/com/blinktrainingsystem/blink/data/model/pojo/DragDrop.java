package com.blinktrainingsystem.blink.data.model.pojo;

import java.util.List;

/**
 * Created by TALE on 9/30/2014.
 */
public class DragDrop {
    public int timer;//:	60
    public String layout;//:	0
    public List<Goal> goals;//[1]
    public List<Goal> items;//[6]
    public String prompt;//:	Mary suffered anxiety and fear when speaking with others, but she took the time to build her confidence. Drag all the words that apply to her.
    public String sound;//:
}
