package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 10/6/2014.
 */
public class Statement {
    public String prompt;//": "Hidden below is one of the three factors that will affect your confidence when speaking. &#10;&#10;This factor has to do with your behavior.",
    public String wrong;//": "when someone|then there is|where there is|to be or not|honesty is|trust yourself",
    public String[] statements;//": [
    public boolean ordered;//": true,
    public int timer;//": 60,
    public String sound;//": ""
}
