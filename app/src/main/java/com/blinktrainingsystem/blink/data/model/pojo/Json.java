package com.blinktrainingsystem.blink.data.model.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by TALE on 9/17/2014.
 */
public class Json {
    public int type;//": 14,
    public Question question;//": {
    public int score;//": "10",
    public int gems;//": 0,
    public boolean quicklink;//": 0,
    public Intro intro;//": {
    public boolean dossier;//": true
    public int unlockscore;
    public String quotation;
    public String image;
    public String unlockActivity;
    public Simon simon;
    public ThisThat thisthat;
    public DragDrop dragdrop;
    public Input input;
    public WordFill wordfill;
    public WordHeat wordheat;
    public List<Presentation> presentation;
    public String ebook;
    public Statement statement;
    public OrderItems orderitems;
    public Question multiple;
    public Rotator rotator;
    public Video video;

    @SerializedName("catch")
    public Catch _catch;
}
