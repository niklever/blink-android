package com.blinktrainingsystem.blink.data.repository;

import android.text.TextUtils;

import com.blinktrainingsystem.blink.data.database.CourseDAO;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Category;
import com.blinktrainingsystem.blink.data.model.parser.CourseHelper;
import com.blinktrainingsystem.blink.data.model.parser.GetCoursesParser;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.UserCourse;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.google.common.collect.Lists;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Created by TALE on 9/10/2014.
 */
public class CoursesRepo {

    final WebServices webServices;
    final DeviceInfo deviceInfo;
    final LocalFileManager localFileManager;
    final CourseDAO courseDAO;
    final LocalSharedPreferences localSharedPreferences;

    @Inject
    public CoursesRepo(WebServices webServices, DeviceInfo deviceInfo, LocalFileManager localFileManager, CourseDAO courseDAO, LocalSharedPreferences localSharedPreferences) {
        this.webServices = webServices;
        this.deviceInfo = deviceInfo;
        this.localFileManager = localFileManager;
        this.courseDAO = courseDAO;
        this.localSharedPreferences = localSharedPreferences;
    }

    public List<Course> getAllLocal() {
        List<Course> courseDAOAll = courseDAO.getAll();
        List<Course> downloadings = getDownloadings();

        Timber.d("getAllLocal => DAOAll's size: %d, downloading's size: %d", courseDAOAll == null ? 0 : courseDAOAll.size(), downloadings == null ? 0 : downloadings.size());

        if (downloadings != null && downloadings.size() > 0) {
            if (courseDAOAll == null) {
                courseDAOAll = Lists.newArrayList();
            }
            courseDAOAll.addAll(downloadings);
        }
        return courseDAOAll;
    }

    public List<Course> getUnitLocal() {
        return courseDAO.getUnits();
    }

    public Observable<List<Course>> getUnitsRemote(final long userId) {
        return getCourseRemote(userId)
                .map(
                        new Func1<Boolean, List<Course>>() {
                            @Override
                            public List<Course> call(Boolean result) {
                                return result ? getUnitLocal() : null;
                            }
                        }
                );
    }

    public Observable<List<Course>> getAllRemote(final long userId) {
        return getCourseRemote(userId)
                .map(
                        new Func1<Boolean, List<Course>>() {
                            @Override
                            public List<Course> call(Boolean result) {
                                return result ? getAllLocal() : null;
                            }
                        }
                );
    }

    private Observable<Boolean> getCourseRemote(final long userId) {
        return webServices.getCoursesObservable(userId)
                .map(
                        new Func1<GetCoursesParser, Boolean>() {
                            @Override
                            public Boolean call(GetCoursesParser getCoursesParser) {
                                if (getCoursesParser != null) {
                                    courseDAO.deleteAll();
                                    if (getCoursesParser.all != null && getCoursesParser.all.size() > 0) {
                                        for (Course course : getCoursesParser.all) {
                                            course.category = Category.all.toString();
                                            courseDAO.insert(course);
                                        }
                                    }
                                    if (getCoursesParser.units != null && getCoursesParser.units.size() > 0) {
                                        for (Course course : getCoursesParser.units) {
                                            course.category = Category.unit.toString();
                                            courseDAO.insert(course);
                                        }
                                    }
                                    if (getCoursesParser.user != null && getCoursesParser.user.size() > 0){
                                        for (UserCourse course : getCoursesParser.user) {
                                            courseDAO.insert(course);
                                        }
                                    }
                                    return getCoursesParser.all != null && getCoursesParser.all.size() > 0
                                            || getCoursesParser.units != null && getCoursesParser.units.size() > 0;
                                }
                                return false;
                            }
                        }
                );
    }

    public List<Course> getCourses(long userId, Category category) {
        switch (category) {
            case all:
                List<Course> allByCategory = courseDAO.getAll();
                if (allByCategory == null) {
                    Timber.d("Local file is empty. Try to load from server.");
                    boolean loadCourseResult = loadCourses(userId);
                    if (loadCourseResult) {
                        localSharedPreferences.courseInit().put(true);
                        return courseDAO.getAll();
                    }
                }
                Timber.d("%d local courses", allByCategory == null ? 0 : allByCategory.size());
                List<Course> downloadings = getDownloadings();
                if (downloadings != null && downloadings.size() > 0) {
                    if (allByCategory == null) {
                        allByCategory = Lists.newArrayList();
                    }
                    allByCategory.addAll(downloadings);
                }
                return allByCategory;
            case user:
                return getUserCourses();
            case unit:
                break;
        }
        return null;
    }

    public List<Course> getDownloadings() {
        File[] files = localFileManager.listFile(LocalFileManager.Folder.full);
        if (files != null && files.length > 0) {
            List<Course> courses = Lists.newArrayList();
            for (int i = 0; i < files.length; i++) {
                try {
                    File file = files[i];
                    final String name = file.getName();
                    if (!file.isDirectory() && !name.endsWith(".zip")) {
                        long courseId = Long.parseLong(name);
                        Course course = readCourseFromDisk(courseId, LocalFileManager.Folder.full);
                        if (course != null) {
                            courses.add(course);
                        }
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            return courses;
        }
        return null;

    }

    public List<Course> getUserCourses() {
        List<Course> result = null;
        List<Course> full = loadCoursesFromDisk(LocalFileManager.Folder.full);
        if (full != null) {
            if (result == null) result = Lists.newArrayList();
            result.addAll(full);
        }
        List<Course> lite = loadCoursesFromDisk(LocalFileManager.Folder.lite);
        if (lite != null) {
            if (result == null) result = Lists.newArrayList();
            result.addAll(lite);
        }
        List<Course> downloading = loadCoursesFromDisk(LocalFileManager.Folder.downloading);
        if (downloading != null) {
            if (result == null) result = Lists.newArrayList();
            result.addAll(downloading);
        }
        List<UserCourse>courses = courseDAO.getUserCourses();
        if (courses!=null && courses.size()>0) {
            if (result == null) result = Lists.newArrayList();
            for (UserCourse usercourse : courses) {
                boolean add = true;
                for (Course localCourse : result) {
                    if (usercourse.id == localCourse.id && usercourse.lite == localCourse.lite) {
                        add = false;
                        break;
                    }
                }
                if (add) {
                    Course course = new Course(usercourse);
                    result.add(course);
                }
            }
        }
        return result;
    }

    private List<Course> loadCoursesFromDisk(LocalFileManager.Folder folder) {
        String[] list = localFileManager.list(folder);
        if (list != null && list.length > 0) {
            List<Course> courses = Lists.newArrayList();
            for (int i = 0; i < list.length; i++) {
                try {
                    String fileName = list[i];
                    Timber.d("course file: " + fileName);
                    if (TextUtils.isDigitsOnly(fileName)) {
                        long courseId = Long.parseLong(fileName);
                        Course course = readCourseFromDisk(courseId, folder);
                        if (course != null) {
                            courses.add(course);
                        }
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            return courses;
        }
        return null;
    }

    private Course readCourseFromDisk(long courseId, LocalFileManager.Folder folder) {
        CourseHelper courseHelper = new CourseHelper(localFileManager, courseId, folder);
        return courseHelper.getCourse();
    }

    public boolean addToUserLite(long courseId) {
        CourseHelper courseHelper = new CourseHelper(localFileManager, courseId, LocalFileManager.Folder.lite);
        Course course = getCourseById(courseId);
        if (course == null) {
            return false;
        }
        return courseHelper.saveToDisk(course);
    }


    public boolean addToUser(long courseId, LocalFileManager.Folder folder) {
        CourseHelper courseHelper = new CourseHelper(localFileManager, courseId, folder);
        return courseHelper.saveAsEmpty();
    }

    public Course getCourseById(long courseId) {
        return courseDAO.getById(courseId);
    }

    public int deleteAll() {
        return courseDAO.deleteAll();
    }

    public boolean loadCourses(long userId) {
        if (deviceInfo.isNetworkConnected()) {
            GetCoursesParser getCoursesParser = webServices.getCourses(userId);
            if (getCoursesParser != null) {
                if (getCoursesParser.all != null && getCoursesParser.all.size() > 0) {
                    courseDAO.deleteAll();
                    for (Course course : getCoursesParser.all) {
                        courseDAO.insert(course);
                    }
                    Timber.d("%d courses are inserted", getCoursesParser.all.size());
                    return true;
                }
            }
        }
        return false;
    }

}
