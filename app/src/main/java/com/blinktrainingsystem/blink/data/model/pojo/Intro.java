package com.blinktrainingsystem.blink.data.model.pojo;

/**
 * Created by TALE on 9/17/2014.
 */
public class Intro {
    public String title;//": "The Basics of Confident Communication",
    public String description;//": "In this_ Blink Training course, you&#39;ll learn how confident communication can improve your life in many ways.&#10;&#10;Blink Training courses serve up content in small units (called Blinks), add just enough gaming to keep it fun and arrange those Blinks into short sequences (called Bursts). Each Burst takes just a few minutes to complete so you can do them while waiting on line for a cappuccino, waiting for a phone call at work, or whenever you have some free time. Blink Training can turn your downtime into learning time.&#10;&#10;Touch Next to start the first Burst.",
    public String image;//": ""
    public String sound;//": ""
    public float volume;//":
}
