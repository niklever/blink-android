package com.blinktrainingsystem.blink.data.net;

import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.parser.DossierParser;
import com.blinktrainingsystem.blink.data.model.parser.GetCoursesParser;
import com.blinktrainingsystem.blink.data.model.parser.PurchaseParser;
import com.blinktrainingsystem.blink.data.model.parser.SpentCreditParser;
import com.blinktrainingsystem.blink.data.model.parser.UpdatePurchaseParser;
import com.blinktrainingsystem.blink.data.model.parser.UserDataParser;
import com.blinktrainingsystem.blink.data.model.pojo.Community;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.model.pojo.PurchaseTransaction;
import com.blinktrainingsystem.blink.data.model.request.RegisterRequest;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by TALE on 9/5/2014.
 */
public interface WebServices {

    String BASE_URL = "http://blinktrainingsystem.com/ws";
    String MEDIA_URL = "http://blinktrainingsystem.com/courses";
    String IMAGE_PATH = "http://blinktrainingsystem.com/courses/images/%s";

    @GET("/login.php") BaseParser fbLogin(@Query("id") String id, @Query("email") String email, @Query("name") String name, @Query("locale") String locale, @Query("gender") String gender, @Query("fb") boolean fb );

    @GET("/login.php") BaseParser login(@Query("email") String email, @Query("password") String password);

    @POST("/register.php") BaseParser register(@Body RegisterRequest registerRequest);

    @POST("/register.php") BaseParser register(
            @Query("email") String email,
            @Query("firstname") String firstname,
            @Query("lastname") String lastname,
            @Query("password") String password
    );

    @GET("/getcourses.php") GetCoursesParser getCourses(@Query("userId") long userId);

    @GET("/getcourses.php") Observable<GetCoursesParser> getCoursesObservable(@Query("userId") long userId);

    @GET("/getcredits.php") Credit getCredits(@Query("userId") long userId);

    @GET("/spendcredits.php") SpentCreditParser spendCredits(@Query("userId") long userId, @Query("courseId") long courseId, @Query("credits") String credits);

    @GET("/clear_user_data.php") BaseParser clearUserData(@Query("courseId") long courseId, @Query("userId") long userId);

    @GET("/save_user_data.php") void saveUserData(@Query("data") String data, @Query("courseId") long courseId, @Query("userId") long userId, @Query("guid") String guid, Callback<BaseParser> callback);

    @GET("/get_user_data.php") Observable<UserDataParser> getUserData(@Query("courseId") long courseId, @Query("userId") long userId, @Query("guid") String guid);

    /**
     * Call to save Dossier
     *
     * @param data     {@link com.blinktrainingsystem.blink.data.model.pojo.Dossier} in json format
     * @param courseId courseId
     * @param userId   userId
     * @return {@link com.blinktrainingsystem.blink.data.model.parser.BaseParser} object
     */
    @GET("/save_dossier.php") Observable<BaseParser> saveDossier(@Query("data") String data, @Query("courseId") long courseId, @Query("userId") long userId);

    /**
     * Call to get Dossiers
     *
     * @param courseId courseId
     * @param userId   userId
     * @param guids    guids
     * @return {@link com.blinktrainingsystem.blink.data.model.parser.BaseParser} object
     */
    @GET("/get_dossier.php") Observable<DossierParser> getDossier(@Query("courseId") long courseId, @Query("userId") long userId, @Query("guids") String guids);

    @GET("/forgottenpassword.php") BaseParser forgottenPassword(@Query("email") String email);

    @GET("/update_community.php") Observable<Community> updateCommunity(@Query("guid") String guid, @Query("userId") long userId, @Query("gems") int gems, @Query("points") double points, @Query("traction") int traction);

    @GET("/get_iap_packages.php") PurchaseParser getIapPackages(@Query("userId") long userId, @Query("platform") String platform);

    @POST("/update_purchase.php") UpdatePurchaseParser updatePurchase(@Query("userId") long userId, @Query("platform") String platform, @Body PurchaseTransaction purchaseTransaction);

    @GET("/update_purchase.php") UpdatePurchaseParser updatePurchaseGet(@Query("userId") long userId, @Query("platform") String platform, @Query("itemId") String itemId, @Query("transactionId") String transactionId);

}
