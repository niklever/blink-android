package com.blinktrainingsystem.blink.data.model.pojo;

import java.util.List;

/**
 * Created by TALE on 10/2/2014.
 */
public class Input {
    public int type;//": 7,
    public String prompt;//": "Think about your own confidence as a communicator. Rate yourself by moving the ball below right (more confident) or left (less). Then write about it. ",
    public boolean locked;//": false,
    public String text;//": "",
    public boolean isNumber;//": false,
    public boolean useImages;//": true,
    public String left;//": "AFF2029D-BECF-F899-4DA5-F22F17F97041.png",
    public String right;//": "C019DB10-E215-2EDD-7C65-234626BACD30.png",
    public int ideal;//": 50
    public String sound;
    public int count;
    public List<InputBoolean> booleans;
    public String max;
    public String min;
    public boolean interger;
    public String value;//": "08-05-2014",
    public Date date;
}
