package com.blinktrainingsystem.blink.data.model.pojo;

import java.util.List;

/**
 * Created by TALE on 10/10/2014.
 */
public class Catch {
    public String prompt;//:	As the words fly towards you, touch those that make up the big idea of this course.
    public List<CatchItem> items;//[10]
    public boolean ordered;//:	true
    public int timer;//:	120
    public String sound;//:
    public int speed;//:	5
    public int frequency;//:	6
}
