package com.blinktrainingsystem.blink.data.model.pojo;

import com.blinktrainingsystem.blink.iap.util.Base64;

/**
 * Created by TALE on 12/26/2014.
 */
public class PurchaseTransaction {
    public String itemId;//: refer to alias_item in 'Purchase List'
    public String transactionId;//: transactionId getting from SDK
    public String receiptId;//: transactionId getting from SDK

    public PurchaseTransaction(String itemId, String transactionId) {
        this.itemId = itemId;
        this.transactionId = transactionId;
        this.receiptId = Base64.encode(transactionId.getBytes());
    }
}
