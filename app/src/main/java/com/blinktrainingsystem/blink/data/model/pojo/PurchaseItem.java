package com.blinktrainingsystem.blink.data.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TALE on 12/24/2014.
 */
public class PurchaseItem implements Parcelable {
    public long id;//": "1",
    public String packageId;//": "com.blinktrainingsystem.credits.package1",
    public String desc;//": "100 credits",
    public double price;//": "0.99",
    public int credits;//": "100",
    public String icon;//": "iap-icon100.png",
    public String iconUrl;//": "http://blinktrainingsystem.com/images/iap-icon100.png"

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.packageId);
        dest.writeString(this.desc);
        dest.writeDouble(this.price);
        dest.writeInt(this.credits);
        dest.writeString(this.icon);
        dest.writeString(this.iconUrl);
    }

    public PurchaseItem() {
    }

    private PurchaseItem(Parcel in) {
        this.id = in.readLong();
        this.packageId = in.readString();
        this.desc = in.readString();
        this.price = in.readDouble();
        this.credits = in.readInt();
        this.icon = in.readString();
        this.iconUrl = in.readString();
    }

    public static final Creator<PurchaseItem> CREATOR = new Creator<PurchaseItem>() {
        public PurchaseItem createFromParcel(Parcel source) {
            return new PurchaseItem(source);
        }

        public PurchaseItem[] newArray(int size) {
            return new PurchaseItem[size];
        }
    };
}
