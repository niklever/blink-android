package com.blinktrainingsystem.blink.data.model.parser;

import android.text.TextUtils;

import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.model.Category;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.util.FileUtils;
import com.blinktrainingsystem.blink.util.JsonParser;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.util.List;

import timber.log.Timber;

/**
 * Created by TALE on 9/17/2014.
 */
public class CourseHelper {

    public static final String BLINK_FILE = "blinks.json";
    public static final String GROWTH_FILE = "growth.json";
    public static final String COURSE_FILE = "course.json";

    final Gson gson = new Gson();
    final LocalFileManager localFileManager;
    private final long courseId;
    private final LocalFileManager.Folder folder;

    public CourseHelper(LocalFileManager localFileManager, long courseId, LocalFileManager.Folder folder) {
        this.localFileManager = localFileManager;
        this.courseId = courseId;
        this.folder = folder;
    }

    public List<Blink> getBlinks() {

        String blinkJson = FileUtils.readTextFile(localFileManager.getCourseFile(courseId, folder).getAbsolutePath() + "/" + BLINK_FILE);
        Timber.d("blinkJson: " + blinkJson);
        if (TextUtils.isEmpty(blinkJson)) {
            return null;
        }


        List<Blink> blinks = JsonParser.parseArray(
                blinkJson, new TypeToken<List<Blink>>() {
                }
        );
        Timber.d("Size: " + blinks.size());
        Ordering<Blink> createByOrdering = Ordering.natural().nullsFirst().onResultOf(
                new Function<Blink, Integer>() {
                    @Override
                    public Integer apply(Blink input) {
                        return input.index;
                    }
                }
        );

        return createByOrdering.sortedCopy(blinks);
    }

//    public Blink getBlink(int index) {
//        getBlinks();
//        if (blinkList != null && blinkList.size() > 0) {
//
//            for (Blink blink : blinkList) {
//                if (blink.index == index) {
//                    return blink;
//                }
//            }
//
//        }
//        return null;
//    }
//
//    public List<Blink> getAllBursts() {
//        getBlinks();
//        if (blinkList != null && blinkList.size() > 0) {
//            List<Blink> bursts = Lists.newArrayList();
//            for (Blink blink : blinkList) {
//                if (blink.type == BlinkType.KBlinkBurstStart) {
//                    bursts.add(blink);
//                }
//            }
//            return bursts;
//        }
//        return null;
//    }
//
//    public int getBurstIndex(int index) {
//        getBlinks();
//        int unit = 0;
//        if (blinkList != null && blinkList.size() > index) {
//            int available = blinkList.size() - 1;
//            if (available > index) {
//                available = index;
//            }
//            for (int i = 0; i <= available; i++) {
//                Blink blink = blinkList.get(i);
//                if (blink.type == BlinkType.KBlinkBurstStart) {
//                    unit++;
//                }
//            }
//        }
//        return unit;
//    }
//
//    public List<Blink> getBlinkOfBurst(int burstIndex) {
//        getBlinks();
//        if (blinkList == null || blinkList.size() == 0 || burstIndex < 0 || burstIndex >= blinkList.size()) {
//            return null;
//        }
//        List<Blink> result = Lists.newArrayList();
//        int i = burstIndex;
//        Blink blink = blinkList.get(i);
//        while (blink.type != BlinkType.KBlinkTypeSpecificContentBurstEnd) {
//            result.add(blink);
//            i++;
//            blink = blinkList.get(i);
//        }
//        return result;
//    }
//
//    public int getUnitIndex(int index) {
//        getBlinks();
//        int unit = 0;
//        if (blinkList != null && blinkList.size() > index) {
//            int available = blinkList.size() - 1;
//            if (available > index) {
//                available = index;
//            }
//            for (int i = 0; i <= available; i++) {
//                Blink blink = blinkList.get(i);
//                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
//                    unit++;
//                }
//            }
//        }
//        return unit;
//    }
//
//    public Blink getUnit(int index) {
//        getBlinks();
//        if (blinkList != null && blinkList.size() > index) {
//            for (int i = index; i >= 0; i--) {
//                Blink blink = blinkList.get(i);
//                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
//                    return blink;
//                }
//            }
//        }
//        return null;
//    }

    public List<Blink> getGrowth() {
        String growthJson = FileUtils.readTextFile(localFileManager.getCourseFile(courseId, folder).getAbsolutePath() + "/" + GROWTH_FILE);
        Timber.d("growthJson: " + growthJson);
        if (TextUtils.isEmpty(growthJson)) {
            return null;
        }

        List<Blink> blinks = gson.fromJson(
                growthJson, new TypeToken<List<Blink>>() {
                }.getType()
        );
        Timber.d("Size: " + blinks.size());
        return blinks;
    }

    public Course getCourse() {
        File courseFile = new File(localFileManager.getCourseFile(courseId, folder), COURSE_FILE);
        Course course = new Course();
        if (courseFile.exists()) {
            String courseJson = FileUtils.readTextFile(courseFile.getAbsolutePath());
            Timber.d("courseJson: " + courseJson);
            if (!TextUtils.isEmpty(courseJson) && !courseJson.equals("empty")) {
                course = gson.fromJson(courseJson, Course.class);
            }
        }
        course.id = courseId;
        course.folderName = folder.toString();
        course.category = Category.user.toString();
        return course;

    }

    public boolean saveToDisk(Course course) {
        File file = localFileManager.getCourseFile(courseId, folder);
        if (!file.exists()) {
            file.mkdirs();
        }
        final String courseJson = gson.toJson(course);
        try {
            FileUtils.write(courseJson.getBytes(), file.getAbsolutePath() + "/" + COURSE_FILE);
        } catch (IOException e) {
            Timber.e(e, "Write course to disk error");
            return false;
        }
        return true;
    }

    public String getContentPath(String name) {
        File courseFile = localFileManager.getCourseFile(courseId, folder);
        return courseFile.getAbsolutePath() + File.separator + name;
    }

    public boolean saveAsEmpty() {
        File file = localFileManager.getCourseFile(courseId, folder);
        if (file.exists()) {
            FileUtils.rm(file);
        }
        try {
            FileUtils.write("empty".getBytes(), file.getAbsolutePath());
        } catch (IOException e) {
            Timber.e(e, "Write empty file error");
            return false;
        }
        return true;
    }
}
