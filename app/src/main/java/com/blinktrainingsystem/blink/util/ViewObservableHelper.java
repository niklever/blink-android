package com.blinktrainingsystem.blink.util;

import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by TALE on 10/1/2014.
 */
public class ViewObservableHelper {

    public static Observable<View> globalLayoutFrom(final View view) {
        final PublishSubject<View> subject = PublishSubject.create();
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (Build.VERSION.SDK_INT >= 16) {
                            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                        subject.onNext(view);
                        subject.onCompleted();
                    }
                }
        );
        return subject;
    }

    public static Observable<Integer> keyboardVisibilityChanged(final View view) {
        final PublishSubject<Integer> subject = PublishSubject.create();
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    private int lastHeight = -1;
                    private boolean isInitialized;

                    @Override
                    public void onGlobalLayout() {
                        final int height = view.getHeight();
                        if (!isInitialized) {
                            isInitialized = lastHeight >= height;
                        }
                        if (isInitialized) {
                            if (lastHeight > height) {
                                subject.onNext(View.VISIBLE);
                            } else {
                                subject.onNext(View.INVISIBLE);
                            }
                        }
                        lastHeight = height;

                    }
                }
        );
        return subject;
    }

    public static Observable<View> longClick(View view) {
        final PublishSubject<View> subject = PublishSubject.create();
        view.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        subject.onNext(v);
                        return true;
                    }
                }
        );
        return subject;
    }

    public static Observable<View> detectDragVertical(View view) {
        final PublishSubject<View> subject = PublishSubject.create();
        view.setOnTouchListener(
                new View.OnTouchListener() {
                    public static final int INVALID_POINTER_ID = -1;
                    // The ‘active pointer’ is the one currently moving our object.
                    private int mActivePointerId = INVALID_POINTER_ID;
                    private float startY = 0;

                    @Override
                    public boolean onTouch(View v, MotionEvent ev) {

                        final int action = MotionEventCompat.getActionMasked(ev);

                        switch (action) {
                            case MotionEvent.ACTION_DOWN: {
                                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                                startY = MotionEventCompat.getY(ev, pointerIndex);
                                // Save the ID of this pointer (for dragging)
                                mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
                                break;
                            }

                            case MotionEvent.ACTION_MOVE: {
                                // Find the index of the active pointer and fetch its position
                                final int pointerIndex =
                                        MotionEventCompat.findPointerIndex(ev, mActivePointerId);

                                final float x = MotionEventCompat.getX(ev, pointerIndex);
                                final float y = MotionEventCompat.getY(ev, pointerIndex);

                                // Calculate the distance moved
                                final float dy = y - startY;

                                if (dy > 50) {
                                    startY = y;
                                    subject.onNext(v);
                                    return true;
                                }

                                break;
                            }

                            case MotionEvent.ACTION_UP: {
                                mActivePointerId = INVALID_POINTER_ID;
                                break;
                            }

                            case MotionEvent.ACTION_CANCEL: {
                                mActivePointerId = INVALID_POINTER_ID;
                                break;
                            }

                            case MotionEvent.ACTION_POINTER_UP: {

                                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                                final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);

                                if (pointerId == mActivePointerId) {
                                    // This was our active pointer going up. Choose a new
                                    // active pointer and adjust accordingly.
                                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                                    mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
                                }
                                break;
                            }
                        }
                        return true;
                    }
                }
        );
        return subject;
    }

    public static <T extends View> Observable<T> clicksFrom(T view) {
        final PublishSubject publishSubject = PublishSubject.create();
        view.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        publishSubject.onNext(v);
                    }
                }
        );
        return publishSubject.asObservable();
    }
}
