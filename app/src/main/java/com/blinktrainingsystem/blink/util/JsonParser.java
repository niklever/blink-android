package com.blinktrainingsystem.blink.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import timber.log.Timber;

/**
 * Utility class use to parse json.
 */
public class JsonParser {

    private static final Gson GSON = new Gson();

    public static <T> T parseObject(String jsonString, Class<? extends T> clazz) {
        final String tempJsonString;
        if (jsonString.contains("\"default\":")) {
            // We need to do replace default by _default because default is a key in Java. So, we can't define a variable called default.
            tempJsonString = jsonString.replace("\"default\":", "\"_default\":");
        } else {
            tempJsonString = jsonString;
        }
        Timber.d("json to parse: " + tempJsonString);
        try {
            return GSON.fromJson(tempJsonString, clazz);
        } catch (Exception e) {
            Timber.e(e, "parse json failure");
        }
        return null;
    }

    public static <T> List<T> parseArray(String jsonString, TypeToken<List<T>> typeToken) {
        final String tempJsonString;
        if (jsonString.contains("\"default\":")) {
            // We need to do replace default by _default because default is a key in Java. So, we can't define a variable called default.
            tempJsonString = jsonString.replace("\"default\":", "\"_default\":");
        } else {
            tempJsonString = jsonString;
        }
        Timber.d("json to parse: " + tempJsonString);
        try {
            return GSON.fromJson(tempJsonString, typeToken.getType());
        } catch (Exception e) {
            Timber.e(e, "parse json failure");
        }
        return null;
    }

}
