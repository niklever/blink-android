package com.blinktrainingsystem.blink.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by TALE on 10/21/2014.
 */
public class ImageLoader {
    private final Picasso picasso;

    @Inject
    public ImageLoader(Picasso picasso) {
        this.picasso = picasso;
    }

    public Observable<Bitmap> load(int resourceId, int targetWidth, int targetHeight) {
        return load(picasso.load(resourceId).resize(targetWidth, targetHeight));
    }

    public Observable<Bitmap> load(File file) {
        return load(picasso.load(file));
    }

    public Observable<Bitmap> load(String url) {
        return load(picasso.load(url));
    }

    public Observable<int[]> size(final String file) {
        return Observable.create(
                new Observable.OnSubscribe<int[]>() {
                    @Override
                    public void call(Subscriber<? super int[]> subscriber) {
                        if (!subscriber.isUnsubscribed()) {
                            try {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(file, options);
                                int[] size = new int[]{options.outWidth, options.outHeight};
                                subscriber.onNext(size);
                            } catch (Exception e) {
                                subscriber.onError(e);
                            } finally {
                                subscriber.onCompleted();
                            }
                        }
                    }
                }
        );
    }

    public Observable<int[]> size(final Resources resources, final int resId) {
        return Observable.create(
                new Observable.OnSubscribe<int[]>() {
                    @Override
                    public void call(Subscriber<? super int[]> subscriber) {
                        if (!subscriber.isUnsubscribed()) {
                            try {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeResource(resources, resId, options);
                                int[] size = new int[]{options.outWidth, options.outHeight};
                                subscriber.onNext(size);
                                subscriber.onCompleted();
                            } catch (Exception e) {
                                subscriber.onError(e);
                            }
                        }
                    }
                }
        );
    }

    private Observable<Bitmap> load(final RequestCreator requestCreator) {
        return Observable.create(
                new Observable.OnSubscribe<Bitmap>() {
                    @Override
                    public void call(Subscriber<? super Bitmap> subscriber) {
                        if (!subscriber.isUnsubscribed()) {
                            try {
                                final Bitmap bitmap = requestCreator.get();
                                subscriber.onNext(bitmap);
                                subscriber.onCompleted();
                            } catch (Exception e) {
                                subscriber.onError(e);
                            }
                        }

                    }
                }
        )
                .cache();
    }
}
