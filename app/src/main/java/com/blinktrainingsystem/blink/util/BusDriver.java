package com.blinktrainingsystem.blink.util;

import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.module.playcourse.menu.Footer3ButtonFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.HeaderMenuFragment;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;

/**
 * Created by TALE on 10/9/2014.
 */
public class BusDriver {

    final EventBus bus;
    private static BusDriver instance;

    private BusDriver(EventBus bus) throws NullPointerException {
        if (bus == null) {
            throw new NullPointerException("bus can not be null");
        }
        this.bus = bus;
    }

    public static BusDriver withBus(EventBus bus) {
        if (instance == null || bus != bus) {
            instance = new BusDriver(bus);
        }
        return instance;
    }

    public BusDriver showHelp(String text) {
        bus.post(Event.ShowHelp.setExtra(text));
        return this;
    }

    public BusDriver closeHelp() {
        bus.post(Event.CloseHelp);
        return this;
    }

    public BusDriver showTimer(int time) {
        bus.post(Event.SetTimer.setExtra(time));
        bus.post(Event.ShowTimer.setExtra(true));
        return this;
    }

    public BusDriver hideTimer() {
        bus.post(Event.ShowTimer.setExtra(false));
        return this;
    }

    public BusDriver setFooterMenu(String left, String mid, String right, boolean disableRightButton) {
        final Footer3ButtonFragment.FooterMenuButton footerMenuButton = new Footer3ButtonFragment.FooterMenuButton(left, mid, right);
        footerMenuButton.type = 1;
        footerMenuButton.disableRightButton = disableRightButton;
        bus.post(footerMenuButton);
        return this;
    }

    public BusDriver setFooterMenu(String left, String mid, String right) {
        bus.post(new Footer3ButtonFragment.FooterMenuButton(left, mid, right));
        return this;
    }

    public BusDriver headerMenuStepClick(int offset) {
        bus.post(Event.HeaderMenuStepClick.setExtra(offset));
        return this;
    }

    public BusDriver hideHeaderMenuProgress() {
        bus.post(new HeaderMenuFragment.MenuConfig(false, 0, 0));
        return this;
    }

    public BusDriver showHeaderMenuProgress(int total, int step) {
        bus.post(new HeaderMenuFragment.MenuConfig(true, total, step));
        return this;
    }


    public BusDriver showHeaderMenuSocial() {
        bus.post(new HeaderMenuFragment.MenuConfig(true, true));
        return this;
    }

    public BusDriver hideHeaderMenuSocial() {
        bus.post(new HeaderMenuFragment.MenuConfig(false, false));
        return this;
    }

    public BusDriver notifyBlinkCompleted() {
        bus.post(Event.BlinkCompleted);
        return this;
    }

    public BusDriver pauseTimer() {
        bus.post(Event.PauseTimer);
        return this;
    }

    public BusDriver startTimer() {
        bus.post(Event.StartTimer);
        return this;
    }

    public BusDriver addTimer(int addedTime) {
        bus.post(Event.AddTimer.setExtra(addedTime));
        return this;
    }

    public BusDriver updateScore(BlinkHandler blinkHandler) {
        bus.post(blinkHandler);
        return this;
    }

    public BusDriver allowSkipChange() {
        bus.post(Event.AllowSkipChange);
        return this;
    }

    public BusDriver enableActivity() {
        bus.post(Event.UnlockActivity);
        return this;
    }

    public BusDriver openDossier() {
        bus.post(Event.OpenDossier);
        return this;
    }

    public BusDriver shareFb() {
        bus.post(Event.ShareFb);
        return this;
    }

    public BusDriver shareTw() {
        bus.post(Event.ShareTw);
        return this;
    }
}
