package com.blinktrainingsystem.blink.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by TALE on 9/10/2014.
 */
public class ViewAnimator {

    private boolean autoRun;

    @Inject
    public ViewAnimator() {
        this(true);
    }

    public ViewAnimator(boolean autoRun) {
        this.autoRun = autoRun;
    }

    public Animator enterLeft(final View target, long duration) {
        return enterLeft(target, duration, null);
    }

    public Animator enterLeft(final View target, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int desPos = 0;
        target.setVisibility(View.VISIBLE);
        final int startPos = -target.getLeft() - target.getWidth();
        target.setTranslationX(startPos);
        ObjectAnimator translateX = ObjectAnimator.ofFloat(target, "translationX", desPos);
        translateX.setDuration(duration);
        translateX.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationX(desPos);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        if (autoRun) {
            translateX.start();
        }
        return translateX;
    }

    public Animator enterRight(final View target, long duration) {
        return enterRight(target, duration, null);
    }

    public Animator enterRight(final View target, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int desPos = 0;
        final int startPos = ((ViewGroup) target.getParent()).getWidth() - target.getLeft();
        target.setTranslationX(startPos);
        target.setVisibility(View.VISIBLE);
        ObjectAnimator translateX = ObjectAnimator.ofFloat(target, "translationX", desPos);
        translateX.setDuration(duration);
        translateX.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationX(desPos);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        if (autoRun) {
            translateX.start();
        }
        return translateX;
    }


    public Animator exitLeft(final View target, long duration) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int originalPos = target.getRight() - target.getWidth();
        if (target.getVisibility() != View.VISIBLE || target.getAlpha() == 0) {
            // Do nothing if it's disappear already.
            target.setVisibility(View.GONE);
            target.setTranslationX(originalPos);
            return null;
        }
        target.setTranslationX(originalPos);
        target.setVisibility(View.VISIBLE);
        ObjectAnimator translateX = ObjectAnimator.ofFloat(target, "translationX", originalPos, -target.getRight());
        translateX.setDuration(duration);
        translateX.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setVisibility(View.GONE);
                        target.setTranslationX(0);
                    }
                }
        );
        if (autoRun) {
            translateX.start();
        }
        return translateX;
    }


    public Animator enterBottom(final View target, long duration) {
        return enterBottom(target, duration, null);
    }

    public Animator enterBottom(final View target, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int startPos = target.getHeight();
        target.setTranslationY(startPos);
        target.setVisibility(View.VISIBLE);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", 0);
        translateY.setDuration(duration);
        translateY.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationY(0);
                    }
                }
        );
        if (autoRun) {
            translateY.start();
        }
        return translateY;
    }

    public Animator bottomUp(final View target, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int startPos = ((ViewGroup) target.getParent()).getHeight() - target.getTop();
        target.setTranslationY(startPos);
        target.setVisibility(View.VISIBLE);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", 0);
        translateY.setDuration(duration);
        translateY.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationY(0);
                    }
                }
        );
        if (autoRun) {
            translateY.start();
        }
        return translateY;
    }

    public Animator exitBottom(final View target, long duration, final Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }
        if (target.getVisibility() != View.VISIBLE || target.getAlpha() == 0) {
            return null;
        }

        final int desPos = target.getHeight();
        target.setTranslationY(0);
        target.setVisibility(View.VISIBLE);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", desPos);
        translateY.setDuration(duration);
        translateY.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setVisibility(View.GONE);
                        target.setTranslationY(0);
                    }
                }
        );
        if (autoRun) {
            translateY.start();
        }
        return translateY;
    }

    public Animator enterFadeIn(final View target, long duration, AnimatorListenerAdapter listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        float alpha = 1;
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(target, "alpha", alpha);
        alphaAnim.setDuration(duration);
        alphaAnim.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setAlpha(1);
                        target.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        target.setAlpha(1);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        target.setAlpha(0);
        target.setVisibility(View.VISIBLE);
        alphaAnim.start();
        return alphaAnim;
    }

    public Animator fade(final View target, float desAlpha, long duration, AnimatorListenerAdapter listener) {
        if (target == null || duration <= 0) {
            return null;
        }
        target.setVisibility(View.VISIBLE);
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(target, "alpha", desAlpha);
        alphaAnim.setDuration(duration);
        if (listener != null) {
            alphaAnim.addListener(listener);
        }
        alphaAnim.start();
        return alphaAnim;
    }

    public Animator exitFadeOut(final View target, long duration, AnimatorListenerAdapter listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        if (target.getVisibility() != View.VISIBLE || target.getAlpha() == 0) {
            target.setAlpha(1);
            target.setVisibility(View.GONE);
            return null;
        }
        float alpha = 0;
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(target, "alpha", alpha);
        alphaAnim.setDuration(duration);
        alphaAnim.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setAlpha(1);
                        target.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        target.setAlpha(1);
                        target.setVisibility(View.GONE);
                    }
                }
        );
        target.setAlpha(1);
        target.setVisibility(View.VISIBLE);
        alphaAnim.start();
        return alphaAnim;
    }

    public Animator blink(final View target, long duration) {
        return blink(target, 7, duration, null);
    }

    public static Animator blink(final View target, int repeatCount, long duration, final Animator.AnimatorListener listener) {
        if (target == null || duration < 0) {
            return null;
        }

        ObjectAnimator alpha = ObjectAnimator.ofFloat(target, "alpha", 1.0f, 0.6f);
        alpha.setRepeatMode(ValueAnimator.REVERSE);
        alpha.setRepeatCount(repeatCount);
        alpha.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setAlpha(1);
                    }
                }
        );
        if (duration > 0) {
            alpha.setDuration(duration);
        }
        alpha.start();
        return alpha;
    }

    public Animator enterLeftTop(final View target, long duration) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int desX = 0;
        final int desY = 0;
        final int startX = -target.getWidth();
        final int startY = -target.getHeight();
        Timber.d("enterLeftTop ==> {desX:%d, desY:%d, startX:%d, startY:%d}", desX, desY, startX, startY);
        target.setTranslationX(startX);
        target.setTranslationY(startY);
        ObjectAnimator translateX = ObjectAnimator.ofFloat(target, "translationX", startX, desX);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", startY, desY);
        target.setVisibility(View.VISIBLE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(translateX, translateY);
        animatorSet.setDuration(duration);
        animatorSet.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationX(0);
                        target.setTranslationY(0);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        if (autoRun) {
            animatorSet.start();
        }
        return animatorSet;
    }

    public Animator enterTop(final View target, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return null;
        }

        final int desY = 0;
        final int startY = -target.getHeight();
        Timber.d("enterTop ==> {desY:%d, startY:%d}", desY, startY);
        target.setTranslationY(startY);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", startY, desY);
        target.setVisibility(View.VISIBLE);
        translateY.setDuration(duration);
        translateY.addListener(
                new AnimatorListenerWrapper(listener) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setTranslationY(0);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        if (autoRun) {
            translateY.start();
        }
        return translateY;
    }

    public void move(final View target, float x, float y, long duration, Animator.AnimatorListener listener) {
        if (target == null || duration <= 0) {
            return;
        }

        Timber.d("enterLeftTop ==> {desX:%f, desY:%f}", x, y);
        ObjectAnimator translateX = ObjectAnimator.ofFloat(target, "translationX", x);
        ObjectAnimator translateY = ObjectAnimator.ofFloat(target, "translationY", y);
        target.setVisibility(View.VISIBLE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(translateX, translateY);
        animatorSet.setDuration(duration);
        animatorSet.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        target.setVisibility(View.VISIBLE);
                    }
                }
        );
        if (autoRun) {
            animatorSet.start();
        }
    }

    static class AnimatorListenerWrapper extends AnimatorListenerAdapter {
        private final Animator.AnimatorListener listener;

        AnimatorListenerWrapper(Animator.AnimatorListener listener) {
            this.listener = listener;
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (listener != null) {
                listener.onAnimationEnd(animation);
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            if (listener != null) {
                listener.onAnimationCancel(animation);
            }
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
            super.onAnimationRepeat(animation);
            if (listener != null) {
                listener.onAnimationRepeat(animation);
            }
        }


        @Override
        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            if (listener != null) {
                listener.onAnimationStart(animation);
            }
        }
    }

}
