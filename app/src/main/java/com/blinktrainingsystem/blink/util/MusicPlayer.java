package com.blinktrainingsystem.blink.util;

import android.app.Application;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by talenguyen on 21/11/2014.
 */
public class MusicPlayer {
    private MediaPlayer mediaPlayer;
    private boolean isInitialized;
    private final Application appContext;
    private boolean isPlaying;
    private boolean isPaused;

    @Inject
    public MusicPlayer(Application appContext) {
        this.appContext = appContext;
    }

    public static interface Callback {
        public void onCompleted();
    }

    public void play(String filePath, final Callback callback, boolean loop) {
        play(filePath, callback, null, loop);
    }

    public void play(String filePath, final Callback callback, final MediaPlayer.OnPreparedListener onPreparedListener, boolean loop) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        } else if (isInitialized) {
            mediaPlayer.reset();
            isInitialized = false;
        }
        mediaPlayer.setOnErrorListener(
                new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        Timber.d("onError");
                        if (callback != null) {
                            callback.onCompleted();
                        }
                        return true;
                    }
                }
        );
        mediaPlayer.setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Timber.d("onCompletion");
                        if (callback != null) {
                            callback.onCompleted();
                        }
                    }
                }
        );
        mediaPlayer.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        isInitialized = true;
                        play();
                        if (onPreparedListener != null) {
                            onPreparedListener.onPrepared(mp);
                        }
                    }
                }
        );
        try {
            Timber.d("Play");
            stop();
            mediaPlayer.reset();
            mediaPlayer.setDataSource(appContext, Uri.fromFile(new File(filePath)));
            mediaPlayer.prepareAsync();
            mediaPlayer.setLooping(loop);
        } catch (IOException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onCompleted();
            }
        }
    }

    public void play() {
        if (isPlaying) {
            return;
        }
        if (isInitialized) {
            mediaPlayer.start();
            isPlaying = true;
            isPaused = false;
        }
    }

    public void resume() {
        if (isPaused) {
            play();
        }
    }

    public void pause() {
        if (mediaPlayer != null && isPlaying) {
            mediaPlayer.pause();
            isPaused = true;
            isPlaying = false;
        }
    }

    public void stop() {
        if (isPlaying || isPaused) {
            mediaPlayer.stop();
            isPlaying = isPaused = false;
            isInitialized = false;
        }
    }

    public void release() {
        if (isInitialized) {
            stop();
            mediaPlayer.release();
            isInitialized = false;
        }
    }
}
