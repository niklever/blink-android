package com.blinktrainingsystem.blink.util;

import android.app.Application;
import android.media.AudioManager;
import android.media.SoundPool;
import android.text.TextUtils;
import android.util.SparseIntArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import timber.log.Timber;

public class SoundManager {
    private Application context;
    private SoundPool sndPool;
    private float rate = 1.0f;
    private float masterVolume = 1.0f;
    private float leftVolume = 1.0f;
    private float rightVolume = 1.0f;
    private float balance = 0.5f;
    private SparseIntArray idMap = new SparseIntArray();
    private Map<String, Integer> pathMap = new HashMap<String, Integer>();
    private List<Integer> playings;
    private boolean isStopped;

    // Constructor, setup the audio manager and store the app context
    @Inject
    public SoundManager(Application appContext) {
        sndPool = new SoundPool(40, AudioManager.STREAM_MUSIC, 100);
        context = appContext;
    }

    /**
     * Load sound from resource's id (e.g R.raw.sound1).
     *
     * @param resId The resource's id to be load.
     */
    public void load(int resId) {
        int id = sndPool.load(context, resId, 1);
        idMap.put(resId, id);
    }

    /**
     * Call to load sound for the given path.
     *
     * @param path The path to sound stored on disk.
     */
    public void load(String path) throws NullPointerException {
        if (TextUtils.isEmpty(path)) {
            throw new NullPointerException("path mus not be null");
        }

        final int id = sndPool.load(path, 1);
        pathMap.put(path, id);
    }

    /**
     * Call to load sound for the given resource's id then play when load completed.
     *
     * @param resId The resource's id to be load.
     */
    public void loadAndPlay(final int resId) {
        loadAndPlay(resId, false);
    }

    /**
     * Call to load sound for the given resource's id then play when load completed.
     *
     * @param resId The resource's id to be load.
     */

    public void loadAndPlay(final int resId, final boolean loop) {
        final int id = idMap.get(resId);
        if (id != 0) {
            playById(id, loop);
        } else {
            sndPool.setOnLoadCompleteListener(
                    new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                            Timber.d("loadAndPlay => onLoadComplete {status: %d, resId: %d, isStopped: %s}", status, resId, isStopped);
                            sndPool.setOnLoadCompleteListener(null); // => Remove onLoadCompleted.
                            if (isStopped) {
                                return;
                            }
                            if (status == 0) {
                                idMap.put(resId, sampleId);
                                playById(sampleId, loop);
                            } else {
                                Timber.e("Load sound error.");
                            }
                        }
                    }
            );
            load(resId);
        }
    }

    /**
     * Call to load sound for the given path then play when load completed.
     *
     * @param path to the file stored on disk.
     */
    public void loadAndPlay(String path) {
        loadAndPlay(path, false);
    }

    /**
     * Call to load sound for the given path then play when load completed.
     *
     * @param path to the file stored on disk.
     */
    public void loadAndPlay(String path, final boolean loop) {
        if (pathMap.containsKey(path)) {
            final int id = pathMap.get(path);
            playById(id, loop);
        } else {
            sndPool.setOnLoadCompleteListener(
                    new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                            sndPool.setOnLoadCompleteListener(null); // => Remove onLoadCompleted.
                            if (isStopped) {
                                return;
                            }
                            if (status == 0) {
                                playById(sampleId, loop);
                            } else {
                                Timber.e("Load sound error.");
                            }
                        }
                    }
            );
            load(path);
        }
    }

    /**
     * Play the sound which is pre-load by its id.
     *
     * @param soundId The id in the sound pool.
     */
    private void playById(int soundId) {
        playById(soundId, false);
    }

    /**
     * Play the sound which is pre-load by its id.
     *
     * @param soundId The id in the sound pool.
     */
    private void playById(int soundId, boolean loop) {
        final int streamId = sndPool.play(soundId, leftVolume, rightVolume, 1, loop ? -1 : 0, rate);
        if (playings == null) {
            playings = new ArrayList<Integer>();
        }
        playings.add(streamId);
    }

    public void stopPlaying() {
        if (playings == null || playings.size() == 0) {
            return;
        }
        for (Integer streamId : playings) {
            sndPool.stop(streamId);
        }
        playings.clear();
    }

    public void pausePlaying() {
        if (playings == null || playings.size() == 0) {
            return;
        }
        for (Integer streamId : playings) {
            sndPool.pause(streamId);
        }
    }

    // Set volume values based on existing balance value
    public void setVolume(float vol) {
        Timber.d("setVolume: %f", vol);
        masterVolume = vol;

        if (balance < 1.0f) {
            leftVolume = masterVolume;
            rightVolume = masterVolume * balance;
        } else {
            rightVolume = masterVolume;
            leftVolume = masterVolume * (2.0f - balance);
        }

    }

    public void setSpeed(float speed) {
        rate = speed;

        // Speed of zero is invalid
        if (rate < 0.01f)
            rate = 0.01f;

        // Speed has a maximum of 2.0
        if (rate > 2.0f)
            rate = 2.0f;
    }

    public void setBalance(float balVal) {
        balance = balVal;

        // Recalculate volume levels
        setVolume(masterVolume);
    }


    // Free ALL the things!
    public void release() {
        isStopped = true;
        if (idMap != null) {
            idMap.clear();
        }
        if (pathMap != null) {
            pathMap.clear();
        }
        stopPlaying();
        sndPool.release();
        sndPool = new SoundPool(27, AudioManager.STREAM_MUSIC, 100);
    }

    public void resumePlaying() {

    }
}
