package com.blinktrainingsystem.blink.util;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.blinktrainingsystem.blink.R;

/**
 * Created by TALE on 9/11/2014.
 */
public class Helper {

    public static void hideSoftKeyboard(View focused) {
        if (focused == null) {
            return;
        }
        final Context context = focused.getContext();
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE
        );

        imm.hideSoftInputFromWindow(focused.getWindowToken(), 0);
    }

    public static String getStringWithReplacements(String input) {
        if (TextUtils.isEmpty(input)) {
            return "";
        }
        String result = input.replace("&#27;", "'");
        result = result.replace("&#39;", "'");
        result = result.replace("&#10;", "\n");
        result = result.replace("&#34;", "\"");
        result = result.replace(" nn ", "\n\n");
        result = result.replace(".nn", "\n\n");
        return result;
    }

    public static String getStringWithReplacementOmitNewLineEnd(String input) {
        if (TextUtils.isEmpty(input)) {
            return "";
        }
        String result = getStringWithReplacements(input);
        int lastIndexOf = result.lastIndexOf('\n');
        int length = result.length();
        if (lastIndexOf == length - 1) {
            final char[] chars = result.toCharArray();
            for (int i = chars.length - 1; i >= 0; i--) {
                if (chars[i] != '\n') {
                    return result.substring(0, i + 1);
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> T from(Object implementor) {
        try {
            return (T) implementor;
        } catch (ClassCastException e) {
            throw new RuntimeException("The parent " + implementor.toString() + " does not inherit / implement the wanted interface", e);
        }
    }

    public static int randomGemId() {
        final int index = (int) (System.currentTimeMillis() % 5);
        switch (index) {
            case 4:
                return R.drawable.gem0006;
            case 3:
                return R.drawable.gem0005;
            case 2:
                return R.drawable.gem0004;
            case 1:
                return R.drawable.gem0003;
            case 0:
                return R.drawable.gem0002;
        }
        return R.drawable.gem0002;
    }
}
