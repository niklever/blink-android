package com.blinktrainingsystem.blink.util;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Inject;

/**
 * Created by TALE on 12/30/2014.
 */
public class TrackerHelper {

    private static final String LOGIN_VIEW = "vLogin";
    private static final String LOGIN_CATE = "vLogin";
    private static final String LOGIN_ACTION = "loginSuccess";
    private static final String REGISTER_VIEW = "vRegister";
    private static final String REGISTER_ACTION = "registerSuccess";
    private static final String COURSE_LISTING_VIEW = "vCoursesListing";
    private static final String MY_COURSES_VIEW = "vMyCoursesListing";
    private static final String PRESENTATION_VIEW = "vPresentation";
    private static final String DRAG_N_DROP_VIEW = "vDragNDrop";
    private static final String QUESTION_VIEW = "vQuestion";
    private static final String INPUT_VIEW = "vInput";
    private static final String WORD_FILL_VIEW = "vWordFill";
    private static final String COURSE_COMPLETED = "vCourseComplete";
    private static final String COURSE_COMPLETED_ACTION = "courseCompleted";
    private static final String COURSE_LITE_COMPLETED = "vLiteCourseComplete";
    private static final String COURSE_LITE_COMPLETED_ACTION = "liteCourseCompleted";
    private static final String FEEDBACK_VIEW = "vFeedback";
    private static final String INTRO_LOAD = "vBurstIntro";
    private static final String PROGRESS_VIEW = "vProgressScreen";
    private static final String PROGRESS_LITE_VIEW = "vProgressScreenLiteCourse";
    private static final String DOSSIER_VIEW = "vDossier";
    private static final String BUILD_STATEMENT_VIEW = "vBuildStatement";
    private static final String VIDEO_VIEW = "vVideo";
    private static final String WORD_HEAT_VIEW = "vWordHeat";
    private static final String ROTATOR_VIEW = "vRotator";
    private static final String THIS_THAT_VIEW = "vThisThat";
    private static final String SIMON_VIEW = "vSimon";
    private static final String ORDER_ITEM_VIEW = "vOrderItems";
    private static final String EBOOK_VIEW = "vEbook";
    private static final String CATCH_GAME_VIEW = "vCatch";
    private static final String CONVERSATION_VIEW = "vConversation";
    private static final String MULTIPLE_VIEW = "vMultipleAnswers";
    private static final String COURSE_DETAIL_VIEW = "vCourseDetails";

    @Inject Tracker tracker;

    @Inject public TrackerHelper() {
    }

    public void loginLoad() {
        trackView(LOGIN_VIEW);
    }

    public void loginSucceed(String email, long userId) {
        trackEvent(LOGIN_CATE, LOGIN_ACTION, email, userId);
    }

    public void registerLoad() {
        trackView(REGISTER_VIEW);
    }

    public void registerSucceed(String email, long userId) {
        trackEvent(REGISTER_VIEW, REGISTER_ACTION, email, userId);
    }

    public void coursesListingLoad() {
        trackView(COURSE_LISTING_VIEW);
    }

    public void myCoursesLoad() {
        trackView(MY_COURSES_VIEW);
    }

    public void presentationLoad() {
        trackView(PRESENTATION_VIEW);
    }

    public void dragNDropLoad() {
        trackView(DRAG_N_DROP_VIEW);
    }

    public void questionLoad() {
        trackView(QUESTION_VIEW);
    }

    public void inputLoad() {
        trackView(INPUT_VIEW);
    }

    public void wordFillLoad() {
        trackView(WORD_FILL_VIEW);
    }

    public void courseCompletedLoad() {
        trackView(COURSE_COMPLETED);
    }

    public void courseCompletedFull(long userId, long courseId) {
        trackEvent(COURSE_COMPLETED, COURSE_COMPLETED_ACTION, String.format("%d_%d", userId, courseId), courseId);
    }

    public void courseCompletedLite(long userId, long courseId) {
        trackEvent(COURSE_LITE_COMPLETED, COURSE_LITE_COMPLETED_ACTION, String.format("%d_%d", userId, courseId), courseId);
    }

    public void feedBackLoad() {
        trackView(FEEDBACK_VIEW);
    }

    public void introLoad() {
        trackView(INTRO_LOAD);
    }

    public void progressLoad() {
        trackView(PROGRESS_VIEW);
    }

    public void progressLoadLite() {
        trackView(PROGRESS_LITE_VIEW);
    }

    public void dossierLoad() {
        trackView(DOSSIER_VIEW);
    }

    public void buildStatementLoad() {
        trackView(BUILD_STATEMENT_VIEW);
    }

    public void videoLoad() {
        trackView(VIDEO_VIEW);
    }

    public void wordHeatLoad() {
        trackView(WORD_HEAT_VIEW);
    }

    public void rotatorLoad() {
        trackView(ROTATOR_VIEW);
    }

    public void thisThatLoad() {
        trackView(THIS_THAT_VIEW);
    }

    public void simonLoad() {
        trackView(SIMON_VIEW);
    }

    public void orderItemLoad() {
        trackView(ORDER_ITEM_VIEW);
    }

    public void ebookLoad() {
        trackView(EBOOK_VIEW);
    }

    public void catchGameLoad() {
        trackView(CATCH_GAME_VIEW);
    }

    public void conversationLoad() {
        trackView(CONVERSATION_VIEW);
    }

    public void multipleLoad() {
        trackView(MULTIPLE_VIEW);
    }

    public void courseDetailLoad() {
        trackView(COURSE_DETAIL_VIEW);
    }

    public void trySample(long userId, long courseId) {
        trackEvent(COURSE_DETAIL_VIEW, "trySample", String.format("%d_%d", userId, courseId), courseId);
    }

    public void buyCourseByCredits(long userId, long courseId) {
        trackEvent(COURSE_DETAIL_VIEW, "buyCourseByCredits", String.format("%d_%d", userId, courseId), courseId);
    }

    public void getCredits(long userId, long courseId) {
        trackEvent(COURSE_DETAIL_VIEW, "getCredits", String.format("%d_%d", userId, courseId), courseId);
    }

    public void buyCreditsPackage(long userId, long courseId, String packageId, double price, long credits) {
        trackEvent("vIAPPopup", "buyCredits", String.format("%d_%d_%s_%f_%d", userId, courseId, packageId, price, credits), courseId);
    }

    public void buyCreditsPackageSuccess(long userId, long courseId, String packageId, double price, long credits) {
        trackEvent("vIAPPopup", "buyCreditsSuccess", String.format("%d_%d_%s_%f_%d", userId, courseId, packageId, price, credits), courseId);
    }

    public void buyCreditsPackageFailure(long userId, long courseId, String packageId, double price, long credits, String msg) {
        trackEvent("vIAPPopup", "buyCreditsFailed", String.format("%d_%d_%s_%f_%d_%s", userId, courseId, packageId, price, credits, msg), courseId);
    }

    private void trackView(String viewName) {
        // Set screen name.
        tracker.setScreenName(viewName);

        // Send a screen view.
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    private void trackEvent(String category, String action, String label, long value) {
        // Build and send an Event.
        tracker.send(
                new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .setLabel(label)
                        .setValue(value)
                        .build()
        );
    }

}
