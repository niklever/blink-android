package com.blinktrainingsystem.blink.util;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.blinktrainingsystem.blink.common.view.anytextview.Util;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by TALE on 10/6/2014.
 */
public class UiHelper {

    final DeviceInfo deviceInfo;

    @Inject
    public UiHelper(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Drawable createRoundedWithStrokeDrawable(int strokeWidth, int color, int strokeColor, float radius) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, new int[]{
                color,
                color,
                color
        }
        );
        gradientDrawable.setGradientType(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadii(createRadiusArray(radius, deviceInfo.density));
        gradientDrawable.setStroke(deviceInfo.getDimensionPixel(strokeWidth), strokeColor);
        return gradientDrawable;
    }

    public Drawable createRoundedDrawable(int color, float radius) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, new int[]{
                color,
                color,
                color
        }
        );
        gradientDrawable.setGradientType(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadii(createRadiusArray(radius, deviceInfo.density));
        return gradientDrawable;
    }

    public static Drawable createRoundedDrawable(int color, float radius, float density) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, new int[]{
                color,
                color,
                color
        }
        );
        gradientDrawable.setGradientType(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadii(createRadiusArray(radius, density));
        return gradientDrawable;
    }

    public int measureTextViewHeightForText(Context context, String text, int textSize, int width) {
        TextView textView = new TextView(context);
        textView.setText(text);
        Util.setTypeface(textView, "MyriadPro-Regular.ttf");
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    public int[] measureText(TextPaint textPaint, String text) {
        int textWidth, textHeight; // Our calculated text bounds
        // Now lets calculate the size of the text
        Rect textBounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), textBounds);
        textWidth = (int) textPaint.measureText(text); // Use measureText to calculate width
        textHeight = textBounds.height(); // Use height from getTextBounds()
        return new int[]{textWidth, textHeight};
    }

    public void adjustFontSize(TextView textView, int maxWidth) {
        final String text = textView.getText().toString();
        final Paint textPaint = textView.getPaint();

        float textWidth;
        int size = (int) (textView.getTextSize() + 0.5f);

        textWidth = (int) textPaint.measureText(text);

        Timber.d("adjustFontSize => maxWidth: %d, text: %s, textWidth: %f, textSize: %d: ", maxWidth, text, textWidth, size);
        if (textWidth > maxWidth) {
            while (textWidth > maxWidth) {
                size--;
                textPaint.setTextSize(size);
                textWidth = textPaint.measureText(text);
            }
        } else {
            while (textWidth < maxWidth) {
                size++;
                textPaint.setTextSize(size);
                textWidth = textPaint.measureText(text);
            }
            textPaint.setTextSize(size - 1);
        }
        textView.requestLayout();
        Timber.d("adjustFontSize => textSize: %d", size);
    }

    public void adjustFontSize(TextView textView, int maxWidth, int maxSize) {
        final String text = textView.getText().toString();
        final Paint textPaint = textView.getPaint();

        float textWidth;
        int size = (int) (textView.getTextSize() + 0.5f);

        textWidth = (int) textPaint.measureText(text);

        Timber.d("adjustFontSize => maxWidth: %d, text: %s, textWidth: %f, textSize: %d: ", maxWidth, text, textWidth, size);
        if (textWidth > maxWidth) {
            while (textWidth > maxWidth) {
                size--;
                textPaint.setTextSize(size);
                textWidth = textPaint.measureText(text);
            }
        } else {
            while (textWidth < maxWidth) {
                size++;
                if (size > maxSize) {
                    size = maxSize - 1;
                    break;
                }
                textPaint.setTextSize(size);
                textWidth = textPaint.measureText(text);
            }
            textPaint.setTextSize(size - 1);
        }
        textView.requestLayout();
        Timber.d("adjustFontSize => textSize: %d", size);
    }

    private static float[] createRadiusArray(float radius, float density) {
        float[] floats = new float[8];
        for (int i = 0; i < floats.length; i++) {
            floats[i] = radius * density;
        }
        return floats;
    }


}
