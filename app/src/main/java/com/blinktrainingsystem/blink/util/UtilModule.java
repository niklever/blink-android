package com.blinktrainingsystem.blink.util;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/9/2014.
 */
@Module(
        complete = false,
        library = true
)
public class UtilModule {

    @Provides
    @Singleton DeviceInfo provideDeviceInfo(Application application) {
        return new DeviceInfo(application);
    }

    @Provides
    @Singleton ViewAnimator provideViewAnimator() {
        return new ViewAnimator();
    }
}
