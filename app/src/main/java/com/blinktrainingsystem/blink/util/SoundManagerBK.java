package com.blinktrainingsystem.blink.util;

import android.app.Application;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseIntArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import timber.log.Timber;

public class SoundManagerBK {
    private static final String TAG = SoundManagerBK.class.getSimpleName();
    private Application context;
    private SoundPool sndPool;
    private float rate = 1.0f;
    private float masterVolume = 1.0f;
    private float leftVolume = 1.0f;
    private float rightVolume = 1.0f;
    private float balance = 0.5f;
    private SparseIntArray idMap = new SparseIntArray();
    private Map<String, Integer> pathMap = new HashMap<String, Integer>();
    private MediaPlayer mediaPlayer;
    private List<Integer> playings;
    private boolean isPaused;
    private int playingId = -1;

    public static interface Callback {
        public void onCompleted();
    }

    // Constructor, setup the audio manager and store the app context
    @Inject
    public SoundManagerBK(Application appContext) {
        sndPool = new SoundPool(27, AudioManager.STREAM_MUSIC, 100);
        context = appContext;
    }


    public void resume() {
        if (mediaPlayer == null) {
            return;
        }
        if (isPaused) {
            mediaPlayer.start();
        }
    }

    public void stop() {
        try {
            if (mediaPlayer != null && (mediaPlayer.isPlaying() || isPaused)) {
                mediaPlayer.stop();
                isPaused = false;
            }
        } catch (Exception e) {
            Timber.e(e, "Stop");
        }
    }

    public void pause() {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                isPaused = true;
            }
        } catch (Exception e) {
            Timber.e(e, "Stop");
        }

    }

    public void play(String filePath, final Callback callback, boolean loop) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        } else if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.setOnErrorListener(
                new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        if (callback != null) {
                            callback.onCompleted();
                        }
                        return true;
                    }
                }
        );
        mediaPlayer.setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (callback != null) {
                            callback.onCompleted();
                        }
                    }
                }
        );
        mediaPlayer.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                        isPaused = false;
                    }
                }
        );
        try {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.setDataSource(context, Uri.fromFile(new File(filePath)));
            mediaPlayer.prepareAsync();
            mediaPlayer.setLooping(loop);
        } catch (IOException e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onCompleted();
            }
        }
    }

    /**
     * Load sound from resource's id (e.g R.raw.sound1).
     *
     * @param resId The resource's id to be load.
     */
    public void load(int resId) {
        int id = sndPool.load(context, resId, 1);
        idMap.put(resId, id);
    }

    /**
     * Call to play sound which is be loaded by load(int resId) method.
     *
     * @param resId The id to be played.
     */
    public void play(int resId) {
        play(resId, false);
    }

    /**
     * Call to play sound which is be loaded by load(int resId) method.
     *
     * @param resId The id to be played.
     */
    public void play(int resId, boolean loop) {
        final int id = idMap.get(resId);
        if (id != 0) {
            playById(id, loop);
        } else {
            loadAndPlay(resId);
        }
    }

    /**
     * Call to load sound for the given path.
     *
     * @param path The path to sound stored on disk.
     */
    public void load(String path) throws NullPointerException {
        if (TextUtils.isEmpty(path)) {
            throw new NullPointerException("path mus not be null");
        }

        final int id = sndPool.load(path, 1);
        pathMap.put(path, id);
    }

    /**
     * Play the sound for given the path.
     *
     * @param path The path to sound stored on disk.
     */
    public void play(String path) {
        if (TextUtils.isEmpty(path)) {
            throw new NullPointerException("path mus not be null");
        }

        if (pathMap.containsKey(path)) {
            final int id = pathMap.get(path);
            playById(id);
        }
    }

    /**
     * Call to load sound for the given resource's id then play when load completed.
     *
     * @param resId The resource's id to be load.
     */
    public void loadAndPlay(final int resId) {
        loadAndPlay(resId, false);
    }

    /**
     * Call to load sound for the given resource's id then play when load completed.
     *
     * @param resId The resource's id to be load.
     */

    public void loadAndPlay(final int resId, final boolean loop) {
        final int id = idMap.get(resId);
        if (id != 0) {
            playById(id);
        } else {
            sndPool.setOnLoadCompleteListener(
                    new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                            sndPool.setOnLoadCompleteListener(null); // => Remove onLoadCompleted.
                            if (status == 0) {
                                idMap.put(resId, sampleId);
                                playById(sampleId, loop);
                            } else {
                                Timber.e("Load sound error.");
                            }
                        }
                    }
            );
            load(resId);
        }
    }

    /**
     * Call to load sound for the given path then play when load completed.
     *
     * @param path to the file stored on disk.
     */
    public void loadAndPlay(String path) {
        loadAndPlay(path, false);
    }

    /**
     * Call to load sound for the given path then play when load completed.
     *
     * @param path to the file stored on disk.
     */
    public void loadAndPlay(String path, final boolean loop) {
        if (pathMap.containsKey(path)) {
            final int id = pathMap.get(path);
            playById(id);
        } else {
            sndPool.setOnLoadCompleteListener(
                    new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                            sndPool.setOnLoadCompleteListener(null); // => Remove onLoadCompleted.
                            if (status == 0) {
                                playById(sampleId, loop);
                            } else {
                                Timber.e("Load sound error.");
                            }
                        }
                    }
            );
            load(path);
        }
    }

    /**
     * Play the sound which is pre-load by its id.
     *
     * @param soundId The id in the sound pool.
     */
    private void playById(int soundId) {
        playById(soundId, false);
    }

    /**
     * Play the sound which is pre-load by its id.
     *
     * @param soundId The id in the sound pool.
     */
    private void playById(int soundId, boolean loop) {
        final int streamId = sndPool.play(soundId, leftVolume, rightVolume, 1, loop ? -1 : 0, rate);
        if (playings == null) {
            playings = new ArrayList<Integer>();
        }
        playings.add(streamId);
    }

    public void stopPlaying() {
        if (playings == null || playings.size() == 0) {
            return;
        }
        for (Integer streamId : playings) {
            sndPool.stop(streamId);
        }
        playings.clear();
    }

    public void pausePlaying() {
        if (playings == null || playings.size() == 0) {
            return;
        }
        for (Integer streamId : playings) {
            sndPool.pause(streamId);
        }
    }

    // Set volume values based on existing balance value
    public void setVolume(float vol) {
        Timber.d("setVolume: %f", vol);
        masterVolume = vol;

        if (balance < 1.0f) {
            leftVolume = masterVolume;
            rightVolume = masterVolume * balance;
        } else {
            rightVolume = masterVolume;
            leftVolume = masterVolume * (2.0f - balance);
        }

    }

    public void setSpeed(float speed) {
        rate = speed;

        // Speed of zero is invalid
        if (rate < 0.01f)
            rate = 0.01f;

        // Speed has a maximum of 2.0
        if (rate > 2.0f)
            rate = 2.0f;
    }

    public void setBalance(float balVal) {
        balance = balVal;

        // Recalculate volume levels
        setVolume(masterVolume);
    }


    // Free ALL the things!
    public void unloadAll() {
        if (idMap != null) {
            idMap.clear();
        }
        sndPool.release();
        sndPool = new SoundPool(27, AudioManager.STREAM_MUSIC, 100);
        if (mediaPlayer != null) {
            stop();
            mediaPlayer.release();
        }
    }

}
