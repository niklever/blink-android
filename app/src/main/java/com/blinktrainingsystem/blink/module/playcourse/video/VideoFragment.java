package com.blinktrainingsystem.blink.module.playcourse.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.data.model.pojo.Video;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.PlayCourseActivity;
import com.blinktrainingsystem.blink.module.playcourse.menu.Footer3ButtonFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class VideoFragment extends BaseBurstFragment {

    Json json;
    Video data;


    @InjectView(R.id.tvTitle)
    TextView mTvTitle;
    @InjectView(R.id.webView)
    WebView mWebView;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    @Inject TrackerHelper trackerHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        initVideo();
        if (data != null) {
            loadData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        trackerHelper.videoLoad();
        ((PlayCourseActivity) getActivity()).pauseSound();
    }

    @Override
    public void onStop() {
        super.onStop();
        mWebView.loadUrl("about:blank");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((PlayCourseActivity) getActivity()).resumeSound();
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    private void initVideo() {

        json = blink == null ? null : blink.loadJsonToObject();
        data = json == null ? null : json.video;
    }

    private void loadData() {
        mTvTitle.setText(Helper.getStringWithReplacements(data.prompt));
        bus.post(new Footer3ButtonFragment.FooterMenuButton(getString(R.string.txt_back), null, getString(R.string.txt_next)));
        String videoId = data.vimeo;
        String style = "width=\"100%\" height=\"100%\" frameborder=\"0\" title=\"0\" portrait=\"0\" badge=\"0\" byline=\"0\"";
        String html = String.format("<iframe src=\"http://player.vimeo.com/video/%s\" %s></iframe>", videoId, style);
        Timber.d("Html: %s", html);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadData(html, "text/html", "utf-8");

    }

}
