package com.blinktrainingsystem.blink.module.playcourse;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Intro;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.google.common.base.Splitter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by talenguyen on 18/09/2014.
 */
public class UnitStartFragment extends PlayCourseFragment {

    @InjectView(R.id.tvTitle)
    TextView mTvTitle;
    @InjectView(R.id.tvDescription)
    TextView mTvDescription;
    @InjectView(R.id.ivImage)
    ImageView _IvImage;
    @InjectView(R.id.tvDescriptionExtra)
    TextView _TvDescriptionExtra;

    @Inject TrackerHelper trackerHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_course_intro, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusDriver.withBus(bus)
                .hideHeaderMenuProgress()
                .setFooterMenu(getString(R.string.menu), null, getString(R.string.txt_next));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        trackerHelper.introLoad();

        final int unitIndex = blinkHandler.getUnitIndex(blinkIndex) - 1;
        final Integer cachedUnitIndex = localSharedPreferences.unitUnlock(course.id).getOr(0);
        if (unitIndex > cachedUnitIndex) {
            localSharedPreferences.unitUnlock(course.id).put(unitIndex);
        }
        bind(blinkHandler.get(blinkIndex));
    }

    @Inject
    Picasso picasso;

    @Inject
    UiHelper uiHelper;

    @Inject
    DeviceInfo deviceInfo;

    private void bind(Blink blink) {
        if (blink.loadJsonToObject() != null) {
            final Intro intro = blink.loadJsonToObject().intro;
            String title = blinkHandler.getUnitTitle(blinkIndex);
            mTvTitle.setText(title);
            if (intro != null) {
                final String stringWithReplacements = Helper.getStringWithReplacements(intro.description);
                if (TextUtils.isEmpty(intro.image)) {
                    mTvDescription.setText(stringWithReplacements);
                    _IvImage.setVisibility(View.GONE);
                    _TvDescriptionExtra.setVisibility(View.GONE);
                } else {

                    boolean IsTablet = getResources().getBoolean(R.bool.is_tablet);
                    int maxHeightImg = getResources().getDimensionPixelOffset(R.dimen.max_h_img_unit);
                    int heightimge = 0;
                    File f = getLocalFile(intro.image);
                    if (f != null && f.exists() && f.isFile()) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;

//Returns null, sizes are in the options variable
                        BitmapFactory.decodeFile(f.getAbsolutePath(), options);
                        int widthine = options.outWidth;
                        Timber.e("widthine: " + widthine);
                        heightimge = options.outHeight;
                        if (IsTablet) {
                            if (heightimge > maxHeightImg) {
                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _IvImage.getLayoutParams();
                                layoutParams.height = maxHeightImg;
                                _IvImage.setLayoutParams(layoutParams);

                                heightimge = maxHeightImg;
                            }

                        } else {
                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) _IvImage.getLayoutParams();
                            layoutParams.height = maxHeightImg;
                            _IvImage.setLayoutParams(layoutParams);
                            heightimge = maxHeightImg;
                        }

                    }
                    loadImage(_IvImage, intro.image);
                    Timber.e("heightimge: " + heightimge + " maxHeightImg :" + maxHeightImg);
                    Timber.e("String: " + stringWithReplacements);
                    final List<String> stringList = Splitter.on('\n').splitToList(stringWithReplacements);
                    String text = "";
                    int count = 0;
                    if (stringList != null && stringList.size() > 0) {
                        for (int i = 0; i < stringList.size(); i++) {
                            if (stringList.get(i).equals("")) {
                                Timber.e("i: " + stringList.get(i));
                                text += "\n";
                                count++;
                            }

                        }
                        Timber.e("text list: " + text.length());
                        int height = uiHelper.measureTextViewHeightForText(getActivity().getBaseContext(), text, getResources().getDimensionPixelOffset(R.dimen.text_normal_masure), deviceInfo.screen_W);
                        height = height + (count * getResources().getDimensionPixelOffset(R.dimen.spacing_text));
                        heightimge = uiHelper.measureTextViewHeightForText(getActivity().getBaseContext(), "\n\n\n\n\n", getResources().getDimensionPixelOffset(R.dimen.text_normal_masure), deviceInfo.screen_W);

                        if (height > heightimge) {
                            int startRegion = stringWithReplacements.indexOf("\n");
                            String firstStr = stringWithReplacements.substring(0, startRegion);
                            mTvDescription.setText(firstStr);
                            while (stringWithReplacements.charAt(startRegion) == '\n') {
                                startRegion++;
                            }
                            final int endRegion = stringWithReplacements.length() - 1;
                            Timber.d("startRegion: %d, endRegion: %d", startRegion, endRegion);
                            if (startRegion < endRegion) {
                                String secondStr = stringWithReplacements.substring(startRegion, endRegion);
                                _TvDescriptionExtra.setText(secondStr);
                                _TvDescriptionExtra.setVisibility(View.VISIBLE);
                            } else {
                                _TvDescriptionExtra.setVisibility(View.GONE);
                            }
                        } else {
                            mTvDescription.setText(stringWithReplacements);
                            _IvImage.setVisibility(View.VISIBLE);
                            _TvDescriptionExtra.setVisibility(View.GONE);
                        }
                    } else {
                        mTvDescription.setText(stringWithReplacements);
                        _IvImage.setVisibility(View.GONE);
                        _TvDescriptionExtra.setVisibility(View.GONE);
                    }

                }
            }
        }
    }

    @Inject
    ViewAnimator viewAnimator;

    private void loadImage(final ImageView target, String image) {
        File f = getLocalFile(image);
        if (f != null && f.exists() && f.isFile()) {
            picasso.load(f).into(
                    target, new Callback() {
                        @Override public void onSuccess() {
                            viewAnimator.bottomUp(target, 500, null);
                        }

                        @Override public void onError() {

                        }
                    }
            );
        }

    }

}
