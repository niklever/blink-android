package com.blinktrainingsystem.blink.module.main;

import android.os.Bundle;

import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.pojo.Course;

/**
 * Created by TALE on 9/8/2014.
 */
public class MainPresenterImpl implements MainPresenter {

    private final MainView mMainView;
    private final LocalSharedPreferences localSharedPreferences;

    public MainPresenterImpl(MainView mMainView, LocalSharedPreferences localSharedPreferences) {
        this.mMainView = mMainView;
        this.localSharedPreferences = localSharedPreferences;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            //if (localSharedPreferences.userId().getOr(0l) == 0l) {
            //    mMainView.showLogin();
            //} else {
                Static.admin = localSharedPreferences.admin().getOr(false);
                mMainView.showCourses();
            //}
        }
    }

    @Override
    public void onLoginSuccess() {
        mMainView.showCourses();
    }

    @Override
    public void onRegisterSuccess(boolean back) {
        if (back){
            mMainView.backToPrevious();
        }else {
            mMainView.showCourses();
        }
    }

    @Override
    public void onRegisterRequest() {
        mMainView.showRegister();
    }

    @Override
    public void backToLoginFromRegister() {
        mMainView.backToPrevious();
    }

    @Override
    public void onLogout() {
        mMainView.showLogin();
    }

    @Override
    public void openCourseDetail(Course course) {
        mMainView.showCourseDetail(course);
    }

    @Override
    public void openPlayCourse(Course course) {
        mMainView.showPlayCourse(course);
    }

    @Override
    public void backToCourses() {
        mMainView.backToPrevious();
    }

}
