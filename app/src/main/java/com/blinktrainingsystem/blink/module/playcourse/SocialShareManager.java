package com.blinktrainingsystem.blink.module.playcourse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;

import timber.log.Timber;

/**
 * Created by TALE on 12/25/2014.
 */
public class SocialShareManager extends Fragment {

    private CallbackManager callbackManager;
    private String name;
    private String caption;
    private String desc;
    private String link;
    private String pictureUrl;
    private boolean isPendingShare;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
    }

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            //displayMessage(profile);
            Log.d("FACEBOOK", profile.toString());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {
            Timber.e("onError => Error: %s", e.toString());
        }
    };

    public void login() {
        /*Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            final Session.OpenRequest openRequest = new Session.OpenRequest(this);
            session.openForPublish(openRequest.setCallback(statusCallback).setPermissions("publish_actions"));
        } else {
            Session.openActiveSession(getActivity(), this, true, statusCallback);
        }*/
    }

    private boolean isLoggedIn() {
        /*Session session = Session.getActiveSession();
        return session.isOpened();*/
        return false;
    }

    /*private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (exception != null) {
                handleException(exception);
            }
            if (state.isOpened()) {
                afterLogin();
            } else if (state.isClosed()) {
                afterLogout();
            }
        }
    }*/

    /*private void handleException(Exception exception) {
        Timber.e(exception, "Failure to login");
    }

    private void afterLogout() {

    }

    private void afterLogin() {
        sharePending();
    }

    private void sharePending() {
        if (isPendingShare) {
            share(name, caption, desc, link, pictureUrl);
        }
    }*/

    public void share(String name, String caption, String desc, String link, String pictureUrl) {
        this.name = name;
        this.caption = caption;
        this.desc = desc;
        this.link = link;
        this.pictureUrl = pictureUrl;
        if (!isLoggedIn()) {
            isPendingShare = true;
            login();
        } else {
            // Fallback. For example, publish the post using the Feed Dialog
            shareWithFeedDialog();
        }

    }

    private void shareWithFeedDialog() {
        Timber.d("shareWithFeedDialog => name: %s, caption: %s, description: %s, link: %s, picture: %s", name, caption, desc, link, pictureUrl);
        Bundle params = new Bundle();
        params.putString("name", name);
        params.putString("caption", caption);
        params.putString("description", desc);
        params.putString("link", link);
        params.putString("picture", pictureUrl);

        /*WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(
                        getActivity(),
                        Session.getActiveSession(),
                        params
                ))
                .setOnCompleteListener(
                        new WebDialog.OnCompleteListener() {

                            @Override
                            public void onComplete(
                                    Bundle values,
                                    FacebookException error
                            ) {
                                name = null;
                                caption = null;
                                desc = null;
                                link = null;
                                pictureUrl = null;
                                isPendingShare = false;
                                if (error == null) {
                                    // When the story is posted, echo the success
                                    // and the post Id.
                                    final String postId = values.getString("post_id");
                                    if (postId != null) {
                                    } else {
                                        // User clicked the Cancel button
                                        Toast.makeText(
                                                getActivity().getApplicationContext(),
                                                "Publish cancelled",
                                                Toast.LENGTH_SHORT
                                        ).show();
                                    }
                                } else if (error instanceof FacebookOperationCanceledException) {
                                    // User clicked the "x" button
                                    Toast.makeText(
                                            getActivity().getApplicationContext(),
                                            "Publish cancelled",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                } else {
                                    // Generic, ex: network error
                                    Toast.makeText(
                                            getActivity().getApplicationContext(),
                                            "Error posting story",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }

                        }
                )
                .build();
        feedDialog.show();*/
    }

    @Override public void onResume() {
        super.onResume();
        //uiHelper.onResume();
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        //uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //uiHelper.onDestroy();
    }
}
