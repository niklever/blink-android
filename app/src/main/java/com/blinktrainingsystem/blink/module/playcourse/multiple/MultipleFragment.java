package com.blinktrainingsystem.blink.module.playcourse.multiple;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.ItemMultipleView;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Answer;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.Question;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.Footer3ButtonFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;

import javax.inject.Inject;

import butterknife.InjectView;

/**
 * Created by TALE on 9/30/2014.
 */
public class MultipleFragment extends BaseBurstFragment {

    @InjectView(R.id.tvText)
    TextView _TvPrompt;
    @InjectView(R.id.llAnswers)
    LinearLayout _LlAnswers;

    @Inject
    DeviceInfo deviceInfo;

    @Inject
    UiHelper uiHelper;

    @Inject TrackerHelper trackerHelper;

    private boolean isAnswerShown;

    private JsonEx json;
    private Question multiple;
    ;

    @Inject
    BlinkApplication application;
    private boolean isNextOnClose;

    @Override public void onResume() {
        super.onResume();
        trackerHelper.multipleLoad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_multiple, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        json = blink.loadJsonToObject();
        multiple = json == null ? null : json.multiple;
        final String title = multiple != null ? multiple.text : "";
        _TvPrompt.setText(Helper.getStringWithReplacements(title));
        if (multiple == null) {
            busDriver.setFooterMenu(null, null, getString(R.string.txt_done));
            return;
        }
        initUi();
        initAnswers();
    }

    @Override
    protected int getTimer() {
        return multiple == null ? 0 : multiple.timer;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.multiple_answer_help);
    }

    private void initUi() {
        if (multiple == null) {
            return;
        }
        busDriver.setFooterMenu(null, null, getString(R.string.txt_done));
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                } else if (isNextOnClose) {
                    busDriver.notifyBlinkCompleted();
                }
                break;
            case DoneClick:
                playRawSound(R.raw.gentleflyby);
            case TimeOut:
                final int childCount = _LlAnswers.getChildCount();
                boolean result = true;
                int selected = 0;
                for (int i = 0; i < childCount; i++) {
                    ItemMultipleView answerView = (ItemMultipleView) _LlAnswers.getChildAt(i);
                    answerView.showAnswer();
                    final Answer answer = answerView.getAnswer();
                    if (answerView.getSelected()) {
                        selected++;
                        result = result && answer.correct;
                    }
                }
                submitResult(selected > 0 && result);
                break;
        }
    }

    private int curSelected = -1;

    private void submitResult(boolean result) {
        final String feedback = json.feedback[result ? 0 : 1];
        if (TextUtils.isEmpty(feedback)) {
            busDriver.notifyBlinkCompleted();
        } else {
            bus.post(Event.ShowHelp.setExtra(feedback));
            isNextOnClose = verifyNextOnClose();
            BusDriver.withBus(bus).pauseTimer();
            isAnswerShown = true;
            bus.post(new Footer3ButtonFragment.FooterMenuButton(null, null, getString(R.string.txt_next)));
            if (result) {
                if (!blinkHandler.updateScore(json.score, blinkIndex, course, json.gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
        }
    }

    private boolean verifyNextOnClose() {
        for (Answer answer : multiple.answers) {
            if (TextUtils.isEmpty(answer.feedback)) {
                return true;
            }
        }
        return false;
    }

    private void initAnswers() {
        if (multiple == null || multiple.answers == null || multiple.answers.size() == 0) {
            return;
        }

        for (int i = 0; i < multiple.answers.size(); i++) {
            final Answer answer = multiple.answers.get(i);
            View answerView = createAnswerView(answer);
            _LlAnswers.addView(answerView);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) answerView.getLayoutParams();
            if (i < multiple.answers.size() - 1) {
                lp.bottomMargin = deviceInfo.getDimensionPixel(16);
            }
        }


    }

    private View createAnswerView(final Answer answer) {
        final ItemMultipleView answerView = new ItemMultipleView(getActivity());
        answerView.setUiHelper(uiHelper);
        answerView.setAnswer(answer);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        answerView.setLayoutParams(layoutParams);
        answerView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isAnswerShown) {
                            if (curSelected >= 0) {
                                final ItemMultipleView view = (ItemMultipleView) _LlAnswers.getChildAt(curSelected);
                                view.setSelectBg(false);
                            }
                            answerView.setSelectBg(true);
                            curSelected = _LlAnswers.indexOfChild(answerView);
                            bus.post(Event.ShowHelp.setExtra(answer.feedback));
                            if (answer.correct) {
                                playRawSound(R.raw.correct);
                            } else {
                                playRawSound(R.raw.incorrect);
                            }
                        } else {
                            answerView.toggle();
                            if (answerView.getSelected()) {
                                playRawSound(R.raw.correct);
                            } else {
                                playRawSound(R.raw.incorrect);
                            }
                        }
                    }
                }
        );
        return answerView;
    }
}
