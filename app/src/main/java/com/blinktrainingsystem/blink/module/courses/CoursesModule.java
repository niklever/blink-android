package com.blinktrainingsystem.blink.module.courses;

import android.app.Application;

import com.blinktrainingsystem.blink.common.adapter.CourseAdapter;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        library = true,
        injects = CoursesFragment.class
)
public class CoursesModule {
    private final CoursesView coursesView;

    public CoursesModule(CoursesView coursesView) {
        this.coursesView = coursesView;
    }

    @Provides
    public CourseAdapter provideCourseAdapter(Application application, EventBus bus, Picasso picasso, LocalFileManager localFileManager, LocalSharedPreferences localSharedPreferences) {
        return new CourseAdapter(application, bus, picasso, localFileManager, localSharedPreferences);
    }

    @Provides
    @Singleton CoursesPresenter provideCoursePresenter(MainPresenter mainPresenter, Application application, LocalSharedPreferences localSharedPreferences, LocalFileManager localFileManager, TaskManager taskManager, DeviceInfo deviceInfo, CoursesRepo coursesRepo, TrackerHelper trackerHelper) {
        return new CoursesPresenter(coursesView, mainPresenter, application, localSharedPreferences, localFileManager, taskManager, deviceInfo, coursesRepo, trackerHelper);
    }

}
