package com.blinktrainingsystem.blink.module.courses;

import com.blinktrainingsystem.blink.common.dialog.ConfirmDialogFragment;
import com.blinktrainingsystem.blink.data.model.pojo.Course;

import java.util.List;

/**
 * Created by TALE on 9/8/2014.
 */
public interface CoursesView {

    void showProgress();

    void hideProgress();

    void showError(String error);

    void showCourses(List<Course> courses);

    void showConfirmLogoutDialog(ConfirmDialogFragment.ConfirmDialogListener listener);

}
