package com.blinktrainingsystem.blink.module.playcourse;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Community;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.google.android.gms.ads.MobileAds;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.Observable;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * Created by talenguyen on 18/09/2014.
 */
public class BurstEndFragment extends BaseBurstFragment {
    final static String TAG = "BurstEndFragment";

    @InjectView(R.id.ivProgress)
    ImageView _IvProgress;
    @InjectView(R.id.ivHuman)
    ImageView ivHuman;
    @InjectView(R.id.flProgress)
    FrameLayout flProgress;
    @InjectView(R.id.llStep)
    FrameLayout _LlStep;
    @InjectView(R.id.tvScore)
    TextView _TvScore;
    @InjectView(R.id.tvCurrentMaxScore)
    TextView _TvCurrentMaxScore;
    @InjectView(R.id.tvGems)
    TextView _TvGems;
    @InjectView(R.id.tvCurrentMaxGems)
    TextView _TvCurrentMaxGems;
    @InjectView(R.id.tvActivitiesUnlocked)
    TextView _TvActivitiesUnlocked;
    @InjectView(R.id.tvActivitiesCompleted)
    TextView _TvActivitiesCompleted;
    @InjectView(R.id.tvLastBurstPlayed)
    TextView _TvLastBurstPlayed;
    @InjectView(R.id.tvBestScore)
    TextView _TvBestScore;
    @InjectView(R.id.tvTargetScore)
    TextView _TvTargetScore;
    @InjectView(R.id.tvMaxScore)
    TextView _TvMaxScore;
    @InjectView(R.id.tvGems1)
    TextView _TvGems1;
    @InjectView(R.id.tvMaxGems)
    TextView _TvMaxGems;
    @InjectView(R.id.tvYourGems)
    TextView _TvYourGems;
    @InjectView(R.id.tvCommunityGems)
    TextView _TvCommunityGems;
    @InjectView(R.id.tvYourPoint)
    TextView _TvYourPoint;
    @InjectView(R.id.tvCommunityPoint)
    TextView _TvCommunityPoint;
    @InjectView(R.id.tvYourEfficiency)
    TextView _TvYourEfficiency;
    @InjectView(R.id.tvCommunityEfficiency)
    TextView _TvCommunityEfficiency;
    @InjectView(R.id.tvYourTraction)
    TextView _TvYourTraction;
    @InjectView(R.id.tvCommunityTraction)
    TextView _TvCommunityTraction;
    @InjectView(R.id.rlProgressImage)
    RelativeLayout rlProgressImage;
    @InjectView(R.id.adView)
    AdView adView;

    @Inject
    ViewAnimator viewAnimator;
    @InjectView(R.id.tvCourseComplete)
    TextView _TvCourseComplete;
    @InjectView(R.id.cbMaxScoreArchived)
    ImageView _CbMaxScoreArchived;
    @InjectView(R.id.cbCurrentMaxGemsArchived)
    ImageView _CbCurrentMaxGemsArchived;
    @InjectView(R.id.cbCompletedActivitiesArchived)
    ImageView _CbCompletedActivitiesArchived;
    @InjectView(R.id.cbTargetScoreArchived)
    ImageView _CbTargetScoreArchived;
    @InjectView(R.id.cbMaxGemsArchived)
    ImageView _CbMaxGemsArchived;

    @Inject WebServices webServices;
    private int progressCheckBoxWidth;
    private int progressCheckBoxHeight;
    private int curStep;
    Activity activity;
    private Observable<Community> communityObservable;
    private Subscription communitySubscription;

    @Inject TrackerHelper trackerHelper;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
    }

    @Override
    protected void configureHeaderMenu() {

    }

    @Inject
    BlinkApplication blinkApplication;

    @Override public void onResume() {
        super.onResume();
        trackerHelper.progressLoad();
    }

    @Override protected boolean isSocialShown() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((PlayCourseActivity) getActivity()).stopLongSound();
    }

    @Override public void onStop() {
        super.onStop();
        if (communitySubscription != null) {
            communitySubscription.unsubscribe();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

    @Override public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case ShareFb:
                // TODO: share Fb
                share(event);
                break;
            case ShareTw:
                // TODO: share Tw
                share(event);
                break;
        }

    }

    private SocialShareManager socialShareManager;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        socialShareManager = (SocialShareManager) getFragmentManager().findFragmentByTag(PlayCourseActivity.SOCIAL_NETWORK_TAG);
        if (socialShareManager == null) {
            socialShareManager = new SocialShareManager();
            getFragmentManager().beginTransaction().add(socialShareManager, PlayCourseActivity.SOCIAL_NETWORK_TAG).commit();
        }
        MobileAds.initialize(getActivity().getApplicationContext(), getString(R.string.banner_app_id));
    }

    private void share(Event event) {
        final String name = getString(R.string.share_name);
        final String caption = getString(R.string.share_caption);
        final String link = getString(R.string.share_link);
        final String picture = getString(R.string.share_picture);
        if (event == Event.ShareFb) {
            final String desc = getString(R.string.share_desc_fb, course.title);
            socialShareManager.share(name, caption, desc, link, picture);
        } else if (event == Event.ShareTw) {
            URL url = null;
            try {
                url = new URL(link);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            final String desc = getString(R.string.share_desc_tw, course.title);
            TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                    .text(desc)
                    .image(Uri.parse(picture));

            if (url != null) {
                builder.url(url);
            }
            builder.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_burst_end, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (blink == null) {
            showCourseComplete();
            busDriver.setFooterMenu(getString(R.string.txt_replay_burst), getString(R.string.txt_bursts_menu), null);
            return;
        }

        final String unlockActivityIds = blinkHandler.loadUnlockActivityIds(blinkIndex);
        if (!TextUtils.isEmpty(unlockActivityIds)) {
            final String unlockedIds = localSharedPreferences.unlockActivityIds(localSharedPreferences.userId().getOr(0l), course).getOr(null);
            if (unlockedIds == null || unlockedIds.length() < unlockActivityIds.length()) {
                localSharedPreferences.unlockActivityIds(localSharedPreferences.userId().getOr(0l), course).put(unlockActivityIds);
                busDriver.enableActivity();
            }
        }

        curStep = blinkHandler.getBurstIndex(localSharedPreferences.highestIndex(course.id).getOr(blinkIndex));
        Timber.d("curStep %d", curStep);
        final int skypeLevel = calculateSkypeLevel();
        initBackgroundProgress(skypeLevel);
        ViewObservableHelper.globalLayoutFrom(rlProgressImage).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        initProgress();
                    }
                }
        );

        // Bind data.
        final long userScore = blinkHandler.getUserScore(course.id);
        final long maxTotalScoresToThisBurst = blinkHandler.getMaxTotalScoresToThisBurst(blinkIndex);
        final long maxTotalGemsToThisBurst = blinkHandler.getMaxTotalGemsToThisBurst(blinkIndex);
        final int userTotalGemsUserCollected = blinkHandler.getUserTotalGemsUserCollected(blinkIndex, course.id);
        final int unitIndex = blinkHandler.getUnitIndex(blinkIndex);
        final long highestUserScoreInBurst = blinkHandler.getHighestUserScoreInBurst(blinkIndex, course.id);
        final int targetToUnlock = blinkHandler.getTargetToUnlock(blinkIndex);
        final long maxScoreOfBurst = blinkHandler.getMaxScoreOfBurst(blinkIndex);
        final int userGemsInBurst = blinkHandler.getUserGemsInBurst(blinkIndex, course.id);
        final int maxGemsInThisBurst = blinkHandler.getMaxGemsInThisBurst(blinkIndex);
        _TvScore.setText(valueOf(userScore));
        _TvCurrentMaxScore.setText(valueOf(maxTotalScoresToThisBurst));
        _TvCurrentMaxGems.setText(valueOf(maxTotalGemsToThisBurst));
        _TvGems.setText(valueOf(userTotalGemsUserCollected));
        _TvLastBurstPlayed.setText(format("Unit %d Burst %d", unitIndex, blinkHandler.getBurstIndexInUnit(blinkIndex)));
        _TvBestScore.setText(valueOf(highestUserScoreInBurst));
        _TvTargetScore.setText(valueOf(targetToUnlock));
        _TvMaxScore.setText(valueOf(maxScoreOfBurst));
        _TvGems1.setText(valueOf(userGemsInBurst));
        _TvMaxGems.setText(valueOf(maxGemsInThisBurst));
        _TvYourGems.setText(valueOf(userGemsInBurst));
        _TvYourPoint.setText(valueOf(highestUserScoreInBurst));
        _TvYourTraction.setText(valueOf(skypeLevel));
        final int unlockedActivities = blinkHandler.calculatesUnlockedActivities(course);
        _TvActivitiesUnlocked.setText(String.valueOf(unlockedActivities));

        final int completedActivities = blinkHandler.calculatesCompletedActivities(course);
        _TvActivitiesCompleted.setText(String.valueOf(completedActivities));

        _CbMaxScoreArchived.setVisibility(userScore > 0 && userScore >= maxTotalScoresToThisBurst ? View.VISIBLE : View.GONE);
        _CbCurrentMaxGemsArchived.setVisibility(userTotalGemsUserCollected > 0 && maxTotalGemsToThisBurst > 0 && userTotalGemsUserCollected >= maxTotalGemsToThisBurst ? View.VISIBLE : View.GONE);
        _CbCompletedActivitiesArchived.setVisibility(completedActivities > 0 && unlockedActivities > 0 && completedActivities >= unlockedActivities ? View.VISIBLE : View.GONE);
        _CbTargetScoreArchived.setVisibility(highestUserScoreInBurst > 0 && targetToUnlock > 0 && highestUserScoreInBurst >= targetToUnlock ? View.VISIBLE : View.GONE);
        _CbMaxGemsArchived.setVisibility(userGemsInBurst > 0 && maxGemsInThisBurst > 0 && userGemsInBurst >= maxGemsInThisBurst ? View.VISIBLE : View.GONE);

        communityObservable = webServices.updateCommunity(blink.guid, localSharedPreferences.userId().getOr(0l), userGemsInBurst, highestUserScoreInBurst, skypeLevel);
        communitySubscription = AndroidObservable.bindFragment(this, communityObservable)
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        new Action1<Community>() {
                            @Override public void call(Community community) {
                                if (community != null && community.success) {
                                    _TvYourEfficiency.setText(valueOf((int) community.user_efficiency));
                                    _TvCommunityEfficiency.setText(valueOf((int) community.efficiency));
                                    _TvCommunityPoint.setText(valueOf((int) community.points));
                                    _TvCommunityGems.setText(valueOf((int) community.gems));
                                    _TvCommunityTraction.setText(valueOf((int) community.traction));
                                }
                            }
                        }
                );

        if (course.lite || course.credits.equals("0")) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice("DBF3DD5A059F74BF9C0BBEF1CF46898D")
                    .build();
            adView.loadAd(adRequest);
            adView.setVisibility(View.VISIBLE);
        }else{
            adView.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    private void showCourseComplete() {
        trackerHelper.courseCompletedLoad();
        trackerHelper.courseCompletedFull(localSharedPreferences.userId().getOr(0l), course.id);

        _TvCourseComplete.setVisibility(View.VISIBLE);
        ViewObservableHelper.globalLayoutFrom(_TvCourseComplete)
                .subscribe(
                        new Action1<View>() {
                            @Override
                            public void call(View view) {
                                _TvCourseComplete.setScaleX(0);
                                _TvCourseComplete.setScaleY(0);
                                _TvCourseComplete.animate()
                                        .scaleX(1)
                                        .scaleY(1)
                                        .setDuration(500)
                                        .start();
                            }
                        }
                );
    }

    private void initProgress() {
        final int currentBurst = blinkHandler.getBurstIndexInCourse(blinkIndex);
        final int totalNumberOfButtons = blinkHandler.getAllBurstInCourse();
        busDriver = BusDriver.withBus(bus);
        final List<Blink> blinkList = blinkHandler.getBlinkList();
        final long userScoreInBurst = blinkHandler.getUserScoreInBurst();
        final int targetToUnlock = blinkHandler.getTargetToUnlock(blinkIndex);
        if (currentBurst >= totalNumberOfButtons || blinkList == null || blinkIndex >= blinkList.size() - 1) {
            showCourseComplete();
            busDriver.setFooterMenu(getString(R.string.txt_replay_burst), getString(R.string.txt_bursts_menu), null);
        } else if (isAllowSkip) {
            busDriver.setFooterMenu(getString(R.string.txt_replay_burst), getString(R.string.txt_bursts_menu), getString(R.string.txt_next_burst));
        } else if (userScoreInBurst < targetToUnlock) {
            busDriver.setFooterMenu(getString(R.string.txt_replay_burst), getString(R.string.txt_bursts_menu), getString(R.string.txt_next_burst), true);
        } else {
            busDriver.setFooterMenu(getString(R.string.txt_replay_burst), getString(R.string.txt_bursts_menu), getString(R.string.txt_next_burst));
        }
        int burstIdx = blinkHandler.getBurstIndexInUnit(blinkIndex - 1);
        int burstsRemaining = blinkHandler.getBurstsRemainingInUnit(blinkIndex - 1);

        showProgressStep();
    }

    private void showProgressStep() {
        final int size = blinkHandler.getAllBurstInCourse();
        if (size == 0) {
            return;
        }
        final int width = flProgress.getWidth();
        final int height = rlProgressImage.getHeight();
        progressCheckBoxWidth = ivHuman.getWidth();
        progressCheckBoxHeight = progressCheckBoxWidth / 2;
        final int stepOffset = size > 1 ? (width - progressCheckBoxWidth) / (size - 1) : width;

        for (int i = 0; i < size; i++) {
            View progressView = newProgressView(i < curStep);
            flProgress.addView(progressView);
            final int leftMargin = i * stepOffset;
            if (leftMargin > 0) {
                Timber.d("step: %d, progressCheckBoxWidth: %d, leftMargin: %d", i, progressCheckBoxWidth, leftMargin);
                ((FrameLayout.LayoutParams) progressView.getLayoutParams()).leftMargin = leftMargin;
            }
        }

        postMainThread(
                new Runnable() {
                    @Override
                    public void run() {
                        final float translateYOffset = (float) (height - ivHuman.getHeight() - 10) / (size);
                        int preStep = blinkHandler.getBurstIndex(blinkIndex);
                        Timber.d("preStep: %d, curStep: %d: ", preStep, curStep);
                        if (curStep == preStep && curStep > 0) {
                            preStep = curStep - 1;
                        }
                        final int preX = (preStep - 1) * stepOffset;
                        ivHuman.setTranslationX(preX);
                        ivHuman.setTranslationY(-(preStep - 1) * translateYOffset);
                        ivHuman.setVisibility(View.VISIBLE);
                        final int leftMargin = (curStep - 1) * stepOffset;
                        Timber.d("preX: %d, leftMargin: %d", preX, leftMargin);
                        viewAnimator.move(ivHuman, leftMargin, -(curStep - 1) * translateYOffset, 500, null);
                    }
                }, 500
        );
//
//
//        _LlStep.getLayoutParams().width = stepOffset;
//        float translateXOffset = (float) width / size;
//        final int _LlStepHeight = (int) (_LlStep.getHeight());
//        float translateYOffset = (float) (height - _LlStepHeight - 10) / (size);
//        Timber.d("stepOffset=%d, translateXOffset=%f, translateYOffset=%f", stepOffset, translateXOffset, translateYOffset);
//        viewAnimator.move(_LlStep, (curStep - 1) * translateXOffset, -(curStep - 1) * translateYOffset, 500, null);
//        for (int i = 0; i < size; i++) {
//            View progressView = newProgressView(i <
// );
//            flProgress.addView(progressView);
//        }
    }

    protected void initBackgroundProgress(int step) {
        Timber.d("initBackgroundProgress => Step: %d", step);
        switch (step) {
            case 1:
                _IvProgress.setBackgroundResource(R.drawable.progress_1_iphone);
                break;
            case 2:
                _IvProgress.setBackgroundResource(R.drawable.progress_2_iphone);
                break;
            case 3:
                _IvProgress.setBackgroundResource(R.drawable.progress_3_iphone);
                break;
            case 4:
                _IvProgress.setBackgroundResource(R.drawable.progress_4_iphone);
                break;
            case 5:
                _IvProgress.setBackgroundResource(R.drawable.progress_5_iphone);
                break;
            case 6:
                _IvProgress.setBackgroundResource(R.drawable.progress_6_iphone);
                break;
            case 7:
                _IvProgress.setBackgroundResource(R.drawable.progress_7_iphone);
                break;
            case 8:
                _IvProgress.setBackgroundResource(R.drawable.progress_8_iphone);
                break;
            case 9:
                _IvProgress.setBackgroundResource(R.drawable.progress_9_iphone);
                break;
            case 10:
                _IvProgress.setBackgroundResource(R.drawable.progress_10_iphone);
                break;
//            default:
//                _IvProgress.setBackgroundResource(R.drawable.progress_10_iphone);
        }
    }

    private View newProgressView(boolean isChecked) {
        ImageView imageView = new ImageView(activity);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(progressCheckBoxWidth, progressCheckBoxHeight));
        imageView.setImageResource(isChecked ? R.drawable.progress_on : R.drawable.progress_off);
        return imageView;
    }

    int calculateSkypeLevel() {

        float yourScore = blinkHandler.getUserScore(course.id);//[[params objectForKey:YOUR_SCORE] floatValue];
        float maxScore = blinkHandler.getMaxTotalScoresToThisBurst(blinkIndex);//[[params objectForKey:MAX_SCORE] floatValue];
        float yourGems = blinkHandler.getUserTotalGemsUserCollected(blinkIndex, course.id);//[[params objectForKey:YOUR_GEMS] floatValue];
        float maxGems = blinkHandler.getMaxTotalGemsToThisBurst(blinkIndex);//[[params objectForKey:MAX_GEMS] floatValue];
        float activitiesUnlocked = blinkHandler.calculatesUnlockedActivities(course);//[[params objectForKey:ACTIVITIES_UNLOCKED] floatValue];
        float activitiesCompleted = blinkHandler.calculatesCompletedActivities(course);//[[params objectForKey:ACTIVITIES_COMPLETE] floatValue];

        if (activitiesUnlocked != 0) {
            int skypeLevel = (int) (((yourScore / maxScore) * 0.6 + (yourGems / maxGems) * 0.2 + (activitiesCompleted / activitiesUnlocked * 0.2)) * 10);
            skypeLevel = skypeLevel > 10 ? 10 : skypeLevel;
            return skypeLevel > 0 ? skypeLevel : 1;
        } else {
            int skypeLevel = (int) (((yourScore / maxScore) * 0.6 + (yourGems / maxGems) * 0.2 + 1 * 0.2) * 10);
            skypeLevel = skypeLevel > 10 ? 10 : skypeLevel;
            return skypeLevel > 0 ? skypeLevel : 1;
        }
    }

}
