package com.blinktrainingsystem.blink.module.playcourse.thisorthat;

import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.ThisThat;
import com.blinktrainingsystem.blink.data.model.pojo.ThisThatQuestion;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TALE on 9/26/2014.
 */
public class ThisOrThatPresenter {
    private final ThisThat thisThat;
    private final List<ThisThatQuestion> questions;
    public final UiHelper uiHelper;
    public final JsonEx jsonEx;
    private ThisOrThatFragment thisOrThatFragment;
    private int step;
    private ThisThatQuestion curQuestion;
    public boolean answer;
    public boolean noscore;
    private List<Integer> cols;
    private int correctCount = 0;
    private Handler handler = new Handler(Looper.getMainLooper());
    public boolean nextBlink;

    public ThisOrThatPresenter(JsonEx jsonEx, UiHelper uiHelper) {
        this.jsonEx = jsonEx;
        this.thisThat = jsonEx.thisthat;
        this.uiHelper = uiHelper;
        questions = thisThat.questions;
        cols = new ArrayList<Integer>();
        cols.add(Color.parseColor("#FFCC00"));
        cols.add(Color.parseColor("#FF9E00"));
        cols.add(Color.parseColor("#FF3333"));
        cols.add(Color.parseColor("#00CC00"));
    }

    public void bind(ThisOrThatFragment thisOrThatFragment) {
        this.thisOrThatFragment = thisOrThatFragment;
    }

    public void start() {
        if (questions != null && questions.size() > 0) {
            thisOrThatFragment.setQuestion(String.format("Question 1 of %d", questions.size()));
            step = 0;
            handleStep(step);
        } else {
            // TODO: handle if there is no question.
        }
    }

    private void handleStep(int step) {
        this.step = step;
        final int questionSize = questions.size();
        if (step >= questionSize) {
            blinkCompleted();
            return;
        }
        curQuestion = questions.get(step);
        if (thisOrThatFragment != null) {
            answer = curQuestion.value;
            noscore = thisThat.noscore;
            thisOrThatFragment.btLeft.setEnabled(true);
            thisOrThatFragment.btRight.setEnabled(true);
            CompatibleHelper.setBackgroundDrawable(thisOrThatFragment.btLeft, uiHelper.createRoundedDrawable(cols.get(0), 6.0f));
            CompatibleHelper.setBackgroundDrawable(thisOrThatFragment.btRight, uiHelper.createRoundedDrawable(cols.get(0), 6.0f));
            thisOrThatFragment.setQuestion(Helper.getStringWithReplacementOmitNewLineEnd(curQuestion.text));
            thisOrThatFragment.setLeftButtonText(curQuestion.correct);
            thisOrThatFragment.setRightButtonText(curQuestion.wrong);
            thisOrThatFragment.setTitleText(String.format("Question %d of %d", step + 1, questionSize));
            thisOrThatFragment.showQuestion();
        }
    }

    private void blinkCompleted() {
        thisOrThatFragment.stopTimer();
        int score = jsonEx.score;
        int gems = jsonEx.gems;
        boolean feedbackShown = false;
        int user_score = (int) ((double) ((4.0 - (double) incorrectCount) / 4.0) * (double) score);
        if (user_score < 0) user_score = 0;

        String str;
        if (correctCount > 0) {
            if (incorrectCount <= 1) {
                if (user_score > 0) {
                    if (incorrectCount == 0) {
                        thisOrThatFragment.updateScore(user_score, gems);
                    } else {
                        thisOrThatFragment.updateScore(user_score, 0);
                    }
                }
                //        if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
                thisOrThatFragment.playRawSound(R.raw.correct);
                str = jsonEx.feedback == null || jsonEx.feedback.length < 1 ? null : jsonEx.feedback[0];
                if (!TextUtils.isEmpty(str)) {
                    thisOrThatFragment.showHelp(str);
                    feedbackShown = true;
                } else if (gems > 0) {
                    // TODO blink gems
                }
            } else {
                if (user_score > 0) thisOrThatFragment.updateScore(user_score, 0);
                thisOrThatFragment.playRawSound(R.raw.incorrect);
                str = jsonEx.feedback == null || jsonEx.feedback.length < 2 ? null : jsonEx.feedback[1];
                if (!TextUtils.isEmpty(str)) {
                    feedbackShown = true;
                    thisOrThatFragment.showHelp(str);
                }
            }
        } else {
            thisOrThatFragment.playRawSound(R.raw.incorrect);
            str = jsonEx.feedback == null || jsonEx.feedback.length < 2 ? null : jsonEx.feedback[1];
            if (!TextUtils.isEmpty(str)) {
                feedbackShown = true;
                thisOrThatFragment.showHelp(str);
            }
        }
        nextBlink = true;
        if (!feedbackShown) thisOrThatFragment.notifyBurstCompleted();
    }

    private int incorrectCount = 0;

    public void truePressed() {
        CompatibleHelper.setBackgroundDrawable(thisOrThatFragment.btLeft, uiHelper.createRoundedDrawable(cols.get(1), 6.0f));
        user_btn = thisOrThatFragment.btLeft;
        if (!answer && !noscore) incorrectCount++;
        trueFalseShowAnswer();
    }

    TextView user_btn;

    public void falsePressed() {
        CompatibleHelper.setBackgroundDrawable(thisOrThatFragment.btRight, uiHelper.createRoundedDrawable(cols.get(1), 6.0f));
        user_btn = thisOrThatFragment.btRight;
        if (answer && !noscore) incorrectCount++;
        trueFalseShowAnswer();
    }

    void trueFalseShowAnswer() {
        thisOrThatFragment.btRight.setEnabled(false);
        thisOrThatFragment.btLeft.setEnabled(false);
        boolean correct = ((answer && user_btn == thisOrThatFragment.btLeft) || (!answer && user_btn == thisOrThatFragment.btRight));
        if (!noscore) {
            if ((answer && user_btn == thisOrThatFragment.btLeft) || (!answer && user_btn == thisOrThatFragment.btRight)) {
                CompatibleHelper.setBackgroundDrawable(user_btn, uiHelper.createRoundedDrawable(cols.get(3), 6.0f));
                thisOrThatFragment.playRawSound(R.raw.correct);
                correctCount++;
                correct = true;
            } else {
                CompatibleHelper.setBackgroundDrawable(user_btn, uiHelper.createRoundedDrawable(cols.get(2), 6.0f));
                thisOrThatFragment.playRawSound(R.raw.incorrect);
            }
        } else {
            thisOrThatFragment.playRawSound(R.raw.slider_down);
        }
        if (!showFeedback(correct)) {
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    handleStep(step + 1);
                }
            }, 1000);
        }
    }

    public boolean showFeedback(boolean result) {
        boolean shown = false;
        if (curQuestion.feedback != null) {
            int idx = (result) ? 0 : 1;
            String feedback = curQuestion.feedback[idx];
            if (!TextUtils.isEmpty(feedback)) {
                shown = true;
                thisOrThatFragment.showHelp(feedback);
            }
        }

        return shown;
    }

    public void unbind() {
        thisOrThatFragment = null;
    }

    public void submitAnswer(String answer) {
        if (Objects.equal(answer, curQuestion.correct)) {
            thisOrThatFragment.showHelp(curQuestion.feedback[0]);
        } else {
            thisOrThatFragment.showHelp(curQuestion.feedback[1]);
        }
    }

    public void resultClosed() {
        handleStep(step + 1);
    }
}
