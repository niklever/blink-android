package com.blinktrainingsystem.blink.module.main;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        library = true,
        injects = MainActivity.class
)
public class MainModule {
    private final MainView mainView;

    public MainModule(MainView mainView) {
        this.mainView = mainView;
    }

    @Provides
    @Singleton
    public MainView provideMainView() {
        return mainView;
    }

    @Provides
    @Singleton
    public MainPresenter providePresenter(LocalSharedPreferences localSharedPreferences) {
        return new MainPresenterImpl(mainView, localSharedPreferences);
    }
}
