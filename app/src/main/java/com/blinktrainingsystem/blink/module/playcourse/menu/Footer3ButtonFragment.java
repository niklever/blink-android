package com.blinktrainingsystem.blink.module.playcourse.menu;

import android.animation.Animator;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by TALE on 10/3/2014.
 */
public class Footer3ButtonFragment extends FooterFragment {
    private static final String KEY_TEXT_MIDDLE = "text_middle";
    private static final String KEY_TYPE = "type";
    private static final String KEY_DISABLE_RIGHT_BUTTON = "disableRightButton";

    @InjectView(R.id.tvFooterMiddle)
    TextView tvFooterMiddle;

    @Inject
    ViewAnimator viewAnimator;

    @Inject TrackerHelper trackerHelper;

    private FooterButtonMenuClickListener buttonMenuClickListener;
    private Animator midAnimator;
    private Animator rightAnimator;
    private Animator leftAnimator;
    private boolean enable = true;

    public static interface FooterButtonMenuClickListener extends ButtonMenuClickListener {
        void onMiddleClick(CharSequence text);
    }

    public static class FooterMenuButton extends MenuButton {
        public final String textMiddle;
        public boolean disableRightButton;
        public int type = 0;

        public FooterMenuButton(String textLeft, String textMiddle, String textRight) {
            super(textLeft, textRight);
            this.type = 0;
            this.disableRightButton = false;
            this.textMiddle = textMiddle;
        }

        @Override
        public String toString() {
            return String.format("FooterMenuButton => type:%d, disableRightButton: %s, textLeft:%s, textMiddle:%s, textRight:%s", type, disableRightButton, textLeft, textMiddle, textRight);
        }
    }

    public static Footer3ButtonFragment newInstance(FooterMenuButton footerMenuButton) {
        return newInstance(footerMenuButton.textLeft, footerMenuButton.textMiddle, footerMenuButton.textRight);
    }

    public static Footer3ButtonFragment newInstance(String textLeft, String textMiddle, String textRight) {
        return newInstance(textLeft, textMiddle, textRight, 0, false);
    }

    public static Footer3ButtonFragment newInstance(String textLeft, String textMiddle, String textRight, int type, boolean disableRightButton) {
        Footer3ButtonFragment fragment = new Footer3ButtonFragment();
        final Bundle args = newArgs(textLeft, textRight);
        if (!TextUtils.isEmpty(textMiddle)) {
            args.putString(KEY_TEXT_MIDDLE, textMiddle);
        }
        if (type != 0) {
            args.putInt(KEY_TYPE, type);
        }
        args.putBoolean(KEY_DISABLE_RIGHT_BUTTON, disableRightButton);
        fragment.setArguments(args);
        return fragment;
    }

    public void onEvent(FooterMenuButton footerMenuButton) {
        Timber.d("updateMenuButton => %s", footerMenuButton.toString());
        if (TextUtils.isEmpty(footerMenuButton.textLeft)) {
            if (tvFooterLeft.getVisibility() == View.VISIBLE || (leftAnimator != null && leftAnimator.isRunning())) {
                if (leftAnimator != null) {
                    leftAnimator.cancel();
                }
                leftAnimator = viewAnimator.exitFadeOut(tvFooterLeft, Constant.ANIMATION_DURATION, null);
            }
        } else {
            tvFooterLeft.setText(footerMenuButton.textLeft);
            if (tvFooterLeft.getVisibility() != View.VISIBLE) {
                if (leftAnimator != null) {
                    leftAnimator.cancel();
                }
                if (enable) {
                    midAnimator = viewAnimator.enterFadeIn(tvFooterLeft, Constant.ANIMATION_DURATION, null);
                } else {
                    midAnimator = viewAnimator.fade(tvFooterLeft, 0.6f, Constant.ANIMATION_DURATION, null);
                }
            }
            tvFooterLeft.setEnabled(enable);
        }

        if (footerMenuButton.type == 1) {
            Timber.d("Type: 1");
            if (footerMenuButton.disableRightButton) {
                tvFooterRight.setText(footerMenuButton.textRight);
                if (tvFooterRight.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterRight, 0.6f, Constant.ANIMATION_DURATION, null);
                    tvFooterRight.setEnabled(false);
                }
            } else {
                if (tvFooterRight.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterRight, 1, Constant.ANIMATION_DURATION, null);
                    tvFooterRight.setEnabled(enable);
                }
            }

        } else if (TextUtils.isEmpty(footerMenuButton.textRight)) {
            if (tvFooterRight.getVisibility() == View.VISIBLE) {
                if (rightAnimator != null) {
                    rightAnimator.cancel();
                }
                rightAnimator = viewAnimator.exitFadeOut(tvFooterRight, Constant.ANIMATION_DURATION, null);
            }
        } else {
            tvFooterRight.setText(footerMenuButton.textRight);
            if (tvFooterRight.getVisibility() != View.VISIBLE || tvFooterRight.getAlpha() != 1 || (rightAnimator != null && rightAnimator.isRunning())) {
                if (rightAnimator != null) {
                    rightAnimator.cancel();
                }
                if (enable) {
                    midAnimator = viewAnimator.enterFadeIn(tvFooterRight, Constant.ANIMATION_DURATION, null);
                } else {
                    midAnimator = viewAnimator.fade(tvFooterRight, 0.6f, Constant.ANIMATION_DURATION, null);
                }
            }
            tvFooterRight.setEnabled(enable);
        }

        if (TextUtils.isEmpty(footerMenuButton.textMiddle)) {
            if (tvFooterMiddle.getVisibility() == View.VISIBLE) {
                if (midAnimator != null) {
                    midAnimator.cancel();
                }
                midAnimator = viewAnimator.exitFadeOut(tvFooterMiddle, Constant.ANIMATION_DURATION, null);
            }
        } else {
            tvFooterMiddle.setText(footerMenuButton.textMiddle);
            if (tvFooterMiddle.getVisibility() != View.VISIBLE || (midAnimator != null && midAnimator.isRunning())) {
                if (midAnimator != null) {
                    midAnimator.cancel();
                }
                if (enable) {
                    midAnimator = viewAnimator.enterFadeIn(tvFooterMiddle, Constant.ANIMATION_DURATION, null);
                } else {
                    midAnimator = viewAnimator.fade(tvFooterMiddle, 0.6f, Constant.ANIMATION_DURATION, null);
                }
            }
            tvFooterMiddle.setEnabled(enable);
        }

    }

    public void onEvent(Event event) {
        Timber.d("updateMenuButton => %s", event.toString());
        switch (event) {
            case HelpOpened:
                enable = false;
                if (tvFooterLeft.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterLeft, 0.6f, Constant.ANIMATION_DURATION, null);
                }
                tvFooterLeft.setEnabled(enable);
                if (tvFooterRight.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterRight, 0.6f, Constant.ANIMATION_DURATION, null);
                }
                tvFooterRight.setEnabled(enable);
                if (tvFooterMiddle.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterMiddle, 0.6f, Constant.ANIMATION_DURATION, null);
                }
                tvFooterMiddle.setEnabled(enable);
                break;
            case HelpClosed:
                enable = true;
                if (tvFooterLeft.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterLeft, 1, Constant.ANIMATION_DURATION, null);
                }
                tvFooterLeft.setEnabled(enable);
                if (tvFooterRight.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterRight, 1, Constant.ANIMATION_DURATION, null);
                }
                tvFooterRight.setEnabled(enable);
                if (tvFooterMiddle.getVisibility() == View.VISIBLE) {
                    viewAnimator.fade(tvFooterMiddle, 1, Constant.ANIMATION_DURATION, null);
                }
                tvFooterMiddle.setEnabled(enable);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof FooterButtonMenuClickListener) {
            buttonMenuClickListener = (FooterButtonMenuClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.footer_3_button, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            final String textLeft = args.getString(KEY_TEXT_LEFT, null);
            final String textRight = args.getString(KEY_TEXT_RIGHT, null);
            final String textMid = args.getString(KEY_TEXT_MIDDLE, null);
            final int type = args.getInt(KEY_TYPE, 0);
            final boolean disableRightButton = args.getBoolean(KEY_DISABLE_RIGHT_BUTTON, false);
            FooterMenuButton footerMenuButton = new FooterMenuButton(textLeft, textMid, textRight);
            footerMenuButton.type = type;
            footerMenuButton.disableRightButton = disableRightButton;
            onEvent(footerMenuButton);
        }
    }

    @OnClick(R.id.tvFooterMiddle)
    public void onFooterMiddleClick() {
        if (violateMultipleClickPolicy()) {
            return;
        }
        if (buttonMenuClickListener != null) {
            buttonMenuClickListener.onMiddleClick(tvFooterMiddle.getText());
        }
    }
}
