package com.blinktrainingsystem.blink.module.main;

import com.blinktrainingsystem.blink.data.model.pojo.Course;

/**
 * Created by TALE on 9/8/2014.
 */
public interface MainView {
    void showLogin();

    void showRegister();

    void showGuestRegister(String courseTitle, long courseId);

    void backToPrevious();

    void showCourses();

    void showCourseDetail(Course course);

    void showPlayCourse(Course course);
}
