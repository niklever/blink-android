package com.blinktrainingsystem.blink.module.courses;

import com.blinktrainingsystem.blink.data.model.pojo.Course;

/**
 * Created by TALE on 9/10/2014.
 */
public interface CoursesUseCase {
    public void logout();

    public void openCourse(Course course);

    public void playCourse(Course course);

    public void downloadCourse(Course course);

    public void viewMyCourse();

    public void viewAllCourse();

    public void cancelLoading();

    public void resetCourse(Course course);

    public void removeCourse(Course course);

    void refreshCourses();

    void updateCourses();

    void viewUnits();
}
