package com.blinktrainingsystem.blink.module.playcourse;

import com.blinktrainingsystem.blink.data.model.pojo.Blink;

import java.util.List;

/**
 * Created by TALE on 9/19/2014.
 */
public class ActivityGridFragment extends BurstGridFragment {

    @Override
    protected String getTitle() {
        return "Activities";
    }

    @Override
    protected List<Blink> loadData() {
        return blinkHandler.getArrayActivitiesStart();
    }

    @Override protected int getUnlockedPosition() {
        final List<Blink> unlockedActivities = blinkHandler.getArrayUnlockedActivities(course);
        return unlockedActivities == null ? 0 : unlockedActivities.size() == 0 ? 1 : unlockedActivities.size() - 1;
    }

    @Override protected Boolean isAllowSkip() {
        return false;
    }
}
