package com.blinktrainingsystem.blink.module.playcourse;

import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;

import java.util.List;

/**
 * Created by TALE on 9/19/2014.
 */
public class DossierGridFragment extends UnitsGridFragment {

    @Override
    public void onItemSelected(Blink blink) {
        Static.dossier = true;
        super.onItemSelected(blink);
    }

    @Override
    protected List<Blink> getBlinkList() {
        return blinkHandler.getDossierBlinks();
    }

    @Override
    protected String getTitle() {
        return "Personal Dossiers";
    }
}
