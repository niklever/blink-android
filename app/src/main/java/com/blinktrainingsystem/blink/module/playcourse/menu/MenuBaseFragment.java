package com.blinktrainingsystem.blink.module.playcourse.menu;

import com.blinktrainingsystem.blink.common.fragment.BaseFragment;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by TALE on 9/24/2014.
 */
public class MenuBaseFragment extends BaseFragment {

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        modules.add(new MenuModule());
        return modules;
    }
}
