package com.blinktrainingsystem.blink.module.coursedetail;

import com.blinktrainingsystem.blink.common.dialog.BuyCourseConfirmationDialog;
import com.blinktrainingsystem.blink.data.model.pojo.Course;

import java.util.List;

/**
 * Created by TALE on 9/8/2014.
 */
public interface CourseDetailView {

    void showProgress();

    void hideProgress();

    void showError();

    void showCourses(List<Course> courses);

    void showBuyCourseConfirmationDialog(String message, BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener confirmationButtonOnClickListener);
}
