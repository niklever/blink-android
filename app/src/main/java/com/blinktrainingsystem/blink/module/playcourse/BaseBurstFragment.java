package com.blinktrainingsystem.blink.module.playcourse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.util.BusDriver;

import timber.log.Timber;

/**
 * Created by talenguyen on 18/09/2014.
 */
public abstract class BaseBurstFragment extends PlayCourseFragment {

    protected BusDriver busDriver;
    protected boolean isHelp;
    protected boolean isAllowSkip;
    private int timer = 0;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        busDriver = BusDriver.withBus(bus);
        isAllowSkip = localSharedPreferences.allowSkip().getOr(false);
        blinkHandler.startCourse(course.id);
        blinkHandler.startNewBurstScore(course.id, blinkIndex);
        if (blink != null) {
            if (!localSharedPreferences.isHelpShown(course.id, blink.type).getOr(false)) {
                isHelp = true;
                showHelp();
                localSharedPreferences.isHelpShown(course.id, blink.type).put(true);
            }
            startBurst();
            showTimer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showFooterMenu();
        onBurstViewCreated(blinkHandler);
        Timber.d("startBurst() => BlinkHandler");
        configureHeaderMenu();
        if (timer > 0 && !isHelp) {
            busDriver.startTimer();
        }
    }

    protected void startBurst() {

    }

    private void showTimer() {
        if (blink != null) {
            timer = getTimer();
            if (timer > 0) {
                busDriver.showTimer(timer);
            }
        }
    }

    protected abstract int getTimer();

    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (event == Event.HelpClosed) {
            if (isHelp && timer > 0) {
                busDriver.startTimer();
            }
        } else if (event == Event.HelpRequest) {
            if (timer > 0) {
                busDriver.pauseTimer();
            }
            showHelp();
            isHelp = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer > 0 && !isHelp) {
            busDriver.pauseTimer();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (busDriver != null) {
            busDriver.closeHelp();
        }
    }

    @Override
    public void onAllowSkipChanged() {
        super.onAllowSkipChanged();
        isAllowSkip = localSharedPreferences.allowSkip().getOr(false);
        showFooterMenu();
    }

    protected void showFooterMenu() {

    }

    protected void configureHeaderMenu() {
        final int totalStepInBurst = blinkHandler.getTotalStepInBurst(blinkIndex);
        if (isShowHeaderMenu() && totalStepInBurst > 1) {
            final int stepInBurst = blinkHandler.getStepInBurst(blinkIndex);
            Timber.d("onResume() => totalStepInBurst: %d, stepInBurst: %d", totalStepInBurst, stepInBurst);
            busDriver.showHeaderMenuProgress(totalStepInBurst, stepInBurst);
        } else {
            busDriver.hideHeaderMenuProgress();
        }
    }

    protected void showHelp() {
        final String helpMessage = getHelpMessage();
        if (!TextUtils.isEmpty(helpMessage)) {
            busDriver.showHelp(helpMessage);
        } else {
            isHelp = false;
        }
    }

    protected String getHelpMessage() {
        return null;
    }

    protected boolean isShowHeaderMenu() {
        return true;
    }

    protected String getTitle() {
        return "";
    }

    protected void onBurstViewCreated(BlinkHandler blinkHandler) {
        playRawSound(R.raw.window_in);
        bus.post(blinkHandler);
    }

    public void notifyBurstCompleted() {
        busDriver.notifyBlinkCompleted();
    }
}
