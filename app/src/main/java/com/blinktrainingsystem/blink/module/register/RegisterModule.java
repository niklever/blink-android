package com.blinktrainingsystem.blink.module.register;

import android.app.Application;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        injects = RegisterFragment.class
)
public class RegisterModule {
    private final RegisterView registerView;

    public RegisterModule(RegisterView registerView) {
        this.registerView = registerView;
    }

    @Provides
    @Singleton RegisterPresenter provideRegisterPresenter(MainPresenter mainPresenter, Application application, WebServices webServices, TaskManager taskManager, DeviceInfo deviceInfo, LocalSharedPreferences localSharedPreferences, TrackerHelper trackerHelper) {
        return new RegisterPresenter(registerView, mainPresenter, application, webServices, taskManager, deviceInfo, localSharedPreferences, trackerHelper);
    }

}
