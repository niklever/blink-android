package com.blinktrainingsystem.blink.module.playcourse.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by TALE on 9/24/2014.
 */
public class MenuBackTitleFragment extends MenuBaseFragment {

    private static final String KEY_TITLE = "title";
    @InjectView(R.id.tvTitle)
    TextView _TvTitle;
    @InjectView(R.id.btBack)
    TextView _BtBack;


    public static MenuBackTitleFragment newInstance(String title) {
        final MenuBackTitleFragment fragment = new MenuBackTitleFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    public void onEvent(Event event) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.header_back_title, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String title = getArguments() == null || getArguments().containsKey(KEY_TITLE) ? null : getArguments().getString(KEY_TITLE);
        _TvTitle.setText(title);
        _BtBack.setText(R.string.txt_back);
    }

    @OnClick(R.id.btBack)
    public void back() {
        bus.post(Event.Back);
    }
}
