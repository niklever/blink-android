package com.blinktrainingsystem.blink.module.playcourse.orderitem;

import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Item;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.OrderItems;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class OrderItemFragment extends BaseBurstFragment {

    private final int DRAG_VIEW_COLOR = Color.parseColor("#FFCC00");
    private final int DROP_VIEW_COLOR = Color.parseColor("#FF9933");
    JsonEx json;
    OrderItems data;
    boolean ordered;
    int inCorrectCount;

    boolean isShowTimeOut;
    boolean isComplete;

    List<JSONObject> drops;
    List<JSONObject> drags;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    @Inject
    AssetManager assetManager;

    @Inject
    UiHelper shape;

    @Inject
    DeviceInfo deviceInfo;

    @InjectView(R.id.tvTitle)
    TextView lblPrompt;

    @InjectView(R.id.contentView)
    LinearLayout contentView;

    @Inject TrackerHelper trackerHelper;

    private View enteredView;
    private View draggingView;
    private int dragSlot;
    private int itemWidth;
    private int itemHeight;
    public int timer;
    @Inject
    UiHelper uiHelper;

    @Override public void onResume() {
        super.onResume();
        trackerHelper.orderItemLoad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_item, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();

        loadOrderedItem();
        initUi();
    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    private void initUi() {
        if (data != null) {

            if (data.sound.length() > 0) {
                playSound(data.sound, false);
            }

            final String longestItem = getLongestItems(data.items);

            itemWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.order_box_width));

            itemHeight = uiHelper.measureTextViewHeightForText(getActivity(), longestItem, getResources().getDimensionPixelSize(R.dimen.question_font_size), itemWidth) + deviceInfo.getDimensionPixel(8);

            lblPrompt.setText(Helper.getStringWithReplacements(data.prompt));

            ViewObservableHelper.globalLayoutFrom(lblPrompt)
                    .subscribe(
                            new Action1<View>() {
                                @Override
                                public void call(View view) {
                                    initItemViews();

                                    final int padding = deviceInfo.getDimensionPixel(10);
                                    final int maxSize = data == null || data.items == null || data.items.size() == 0 ? 0 : data.items.size() > 2 ? 3 : data.items.size();
                                    final int maxWidth = maxSize == 0 ? 0 : maxSize * itemWidth + (maxSize - 1) * padding;
                                    final int rightMargin = getResources().getDimensionPixelSize(R.dimen.score_bg_w);
                                    final int maxContainerWidth = view.getWidth() - rightMargin;
                                    if (maxWidth < maxContainerWidth) {
                                        contentView.setPadding(0, 0, rightMargin, 0);
                                        contentView.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }
                                }
                            }
                    );
        }
    }

    private String getLongestItems(List<Item> items) {
        if (items != null && items.size() > 0) {
            String longestItem = "";
            for (Item item : items) {
                final int length = item == null || item.text == null ? 0 : item.text.length();
                if (length > longestItem.length()) {
                    longestItem = item.text;
                }
            }
            return longestItem;
        }
        return null;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.order_item_help);
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_done));
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                }
                if (isShowTimeOut) {
                    bus.post(Event.BlinkCompleted);
                } else if (isComplete) {
                    bus.post(Event.BlinkCompleted);
                }
                break;

            case DoneClick:
                blinkComplete();
                break;
            case TimeOut: {
                timeOut();
                break;
            }
        }
    }


    private void showHelp(String message) {
        busDriver.showHelp(message);
    }

    private void loadOrderedItem() {
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json.orderitems;
        ordered = data == null ? false : Boolean.getBoolean(data.ordered);
        inCorrectCount = 0;
    }

    private void timeOut() {
        contentView.setEnabled(false);
        isShowTimeOut = true;
        showHelp("Sorry, you're out of time.");
    }

    public void blinkComplete() {
        busDriver.pauseTimer();
        String str;
        int score = json.score;
        int gems = json.gems;
        boolean feedbackShown = false;
//        if (drags!=nil){
//            for(NSMutableDictionary *dict in drags){
//                UILabel *lbl = [dict objectForKey:@"drag"];
//                lbl.userInteractionEnabled = NO;
//                id tmp = [dict objectForKey:@"dropIndex"];
//                BOOL correct = NO;
//                if (tmp!=nil){
//                    int index = [tmp intValue];
//                    correct = (lbl.tag==index || lbl.tag==0);
//                }
//                if (!correct) inCorrectCount++;
//            }
//        }

        int user_score = (int) ((double) ((4.0 - (double) inCorrectCount) / 4.0) * (double) score);
        if (user_score < 0) user_score = 0;

        if (inCorrectCount <= 1) {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
//        if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
            playRawSound(R.raw.correct);
            str = json.feedback == null || json.feedback.length < 1 ? null : json.feedback[0];
            if (!TextUtils.isEmpty(str)) {
                showHelp(str);
                feedbackShown = true;
            } else if (gems > 0) {
                // TODO: show gems
//                BlkGemVC * gem_vc =[[BlkGemVC alloc]initWithGems:
//                gems];
//                [self.view addSubview:gem_vc.view];
            }
        } else {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.incorrect);
            str = json.feedback == null || json.feedback.length < 2 ? null : json.feedback[1];
            if (!TextUtils.isEmpty(str)) {
                showHelp(str);
                feedbackShown = true;
            }
        }
        isComplete = true;
        if (!feedbackShown) {
            BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        }
    }


//
//    private void handleDragEvent() {
//        (IBAction)handleDrag:(UIPanGestureRecognizer *)recognizer {
//    CGRect frame = recognizer.view.frame;
//    if(recognizer.state == UIGestureRecognizerStateBegan){
//        [self.view bringSubviewToFront:recognizer.view];
//        NSMutableDictionary *dict = [self getDictionaryFromView:recognizer.view];
//        if (dict!=nil){
//            NSLog(@"Drag item found");
//            CGRect frm;
//            CGRectMakeWithDictionaryRepresentation((CFDictionaryRef)[dict objectForKey:@"frame"], &frm);
//            org = CGPointMake(frm.origin.x, frm.origin.y);
//            if ([dict objectForKey:@"drop"]!=[NSNull null]){
//                 NSMutableDictionary *dct = [dict objectForKey:@"drop"];
//                [dct setObject:[NSNumber numberWithBool:NO] forKey:@"occupied"];
//                [dict setObject:[NSNull null] forKey:@"drop"];
//                [blkNavControllerRef playSfx:@"This"];
//            }
//        }else{
//            NSLog(@"Drag item not found");
//            org = CGPointMake(frame.origin.x, frame.origin.y);
//        }
//        //[blkNavControllerRef showNextButton];
//    }else if (recognizer.state == UIGestureRecognizerStateEnded){
//        bool dropped = false;
//        bool correct = false;
//        int index = 1;
//        for(NSMutableDictionary *dict in drops){
//            BOOL occupied = [[dict objectForKey:@"occupied"] boolValue];
//            if (!occupied){
//                UILabel *lbl = [dict objectForKey:@"drop"];
//                if (CGRectContainsPoint(lbl.frame, recognizer.view.center)){
//                    NSLog(@"Dropped goal:%d correct:%d index:%d", recognizer.view.tag, recognizer.view.tag, index);
//                    dropped = true;
//                    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"occupied"];
//                    NSMutableDictionary *dct = [self getDictionaryFromView:recognizer.view];
//                    [dct setObject:dict forKey:@"drop"];
//                    [dct setObject:[NSNumber numberWithInt:index] forKey:@"dropIndex"];
//                    correct = (recognizer.view.tag==index || recognizer.view.tag==0);
//                    frame = lbl.frame;
//                    recognizer.view.frame = frame;
//                    if (!correct){
//                        inCorrectCount++;
//                        //if (inCorrectCount>=3) [self blinkComplete];
//                    }
//                    int count=0;
//                    for(NSMutableDictionary *dct in drops){
//                        occupied = [[dct objectForKey:@"occupied"] boolValue];
//                        if (occupied) count++;
//                    }
//                    //if (count == [drops count]) [self blinkComplete];
//                    [blkNavControllerRef playSfx:@"That"];
//                    break;
//                }
//            }
//            index++;
//        }
//        if (!dropped){
//            CGRect frame = recognizer.view.frame;
//            frame.origin.x = org.x;
//            frame.origin.y = org.y;
//            recognizer.view.frame = frame;
//            [blkNavControllerRef playSfx:@"Suction_Remove"];
//        }
//    }
//    CGPoint translation = [recognizer translationInView:self.view];
//    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
//                                         recognizer.view.center.y + translation.y);
//    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
//    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void initItemViews() {

        List<Item> items = data.items;
        inCorrectCount = items.size();

        Collections.shuffle(items);

        LinearLayout row = newRow();
        final int rowMargin = deviceInfo.getDimensionPixel(10);
        final int paragraphMargin = deviceInfo.getDimensionPixel(24);

        final int itemCount = (data.slots == null ? 0 : data.slots.size()) + (data.items == null ? 0 : data.items.size());
        final int totalRows = (itemCount) / 3 + (itemCount % 3 == 0 ? 0 : 1);

        int minHeight = (contentView.getHeight() - lblPrompt.getBottom() - (2 * paragraphMargin + (totalRows - 2) * rowMargin) - rowMargin) / totalRows;
        int smallestHeight = deviceInfo.getDimensionPixel(32);
        if (minHeight < smallestHeight) {
            minHeight = smallestHeight;
        }
        if (itemHeight < minHeight) {
            itemHeight = minHeight;
        }

        Timber.d("itemCount: %d, totalRows: %d, itemHeight: %d", itemCount, totalRows, itemHeight);
        row.setPadding(0, paragraphMargin, 0, 0);

        contentView.addView(row);
        for (int i = 0; i < data.slots.size(); i++) {
            if (i > 0 && i % 3 == 0) {
                row = newRow();
                row.setPadding(0, rowMargin, 0, 0);
                contentView.addView(row);
            }
            String item = data.slots.get(i);
            View dropView = newDropView(i, item);
            row.addView(dropView);
            ((LinearLayout.LayoutParams) dropView.getLayoutParams()).rightMargin = rowMargin;

        }

        row = newRow();
        row.setPadding(0, paragraphMargin, 0, 0);
        contentView.addView(row);

        for (int i = 0; i < items.size(); i++) {
            if (i > 0 && i % 3 == 0) {
                row = newRow();
                row.setPadding(0, rowMargin, 0, 0);
                contentView.addView(row);
            }
            final Item item = items.get(i);
            if (item.slot < 1) {
                inCorrectCount--;
            }
            View dragView = newDragView(item.slot, item.text);
            row.addView(dragView);
            ((LinearLayout.LayoutParams) dragView.getLayoutParams()).rightMargin = rowMargin;
        }

    }

    private LinearLayout newRow() {
        Timber.d("Request a new row");
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        return linearLayout;
    }

    private TextView newTextView(String text, int color) {
        TextView lbl = new TextView(getActivity());
        lbl.setTextColor(Color.WHITE);
        lbl.setGravity(Gravity.CENTER);
        lbl.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        Util.setTypeface(lbl, getString(R.string.font_myriad_pro_regular));
        lbl.setBackgroundColor(color);
        lbl.setText(Helper.getStringWithReplacements(text));
        final FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(itemWidth, itemHeight);
        lbl.setLayoutParams(lp);
        return lbl;
    }

    private boolean result;

    private View newDropView(final int index, String text) {
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, itemHeight);
        final FrameLayout dropView = new FrameLayout(getActivity());
        dropView.setLayoutParams(params);

        TextView textView = newTextView(text, DROP_VIEW_COLOR);
        dropView.addView(textView);

        dropView.setOnDragListener(
                new View.OnDragListener() {
                    public boolean onDrag(final View v, DragEvent event) {
                        final int action = event.getAction();
                        switch (action) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                enteredView = null;
                                return true;
                            case DragEvent.ACTION_DRAG_ENDED:
                                Timber.d("ACTION_DRAG_ENDED %s", enteredView != null);
                                if (enteredView == null) {
                                    draggingView.setVisibility(View.VISIBLE);
                                    playRawSound(R.raw.suction_remove);
                                } else if (enteredView == v) {
                                    playRawSound(R.raw.that);
                                    if (((ViewGroup) enteredView).getChildCount() < 2) {
                                        // Catch the draggingView.
                                        postMainThread(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (dragSlot == index + 1) {
                                                            inCorrectCount--;
                                                        }
                                                        Timber.d("DragSlot: %d, index: %d, inCorrectCount: %d", dragSlot, index + 1, inCorrectCount);
                                                        final ViewGroup parent = (ViewGroup) draggingView.getParent();
                                                        parent.removeView(draggingView);
                                                        ((ViewGroup) v).addView(draggingView);
                                                        draggingView.setVisibility(View.VISIBLE);
                                                    }
                                                }
                                        );
                                    } else {
                                        draggingView.setVisibility(View.VISIBLE);
                                    }
                                }


                                return true;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                Timber.d("ACTION_DRAG_ENTERED");
                                enteredView = v;
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                enteredView = null;
                                Timber.d("ACTION_DRAG_EXITED");
                                break;
                        }
                        return false;
                    }
                }
        );
        return dropView;
    }

    private View newDragView(final int index, String text) {
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, itemHeight);
        final FrameLayout dropView = new FrameLayout(getActivity());
        dropView.setLayoutParams(params);

        final TextView textView = newTextView(text, DRAG_VIEW_COLOR);
        textView.setTextColor(Color.BLACK);
        dropView.addView(textView);
        ViewObservableHelper.longClick(textView).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        view.setVisibility(View.INVISIBLE);
                        draggingView = textView;
                        dragSlot = index;
                        textView.startDrag(null, new View.DragShadowBuilder(view), null, 0);
                        playRawSound(R.raw.this_);
                    }
                }
        );
        textView.setTag(index);
        return dropView;
    }
}
