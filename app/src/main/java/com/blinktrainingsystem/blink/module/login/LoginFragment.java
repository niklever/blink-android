package com.blinktrainingsystem.blink.module.login;

import org.json.JSONObject;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.fragment.StructureFragment;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.module.coursedetail.CourseDetailFragment;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.google.common.collect.Lists;
import com.blinktrainingsystem.blink.module.main.MainActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphResponse;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.LoginManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by TALE on 9/8/2014.
 */
public class LoginFragment extends StructureFragment implements LoginView {

    @InjectView(R.id.etEmail)
    EditText etEmail;

    @InjectView(R.id.etPassword)
    EditText etPassword;

    @InjectView(R.id.tvError)
    TextView tvError;

    @Inject
    LoginPresenter loginPresenter;

    @Override
    protected int getMenuLayoutId() {
        return 0;
    }

    @Override
    protected int getHeaderLayoutId() {
        return 0;
    }

    @Override
    protected int getFooterLayoutId() {
        return R.layout.footer_copy_right;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void onPause() {
        super.onPause();
        loginPresenter.cancelLoginTask();
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(hideErrorRunnable);
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        modules.add(new LoginModule(this));
        return modules;
    }

    @Inject
    LocalSharedPreferences localSharedPreferences;
    @Inject
    DeviceInfo deviceInfo;
    @Inject TrackerHelper trackerHelper;

    private LoginButton loginButton;
    private final String TAG = "LoginFragment";
    private CallbackManager callbackManager;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) view.findViewById(R.id.fbLogin);
        loginButton.setReadPermissions("email,public_profile");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                final AccessToken accessToken = loginResult.getAccessToken();
                GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                        String email = user.optString("email");
                        if (email==null || email==""){
                            showError("Your Facebook account does not contain an email address which is needed for Blink. Please login using your email and password or register by pressing the button below");
                        }else{
                            loginPresenter.fbLogin(user);
                            String info = String.format("fblogin callback id:%s name:%s email:%s gender:%s locale:%s", user.optString("id",""), user.optString("name",""),
                                    user.optString("email",""), user.optString("gender",""), user.optString("locale",""));
                            Log.d(TAG, info);
                        }
                    }
                }).executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "User cancelled");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "There was an error " + exception.toString());
            }
        });

        Util.setTypeface(etEmail, getString(R.string.font_myriad_pro_regular));
        Util.setTypeface(etPassword, getString(R.string.font_myriad_pro_regular));
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        etPassword.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            login();
                            return true;
                        }
                        return false;
                    }
                }
        );
        final String usr = localSharedPreferences.usr().getOr(null);
        final String pwd = localSharedPreferences.pwd().getOr(null);
        if (usr != null) {
            etEmail.setText(usr);
        }
        if (pwd != null) {
            etPassword.setText(pwd);
        }
        trackerHelper.loginLoad();
    }

    @OnClick(R.id.btGuest)
    public void enterAsGuest() {
        Helper.hideSoftKeyboard(getActivity().getCurrentFocus());
        loginPresenter.enterAsGuest();
    }

    @OnClick(R.id.btLogin)
    public void login() {
        Helper.hideSoftKeyboard(getActivity().getCurrentFocus());
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        loginPresenter.login(email, password);
    }

    @OnClick(R.id.btForgotPassword)
    public void forgotPassword() {
        final String email = etEmail.getText().toString();
        loginPresenter.forgotPassword(email);
    }

    @OnClick(R.id.btRegister)
    public void register() {
        loginPresenter.requestRegister();
    }

    @Override
    public void showProgress() {
        showProgress("Loading...");
    }

    @Override
    public void hideProgress() {
        dismissProgress();
    }

    @Override
    public void showError(String error) {
        tvError.setText(error);
        tvError.setVisibility(View.VISIBLE);
        handler.removeCallbacks(hideErrorRunnable);
        handler.postDelayed(hideErrorRunnable, Constant.ERROR_DURATION);
    }

    private boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onResume(){
        if (isLoggedIn()) LoginManager.getInstance().logOut();
        super.onResume();
    }

    @Inject
    Handler handler;

    private Runnable hideErrorRunnable = new Runnable() {
        @Override
        public void run() {
            tvError.setVisibility(View.INVISIBLE);
        }
    };

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //super.onActivityResult(requestCode, resultCode, data);
    }
}
