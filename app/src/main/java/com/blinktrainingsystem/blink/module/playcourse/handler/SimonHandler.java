package com.blinktrainingsystem.blink.module.playcourse.handler;

import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.Simon;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by TALE on 9/23/2014.
 */
public class SimonHandler {

    private final Blink blink;
    private JsonEx jsonObject;
    private List<String> phrases;
    private List<String> data;

    public SimonHandler(Blink blink) throws IllegalArgumentException {
        if (blink.type != BlinkType.KBlinkSimon) {
            throw new IllegalArgumentException("Wrong type, this_ expect a KBlinkSimon type");
        }
        this.blink = blink;
        jsonObject = blink.loadJsonToObject();
        if (jsonObject != null) {
            Simon simon = jsonObject.simon;
            if (simon != null) {
                Splitter splitter = Splitter.on("|");
                List<String> strings = splitter.splitToList(simon.phrase);
                if (strings != null) {
                    if (data == null) {
                        data = Lists.newArrayList();
                    }
                    data.addAll(strings);
                    if (phrases == null) {
                        phrases = Lists.newArrayList();
                    }
                    phrases.addAll(strings);
                }
                strings = splitter.splitToList(simon.wrong);
                if (strings != null) {
                    if (data == null) {
                        data = Lists.newArrayList();
                    }
                    data.addAll(strings);
                }
            }
        }

    }

    public List<String> getData() {
        return data;
    }

    public List<String> getPhrases() {
        return phrases;
    }

    public String getFeedback(int position) {
        if (jsonObject.feedback == null || jsonObject.feedback.length <= position || position < 0) {
            return "";
        }
        return jsonObject.feedback[position];
    }

    public String[] getSounds() {
        if (jsonObject != null && jsonObject.simon != null) {
            return jsonObject.simon.sounds;
        }
        return null;
    }

    public String getSoundNameForText(String query) {
        if (data == null) {
            return null;
        }
        final int indexOf = data.indexOf(query);
        final Simon simon = jsonObject.simon;
        final String[] sounds = simon == null ? null : simon.sounds;
        if (sounds != null && indexOf < sounds.length && indexOf >= 0) {
            return sounds[indexOf];
        }
        return null;
    }

    public String getSummary() {
        return jsonObject == null || jsonObject.simon == null ? "" : jsonObject.simon.prompt;
    }
}
