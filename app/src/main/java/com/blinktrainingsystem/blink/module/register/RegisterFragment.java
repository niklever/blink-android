package com.blinktrainingsystem.blink.module.register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.fragment.StructureFragment;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.module.main.MainActivity;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.common.collect.Lists;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by TALE on 9/9/2014.
 */
public class RegisterFragment extends StructureFragment implements RegisterView {

    @InjectView(R.id.btBack)
    TextView btBack;
    @InjectView(R.id.btSubmit)
    TextView btSubmit;
    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.tvError)
    TextView tvError;
    @InjectView(R.id.etFirstname)
    EditText etFirstname;
    @InjectView(R.id.etLastName)
    EditText etLastName;
    @InjectView(R.id.etEmail)
    EditText etEmail;
    @InjectView(R.id.etPassword)
    EditText etPassword;
    @InjectView(R.id.fbLogin)
    LoginButton loginButton;

    private final String TAG = "RegisterFragment";

    private CallbackManager callbackManager;

    @Inject
    RegisterPresenter registerPresenter;

    @Inject
    ViewAnimator viewAnimator;

    @Inject TrackerHelper trackerHelper;

    @Inject
    DeviceInfo deviceInfo;

    private String returnCourseTitle;
    private long returnCourseId;

    public void setReturnCourse(String title, long id){
        returnCourseTitle = title;
        returnCourseId = id;
        if (registerPresenter!=null) registerPresenter.backIsCourseDetail = true;
    }

    @Override
    protected int getMenuLayoutId() {
        return 0;
    }

    @Override
    protected int getHeaderLayoutId() {
        return R.layout.header_back_title;
    }

    @Override
    protected int getFooterLayoutId() {
        return R.layout.footer_copy_right_submit;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_register;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) view.findViewById(R.id.fbLogin);
        loginButton.setReadPermissions("email,public_profile");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                final AccessToken accessToken = loginResult.getAccessToken();
                GraphRequestAsyncTask request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                        String email = user.optString("email");
                        if (email==null || email==""){
                            showError("Your Facebook account does not contain an email address which is needed for Blink. Please login using your email and password or register by pressing the button below");
                        }else{
                            registerPresenter.fbLogin(user);
                            String info = String.format("fblogin callback id:%s name:%s email:%s gender:%s locale:%s", user.optString("id",""), user.optString("name",""),
                                    user.optString("email",""), user.optString("gender",""), user.optString("locale",""));
                            Log.d(TAG, info);
                        }
                    }
                }).executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "User cancelled");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "There was an error " + exception.toString());
            }
        });

        if (returnCourseTitle!=null){
            btBack.setText(returnCourseTitle);
            if (registerPresenter!=null) registerPresenter.backIsCourseDetail = true;
        }else {
            btBack.setText(R.string.txt_login);
        }
        tvTitle.setText(R.string.txt_register);
        setUpFonts();
        trackerHelper.registerLoad();
    }

    private void setUpFonts() {
        Util.setTypeface(tvError, getString(R.string.font_myriad_pro_regular));
        Util.setTypeface(etPassword, getString(R.string.font_myriad_pro_regular));
        Util.setTypeface(etFirstname, getString(R.string.font_myriad_pro_regular));
        Util.setTypeface(etLastName, getString(R.string.font_myriad_pro_regular));
        Util.setTypeface(etEmail, getString(R.string.font_myriad_pro_regular));
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    @OnClick(R.id.btBack)
    public void backPressed() {
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewAnimator.enterLeft(btBack, Constant.ANIMATION_DURATION);
    }

    @Override
    public void onPause() {
        super.onPause();
        registerPresenter.cancelRegisterTask();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btSubmit)
    public void submit() {
        Helper.hideSoftKeyboard(getActivity().getCurrentFocus());
        final String firstName = etFirstname.getText().toString();
        final String lastName = etLastName.getText().toString();
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        registerPresenter.submitRegisterForm(firstName, lastName, email, password);
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        modules.add(new RegisterModule(this));
        return modules;
    }


    @Override
    public void showProgress() {
        showProgress("Registering...");
    }

    @Override
    public void hideProgress() {
        dismissProgress();
    }

    @Override
    public void showError(String error) {
        if (tvError != null) {
            tvError.setText(error);
            tvError.setVisibility(View.VISIBLE);
        }
        if (handler != null) {
            handler.removeCallbacks(hideErrorRunnable);
            handler.postDelayed(hideErrorRunnable, Constant.ERROR_DURATION);
        }
    }

    @Inject
    Handler handler;

    private Runnable hideErrorRunnable = new Runnable() {
        @Override
        public void run() {
            if (tvError != null) {
                tvError.setVisibility(View.INVISIBLE);
            }
        }
    };
}
