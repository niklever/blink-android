package com.blinktrainingsystem.blink.module.playcourse.wordfill;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.WordFill;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.Optional;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class WordFillFragment extends BaseBurstFragment {

    //    private final String FAILURE = "F_A_I_L_U_R_E";
//    private final String SUCCEED = "S_U_C_C_E_E_D";
//    private final String SPACE = "S_P_A_C_E";
    int correctCount, incorrectCount, itemCount;
    JsonEx json;
    WordFill data;
    int dropCount;
    ArrayList<TextView> drags = Lists.newArrayList();
    boolean isComplete;
    boolean isTimeOut;

    @InjectView(R.id.tvTitle)
    TextView lblPrompt;

    @InjectView(R.id.flSentences)
    LinearLayout flSentences;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    @Inject
    UiHelper uiHelper;

    @InjectView(R.id.llQuestion)
    LinearLayout _LlQuestion;
    @Optional @InjectView(R.id.scroller)
    ScrollView scroller;
    @Optional
    @InjectView(R.id.llDrags)
    LinearLayout _LlDrags;
    @Optional
    @InjectView(R.id.hsvScroller)
    HorizontalScrollView _HsvScroller;

    //    private Queue<Integer> indexes;
//    private Queue<String> texts;
    private int boxWidth;
    private int boxHeight;

    @Inject
    DeviceInfo deviceInfo;
    @Inject TrackerHelper trackerHelper;
    private View selectedView;
    private String selectedWord;
    private View enteredView;
    ;
    private FlowLayout sentenceLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_word_fill, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();

        trackerHelper.wordFillLoad();


        boxWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_box_width));
        boxHeight = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_box_height)) + deviceInfo.getDimensionPixel(4) * 2;

        initWordFill();

        if (data == null) {
            return;
        }

        ViewObservableHelper.globalLayoutFrom(_LlQuestion).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
                        if (isTablet) {
                            buildDragViewTablet();
                        } else {
                            buildDragView();
                        }
                    }
                }
        );
        loadData();
    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    private FlowLayout newSentenceLayout() {
        FlowLayout flowLayout = new FlowLayout(getActivity());
        flowLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return flowLayout;
    }

    private void showHelp(String help) {
        if (TextUtils.isEmpty(help)) {
            busDriver.notifyBlinkCompleted();
        } else {
            busDriver.showHelp(help);
        }
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isComplete || isTimeOut) {
                    busDriver.notifyBlinkCompleted();
                } else if (isHelp) {
                    isHelp = false;
                }
                break;

            case TimeOut: {
                /* time out */
                timedOut();
                break;
            }
        }
    }

    private void initWordFill() {
        correctCount = incorrectCount = 0;
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json.wordfill;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.word_fill_help);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void loadData() {

        itemCount = 0;

            /* load prompt */
        if (data != null && data.prompt != null) {
            String prompt = Helper.getStringWithReplacements(data.prompt);
            lblPrompt.setText(prompt);
            Util.setTypeface(lblPrompt, getString(R.string.font_myriad_pro_regular));
            lblPrompt.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        }


        String str1 = Helper.getStringWithReplacements(data.text);
        str1 = str1.replace("  ", "\n");
        Timber.d("Sentences: %s", str1);

        final List<String> sentences = Splitter.on("\n").omitEmptyStrings().splitToList(str1);
        if (sentences != null && sentences.size() > 0) {
            dropCount = 0;
            for (int i = 0; i < sentences.size(); i++) {
                final String sentence = sentences.get(i).trim();
                Timber.d("Sentence %d: %s", i, sentence);
                addSentenceToUi(sentence);
            }
        }

    }

    private void addSentenceToUi(String sentence) {
        final List<String> tokens = Splitter.on("##").omitEmptyStrings().splitToList(sentence);
        if (tokens != null && tokens.size() > 0) {
            sentenceLayout = newSentenceLayout();
            flSentences.addView(sentenceLayout);

            for (int i = 0; i < tokens.size(); i++) {
                String token = tokens.get(i);
                if (TextUtils.isDigitsOnly(token)) {
                    final View spaceView = newSpaceView(Integer.parseInt(token));
                    sentenceLayout.addView(spaceView);
                    dropCount++;
                } else {
                    addSentence(token);
                }
            }
        }

    }

    private void addSentence(String token) {
        if (TextUtils.isEmpty(token)) {
            return;
        }

        if (token.startsWith(" ")) {
            addWord(" ");
        }

        final List<String> words = Splitter.on(" ").omitEmptyStrings().splitToList(token);
        if (words != null && words.size() > 0) {
            final int size = words.size();
            for (int i = 0; i < size; i++) {
                final String word = words.get(i) + " ";
                addWord(word);
            }
        }
        if (token.length() > 1 && token.endsWith(" ")) {
            addWord(" ");
        }
    }

    private void addWord(String word) {
        final TextView textView = newTextView();
        textView.setTextIsSelectable(false);
        textView.setText(word);
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        sentenceLayout.addView(textView);
    }

    private void buildDragView() {
//        LinearLayout row = newRow();
//        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) row.getLayoutParams();
//        lp.topMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_top_margin));
//        lp.bottomMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));

        final List<String> words = data.words;
        final int size = words.size();
        for (int i = 0; i < size; i++) {
//            if (i % 3 == 0) {
//                if (i > 0) {
////                    row = newRow();
//                    lp = (LinearLayout.LayoutParams) row.getLayoutParams();
//                    lp.bottomMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));
//                }
//                _LlQuestion.addView(row);
//            }
            final View dragView = newDragView(words.get(i));
            if (i == size - 1) {
                ((LinearLayout.LayoutParams) dragView.getLayoutParams()).rightMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));
            }
            _LlDrags.addView(dragView);
        }

    }

    private void buildDragViewTablet() {
        LinearLayout row = newRow();
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) row.getLayoutParams();
        lp.topMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_top_margin));
        lp.bottomMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));

        final List<String> words = data.words;
        final int size = words.size();
        for (int i = 0; i < size; i++) {
            if (i % 3 == 0) {
                if (i > 0) {
                    row = newRow();
                    lp = (LinearLayout.LayoutParams) row.getLayoutParams();
                    lp.bottomMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));
                }
                _LlQuestion.addView(row);
            }
            final View dragView = newDragView(words.get(i));
            if (i == size - 1) {
                ((LinearLayout.LayoutParams) dragView.getLayoutParams()).rightMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));
            }
            row.addView(dragView);
        }

        ViewObservableHelper.globalLayoutFrom(_LlQuestion).subscribe(
                new Action1<View>() {
                    @Override public void call(View view) {
                        final int scrollerHeight = scroller.getHeight();
                        final int viewHeight = view.getHeight();
                        Timber.d("scrollerHeight: %d, viewHeight: %d", scrollerHeight, viewHeight);
                        if (viewHeight > scrollerHeight) {
                            scrollDownUp(viewHeight);
                        }
                    }
                }
        );
    }

    @Override public void onStop() {
        super.onStop();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void scrollDownUp(final int viewHeight) {
        mHandler.postDelayed(
                new Runnable() {
                    @Override public void run() {
                        scroller.smoothScrollTo(0, viewHeight);
                        mHandler.postDelayed(
                                new Runnable() {
                                    @Override public void run() {
                                        scroller.smoothScrollTo(0, 0);
                                    }
                                }, 1400
                        );
                    }
                }, 700
        );
    }
//
//    @OnClick(R.id.btScrollLeft)
//    public void scrollLeft() {
//        _HsvScroller.smoothScrollBy(-boxWidth, 0);
//    }
//
//    @OnClick(R.id.btScrollRight)
//    public void set_BtScrollRight() {
//        _HsvScroller.smoothScrollBy(boxWidth, 0);
//    }

    public int getTextWidth(String text, Paint paint) {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int width = bounds.left + bounds.width();
        return width;
    }

    View newSpaceView(int index) {
        Timber.d("newSpaceView => index: %d", index);
        final FrameLayout view = new FrameLayout(getActivity());
        final FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER_VERTICAL;
        view.setLayoutParams(lp);
        view.setPadding(0, 4, 0, 4);

        final ImageView imageView = new ImageView(getActivity());
        final FrameLayout.LayoutParams imageLp = new FrameLayout.LayoutParams(boxWidth, boxHeight);
        imageView.setBackgroundResource(R.drawable.empty_btn);
        view.addView(imageView, imageLp);

        view.setTag(index);
        view.setOnDragListener(
                new View.OnDragListener() {
                    public boolean onDrag(View v, DragEvent event) {
                        final int action = event.getAction();
                        switch (action) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                enteredView = null;
                                return true;
                            case DragEvent.ACTION_DRAG_ENDED:
                                Timber.d("ACTION_DRAG_ENDED %s", enteredView != null);
                                if (enteredView != null && enteredView == v) {
                                    postMainThread(
                                            new Runnable() {
                                                @Override
                                                public void run() {
                                                    final Integer index = (Integer) enteredView.getTag();
                                                    final int indexOfWord = data.words.indexOf(selectedWord) + 1;
                                                    Timber.d("Box's index: %d, indexOfWord: %d, selectedWord:%s", index, indexOfWord, selectedWord);
                                                    final boolean result = index == indexOfWord;
                                                    if (result) {
                                                        final View dropView = newDragView(((TextView) selectedView).getText().toString());
                                                        view.addView(dropView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, boxHeight));
                                                        selectedView.setVisibility(View.INVISIBLE);
                                                    }
                                                    submitAnswer(result);
                                                }
                                            }
                                    );
                                } else {
                                    selectedView.setVisibility(View.VISIBLE);
                                }
                                return true;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                Timber.d("ACTION_DRAG_ENTERED");
                                enteredView = v;
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                enteredView = null;
                                Timber.d("ACTION_DRAG_EXITED");
                                break;
                        }
                        return false;
                    }
                }
        );
        return view;
    }

    private void submitAnswer(boolean correct) {
        Timber.d("SubmitAnswer: %s", correct);
        if (correct) {
            correctCount++;
            playRawSound(R.raw.correct);
        } else {
            incorrectCount++;
            playRawSound(R.raw.incorrect);
            if (incorrectCount >= 3) blinkComplete();
        }
        if (correctCount == dropCount) blinkComplete();
    }

    private View newDragView(final String word) {
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, boxHeight);
        ((LinearLayout.LayoutParams) params).leftMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.word_fill_bottom_margin));
        FrameLayout frameLayout = new FrameLayout(getActivity());
        frameLayout.setLayoutParams(params);
        CompatibleHelper.setBackgroundDrawable(frameLayout, uiHelper.createRoundedDrawable(Color.LTGRAY, 6));

        params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView textView = new TextView(getActivity());
        textView.setMinWidth(boxWidth);
        int padding = getResources().getDimensionPixelSize(R.dimen.base_padding);
        textView.setPadding(padding, 0, padding, 0);
        frameLayout.addView(textView);
        textView.setLayoutParams(params);
        CompatibleHelper.setBackgroundDrawable(textView, uiHelper.createRoundedDrawable(Color.parseColor("#33CCCC"), 6));
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        textView.setText(word);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.WHITE);
        ViewObservableHelper.longClick(textView).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        selectedWord = word;
                        view.setVisibility(View.INVISIBLE);
                        selectedView = view;
                        view.startDrag(null, new View.DragShadowBuilder(view), null, 0);
                    }
                }
        );
        return frameLayout;
    }

    public TextView newTextView() {
        TextView textView = new TextView(getActivity());
        final FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER_VERTICAL;
        textView.setLayoutParams(lp);
        textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        return textView;
    }

    public LinearLayout newRow() {
        Timber.d("Request for a new row");
        LinearLayout row = new LinearLayout(getActivity());
        row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setOrientation(LinearLayout.HORIZONTAL);
        return row;
    }

    private void blinkComplete() {
        isComplete = true;
        busDriver.pauseTimer();
        int score = json.score;
        int gems = json.gems;
        int user_score;
        for (int i = 0; i < drags.size(); i++) {
            TextView lbl = drags.get(i);
            lbl.setEnabled(false);
        }
        if (incorrectCount <= 1) {
            user_score = (incorrectCount == 0) ? score : (int) (score * 0.75);
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.correct);

            String str = json.feedback[0];
            if (str.length() > 0) {
                showHelp(str);
            }
        } else {
            user_score = (incorrectCount == 2) ? score / 2 : 0;
            if (incorrectCount > 3) user_score = 0;
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.incorrect);
            String str = json.feedback[1];
            if (str.length() > 0) {
                showHelp(str);
            }
        }
    }


    private void timedOut() {

        for (int i = 0; i < drags.size(); i++) {
            TextView lbl = drags.get(i);
            lbl.setEnabled(false);
        }

        isTimeOut = true;
        showHelp("Sorry, you're out of time.");
    }
}
