package com.blinktrainingsystem.blink.module.playcourse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.google.common.base.Objects;
import com.squareup.picasso.Picasso;

import java.io.File;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by TALE on 10/23/2014.
 */
public class LiteProgressFragment extends BaseBurstFragment {

    @InjectView(R.id.tvCongratulation)
    TextView lblCongrats;
    @InjectView(R.id.tvCompleteCourse)
    TextView lblCompleteCourse;
    @InjectView(R.id.tvCourseTitle)
    TextView lblCourseTitle;
    @InjectView(R.id.ivIcon)
    ImageView imvCourseIcon;
    @InjectView(R.id.tvCourseDescription)
    TextView lblCourseDescription;
    @InjectView(R.id.btnBuy)
    Button btnBuy;

    @Inject TrackerHelper trackerHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lite_progress, container, false);
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.progressLoadLite();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        trackerHelper.courseCompletedLite(localSharedPreferences.userId().getOr(0l), course.id);
        String myFont = getString(R.string.font_myriad_pro_regular);
        int myFontSize = 22;
        String descriptFont = myFont;
        int descriptFontSize = 18;
        Util.setTypeface(lblCongrats, myFont);
        lblCongrats.setTextSize(TypedValue.COMPLEX_UNIT_SP, myFontSize);
        Util.setTypeface(lblCompleteCourse, myFont);
        lblCompleteCourse.setTextSize(TypedValue.COMPLEX_UNIT_SP, myFontSize);
        Util.setTypeface(lblCourseTitle, myFont);
        lblCourseTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, myFontSize);
        Util.setTypeface(lblCourseDescription, descriptFont);
        lblCourseDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, descriptFontSize);


        lblCourseTitle.setText(course.title);

        boolean lite = Objects.equal(course.folderName, LocalFileManager.Folder.lite.toString());
        long courseId = course.id;
        File imageFile;
        if (lite) {
            imageFile = new File(localFileManager.getCourseFile(courseId, LocalFileManager.Folder.lite), "icon.jpg");
        } else {
            imageFile = new File(localFileManager.getCourseFile(courseId, LocalFileManager.Folder.full), "icon.jpg");
        }

        loadImageIcon(imageFile);
        lblCourseDescription.setText(course.summary);
        busDriver.setFooterMenu(null, null, null);
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Inject
    Picasso picasso;

    public void loadImageIcon(File imageFile) {
        picasso.load(imageFile).into(imvCourseIcon);

    }

    @OnClick(R.id.btnBuy)
    public void openCourseDetail() {
        bus.post(Event.ViewCourseDetail);
    }
}
