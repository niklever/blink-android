package com.blinktrainingsystem.blink.module.playcourse.handler;

import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Input;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;

/**
 * Created by TALE on 9/23/2014.
 */
public class InputHandler {

    private final Blink blink;
    private final Input input;
    private JsonEx jsonObject;

    public InputHandler(Blink blink) throws IllegalArgumentException {
        if (blink.type != BlinkType.KBlinkInput) {
            throw new IllegalArgumentException("Wrong type, this expect a KBlinkDragAndDrop type");
        }
        this.blink = blink;
        jsonObject = blink.loadJsonToObject();
        if (jsonObject != null) {
            input = jsonObject.input;
        } else {
            input = null;
        }

    }

    public Input getData() {
        return input;
    }

    public JsonEx getJsonObject() {
        return jsonObject;
    }

    public String getFeedback() {
        return jsonObject == null || jsonObject.feedback == null || jsonObject.feedback.length == 0 ? "" : jsonObject.feedback[0];
    }

    public String getPrompt() {
        return input == null ? "" : input.prompt;
    }
}
