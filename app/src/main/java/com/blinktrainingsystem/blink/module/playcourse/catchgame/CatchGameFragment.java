package com.blinktrainingsystem.blink.module.playcourse.catchgame;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.CatchView;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Catch;
import com.blinktrainingsystem.blink.data.model.pojo.CatchItem;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ImageLoader;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.InjectView;
import hugo.weaving.DebugLog;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class CatchGameFragment extends BaseBurstFragment {

    public final Random random = new Random();
    @InjectView(R.id.tvTitle)
    TextView _TvTitle;
    @InjectView(R.id.contentView)
    FrameLayout _ContentView;

    @Inject TrackerHelper trackerHelper;

    int incorrectCount;
    JsonEx json;
    Catch data;
    List<CatchItem> items;
    ArrayList<Integer> itemIndices;
    ArrayList<CatchView> btns = new ArrayList<CatchView>();
    ArrayList<JSONObject> slotList;

    boolean timed;
    double speed;
    double frequency;
    boolean ordered;
    int score;
    int points;
    boolean gameOver;
    long btnCreateTime;
    boolean isTimeOut;
    boolean isComplete;

    @InjectView(R.id.tvTitle)
    TextView lblPrompt;

    @InjectView(R.id.contentView)
    FrameLayout contentView;

    @InjectView(R.id.bottomContent)
    LinearLayout bottomContent;

    @Inject
    LocalSharedPreferences localSharedPreferences;
    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            render();
        }
    };
    @Inject
    Picasso picasso;
    public int timer;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_catch_game, container, false);
    }

    boolean isPaused;

    @Override
    public void onResume() {
        super.onResume();

        trackerHelper.catchGameLoad();

        if (!isHelp && isPaused && !isComplete && !gameOver) {
            isPaused = false;
            render();
        }

    }

    @Inject ImageLoader imageLoader;

    private int catchViewWidth;
    private int catchViewHeight;

    @Override
    protected void startBurst() {
        super.startBurst();
        initCatchGame();
        loadData();
        if (!isHelp) {
            startRender();
        }

    }

    private void startRender() {
        /* start timer to play update */
        final Observable<int[]> size = imageLoader.size(getResources(), R.drawable.catch1);
        final Observable<View> globalLayoutFrom = ViewObservableHelper.globalLayoutFrom(contentView);
        Observable.zip(
                size, globalLayoutFrom, new Func2<int[], View, int[]>() {
                    @Override public int[] call(int[] size, View view) {
                        return size;
                    }
                }
        ).subscribe(
                new Action1<int[]>() {

                    @Override
                    public void call(int[] size) {
                        catchViewWidth = size[0];
                        catchViewHeight = size[1];
                        render();
                    }
                }
        );

    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.catch_help);
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @Override
    protected int getLeftMargin() {
        return 0;
    }

    private void render() {
        Timber.d("Render");
        if (items == null) {
            return;
        }
        createBtn();
        mHandler.postDelayed(updateTask, (long) frequency);
    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
    }

    public void pause() {
        cleanAnim();
        isPaused = true;
        busDriver.pauseTimer();
        for (CatchView catchView : btns) {
            contentView.removeView(catchView);
        }
        btns.clear();
    }

    private void cleanAnim() {
        mHandler.removeCallbacks(updateTask);
        for (CatchView catchView : btns) {
            if (catchView != null && catchView.anim != null) {
                catchView.anim.cancel();
            }

        }
    }

    @Override
    protected String getTitle() {
        return "";
    }

    private void initCatchGame() {
        incorrectCount = 0;
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json._catch;
        items = data == null ? null : data.items;

        if (items == null || items.size() == 0) {
            return;
        }

        int i = data.speed;
        speed = (double) i / 2.0;
        i = data.frequency;
        frequency = (double) (11 - i) / 4.0 * 1000;
        ordered = data.ordered;
        score = json.score;
        points = score;

        respawnIndices();
    }

    private void respawnIndices() {
        itemIndices = new ArrayList<Integer>();
        for (int i = 0; i < items.size(); i++) {
            itemIndices.add(i);
        }
    }

    @Inject
    DeviceInfo deviceInfo;

    private void loadData() {
        if (items == null) {
            return;
        }
        // Do any additional setup after loading the view from its nib.
        try {
            if (TextUtils.isEmpty(data.prompt)) {
                lblPrompt.setVisibility(View.INVISIBLE);
            } else {
                lblPrompt.setText(Helper.getStringWithReplacements(data.prompt));
                playSound(data.sound, false);
            }

            if (ordered) {
                //UIView *slots_vw = [[UIView alloc] initWithFrame:CGRectMake(0, 768, 1024, 200)];
                //[self.view addSubview:slots_vw];
                int slots = 0;
                for (CatchItem item : items) {
                    int slot = item.slot;
                    if (slot > slots) slots = slot;
                }

                int slotSize = getResources().getInteger(R.integer.wh_slotsize);

                slotList = new ArrayList<JSONObject>();
                for (int i = 0; i < slots; i++) {
                    final View img_vw = newSlotView(slotSize);
                    bottomContent.addView(img_vw);


                    JSONObject dict = new JSONObject();
                    dict.put("slot", img_vw);
                    dict.put("occupied", false);
                    slotList.add(dict);
                }
//                Collections.shuffle(slotList);
            }

            gameOver = false;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private View newSlotView(int slotSize) {

        ImageView img_vw = new ImageView(getActivity());
        img_vw.setImageResource(R.drawable.slot);

        FrameLayout slotView = new FrameLayout(getActivity());
        slotView.setLayoutParams(new ViewGroup.LayoutParams(slotSize, slotSize));

        slotView.addView(img_vw, new ActionBar.LayoutParams(slotSize, slotSize));

        return slotView;
    }

    private void showHelp(String help) {
        bus.post(Event.ShowHelp.setExtra(help));
    }

    private void createBtn() {
        btnCreateTime = System.currentTimeMillis();

        int idx = (int) (random.nextInt(itemIndices.size()));
        int index = itemIndices.get(idx);
        itemIndices.remove(idx);
        if (itemIndices.size() == 0) {
            respawnIndices();
        }

        CatchItem item = items.get(index);
        boolean isImage = item.isImage;
        CatchView vw;
        boolean correct = item.correct;

        int slot = item.slot;
        int bonus = item.bonus;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, catchViewHeight);
        layoutParams.gravity = Gravity.CENTER;

        if (isImage) {
            vw = new CatchView(getActivity(), picasso, contentView.getWidth(), contentView.getHeight(), getLocalFile(item.image), correct, speed, slot, bonus);
        } else {
            vw = new CatchView(getActivity(), picasso, contentView.getWidth(), contentView.getHeight(), item.text, correct, speed, slot, bonus);
        }
        vw.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        contentView.addView(vw, 0, layoutParams);
        vw.setEventBus(bus);
        vw.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        catchViewPressed((CatchView) v);
                    }
                }
        );
        vw.start();
        btns.add(vw);
        Timber.d("View added");
        playRawSound(R.raw.gentleflyby);
    }

    public void onEvent(CatchView catchView) {
        contentView.removeView(catchView);
    }


    public void catchViewPressed(CatchView catchView) {
        try {
            catchView.anim.cancel();
            btns.remove(catchView);
            contentView.removeView(catchView);

            boolean remove = true;
            switch (catchView.bonus) {
                case 1://Catch Point
                    points += (int) (score / 10.0);
                    break;
                case 2://Catch Time
                    busDriver.addTimer(10);
                    break;
            }

            if (catchView.correct) {
                Timber.d("catchView.slot=%d", catchView.slot);
                if (catchView.slot > 0) {
                    JSONObject dict = slotList.get(catchView.slot - 1);
                    boolean occupied = dict.getBoolean("occupied");
                    if (!occupied) {
                        playRawSound(R.raw.correct);
                        FrameLayout slot = (FrameLayout) dict.get("slot");
                        dict.put("occupied", true);

                        final int width = slot.getWidth();
                        final int catchViewWidth = catchView.getWidth();
                        float scale = (float) width / catchViewWidth;
                        Timber.d("width: %d, catchViewWidth: %d, scale: %f", width, catchViewWidth, scale);
                        catchView.setScaleX(scale);
                        catchView.setScaleY(scale);
                        catchView.inSlot = true;
                        catchView.setTranslationX(0);
                        catchView.setTranslationY(0);

                        slot.addView(catchView);
                        ((FrameLayout.LayoutParams) catchView.getLayoutParams()).gravity = Gravity.CENTER;
                        remove = false;
                    }
                }
                playRawSound(R.raw.correct);
            } else {
                playRawSound(R.raw.incorrect);
                incorrectCount++;
                if (incorrectCount >= 3) {
                    blinkComplete();
                }
            }

            if (ordered && !remove) {
                int count = 0;
                for (JSONObject dict : slotList) {
                    boolean occupied = dict.getBoolean("occupied");
                    if (occupied) count++;
                }
                if (count == slotList.size()) {
                    postMainThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    blinkComplete();
                                }
                            }, 300
                    );
                }
            }

            if (remove) {
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    int catchedCount = 0;

    private void timeOut() {
        cleanAnim();
        isTimeOut = true;
        int score = json.score;
        int user_score = points - score;
        int gems = json.gems;
        int user_gems = (user_score > score) ? gems : 0;
//        if (user_score != 0) {
//            if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
//                playRawSound(R.raw.points_lost);
//            } else {
//                busDriver.updateScore(blinkHandler);
//            }
//        }

        if (btns != null) {
            for (CatchView vw : btns) {
                vw.setEnabled(false);
                if (!vw.inSlot) {
                    contentView.removeView(vw);
                }
            }
        }

        if (ordered) {
            showHelp(getString(R.string.time_out_message));
        } else {
            showHelp("");
        }

        gameOver = true;
    }

    private void blinkComplete() {
        pause();
        isComplete = true;
        gameOver = true;
        bus.post(Event.PauseTimer);
        int score = json.score;
        int gems = json.gems;

        if (btns != null) {
            for (CatchView vw : btns) {
                vw.setEnabled(false);
                if (!vw.inSlot) {
                    contentView.removeView(vw);
                }
            }
            //[btns removeAllObjects];
        }

        int user_score = (int) ((double) ((4.0 - (double) incorrectCount) / 4.0) * (double) points);
        if (user_score < 0) user_score = 0;

        if (incorrectCount <= 1) {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.correct);
            showHelp(json.feedback[0]);
        } else {
            playRawSound(R.raw.incorrect);
            showHelp(json.feedback[1]);
        }
    }


    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                    startRender();
                } else if (isTimeOut) {
                    bus.post(Event.BlinkCompleted);
                } else if (isComplete) {
                    bus.post(Event.BlinkCompleted);
                }
                break;

            case TimeOut:
                timeOut();
                break;

            case DoneClick:
                blinkComplete();
                break;
        }
    }
}
