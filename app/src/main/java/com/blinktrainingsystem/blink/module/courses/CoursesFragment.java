package com.blinktrainingsystem.blink.module.courses;

import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.adapter.CourseAdapter;
import com.blinktrainingsystem.blink.common.dialog.ConfirmDialogFragment;
import com.blinktrainingsystem.blink.common.fragment.StructureFragment;
import com.blinktrainingsystem.blink.common.view.CourseGridItemView;
import com.blinktrainingsystem.blink.common.view.RadioGroupController;
import com.blinktrainingsystem.blink.common.view.TextRadioButton;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.google.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Created by TALE on 9/10/2014.
 */
public class CoursesFragment extends StructureFragment implements CoursesView {

    @InjectView(R.id.btBack)
    TextView btBack;
    @InjectView(R.id.trbMyCourses)
    TextRadioButton trbMyCourses;
    @InjectView(R.id.trbAllCourses)
    TextRadioButton trbAllCourses;
    @InjectView(R.id.trbUnits)
    TextRadioButton trbUnits;
    @InjectView(R.id.gvCourses)
    GridView gvCourses;
    @InjectView(R.id.tv_version)
    TextView txtVersion;

    @Inject
    Application application;
    @Inject
    CourseAdapter courseAdapter;

    @Inject
    EventBus bus;

    @Inject
    CoursesPresenter presenter;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    RadioGroupController radioGroupController = new RadioGroupController();
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Course selectedCourse;

    @Override
    protected int getMenuLayoutId() {
        return 0;
    }

    @Override
    protected int getHeaderLayoutId() {
        return R.layout.header_back_title;
    }

    @Override
    protected int getFooterLayoutId() {
        return R.layout.footer_copy_right_tab;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_courses;
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        modules.add(new CoursesModule(this));
        return modules;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configureHeader();
        configureFooter();
        configureContent();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bus != null) {
            bus.register(this);
        }
        configureHeader();
        onEvent(Event.CourseRefresh);
        if (Static.openMyCourseOnBack) {
            presenter.viewMyCourse();
            Static.openMyCourseOnBack = false;
        } else {
            radioGroupController.setSelection(1);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
        presenter.cancelLoading();
    }

    public void onEvent(Event event) {
        Timber.d("onEvent: " + event);
        switch (event) {
            case CourseUpdate:
                mHandler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                presenter.updateCourses();
                            }
                        }
                );
                break;
            case CourseRefresh:
                mHandler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                presenter.refreshCourses();
                            }
                        }
                );
                break;
        }
    }

    private void configureContent() {
        setHasOptionsMenu(true);
        gvCourses.setAdapter(courseAdapter);
        gvCourses.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Course course = (Course) parent.getItemAtPosition(position);
                        if (radioGroupController.getCheckedRadioButtonId() == R.id.trbMyCourses) {
                            if (course.cloud){
                                WorkerService.intent(application).downloadCourse(course.id, (course.lite) ? LocalFileManager.Folder.lite : LocalFileManager.Folder.full).start();
                                presenter.refreshCourses();
                            }else {
                                presenter.playCourse(course);
                            }
                        } else {
                            presenter.openCourse(course);
                        }
                    }
                }
        );
        gvCourses.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedCourse = (Course) parent.getItemAtPosition(position);
                        return false;
                    }
                }
        );
        registerForContextMenu(gvCourses);
    }

    @Override
    public void onCreateContextMenu(
            ContextMenu menu, View v,
            ContextMenu.ContextMenuInfo menuInfo
    ) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (radioGroupController.getCheckedRadioButtonId() == R.id.trbAllCourses) {
            return;
        }
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (selectedCourse.category == null || selectedCourse.folderName.equals(LocalFileManager.Folder.downloading.toString())) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.action_reset:
                presenter.resetCourse(selectedCourse);
                return true;
            case R.id.action_delete:
                presenter.removeCourse(selectedCourse);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private String getVersionName(){
        String versionName = "";
        Context context = getContext();
        final PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
                versionName = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                Timber.d("Problem getting versionName");
            }
        }
        return versionName;
    }
    private void configureFooter() {
        trbMyCourses.setCheckedBg(R.drawable.radio_left_checked);
        trbMyCourses.setUnCheckedBg(R.drawable.radio_left_unchecked);
        if (Static.admin) {
            trbUnits.setVisibility(View.VISIBLE);
            trbUnits.setCheckedBg(R.drawable.radio_right_checked);
            trbUnits.setUnCheckedBg(R.drawable.radio_right_unchecked);
            trbAllCourses.setCheckedBg(R.drawable.radio_mid_checked);
            trbAllCourses.setUnCheckedBg(R.drawable.radio_mid_unchecked);
            radioGroupController.setRadioButtons(trbMyCourses, trbAllCourses, trbUnits);
        } else {
            trbAllCourses.setCheckedBg(R.drawable.radio_right_checked);
            trbAllCourses.setUnCheckedBg(R.drawable.radio_right_unchecked);
            radioGroupController.setRadioButtons(trbMyCourses, trbAllCourses);
        }

        txtVersion.setText(getVersionName());

        radioGroupController.setOnCheckedChangeListener(
                new RadioGroupController.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(int checkedId, int position) {
                        switch (checkedId) {
                            case R.id.trbMyCourses:
                                if (presenter != null) {
                                    presenter.viewMyCourse();
                                }

                                break;
                            case R.id.trbAllCourses:
                                if (presenter != null) {
                                    presenter.viewAllCourse();
                                }

                                break;
                            case R.id.trbUnits:
                                if (presenter != null) {
                                    presenter.viewUnits();
                                }

                                break;
                        }
                    }
                }
        );
    }

    private void configureHeader() {
        String usr = localSharedPreferences.usr().getOr(null);
        if (usr.equals("guest")){
            btBack.setText(R.string.txt_register);
        }else {
            btBack.setText(R.string.txt_logout);
        }
    }

    @OnClick(R.id.btBack)
    public void logout() {
        presenter.logout();
    }

    @Override
    public void showProgress() {
        showProgress("Loading...");
    }

    @Override
    public void hideProgress() {
        dismissProgress();
    }

    @Override
    public void showError(String error) {
        showAlert(getString(R.string.error), error);
    }

    @Override
    public void showCourses(List<Course> courses) {
        courseAdapter.changeDataSet(courses);
    }

    @Override
    public void showConfirmLogoutDialog(ConfirmDialogFragment.ConfirmDialogListener listener) {
        new ConfirmDialogFragment.Builder()
                .setMessage(getString(R.string.logout_confirm_message))
                .setPositiveText(getString(R.string.logout_confirm_positive))
                .setNegativeText(getString(R.string.txt_cancel))
                .setListener(listener)
                .show(getFragmentManager(), "confirm");
    }
}
