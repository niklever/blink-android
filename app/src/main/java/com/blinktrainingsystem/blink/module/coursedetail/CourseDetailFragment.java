package com.blinktrainingsystem.blink.module.coursedetail;

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.dialog.BuyCourseConfirmationDialog;
import com.blinktrainingsystem.blink.common.dialog.PurchasePackagesDialog;
import com.blinktrainingsystem.blink.common.fragment.StructureFragment;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.parser.PurchaseParser;
import com.blinktrainingsystem.blink.data.model.parser.SpentCreditParser;
import com.blinktrainingsystem.blink.data.model.parser.UpdatePurchaseParser;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.model.pojo.PurchaseItem;
import com.blinktrainingsystem.blink.data.model.pojo.PurchaseTransaction;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.net.WebServicesWrapper;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.blinktrainingsystem.blink.iap.util.IabHelper;
import com.blinktrainingsystem.blink.iap.util.IabResult;
import com.blinktrainingsystem.blink.iap.util.Inventory;
import com.blinktrainingsystem.blink.iap.util.Purchase;
import com.blinktrainingsystem.blink.module.main.MainActivity;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.module.playcourse.PlayCourseActivity;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.ImageLoader;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.google.common.collect.Lists;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by TALE on 9/12/2014.
 */
public class CourseDetailFragment extends StructureFragment implements CourseDetailView {

    private static final String KEY_COURSE = "course";
    private static final int PURCHASE_REQUEST_CODE = 1001;
    @InjectView(R.id.ivIcon)
    ImageView ivIcon;
    @Optional
    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.tvCourseTitle)
    TextView tvCourseTitle;
    @InjectView(R.id.tvSummary)
    TextView tvSummary;
    @InjectView(R.id.ivImage)
    ImageView ivImage;
    @InjectView(R.id.tvCredit)
    TextView tvCredit;
    @InjectView(R.id.btTrySample)
    Button btTrySample;
    @InjectView(R.id.btBuyCredit)
    Button btBuyCredit;
    @InjectView(R.id.btGetCredit)
    Button btGetCredit;
    @InjectView(R.id.tvDescription)
    TextView tvDescription;

    @Optional
    @InjectView(R.id.btBack)
    TextView btBack;

    @Inject
    LocalSharedPreferences localSharedPreferences;
    @Inject
    Picasso picasso;

    @Inject
    Application application;
    @InjectView(R.id.btDownload)
    Button _BtDownload;

    @Inject
    ImageLoader imageLoader;

    @Inject DeviceInfo deviceInfo;

    @Inject TrackerHelper trackerHelper;

    private Course mCourse;
    private Subscription subscription;

    private IabHelper mHelper;
    private PurchasePackagesDialog purchasePackagesDialog;

    @Override
    protected int getMenuLayoutId() {
        return 0;
    }

    @Override
    protected int getHeaderLayoutId() {
        if ("lite".equals(mCourse.folderName)) {
            return 0;
        }
        return R.layout.header_back_title;
    }

    @Override
    protected int getFooterLayoutId() {
        if ("lite".equals(mCourse.folderName)) {
            return 0;
        }
        return R.layout.footer_none;
    }

    @Override
    protected int getFragmentLayoutId() {
        return R.layout.fragment_course_detail;
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        modules.add(new CourseDetailModule(this));
        return modules;
    }

    @Optional
    @OnClick(R.id.btBack)
    public void back() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.btTrySample)
    public void trySample() {
        if (mCourse == null) {
            return;
        }
        trySample(mCourse.id);
    }

    @OnClick(R.id.btBuyCredit)
    public void buyForCredit() {
        if (mCourse == null) {
            return;
        }
        trackerHelper.buyCourseByCredits(localSharedPreferences.userId().getOr(0l), mCourse.id);
        buyForCredit(mCourse.title, mCourse.id, mCourse.credits);
    }

    @OnClick(R.id.btGetCredit)
    public void onGetCreditsClick() {
        if (mCourse == null) {
            return;
        }

        trackerHelper.getCredits(localSharedPreferences.userId().getOr(0l), mCourse.id);

        final Long userId = localSharedPreferences.userId().getOr(0l);
        if (userId != 0l && deviceInfo.isNetworkConnected()) {
            showProgress();
            AndroidObservable.bindFragment(this, ((WebServicesWrapper) webServices).getIapPackagesObservable(userId))
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<PurchaseParser>() {
                                @Override public void onCompleted() {
                                    hideProgress();
                                }

                                @Override public void onError(Throwable e) {
                                    showServerError();
                                }

                                @Override public void onNext(PurchaseParser purchaseParser) {
                                    if (purchaseParser != null && purchaseParser.success && purchaseParser.packages != null && purchaseParser.packages.size() > 0) {
                                        showPurchasePackagesPopup(purchaseParser.packages);
                                    } else {
                                        showServerError();
                                    }
                                }
                            }
                    );

        }

    }

    private void showServerError() {
        // TODO: show server error.
    }

    @Override public void onStop() {
        super.onStop();
        if (purchasePackagesDialog != null) {
            purchasePackagesDialog.setOnItemSelectedListener(null);
        }
    }

    @Override public void onStart() {
        super.onStart();
        if (purchasePackagesDialog != null) {
            purchasePackagesDialog.setOnItemSelectedListener(onPurchaseItemSelectedListener);
        }
    }

    private PurchasePackagesDialog.OnItemSelectedListener onPurchaseItemSelectedListener = new PurchasePackagesDialog.OnItemSelectedListener() {
        @Override public void onItemSelected(final PurchaseItem purchaseItem) {
            Timber.d("Selected item => %d", purchaseItem.id);
            final String payload = UUID.randomUUID().toString();
            final Long userId = localSharedPreferences.userId().getOr(0l);
            localSharedPreferences.payload(userId, mCourse.id, purchaseItem.id).put(payload);
            trackerHelper.buyCreditsPackage(userId, mCourse.id, purchaseItem.packageId, purchaseItem.price, purchaseItem.credits);

            mHelper.launchPurchaseFlow(
                    getActivity(), purchaseItem.packageId, PURCHASE_REQUEST_CODE, new IabHelper.OnIabPurchaseFinishedListener() {
                        @Override public void onIabPurchaseFinished(IabResult result, Purchase info) {
                            if (result.isSuccess()) {
                                trackerHelper.buyCreditsPackageSuccess(userId, mCourse.id, purchaseItem.packageId, purchaseItem.price, purchaseItem.credits);
                                Timber.d("orderId: %s, payload: %s, packageName: %s", info.getToken(), info.getDeveloperPayload(), info.getPackageName());
                                updatePurchase(purchaseItem.packageId, info.getOrderId());
                            } else {
                                trackerHelper.buyCreditsPackageFailure(userId, mCourse.id, purchaseItem.packageId, purchaseItem.price, purchaseItem.credits, result.getMessage());
                                Timber.e("Error => purchase item with result: %s", result.getMessage());
                            }
                        }
                    }, payload
            );
        }
    };

    private void updatePurchase(final String packageId, final String orderId) {
        showProgress();
        AndroidObservable.bindFragment(this, ((WebServicesWrapper) webServices).updatePurchase(localSharedPreferences.userId().getOr(0l), new PurchaseTransaction(packageId, orderId)))
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        new Observer<UpdatePurchaseParser>() {
                            @Override public void onCompleted() {
                                showProgress();
                            }

                            @Override public void onError(Throwable e) {
                                Timber.d(e, "update purchase to server error");
                                localSharedPreferences.pendingPurchase(localSharedPreferences.userId().getOr(0l), mCourse.id).put(String.format("%s,%s", packageId, orderId));
                            }

                            @Override public void onNext(UpdatePurchaseParser purchaseParser) {
                                Timber.d("Update to server success=> %s, %s", purchaseParser.success, purchaseParser.message);
                                getCredits();
                            }
                        }
                );
    }

    private void showPurchasePackagesPopup(List<PurchaseItem> purchases_list) {
        Timber.d("showPurchasePackagesPopup => %d items", purchases_list == null ? 0 : purchases_list.size());
        purchasePackagesDialog = PurchasePackagesDialog.newInstance("Choose how many credits you want to buy then touch the button to buy them!", ((ArrayList) purchases_list));
        purchasePackagesDialog.setOnItemSelectedListener(onPurchaseItemSelectedListener);
        purchasePackagesDialog.show(getFragmentManager(), "PurchasePackageDialog");
    }

    @OnClick(R.id.btDownload)
    public void download() {
        if (mCourse == null) {
            return;
        }

        WorkerService.intent(application).downloadCourse(mCourse.id, LocalFileManager.Folder.full).start();
        Static.openMyCourseOnBack = true;
        postMainThread(
                new Runnable() {
                    @Override
                    public void run() {
                        backToMainActivity();
                    }
                }
        );

    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.d("onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Timber.d("onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mCourse = args == null ? null : (Course) args.getParcelable(KEY_COURSE);

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkJdOu9KsBEg6TjnMOa7gUcWVYmnDCfYAvhvIGZ3dwJUMqg8wWc7oJAjCN1QpZiQFZyjxuXGCiVO7dUuU3F/WAoPcYNB3oRujysPtL/V6t+ZYTY1U5GWCcgBr3wIgZDw8zvwG7KOF8r120Xwgo9zULj5K7d66QPpe7AUBIcPhDO89uelLTyQiONNTYvoZXZ7RsOrigwpUlFOJFiHYSpt+JGlu75Ncb1vj2du57MP3NmT6+v/rnCEkER/lb4RrLIUfk3TPrNz8/7RQS9r9FInqBx9ogD7yddPt67P2H5dSczZ1+yhWNo1LMEjx6wR2troxBQ24ljHHgcVlttNXKbOalwIDAQAB";

        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(getActivity(), base64EncodedPublicKey);
        mHelper.startSetup(
                new IabHelper.OnIabSetupFinishedListener() {
                    public void onIabSetupFinished(IabResult result) {
                        if (!result.isSuccess()) {
                            // Oh noes, there was a problem.
                            Timber.d("Problem setting up In-app Billing: " + result);
                        }
                        Timber.d("// Hooray, IAB is fully set up! -> " + result.getMessage());
                        // Hooray, IAB is fully set up!
                        queryItems();
                    }
                }
        );
    }

    private void queryItems() {
        List<String> skus = new ArrayList<>();
        skus.add("com.blinktrainingsystem.credits.package1");
        mHelper.queryInventoryAsync(
                true, skus,
                new IabHelper.QueryInventoryFinishedListener() {
                    @Override public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                        Timber.d("result: %s", result.getMessage());
                        Timber.d("inventory: %s", inv);
                        Timber.d("has purchase: %s", inv.hasPurchase("com.blinktrainingsystem.blink.item1"));

                        if (result.isSuccess()) {

                        }
                    }
                }
        );
    }

    @Override public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (btBack != null) {
            btBack.setText(R.string.txt_courses);
        }
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.courseDetailLoad();

        mHandler.postDelayed(
                new Runnable() {
                    @Override public void run() {
                        initUi();
                    }
                }, 500
        );
    }

    private void initUi() {
        if (mCourse != null) {
            if (tvTitle != null) {
                tvTitle.setText(mCourse.title);
            }
            tvCourseTitle.setText(mCourse.title);
            tvSummary.setText(mCourse.summary);
            tvDescription.setText(mCourse.description);
            btBuyCredit.setText(getString(R.string.buy_for_credits_pattern, mCourse.credits));

            _BtDownload.setVisibility(localSharedPreferences.admin().getOr(false) ? View.VISIBLE : View.INVISIBLE);

            final String iconUrl = String.format(WebServices.IMAGE_PATH, mCourse.iconURL);
            Timber.d("iconUrl: " + iconUrl);
            picasso.load(iconUrl).into(ivIcon);

            final String imageUrl = String.format(WebServices.IMAGE_PATH, mCourse.imageURL);
            Timber.d("iconUrl: " + imageUrl);
//            picasso.load(imageUrl);
            final Subscription subscription1 = imageLoader.load(imageUrl)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Observer<Bitmap>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Bitmap bitmap) {
                                    final int width = bitmap.getWidth();
                                    final int height = bitmap.getHeight();

                                    ivImage.setImageBitmap(bitmap);
                                    final int ivImageWidth = ivImage.getWidth();
                                    float scale = (float) ivImageWidth / width;
                                    Timber.d("Load bitmap: => scale: %f, bitmap's width: %d, imageView's width: %d", scale, width, ivImageWidth);
                                    ivImage.getLayoutParams().height = (int) (height * scale);
                                }
                            }
                    );
            takeCareSubscription(subscription1);

            getCredits();
        }
    }

    private void getCredits() {
        tvCredit.setText("");
        final Long userId = localSharedPreferences.userId().getOr(0l);
        if (userId != 0l) {
            showProgress();
            final Subscription getCreditSubscription = AndroidObservable.bindFragment(this, ((WebServicesWrapper) webServices).getCreditsObservable(userId))
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(
                            new Observer<Credit>() {
                                @Override public void onCompleted() {
                                    hideProgress();
                                }

                                @Override public void onError(Throwable e) {

                                }

                                @Override public void onNext(Credit credit) {
                                    if (credit.success) {
                                        tvCredit.setText(getString(R.string.credits_available_pattern, credit.credits));
                                        localSharedPreferences.credits().put(credit.credits);
                                        int creditsInt;
                                        try {
                                            creditsInt = Integer.parseInt(credit.credits);
                                            final int courseCredits = Integer.parseInt(mCourse.credits);
                                            if (creditsInt < courseCredits) {
                                                btBuyCredit.setEnabled(false);
                                            } else {
                                                btBuyCredit.setEnabled(true);
                                            }

                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                    );
            takeCareSubscription(getCreditSubscription);
        }
    }

    public static CourseDetailFragment newInstance(Course course) {
        final Bundle args = new Bundle();
        args.putParcelable(KEY_COURSE, course);
        final CourseDetailFragment fragment = new CourseDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (subscription != null) {
            subscription.unsubscribe();
        }

        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void showProgress() {
        showProgress("Processing...");
    }

    @Override
    public void hideProgress() {
        dismissProgress();
    }

    @Override
    public void showError() {
        super.showAlert(getString(R.string.error), null);
    }


    @Override
    public void showCourses(List<Course> courses) {

    }

    @Override
    public void showBuyCourseConfirmationDialog(String message, BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener confirmationButtonOnClickListener) {
        BuyCourseConfirmationDialog dialogFragment = (BuyCourseConfirmationDialog) getFragmentManager().findFragmentByTag("confirm");
        if (dialogFragment != null) {
            dialogFragment.dismiss();
        }
        dialogFragment = BuyCourseConfirmationDialog.newInstance(message, confirmationButtonOnClickListener);
        dialogFragment.show(getFragmentManager(), "confirm");
    }

    public void showRegister(){
        final FragmentActivity activity = getActivity();
        if (activity instanceof MainActivity) {
            //We're OK
        } else {
            final Intent intent = new Intent(activity, MainActivity.class);
            activity.startActivity(intent);
        }
        ((MainActivity) activity).showGuestRegister(mCourse.title, mCourse.id);
    }

    public void trySample(final long courseId) {
        long userId = localSharedPreferences.userId().getOr(0l);
        if (userId==0){
            //We're a guest, so show the register screen
            showRegister();
        }else {
            trackerHelper.trySample(userId, courseId);
            Static.openMyCourseOnBack = true;
            WorkerService.intent(application).downloadCourse(courseId, LocalFileManager.Folder.lite).start();
            if (getActivity() instanceof PlayCourseActivity) {
                PlayCourseActivity activity = (PlayCourseActivity) getActivity();
                activity.returnFromCourseDetail();
            } else {
                backToMainActivity();
            }
        }
    }

    private void backToMainActivity() {
        showProgress();
        postMainThread(
                new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        final FragmentActivity activity = getActivity();
                        if (activity instanceof MainActivity) {
                            ((MainActivity) activity).backToPrevious();
                        } else {
                            final Intent intent = new Intent(activity, MainActivity.class);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    }
                }, 500
        );

    }

    public void buyForCredit(String courseTitle, final long courseId, final String credit) {
        final String message = application.getResources().getString(R.string.txt_buy_course_confirmation_message, courseTitle, credit);
        showBuyCourseConfirmationDialog(
                message, new BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOK() {
                        showProgress();
                        subscription = AndroidObservable.bindFragment(
                                CourseDetailFragment.this, spendCredits(localSharedPreferences.userId().getOr(0l), courseId, credit)
                                .doOnNext(
                                        new Action1<SpentCreditParser>() {
                                            @Override
                                            public void call(SpentCreditParser spentCreditParser) {
                                                if (spentCreditParser.spentcredits) {
                                                    WorkerService.intent(application).downloadCourse(courseId, LocalFileManager.Folder.full).start();
                                                    try {
                                                        String creditsString = localSharedPreferences.credits().getOr("0");
                                                        int courseCredit = Integer.parseInt(credit);
                                                        int userCredits = Integer.parseInt(creditsString);
                                                        int remain = userCredits - courseCredit;
                                                        localSharedPreferences.credits().put(String.valueOf(remain));
                                                    } catch (NumberFormatException e) {
                                                        Credit credits = webServices.getCredits(localSharedPreferences.userId().getOr(0l));
                                                        localSharedPreferences.credits().put(credits.credits);
                                                    }
                                                }
                                            }
                                        }
                                )
                                .map(
                                        new Func1<SpentCreditParser, Boolean>() {
                                            @Override
                                            public Boolean call(SpentCreditParser spentCreditParser) {
                                                return spentCreditParser != null && spentCreditParser.success;
                                            }
                                        }
                                )
                        )
                                .subscribeOn(Schedulers.newThread())
                                .subscribe(
                                        new Observer<Boolean>() {
                                            @Override
                                            public void onCompleted() {
                                                hideProgress();
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                Timber.e(e, "Buy course error");
                                                showError();
                                            }

                                            @Override
                                            public void onNext(Boolean result) {
                                                if (result) {
                                                    Static.openMyCourseOnBack = true;
                                                    backToMainActivity();
                                                } else {
                                                    showError();
                                                }
                                            }
                                        }
                                );

                    }
                }
        );

    }

    @Inject
    WebServices webServices;

    private Observable<SpentCreditParser> spendCredits(final long userId, final long courseId, final String credit) {
        return Observable.defer(
                new Func0<Observable<SpentCreditParser>>() {
                    @Override
                    public Observable<SpentCreditParser> call() {
                        return Observable.just(webServices.spendCredits(userId, courseId, credit));
                    }
                }
        );
    }

    public void buyForCash(String courseTitle, float cash) {
        final String message = String.format("Are you sure you want to purchase the course %s for $%4.2f credits?", courseTitle, cash);
        showBuyCourseConfirmationDialog(
                message, new BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOK() {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.blinktrainingsystem.com"));
                        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        application.startActivity(browserIntent);
                    }
                }
        );
    }

}
