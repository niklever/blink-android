package com.blinktrainingsystem.blink.module.register;

import android.app.Application;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.model.request.RegisterRequest;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.Task;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import org.json.JSONObject;

/**
 * Created by TALE on 9/10/2014.
 */
public class RegisterPresenter implements RegisterUseCase {
    final WebServices webServices;

    final MainPresenter mainPresenter;

    final TaskManager taskManager;

    final DeviceInfo deviceInfo;

    final Application application;

    final LocalSharedPreferences localSharedPreferences;

    private final TrackerHelper trackerHelper;

    final RegisterView registerView;
    private final String ERROR_NETWORK;
    private final String ERROR_FIRSTNAME_EMPTY;
    private final String ERROR_LASTNAME_EMPTY;
    private final String ERROR_EMAIL_EMPTY;
    private final String ERROR_PASSWORD_EMPTY;
    private int taskId;

    public boolean backIsCourseDetail = false;

    public RegisterPresenter(RegisterView registerView, MainPresenter mainPresenter, Application application, WebServices webServices, TaskManager taskManager, DeviceInfo deviceInfo, LocalSharedPreferences localSharedPreferences, TrackerHelper trackerHelper) {
        this.webServices = webServices;
        this.mainPresenter = mainPresenter;
        this.taskManager = taskManager;
        this.deviceInfo = deviceInfo;
        this.application = application;
        this.registerView = registerView;
        this.localSharedPreferences = localSharedPreferences;
        this.trackerHelper = trackerHelper;
        ERROR_NETWORK = application.getResources().getString(R.string.error_network_disconnected);
        ERROR_FIRSTNAME_EMPTY = application.getResources().getString(R.string.error_empty, "first name");
        ERROR_LASTNAME_EMPTY = application.getResources().getString(R.string.error_empty, "last name");
        ERROR_EMAIL_EMPTY = application.getResources().getString(R.string.error_empty, "email");
        ERROR_PASSWORD_EMPTY = application.getResources().getString(R.string.error_empty, "password");
    }

    @Override
    public void submitRegisterForm(@Nullable final String firstName, @Nullable final String lastName, final String email, final String password) {
        if (TextUtils.isEmpty(email)) {
            registerView.showError(ERROR_EMAIL_EMPTY);
            return;
        }

        if (TextUtils.isEmpty(password)) {
            registerView.showError(ERROR_PASSWORD_EMPTY);
            return;
        }

        if (!deviceInfo.isNetworkConnected()) {
            registerView.showError(ERROR_NETWORK);
            return;
        }

        final RegisterRequest registerRequest = new RegisterRequest(firstName, lastName, email, password);
        Task<BaseParser> task = new Task<BaseParser>(taskManager) {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                registerView.showProgress();
            }

            @Override
            protected BaseParser doInBackground(Object... params) {
                BaseParser baseParser = webServices.register(email, firstName, lastName, password);
                if (baseParser != null && baseParser.success) {
                    Credit credits = webServices.getCredits(baseParser.userId);
                    if (credits.success) {
                        localSharedPreferences.credits().put(credits.credits);
                        return baseParser;
                    } else {
                        return null;
                    }
                }
                return baseParser;
            }

            @Override
            protected void onPostExecute(BaseParser baseParser) {
                super.onPostExecute(baseParser);
                if (baseParser == null) {
                    registerView.showError(Constant.ERROR_FAIL_TO_CONNECT_SERVER);
                } else if (!baseParser.success) {
                    registerView.showError(baseParser.msg);
                } else {
                    localSharedPreferences.userId().put(baseParser.userId);
                    mainPresenter.onRegisterSuccess(backIsCourseDetail);
                    trackerHelper.registerSucceed(email, baseParser.userId);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                registerView.hideProgress();
            }
        };
        taskId = taskManager.enqueue(task);

    }

    private boolean verifyNetwork() {
        if (deviceInfo.isNetworkConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void fbLogin(final JSONObject json) {
        if (!verifyNetwork()) {
            return;
        }
        registerView.showProgress();
        Task<BaseParser> task = new Task<BaseParser>(taskManager) {
            @Override
            protected BaseParser doInBackground(Object... params) {
                String id = json.optString("id","");
                String name = json.optString("name","");
                String email = json.optString("email","");
                String gender = json.optString("gender","");
                String locale = json.optString("locale","");
                BaseParser baseParser = webServices.fbLogin(id,email,name,locale,gender,true);
                if (baseParser != null && baseParser.success) {
                    Credit credits = webServices.getCredits(baseParser.userId);
                    if (credits.success) {
                        localSharedPreferences.credits().put(credits.credits);
                        localSharedPreferences.usr().put(json.optString("email", ""));
                        localSharedPreferences.pwd().put("");
                        localSharedPreferences.gender().put(json.optString("gender", ""));
                        localSharedPreferences.locale().put(json.optString("locale", ""));
                        localSharedPreferences.admin().put(baseParser.debug);
                        Static.admin = baseParser.debug;
                        return baseParser;
                    } else {
                        return null;
                    }
                }
                return baseParser;
            }

            @Override
            protected void onPostExecute(BaseParser baseParser) {
                super.onPostExecute(baseParser);
                if (baseParser == null) {
                    registerView.showError(Constant.ERROR_FAIL_TO_CONNECT_SERVER);
                } else if (!baseParser.success) {
                    registerView.showError(baseParser.msg);
                } else {
                    localSharedPreferences.userId().put(baseParser.userId);
                    mainPresenter.onRegisterSuccess(backIsCourseDetail);
                    String email = json.optString("email","");
                    trackerHelper.loginSucceed(email, baseParser.userId);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                registerView.hideProgress();
            }
        };
        taskId = taskManager.enqueue(task);
    }

    @Override
    public void backToLogin() {
        mainPresenter.backToLoginFromRegister();
    }

    @Override
    public void cancelRegisterTask() {
        taskManager.cancel(taskId);
    }
}
