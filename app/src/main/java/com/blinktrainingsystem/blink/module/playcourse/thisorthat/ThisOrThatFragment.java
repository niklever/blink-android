package com.blinktrainingsystem.blink.module.playcourse.thisorthat;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import rx.functions.Action1;

/**
 * Created by TALE on 9/26/2014.
 */
public class ThisOrThatFragment extends BaseBurstFragment {

    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.tvQuestion)
    TextView tvQuestion;
    @InjectView(R.id.btLeft)
    TextView btLeft;
    @InjectView(R.id.btRight)
    TextView btRight;
    @InjectView(R.id.llQuestion)
    LinearLayout llQuestion;
    @InjectView(R.id.rlButton)
    RelativeLayout rlButton;
    @InjectView(R.id.spLeftMargin)
    View spLeftMargin;

    @Inject TrackerHelper trackerHelper;

    private ThisOrThatPresenter thisOrThatPresenter;

    @Inject
    ViewAnimator viewAnimator;
    @Inject
    UiHelper uiHelper;
    private JsonEx json;
    ;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_this_or_that, container, false);
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.thisThatLoad();
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        final Blink blink = courseHelper.getBlinks() == null ? null : courseHelper.getBlinks().get(blinkIndex);
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        thisOrThatPresenter = json == null || json.thisthat == null ? null : new ThisOrThatPresenter(json, uiHelper);
        if (thisOrThatPresenter != null) {
            btLeft.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
            Util.setTypeface(btLeft, getString(R.string.font_myriad_pro_regular));
            btRight.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
            Util.setTypeface(btRight, getString(R.string.font_myriad_pro_regular));
            tvTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
            Util.setTypeface(tvTitle, getString(R.string.font_myriad_pro_regular));
            tvQuestion.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
            Util.setTypeface(tvQuestion, getString(R.string.font_myriad_pro_regular));

            final boolean isTable = getResources().getBoolean(R.bool.is_tablet);
            btLeft.setTextColor(isTable ? Color.WHITE : Color.BLACK);
            btRight.setTextColor(isTable ? Color.WHITE : Color.BLACK);

            ViewObservableHelper.globalLayoutFrom(spLeftMargin).subscribe(
                    new Action1<View>() {
                        @Override
                        public void call(View view) {
                            final int width = spLeftMargin.getWidth();
                            final GridLayout.LayoutParams layoutParams = (GridLayout.LayoutParams) spLeftMargin.getLayoutParams();
                            layoutParams.setGravity(Gravity.NO_GRAVITY);
                            layoutParams.width = width / 2;
                            thisOrThatPresenter.bind(ThisOrThatFragment.this);
                            thisOrThatPresenter.start();
                        }
                    }
            );
        } else {
            llQuestion.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getTimer() {
        return json == null || json.thisthat == null ? 0 : json.thisthat.timer;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.this_that_help);
    }

    @OnClick(R.id.btLeft)
    public void answerPositive() {
        thisOrThatPresenter.truePressed();

    }

    @OnClick(R.id.btRight)
    public void answerNegative() {
        thisOrThatPresenter.falsePressed();
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                } else {
                    if (thisOrThatPresenter.nextBlink) {
                        notifyBurstCompleted();
                    } else {
                        thisOrThatPresenter.resultClosed();
                    }
                }
                break;
            case TimeOut:
                showHelp(getString(R.string.time_out_message));
                break;
        }
    }

    public void showQuestion() {
        viewAnimator.enterLeft(tvQuestion, 700);
        viewAnimator.enterLeft(btLeft, 700);
        viewAnimator.enterLeft(btRight, 700);
    }

    public void setTitleText(CharSequence title) {
        tvTitle.setText(title);
    }

    public void setQuestion(CharSequence question) {
        tvQuestion.setText(question);
    }

    public void setLeftButtonText(CharSequence text) {
        btLeft.setText(text);
    }

    public void setRightButtonText(CharSequence text) {
        btRight.setText(text);
    }

    public void showHelp(String msg) {
        busDriver.pauseTimer();
        busDriver.showHelp(msg);
    }

    public void updateScore(int score, int gems) {

        if (!blinkHandler.updateScore(score, blinkIndex, course, gems)) {
            playRawSound(R.raw.points_lost);
        } else {
            busDriver.updateScore(blinkHandler);
        }
    }

    public void showNextButton() {
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
    }

    public void stopTimer() {
        busDriver.pauseTimer();
    }
}
