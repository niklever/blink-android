package com.blinktrainingsystem.blink.module.playcourse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.adapter.BurstGridAdapter;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.util.BusDriver;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by TALE on 9/19/2014.
 */
public class BurstGridFragment extends PlayCourseFragment {

    @InjectView(R.id.tvTitle)
    TextView mTvTitle;
    @InjectView(R.id.gridView)
    GridView mGridView;
    @Inject
    BurstGridAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_grid_menu, container, false);
    }

    protected int getUnlockedPosition() {
        return localSharedPreferences.burstUnlock(course.id, blinkHandler.getUnitIndex(blinkIndex)).getOr(0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((PlayCourseActivity) getActivity()).stopLongSound();

        final int unlockedBursts = getUnlockedPosition();
        Timber.d("onViewCreated => unlockedPosition: %d", unlockedBursts);
        adapter.unlockIndex = unlockedBursts;
        adapter.courseId = course.id;
        adapter.blinkHandler = blinkHandler;
        final Boolean allowSkip = isAllowSkip();
        adapter.allowSkip = allowSkip;
        mGridView.setAdapter(adapter);
        mGridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position <= unlockedBursts || allowSkip) {
                            Blink blink = (Blink) parent.getItemAtPosition(position);
                            bus.post(blink);
                        }
                    }
                }
        );
        final List<Blink> allBursts = loadData();
        adapter.changeDataSet(allBursts);
        bind(blinkHandler.getUnit(blinkIndex));
    }

    protected Boolean isAllowSkip() {
        return localSharedPreferences.allowSkip().getOr(false);
    }

    protected List<Blink> loadData() {
        return blinkHandler.getAllBurstsOfUnit(blinkIndex);
    }

    @Override
    public void onAllowSkipChanged() {
        super.onAllowSkipChanged();
        updateAdapterAllowSkip();
    }

    private void updateAdapterAllowSkip() {
        final Boolean allowSkip = localSharedPreferences.allowSkip().getOr(false);
        Timber.d("updateAdapterAllowSkip => %s", allowSkip);
        if (!allowSkip) {
            adapter.allowSkip = allowSkip;
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusDriver.withBus(bus).setFooterMenu(null, null, null);
    }

    private void bind(Blink blink) {
        this.blink = blink;
        String title = getTitle();
        Timber.d("Title: " + title);
        mTvTitle.setText(title);
    }

    protected String getTitle() {
        if (blink == null) {
            return null;
        }
        return "Burst in " + blink.summary;
    }
}
