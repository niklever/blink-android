package com.blinktrainingsystem.blink.module.playcourse.menu;

import dagger.Module;

/**
 * Created by TALE on 9/24/2014.
 */
@Module(
        complete = false,
        injects = {
                FooterFragment.class,
                Footer3ButtonFragment.class,
                HeaderMenuFragment.class,
                MenuBackTitleFragment.class
        }
)
public class MenuModule {
}
