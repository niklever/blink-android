package com.blinktrainingsystem.blink.module.login;

import android.app.Application;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        injects = LoginFragment.class
)
public class LoginModule {
    private final LoginView loginView;

    public LoginModule(LoginView loginView) {
        this.loginView = loginView;
    }

    @Provides
    @Singleton LoginPresenter provideLoginPresenter(MainPresenter mainPresenter, Application application, WebServices webServices, TaskManager taskManager, DeviceInfo deviceInfo, LocalSharedPreferences localSharedPreference, TrackerHelper trackerHelper) {
        return new LoginPresenter(loginView, mainPresenter, application, webServices, taskManager, deviceInfo, localSharedPreference, trackerHelper);
    }
}
