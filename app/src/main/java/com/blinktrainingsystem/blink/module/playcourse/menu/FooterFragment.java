package com.blinktrainingsystem.blink.module.playcourse.menu;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.util.ViewAnimator;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by TALE on 9/24/2014.
 */
public class FooterFragment extends MenuBaseFragment {

    public static final String KEY_TEXT_LEFT = "text_left";
    public static final String KEY_TEXT_RIGHT = "text_right";

    @InjectView(R.id.tvFooterLeft)
    TextView tvFooterLeft;
    @InjectView(R.id.tvFooterRight)
    TextView tvFooterRight;
    @Inject
    ViewAnimator viewAnimator;

    public static final int CLICK_DURATION = 1000; // 1 second.

    private long lastClickedTimeStamp = System.currentTimeMillis();

    private ButtonMenuClickListener buttonMenuClickListener;

    public static interface ButtonMenuClickListener {
        void onLeftClick(CharSequence text);

        void onRightClick(CharSequence text);
    }

    public static class MenuButton {
        public final String textLeft;
        public final String textRight;

        public MenuButton(String textLeft, String textRight) {
            this.textLeft = textLeft;
            this.textRight = textRight;
        }
    }

    public static FooterFragment newInstance(String textLeft, String textRight) {
        FooterFragment fragment = new FooterFragment();
        Bundle args = newArgs(textLeft, textRight);
        fragment.setArguments(args);
        return fragment;
    }

    protected static Bundle newArgs(String textLeft, String textRight) {
        Bundle args = new Bundle();
        if (!TextUtils.isEmpty(textLeft)) {
            args.putString(KEY_TEXT_LEFT, textLeft);
        }
        if (!TextUtils.isEmpty(textRight)) {
            args.putString(KEY_TEXT_RIGHT, textRight);
        }
        return args;
    }

    protected boolean violateMultipleClickPolicy() {
        if (System.currentTimeMillis() < lastClickedTimeStamp + CLICK_DURATION) {
            return true;
        }
        lastClickedTimeStamp = System.currentTimeMillis();
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ButtonMenuClickListener) {
            buttonMenuClickListener = (ButtonMenuClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.footer_2_button, container, false);
    }

    @OnClick(R.id.tvFooterLeft)
    public void onFooterLeftClick() {
        if (violateMultipleClickPolicy()) {
            return;
        }
        if (buttonMenuClickListener != null) {
            buttonMenuClickListener.onLeftClick(tvFooterLeft.getText());
        }
    }

    @OnClick(R.id.tvFooterRight)
    public void onFooterRightClick() {
        if (violateMultipleClickPolicy()) {
            return;
        }
        if (buttonMenuClickListener != null) {
            buttonMenuClickListener.onRightClick(tvFooterRight.getText());
        }
    }

}
