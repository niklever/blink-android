package com.blinktrainingsystem.blink.module.coursedetail;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.dialog.BuyCourseConfirmationDialog;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.parser.SpentCreditParser;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.Task;
import com.blinktrainingsystem.blink.task.TaskManager;

/**
 * Created by TALE on 9/10/2014.
 */
public class CourseDetailPresenter implements CourseDetailUseCase {

    private final CourseDetailView courseDetailView;
    private final MainPresenter mainPresenter;
    private final WebServices webServices;
    private final TaskManager taskManager;
    private final LocalSharedPreferences localSharedPreferences;
    private final Application application;
    private final CoursesRepo coursesRepo;
    private int taskId;

    public CourseDetailPresenter(Application application, CourseDetailView courseDetailView, MainPresenter mainPresenter, WebServices webServices, TaskManager taskManager, LocalSharedPreferences localSharedPreferences, CoursesRepo coursesRepo) {
        this.courseDetailView = courseDetailView;
        this.mainPresenter = mainPresenter;
        this.webServices = webServices;
        this.taskManager = taskManager;
        this.localSharedPreferences = localSharedPreferences;
        this.application = application;
        this.coursesRepo = coursesRepo;
    }

    @Override
    public void backToCourses() {
        mainPresenter.backToCourses();
    }

    @Override
    public void trySample(final long courseId) {
        Static.openMyCourseOnBack = true;
        WorkerService.intent(application).downloadCourse(courseId, LocalFileManager.Folder.lite).start();
        backToCourses();


    }

    @Override
    public void buyForCredit(String courseTitle, final long courseId, final String credit) {
        final String message = application.getResources().getString(R.string.txt_buy_course_confirmation_message, courseTitle, credit);
        courseDetailView.showBuyCourseConfirmationDialog(
                message, new BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOK() {
                        Task<SpentCreditParser> task = new Task<SpentCreditParser>(taskManager) {
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                courseDetailView.showProgress();
                            }

                            @Override
                            protected void onFinished() {
                                super.onFinished();
                                courseDetailView.hideProgress();
                            }

                            @Override
                            protected SpentCreditParser doInBackground(Object... params) {
                                SpentCreditParser spentCreditParser = webServices.spendCredits(localSharedPreferences.userId().getOr(0L), courseId, credit);
                                if (spentCreditParser.spentcredits) {
                                    WorkerService.intent(application).downloadCourse(courseId, LocalFileManager.Folder.full).start();
                                    try {
                                        String creditsString = localSharedPreferences.credits().getOr("0");
                                        int courseCredit = Integer.parseInt(credit);
                                        int userCredits = Integer.parseInt(creditsString);
                                        int remain = userCredits - courseCredit;
                                        localSharedPreferences.credits().put(String.valueOf(remain));
                                    } catch (NumberFormatException e) {
                                        Credit credits = webServices.getCredits(localSharedPreferences.userId().getOr(0l));
                                        localSharedPreferences.credits().put(credits.credits);
                                    }
                                }
                                return spentCreditParser;
                            }

                            @Override
                            protected void onPostExecute(SpentCreditParser spentCreditParser) {
                                super.onPostExecute(spentCreditParser);
                                if (spentCreditParser == null) {
                                    courseDetailView.showError();
                                } else if (spentCreditParser.success) {
                                    Static.openMyCourseOnBack = true;
                                    backToCourses();
                                } else {
                                    courseDetailView.showError();
                                }
                            }
                        };
                        taskId = taskManager.enqueue(task);
                    }
                }
        );

    }

    @Override
    public void buyForCash(String courseTitle, float cash) {
        final String message = String.format("Are you sure you want to purchase the course %s for $%4.2f credits?", courseTitle, cash);
        courseDetailView.showBuyCourseConfirmationDialog(
                message, new BuyCourseConfirmationDialog.ConfirmationButtonOnClickListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOK() {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.blinktrainingsystem.com"));
                        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        application.startActivity(browserIntent);
                    }
                }
        );
    }

    @Override
    public void cancelTransaction() {
        taskManager.cancel(taskId);
    }
}
