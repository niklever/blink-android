package com.blinktrainingsystem.blink.module.playcourse.handler;

import android.text.TextUtils;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.parser.CourseHelper;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.Dossier;
import com.blinktrainingsystem.blink.data.model.pojo.Input;
import com.blinktrainingsystem.blink.data.model.pojo.Intro;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.tale.sharepref.LongEditor;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import hugo.weaving.DebugLog;
import timber.log.Timber;

/**
 * Created by TALE on 9/23/2014.
 */
public class BlinkHandler {

    private static final String KEY_GEMS_COLLECTED = "gemCollected";
    private static final String KEY_HIGHEST_BURST_SCORES = "highestBurstSocres";
    private static final String KEY_COURSE_SCORE = "courseScore";
    private static final String KEY_BURST_COURSE = "burstScore";


    private List<Blink> blinkList;
    private final  LocalSharedPreferences localSharedPreferences;
    public final CourseHelper courseHelper;
    public final List<Blink> growths;
    private int startIndex;

    private long userScore = 0;
    private long burstScore = 0;

    public BlinkHandler(CourseHelper courseHelper, LocalSharedPreferences localSharedPreferences) {
        this.localSharedPreferences = localSharedPreferences;
        this.courseHelper = courseHelper;
        this.growths = courseHelper.getGrowth();
        this.blinkList = courseHelper.getBlinks();
    }

    public void setEnableActivityMode(boolean enable) {
        blinkList.clear();
        blinkList = enable ? courseHelper.getGrowth() : courseHelper.getBlinks();
        if (blinkList == null || blinkList.size() == 0) {
            return;
        }
        Blink blink = blinkList.get(0);
        startIndex = blink == null ? 0 : blink.index;
    }

    public List<Blink> getBlinkList() {
        return blinkList;
    }

    /**
     * Get blink for blink index.
     *
     * @param index The index of blink.
     * @return Blink object at the index.
     */
    public Blink get(int index) {
        if (blinkList == null) {
            return null;
        }

        if (index < 0 || index >= blinkList.size()) {
            return null;
        }
        return blinkList.get(index);
    }

    public Blink getByBlinkIndex(int blinkIndex) {
        if (blinkList == null || blinkList.size() == 0) {
            return null;
        }
        int mapIndex = blinkIndex - startIndex;
        if (mapIndex < 0 || mapIndex >= blinkList.size()) {
            return null;
        }
        return blinkList.get(mapIndex);
    }

    public int translateIndex(int input) {
        return input - startIndex;
    }

    public List<Blink> getAllBursts() {
        if (blinkList != null && blinkList.size() > 0) {
            List<Blink> bursts = Lists.newArrayList();
            for (Blink blink : blinkList) {
                if (blink.type == BlinkType.KBlinkBurstStart) {
                    bursts.add(blink);
                }
            }
            return bursts;
        }
        return null;
    }

    public int getIndexOfUnit(int blinkIndex) {
        if (blinkIndex >= 0 && blinkList != null && blinkList.size() > blinkIndex) {
            for (int i = blinkIndex; i >= 0; i--) {
                final Blink blink = blinkList.get(i);
                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
                    return i;
                }
            }
        }
        return -1;
    }

    public List<Blink> getAllBurstsOfUnit(int blinkIndex) {

        final int indexOfUnit = getIndexOfUnit(blinkIndex);
        if (indexOfUnit != -1) {
            List<Blink> bursts = Lists.newArrayList();
            for (int i = indexOfUnit; i < blinkList.size(); i++) {
                final Blink blink = blinkList.get(i);
                if (blink.type == BlinkType.KBlinkTypeSpecificContentLearningUnitEnd) {
                    return bursts;
                }
                if (blink.type == BlinkType.KBlinkBurstStart) {
                    bursts.add(blink);
                }
            }
            return bursts;
        }
        return null;
    }

    public int getTargetToUnlock(int blinkID) {
        if (!inBurst(blinkID)) return 0;
        int score = 0;
        for (int i = blinkID; i >= 0; i--) {
            Blink dict = blinkList.get(i);
            int type = dict.type;
            if (type == BlinkType.KBlinkBurstStart) {
                final Json json = dict.loadJsonToObject();
                if (json != null) {
                    score = json.unlockscore;
                    break;
                }
            }
        }

        return score;
    }

    boolean inBurst(int blinkID) {
        if (blinkList == null || blinkID >= blinkList.size()) return false;
        boolean start = false;
        boolean end = false;
        Blink dict;
        int type;
        int index = blinkID;
        for (int i = blinkID; i >= 0; i--) {
            dict = blinkList.get(i);
            type = dict.type;
            if (type == 4 && i != blinkID) break;
            if (type == BlinkType.KBlinkBurstStart) {
                if (i == blinkID) index++;
                start = true;
                break;
            }
        }
        for (int i = index; i < blinkList.size(); i++) {
            dict = blinkList.get(i);
            type = dict.type;
            if (type == BlinkType.KBlinkBurstStart) break;
            if (type == 4) {
                end = true;
                break;
            }
        }

        return start && end;
    }


    public Blink getUnit(int index) {
        if (index >= 0 && blinkList != null && blinkList.size() > index) {
            for (int i = index; i >= 0; i--) {
                Blink blink = get(i);
                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
                    return blink;
                }
            }
        }
        return null;
    }

    public String getUnitTitle(int blinkIndex) {
        final Blink blink = getUnit(blinkIndex);
        if (blink != null && blink.loadJsonToObject() != null) {
            Json json = blink.loadJsonToObject();
            Intro intro = json.intro;
            if (intro != null) {
                return blink.type == BlinkType.KBlinkLearningUnitStart ? "Unit " + getUnitIndex(blinkIndex) + " - " + intro.title : intro.title;
            } else {
                return "Unit " + getUnitIndex(blinkIndex) + " - ";
            }
        }
        return null;
    }

    public int getUnitIndex(int index) {
        int unit = 0;
        if (blinkList != null && blinkList.size() > index) {
            int available = blinkList.size() - 1;
            if (available > index) {
                available = index;
            }
            for (int i = 0; i <= available; i++) {
                Blink blink = blinkList.get(i);
                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
                    unit++;
                }
            }
        }
        return unit;
    }

    public int getBurstIndexInUnit(int index) {
        if (blinkList == null || index >= blinkList.size()) return 0;
        int result = 0;
        for (int i = index; i >= 0; i--) {
            final Blink dict = blinkList.get(i);
            int type = dict.type;
            if (type == BlinkType.KBlinkBurstStart) result++;
            if (type == 1) break;
        }
        return result;

    }

    public int getBurstsRemainingInUnit(int blinkID) {
        if (blinkList == null || blinkID >= blinkList.size() - 1) return 0;
        int idx = 0;
        for (int i = blinkID + 1; i < blinkList.size(); i++) {
            Blink dict = blinkList.get(i);
            int type = dict.type;
            if (type == BlinkType.KBlinkBurstStart) idx++;
            if (type == BlinkType.KBlinkTypeSpecificContentLearningUnitEnd) break;//2
        }
        return idx;
    }

    public int getFirstBlinkIndexInThisBurst(int blinkIndex) {
        if (blinkList == null || blinkIndex >= blinkList.size() || blinkIndex < 0) {
            return -1;
        }

        int index = blinkIndex;
        if (index > 0) {
            type = blinkList.get(index).type;
            while (index > 0 && type != 3 && type != 1) {
                index--;
                type = blinkList.get(index).type;
            }
        }
        return index;
    }

    public int getBlinkIndex(Blink blink) {
        for (int i = 0; i < blinkList.size(); i++) {
            final Blink blk = blinkList.get(i);
            if (blk.id == blink.id) {
                return i;
            }
        }
        return -1;
    }

    public int getBurstIndex(int index) {
        int unit = 0;
        if (blinkList != null && blinkList.size() > index) {
            int available = blinkList.size() - 1;
            if (available > index) {
                available = index;
            }
            for (int i = 0; i <= available; i++) {
                Blink blink = blinkList.get(i);
                if (blink.type == BlinkType.KBlinkBurstStart) {
                    unit++;
                }
            }
        }
        return unit;
    }

    public List<Blink> getAllUnit() {
        if (blinkList != null && blinkList.size() > 0) {
            List<Blink> units = Lists.newArrayList();
            for (Blink blink : blinkList) {
                if (blink.type == BlinkType.KBlinkLearningUnitStart) {
                    units.add(blink);
                }
            }
            return units;
        }
        return null;
    }

    public void startCourse(long courseId) {
        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_COURSE_SCORE + "_" + courseId;
        userScore = this.localSharedPreferences.getLongOr(key, 0l);
        Timber.d("startCourse => {key:%s, userScore: %d)", key, userScore);
    }

    public void startNewBurstScore(long courseId, int blinkIndex) {

        int currentBurst = getBurstIndex(blinkIndex);
        int unitIndex = getUnitIndex(blinkIndex);

        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_BURST_COURSE + "_" + courseId + "_" + unitIndex + "_" + currentBurst;
        burstScore = this.localSharedPreferences.getLongOr(key, 0l);
    }

    public int getBurstStartIndex(int blinkId) {
        if (blinkList != null && blinkList.size() > blinkId && blinkId > 0) {
            for (int i = blinkId; i > 0; i--) {
                Blink blink = blinkList.get(i);
                if (blink.type == BlinkType.KBlinkBurstStart) {
                    return i;
                }
            }
        }
        return 0;
    }

    public List<Dossier> getDossiers(int highestIndex, int blinkIndex) {
        //index should be pointing to a unit start

        if (blinkList == null || blinkIndex < 0 || blinkIndex >= blinkList.size()) {
            return null;
        }
        final List<Dossier> dossiers = new ArrayList<Dossier>();

//        NSDictionary *dict = [blinks objectAtIndex:index];
        Blink dict = blinkList.get(blinkIndex);
        if (dict == null) {
            return null;
        }

        int type = dict.type;//[[dict objectForKey:@"type"] intValue];
        if (type != 1) {
            Timber.d("getDossier: index:%d type:%d", blinkIndex, type);
            return null;
        }

        Json json = dict == null ? null : dict.loadJsonToObject();
//        NSDictionary *json = [self dictionaryFromString:[dict objectForKey:@"json"]];
//        NSDictionary *intro = [json objectForKey:@"intro"];
        final Intro intro = json == null ? null : json.intro;
        String title = intro == null ? null : intro.title;// objectForKey:@"title"];
        String str;
        String guid;
        Map<String, String> item = new Hashtable<String, String>();

//        dossier = [[NSMutableArray alloc] init];
        int index = blinkIndex + 1;
        do {
            dict = blinkList.get(index);
            type = dict.type;// objectForKey:@ "type"]intValue];
            if (type == BlinkType.KBlinkTypeSpecificContentLearningUnitEnd) break;
            if (type == BlinkType.KBlinkInput) {//==7
                //Input type
                json = dict.loadJsonToObject();
                Input input = json.input;//
                type = input.type;// objectForKey:@ "type"]intValue];
                //NSLog(@"Dossier index:%d type:%d", index, type);
                if (type == 1) {
                    //Text type
                    str = input.prompt;//prompt"];
                    guid = dict.guid;//[dict objectForKey:@ "guid"];
                    final Dossier dossier = new Dossier();
                    dossier.guid = guid;
                    dossier.label = str;
                    dossier.type = type;
                    dossiers.add(dossier);
                } else if (type == 2) {
                    //Text type
                    str = input.prompt;//prompt"];
                    guid = dict.guid;//[dict objectForKey:@ "guid"];
                    int count = input.count;// objectForKey:@ "count"]intValue];
                    final Dossier dossier = new Dossier();
                    dossier.guid = guid;
                    dossier.label = str;
                    dossier.type = type;
                    dossier.count = count;
                    dossiers.add(dossier);
                } else if (type >= 6) {
                    str = input.prompt;//prompt"];
                    guid = dict.guid;//[dict objectForKey:@ "guid"];
                    boolean isNumber = input.isNumber;
                    int min = (isNumber) ? Integer.parseInt(input.min) : 1;
                    int max = (isNumber) ? Integer.parseInt(input.max) : 100;
                    final Dossier dossier = new Dossier();
                    dossier.guid = guid;
                    dossier.label = str;
                    dossier.type = type;
                    dossier.min = min;
                    dossier.max = max;
                    dossiers.add(dossier);
                }
            }
            index++;
        } while (index < highestIndex && index < blinkList.size());
        return dossiers;
    }

    public List<Blink> getDossierBlinks() {
        if (blinkList == null || blinkList.size() == 0) {
            return null;
        }
//        NSMutableArray * tmp =[[NSMutableArray alloc]init];

        final List<Blink> result = new ArrayList<Blink>();
        for (Blink blink : blinkList) {
            int type = blink.type;//[[blink objectForKey:@"type"] intValue];
            if (type == 1) {
//                NSString *str = [blink objectForKey:@"json"];
//                NSDictionary *json = [blkNavControllerRef dictionaryFromString:str];
                final Json json = blink.loadJsonToObject();
                boolean dossier = json.dossier;//[;[json objectForKey:@"dossier"] boolValue];
                if (dossier) {
//                    NSDictionary *intro = [json objectForKey:@"intro"];
//                    final Intro intro = json.intro;
//                    String str = "";
//                    boolean enabled = (index<=[blkNavControllerRef highestIndex]);
//                    if (blkNavControllerRef.topBar.skip) enabled = YES;
//                    if (intro != null) str = intro.title;// objectForKey:@"title"];
//                    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:str, @"title", [NSNumber numberWithInt:index], @"index", [NSNumber numberWithBool:enabled], @"enabled", nil];
//                    [blink setObject:[NSNumber numberWithBool:enabled] forKey:[NSString stringWithFormat:ENABLE, [blkNavControllerRef getSetting:@"email"]]];
//                    [tmp addObject:blink];
                    result.add(blink);
                }
            }
        }
        return result;
    }

    public long getUserScore(long courseId) {
        long userId = 0l;
        try {
            userId = this.localSharedPreferences.userId().getOr(01l);
        }catch(VerifyError ve) {
            return 0l;
        }
        String key = userId + "_" + KEY_COURSE_SCORE + "_" + courseId;
        return this.localSharedPreferences.getLongOr(key, userScore);
    }

    public long getUserScoreInBurst() {
        return burstScore;
    }

    public long getHighestUserScoreInBurst(int blinkIndex, long courseId) {
        int unitIndex = getUnitIndex(blinkIndex);
        int currentBurst = getBurstIndex(blinkIndex);
        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + courseId + KEY_HIGHEST_BURST_SCORES + "_" + unitIndex + "_" + currentBurst;
        return this.localSharedPreferences.getLongOr(key, 0l);
    }

    public void updateHighestSocresInBurst(long score, long courseId, int unitIndex, int currentBurst) {
        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + courseId + KEY_HIGHEST_BURST_SCORES + "_" + unitIndex + "_" + currentBurst;
        this.localSharedPreferences.putLong(key, score);
    }

    public boolean updateScore(long newScore, int blinkIndex, Course course, int gem) {
        if (blinkList == null && blinkList.size() < 0 || blinkIndex < 0 || blinkIndex >= blinkList.size()) {
            return false;
        }
        final Blink itemDict = blinkList.get(blinkIndex);
        final Json json = itemDict.loadJsonToObject();

        boolean needToUpdate = false;
        final Long userId = localSharedPreferences.userId().getOr(0l);
        int previousScore = localSharedPreferences.blinkScore(userId, course.id, course.category, itemDict.guid).getOr(0);
        if (newScore > 0) {
            if (previousScore == 0) {
                needToUpdate = true;
            } else {
                if (newScore > previousScore) {
                    if (newScore > json.score) {
                        newScore = json.score;
                    }
                    needToUpdate = true;
                }
            }
        }
        if (needToUpdate) {
            localSharedPreferences.blinkScore(userId, course.id, course.category, itemDict.guid).put((int) newScore);

            int unitIndex = getUnitIndex(blinkIndex);
            int currentBurst = getBurstIndex(blinkIndex);
            String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_BURST_COURSE + "_" + course.id + "_" + unitIndex + "_" + currentBurst;


            burstScore = burstScore - previousScore + newScore;

            /* saving burst score */
            localSharedPreferences.putLong(key, burstScore);

            userScore = userScore - previousScore + newScore;

            /* saving  user score in course */
           /* saving user score in course */
            key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_COURSE_SCORE + "_" + course.id;
            Timber.d("updateScore => {userScoreKey: %s, userScore: %d, burstScore: %d, gem: %d}", key, userScore, burstScore, gem);
            this.localSharedPreferences.putLong(key, userScore);



            /*=========================================*/
            /*==================DUYLEPT================*/
            /*=========================================*/
            /*
             SAVING THE BEST SCORE OF BURST
             */
            /* get the best score of burst to compare */
            long highestScoreInBurst = getHighestUserScoreInBurst(blinkIndex, course.id);
            if (burstScore > highestScoreInBurst) {
                /* update highest score */
                updateHighestSocresInBurst(burstScore, course.id, unitIndex, currentBurst);
            }

            /*=========================================*/
            /*=========================================*/
            /*=========================================*/

        /* update gems */
            updateGem(gem, blinkIndex, course.id);
            return true;
        } else {
            return false;
        }
    }

//    public void updateScore(long score, int blinkIndex, long courseId, int gem) {
//        updateScore(score, )
//        int unitIndex = getUnitIndex(blinkIndex);
//        int currentBurst = getBurstIndex(blinkIndex);
//
//        /* saving burst score */
//        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_BURST_COURSE + "_" + courseId + "_" + unitIndex + "_" + currentBurst;
//        long previousScore = this.localSharedPreferences.getLongOr(key, 0l);
//        if (score > 0 && score > previousScore) {
//            burstScore = burstScore - previousScore + score;
//            userScore = userScore - previousScore + score;
//
//            this.localSharedPreferences.putLong(key, burstScore);
//
//            /* saving user score in course */
//            key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_COURSE_SCORE + "_" + courseId;
//            Timber.d("updateScore => {userScoreKey: %s, userScore: %d, burstScore: %d, gem: %d}", key, userScore, burstScore, gem);
//            this.localSharedPreferences.putLong(key, userScore);
//
//            long highestScoreInBurst = getHighestUserScoreInBurst(blinkIndex, courseId);
//            if (burstScore > highestScoreInBurst) {
//                /* update highest score */
//                updateHighestSocresInBurst(burstScore, courseId, unitIndex, currentBurst);
//            }
//
//        }
//        if (gem > 0) {
//            updateGem(gem, blinkIndex, courseId);
//        }
//    }


    public void updateGem(long gem, int blinkIndex, long courseId) {
        int currentBurst = getBurstIndex(blinkIndex);
        int unitIndex = getUnitIndex(blinkIndex);
        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_GEMS_COLLECTED + "_" + courseId + "_" + unitIndex + "_" + currentBurst;
        long gems = this.localSharedPreferences.getLongOr(key, 0l);
        gems += gem;

        Timber.d("updateGem => blinkIndex: %d, keyGem: %s, gems: %d", blinkIndex, key, gems);
        this.localSharedPreferences.putLong(key, gems);
    }

    public int getUserGemsInBurst(int blinkIndex, long courseId) {
        int currentBurst = getBurstIndex(blinkIndex);
        int unitIndex = getUnitIndex(blinkIndex);
        String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_GEMS_COLLECTED + "_" + courseId + "_" + unitIndex + "_" + currentBurst;
        final int gems = (int) this.localSharedPreferences.getLongOr(key, 0l);
        Timber.d("getUserGemsInBurst => blinkIndex: %d, keyGem: %s, gems: %d", blinkIndex, key, gems);
        return gems;
    }

    public int getUserTotalGemsUserCollected(int blinkIndex, long courseId) {
        int currentBurst = getBurstIndex(blinkIndex);
        int unitIndex = getUnitIndex(blinkIndex);
        int totalGemsCollected = 0;
        for (int index = 0; index < currentBurst; index++) {
            String key = this.localSharedPreferences.userId().getOr(01l) + "_" + KEY_GEMS_COLLECTED + "_" + courseId + "_" + unitIndex + "_" + (index + 1);
            long gem = this.localSharedPreferences.getLongOr(key, 0l);
            totalGemsCollected += gem;
        }

        return totalGemsCollected;
    }


    public long getMaxTotalScoresToThisBurst(int blinkIndex) {
        if (blinkList == null || blinkIndex >= blinkList.size()) {
            return 0;
        }

        int index = blinkIndex - 1;
        long totalScore = 0;
        int type = 1;
        /* do the loop previous until we get to the starting of unit to get max point to this burst */
        do {
            Blink item = blinkList.get(index);
            if (item != null) {
                type = item.type;
                if (type != 3) {
                    Json json = item.loadJsonToObject();
                    int score = json.score;
                    totalScore += score;
                }
            }
            index--;
        } while (index > 0);


        return totalScore;
    }

    public long getMaxTotalGemsToThisBurst(int blinkIndex) {
        if (blinkList == null || blinkIndex >= blinkList.size()) {
            return 0;
        }
        int index = blinkIndex;
        long totalScore = 0;
        int type = 0;
        do {
            Blink item = blinkList.get(index);
            if (item != null) {
                type = item.type;
                if (type != 3) {
                    Json json = item.loadJsonToObject();
                    Timber.d("getMaxGemsInThisBurst => index: %d, type: %d, gems: %d, score: %d", index, type, json.gems, json.score);
                    int score = json.gems;
                    totalScore += score;
                }
            }
            index--;
        } while (index > 0);


        return totalScore;
    }

    public long getMaxScoreOfBurst(int blinkIndex) {
        Timber.d("getMaxScoreOfBurst => blinkIndex: %d", blinkIndex);
        if (blinkList == null || blinkIndex >= blinkList.size() || blinkIndex < 0) {
            return 0;
        }

        int index = getFirstBlinkIndexInThisBurst(blinkIndex);

        long totalScore = 0;
        while (type != 4 && index < blinkList.size()) {
            Blink item = blinkList.get(index);
            if (item != null) {
                Json json = item.loadJsonToObject();
                Timber.d("getMaxGemsInThisBurst => index: %d, type: %d, gems: %d, score: %d", index, type, json.gems, json.score);
                type = json != null ? json.type : item.type;
                if (item.type != 3) {
                    int score = json.score;
                    totalScore += score;
                    Timber.d("score: %d", score);
                }
            }
            index++;
        }

        return totalScore;
    }

    private int type = 0;

    public int getMaxGemsInThisBurst(int blinkIndex) {
        int index = getFirstBlinkIndexInThisBurst(blinkIndex);
        Timber.d("getFirstBlinkIndexInThisBurst: %d", index);
        int totalGems = 0;
        if (index != -1) {
            type = blinkList.get(index).type;
            while (type != 4 && type != 2 && index < blinkList.size()) {
                final Blink blink = blinkList.get(index);
                if (blink != null) {
                    Json json = blink.loadJsonToObject();
                    Timber.d("getMaxGemsInThisBurst => index: %d, type: %d, gems: %d, score: %d", index, type, json.gems, json.score);
                    type = json != null ? json.type : blink.type;
                    if (blink.type != 3) {
                        int score = json.gems;
                        totalGems += score;
                    }
                }
                index++;
            }
        }

        return totalGems;

    }

    public long calculatesSkype(JSONObject json) {
        return 1;
    }

    public int getTotalStepInBurst(int index) {
        int count = getStepInBurst(index);
        if (blinkList == null || index >= blinkList.size() - 1) {
            return count;
        }
        for (int i = index + 1; i < blinkList.size(); i++) {
            Blink blink = blinkList.get(i);
            if ((blink.type == BlinkType.KBlinkTypeSpecificContentBurstEnd || blink.type == BlinkType.KBlinkTypeSpecificContentLearningUnitEnd)) {
                break;
            }
            count++;
        }
        return count < blinkList.size() - 1 ? count : 0;
    }

    public int getStepInBurst(int index) {
        if (blinkList == null || index < 0 || index >= blinkList.size()) {
            return 0;
        }
        int count = 0;
        for (int i = index; i >= 0; i--) {
            Blink blink = blinkList.get(i);
            if ((blink.type == BlinkType.KBlinkBurstStart || blink.type == BlinkType.KBlinkLearningUnitStart)) {
                return count;
            }
            count++;
        }
        return count;
    }

    public int getAllBurstInCourse() {
        if (blinkList == null || blinkList.size() == 0) {
            return 0;
        }
        int count = 0;
        for (Blink blink : blinkList) {
            if (blink.type == BlinkType.KBlinkBurstStart) {
                count++;
            }
        }
        return count;
    }

    public int getBurstIndexInCourse(int blinkID) {
        if (blinkList == null) {
            return -1;
        }
        int startNumber = (blinkID >= blinkList.size()) ? blinkID - 1 : blinkID;
        int index = 0;
        for (int i = startNumber; i >= 0; i--) {
//            NSDictionary * dict =[blinks objectAtIndex:i];
            final Blink blink = blinkList.get(i);
//            int type =bli[[dict objectForKey:@ "type"]intValue];
            if (blink.type == BlinkType.KBlinkBurstStart) index++;
        }
        return index;
    }

    public List<Blink> getArrayActivitiesStart() {
        final List<Blink> growth = growths;
        if (growth == null || growth.size() == 0) {
            return null;
        }

        List<Blink> result = new ArrayList<Blink>();
        for (int i = 0; i < growth.size(); i++) {
            final Blink blink = growth.get(i);
            if (blink.type == BlinkType.KBlinkBurstStart) {
                result.add(blink);
            }
        }
        return result;
    }

    public List<Blink> getArrayUnlockedActivities(Course course) {
        final List<Blink> growth = growths;
        if (growth == null || growth.size() == 0) {
            return null;
        }

        final String unlockActivityIds = localSharedPreferences.unlockActivityIds(localSharedPreferences.userId().getOr(0l), course).getOr(null);
        if (TextUtils.isEmpty(unlockActivityIds)) {
            return null;
        }

        List<Blink> result = new ArrayList<Blink>();
        for (int i = 0; i < growth.size(); i++) {
            final Blink blink = growth.get(i);
            if (unlockActivityIds.contains(String.valueOf(blink.id))) {
                result.add(blink);
            }
        }
        return result;
    }

    public int calculatesCompletedActivities(Course course) {
        final Long userId = localSharedPreferences.userId().getOr(0l);
        final Splitter splitter = Splitter.on(",").omitEmptyStrings();
        final String completedIds = localSharedPreferences.completedActivityIds(userId, course).getOr(null);
        return TextUtils.isEmpty(completedIds) ? 0 : splitter.splitToList(completedIds).size();
    }

    public int calculatesUnlockedActivities(Course course) {

        final Long userId = localSharedPreferences.userId().getOr(0l);
        if (userId == 0l) {
            return 0;
        }

        final String unlockedIds = localSharedPreferences.unlockActivityIds(userId, course).getOr(null);
        if (unlockedIds == null) {
            return 0;
        }
        int unlockedCount = 0;
        final List<Blink> arrayActivitiesStart = getArrayActivitiesStart();
        if (arrayActivitiesStart != null && arrayActivitiesStart.size() > 0) {
            for (Blink blink : arrayActivitiesStart) {
                if (unlockedIds.contains(String.valueOf(blink.id))) {
                    unlockedCount++;
                }
            }
        }
        return unlockedCount;
    }

    public String loadUnlockActivityIds(int blinkIdx) {
        if (blinkList == null || blinkList.size() == 0 || blinkList.size() < blinkIdx || blinkIdx < 0) {
            return null;
        }
        int blinkId = blinkIdx - 1;
        String guid = "";
        int type = 0;
        StringBuilder result = new StringBuilder("");
        while (blinkId < blinkList.size() && blinkId > 0) {
//            NSDictionary * item =[blkNavControllerRef.blinks objectAtIndex:blinkId];
            final Blink item = blinkList.get(blinkId);
            type = item.type;//[[item objectForKey:@ "type"]intValue];
            Timber.d("loadUnlockActivityIds => blinkId: %d, type: %d", blinkId, type);
            if (type == 3) {
//                String strJson =[item objectForKey:@ "json"];
                final Json json = item.loadJsonToObject();
//                NSDictionary * json =[blkNavControllerRef dictionaryFromString:strJson];

                guid = json.unlockActivity;//un[json objectForKey:@ "unlockActivity"];
                Timber.d("loadUnlockActivityIds => guid: %s", guid);

                if (!TextUtils.isEmpty(guid)) {
                    /* check this burst having the activities or not to unlock */
                    for (Blink blink : growths) {
                        if (guid.equals(blink.guid)) {
                            result.append(blink.id);
                            result.append(",");
                        }
                    }
                }
            }

            blinkId--;
        }
        return result.toString();
    }

    public Blink getGA(int blinkIndexId) {
        return growths.get(blinkIndexId);
    }

    public int getGAIndex(Course course, int index) {
        Timber.d("BlkNavController showGABlink:%d", index);
//        UIViewController *vc;
//        [timer hideTimer];
//        timerDelegate = nil;
//
//        bottomBar.growing_activity = YES;

        if (index >= growths.size()) {
            Timber.d("Course complete");
//        vc = [[BlkCourseCompleteVC alloc] initWithNibName:@"BlkCourseCompleteVC" bundle:nil];
            return -1;
        } else {
            final Long userId = localSharedPreferences.userId().getOr(0l);

            do {
                Blink blink = growths.get(index);//[self.growing_activities objectAtIndex:index];
                int type = blink.type;//[[blink objectForKey:@"type" ] intValue];
                switch (type) {
                    case 1://Learning unit start
                    case 2://Learning unit end
                    case 4://Burst end
                    {

                        // get the first blink /
//                        NSUserDefaults *prefs   =   [NSUserDefaults standardUserDefaults];
                        if (type == 4) {
                            String completedIds = localSharedPreferences.completedActivityIds(userId, course).getOr(null);
                            int startIndex = index;
                            Blink dict = growths.get(startIndex);
                            int checkType = dict.type;//[[dict objectForKey:@"type"] intValue];
                            do {
                                if (checkType == 3) {
                                    break;
                                }
                                startIndex--;
                                dict = growths.get(startIndex);
                                checkType = dict.type;//[[dict objectForKey:@"type"] intValue];
                            } while (checkType != 3 && startIndex >= 0);
                            if (completedIds == null || !completedIds.contains(String.valueOf(dict.id))) {
                                completedIds = completedIds == null ? String.valueOf(dict.id) : completedIds + "," + dict.id;
                                localSharedPreferences.completedActivityIds(userId, course).put(completedIds);
                                Timber.d("getGAIndex => %s", completedIds);
                            }
                            return -1;
                        }

                        index++;
                        if (index >= growths.size()) {
                            Timber.d("Activities complete");
                            return -1;
                        }


                        break;
                    }
                    case 3:
                    case 5:
                    case 6:
                    case 7:
                    case 9:
                    case 10:
                    case 14:
                        return index;
                    default:
                        return -2;
                }
            } while (true);
        }

//        vc.title =[NSString stringWithFormat:@ "Blink %d", index];
//        [self pushViewController:vc animated:YES];
//
//        if (newBurst)[self setGABurstInfo:index];
//
//        [topBar setCurrentBlink:index];
////    if (index<=firstGAIdx) [bottomBar setMode:_NONE];
//
//        gaId = index;
//
//        [self playSfx:@ "Next_Page"];
    }

//    public int getGAIndex(Course course, int index) {
//        Timber.d("BlkNavController showGABlink:%d", index);
////        UIViewController *vc;
//        boolean newBurst = false;
//        if (index >= growths.size()) {
//            return -1;
//        } else {
//            do {
////                NSDictionary *blink = [self.growing_activities objectAtIndex:index];
//                final Blink blink = growths.get(index);
//                int type = blink.type;//[[blink objectForKey:@"type" ] intValue];
//                switch (type) {
//                    case 1://Learning unit start
//                    case 2://Learning unit end
//                    case 3://Burst start
//                    case 4://Burst end
//                    case 5:
//                    /* get the first blink */
////                        NSUserDefaults *prefs   =   [NSUserDefaults standardUserDefaults];
//                        int previousIndex = index - 1;
//                        boolean isNeedToUpdate = false;
//
//                        final Long userId = localSharedPreferences.userId().getOr(0l);
//                        if (previousIndex > 0) {
//                        /* find the start burst */
//                            int prevType;
//                            do {
//
////                                NSDictionary *dict  =   [growing_activities objectAtIndex:previousIndex];
//                                final Blink dict = growths.get(previousIndex);
//                                prevType = dict.type;//[[dict objectForKey:@"type"] intValue];
//                                if (prevType == 3) {
//                                    String guid = dict.guid;//[dict objectForKey:@"guid"];
//                                    String keyCheck = localSharedPreferences.completedActivityIds(userId, course).getOr(null);
//                                    boolean unlocked = keyCheck == null ? false : keyCheck.contains(String.valueOf(dict.id));
//                                    if (!unlocked) {
//                                        isNeedToUpdate = true;
////                                        [prefs setBool:YES forKey:keyCheck];
//                                        localSharedPreferences.unlockedActivity(userId, course, guid).put(true);
//                                        break;
//                                    }
//                                }
//
//                                previousIndex--;
//
//                            } while (prevType != 3 && previousIndex >= 0);
//                        }
//
//                        index++;
//                        if (index >= growths.size()) {
//                            Timber.d("Activities complete");
//                            if (isNeedToUpdate) {
////                            NSString *key   =   [NSString stringWithFormat:COMPLETED_ACTIVITIES_NUMBER, [self getSetting:@"email"], [self getCourseId], [self getTypeOfCourse]];
//                                String completedActivityIds = localSharedPreferences.completedActivityIds(userId, course).getOr(null);
//                                if (TextUtils.isEmpty(completedActivityIds)) {
//                                    completedActivityIds = String.format("%d,", blink.id);
////                                int number =[[prefs objectForKey:key]intValue];
////                                [prefs setObject:[NSNumber numberWithInt:number + 1]forKey:
////                                key];
//                                } else {
//                                    completedActivityIds = String.format("%s,%d", completedActivityIds, blink.id);
//                                }
//                                localSharedPreferences.completedActivityIds(userId, course).put(completedActivityIds);
//                            }
//                            return -1;
//                        }
//                        break;
//                    case 6:
//                    case 7:
//                    case 9:
//                    case 10:
//                    case 14:
//                        return index;
//                    default:
//                        return -2;
//                }
//            } while (true);
//        }
//
//        vc.title =[NSString stringWithFormat:@ "Blink %d", index];
//        [self pushViewController:vc animated:YES];
//
//        if (newBurst)[self setGABurstInfo:index];
//        [topBar setCurrentBlink:index];
//        if (index <= firstGAIdx)[bottomBar setMode:_NONE];
//
//        gaId = index;
//
//        [self playSfx:@ "Next_Page"];

//    }

    public int getMaxIndex() {
        return blinkList == null ? 0 : blinkList.size();
    }

}
