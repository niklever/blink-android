package com.blinktrainingsystem.blink.module.main;

import android.os.Bundle;

import com.blinktrainingsystem.blink.data.model.pojo.Course;

/**
 * Created by TALE on 9/8/2014.
 */
public interface MainPresenter {

    void onCreate(Bundle savedInstanceState);

    void onLoginSuccess();

    void onRegisterRequest();

    void backToLoginFromRegister();

    void onRegisterSuccess(boolean back);

    void onLogout();

    void openCourseDetail(Course course);

    void backToCourses();

    void openPlayCourse(Course course);

}
