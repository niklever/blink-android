package com.blinktrainingsystem.blink.module.courses;

import android.app.Application;
import android.text.TextUtils;

import com.blinktrainingsystem.blink.common.dialog.ConfirmDialogFragment;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Category;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.FileUtils;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import java.io.File;
import java.util.List;
import java.util.Map;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by TALE on 9/10/2014.
 */
public class CoursesPresenter implements CoursesUseCase {

    final CoursesView coursesView;

    final MainPresenter mainPresenter;

    final TaskManager taskManager;

    final DeviceInfo deviceInfo;

    final Application application;

    final LocalSharedPreferences localSharedPreferences;

    final LocalFileManager localFileManager;

    final CoursesRepo coursesRepo;
    private final TrackerHelper trackerHelper;

    Category category;

    private Subscription subscription;

    public CoursesPresenter(CoursesView coursesView, MainPresenter mainPresenter, Application application, LocalSharedPreferences localSharedPreferences, LocalFileManager localFileManager, TaskManager taskManager, DeviceInfo deviceInfo, CoursesRepo coursesRepo, TrackerHelper trackerHelper) {
        this.coursesView = coursesView;
        this.mainPresenter = mainPresenter;
        this.localFileManager = localFileManager;
        this.taskManager = taskManager;
        this.deviceInfo = deviceInfo;
        this.application = application;
        this.localSharedPreferences = localSharedPreferences;
        this.coursesRepo = coursesRepo;
        this.trackerHelper = trackerHelper;
    }

    @Override
    public void refreshCourses() {
        loadCourses(category);
    }

    @Override
    public void logout() {
        long userId = localSharedPreferences.userId().getOr(0l);
        if (userId!=0l) {
            coursesView.showConfirmLogoutDialog(
                    new ConfirmDialogFragment.ConfirmDialogListener() {
                        @Override
                        public void onPositiveClick() {
                            localSharedPreferences.userId().remove();
                            coursesRepo.deleteAll();
                            mainPresenter.onLogout();
                        }
                        @Override
                        public void onNegativeClick() {

                        }
                    }
            );
        }else {
            coursesRepo.deleteAll();
            mainPresenter.onLogout();
        }
    }

    @Override
    public void downloadCourse(Course course) {

    }

    @Override
    public void openCourse(Course course) {
        mainPresenter.openCourseDetail(course);
    }

    @Override
    public void playCourse(Course course) {
        if (!TextUtils.isEmpty(course.title)) {
            mainPresenter.openPlayCourse(course);
        }
    }


    @Override
    public void viewMyCourse() {
        loadCourses(Category.user);
    }

    @Override
    public void viewAllCourse() {
        loadCourses(Category.all);
    }

    @Override
    public void viewUnits() {
        loadCourses(Category.unit);
    }

    @Override
    public void cancelLoading() {
        unsubscribe();
    }

    @Override
    public void resetCourse(Course course) {
        /* get all key save on preferences */
        long userId = this.localSharedPreferences.userId().getOr(01l);
        Map<String, ?> allKeys = this.localSharedPreferences.getAllKeys();
        for (Map.Entry<String, ?> entry : allKeys.entrySet()) {
            String key = entry.getKey();
            if (key.contains(String.valueOf(course.id)) && (key.contains(String.valueOf(userId)) || key.contains("isHelpShown"))) {
                this.localSharedPreferences.removeKey(key);
            }
        }
    }

    @Override
    public void removeCourse(Course course) {
        resetCourse(course);
        final File file = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.get(course.folderName));
        Timber.d("removeCourse for course's id %d, path %s", course.id, file.getAbsolutePath());
        FileUtils.rm(file);
        refreshCourses();
    }

    @Override
    public void updateCourses() {
        coursesView.showCourses(coursesRepo.getAllLocal());
    }

    private void loadCourses(final Category category) {
        this.category = category;
        if (category == Category.all) {
            final List<Course> courses = coursesRepo.getAllLocal();
            if (courses == null) {
                coursesView.showProgress();
            } else {
                coursesView.showCourses(courses);
            }
            subscription = coursesRepo.getAllRemote(localSharedPreferences.userId().getOr(0l))
                    .map(
                            new Func1<List<Course>, List<Course>>() {
                                @Override
                                public List<Course> call(List<Course> courses) {
                                    final List<Course> downloadings = coursesRepo.getDownloadings();
                                    if (courses == null) {
                                        return downloadings;
                                    } else if (downloadings != null) {
                                        courses.addAll(downloadings);
                                    }
                                    return courses;
                                }
                            }
                    )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Observer<List<Course>>() {
                                @Override
                                public void onCompleted() {
                                    Timber.d("onCompleted for category %s", category);
                                    coursesView.hideProgress();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    coursesView.showError("Can not get data at this time. Please try again later.");
                                    coursesView.hideProgress();
                                }

                                @Override
                                public void onNext(List<Course> courses) {
                                    coursesView.showCourses(courses);
                                    trackerHelper.coursesListingLoad();
                                }
                            }
                    );
        } else if (category == Category.unit) {
            final List<Course> courses = coursesRepo.getUnitLocal();
            if (courses == null) {
                coursesView.showProgress();
            } else {
                coursesView.showCourses(courses);
            }
            subscription = coursesRepo.getUnitsRemote(localSharedPreferences.userId().getOr(0l))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Observer<List<Course>>() {
                                @Override
                                public void onCompleted() {
                                    Timber.d("onCompleted for category %s", category);
                                    coursesView.hideProgress();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    coursesView.showError("Can not get data at this time. Please try again later.");
                                    coursesView.hideProgress();
                                }

                                @Override
                                public void onNext(List<Course> courses) {
                                    coursesView.showCourses(courses);
                                }
                            }
                    );
        } else {
            unsubscribe();
            coursesView.showCourses(coursesRepo.getUserCourses());
            trackerHelper.myCoursesLoad();
        }
    }

    private void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

}
