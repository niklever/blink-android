package com.blinktrainingsystem.blink.module.playcourse.draganddrop;

import android.animation.Animator;
import android.app.Application;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.DragDrop;
import com.blinktrainingsystem.blink.data.model.pojo.Goal;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.handler.DragDropHandler;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ImageLoader;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class DragAndDropFragment extends BaseBurstFragment {

    public final Random random = new Random();
    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.llGoals)
    LinearLayout llGoals;

    @InjectView(R.id.llItems)
    LinearLayout llItems;
    @InjectView(R.id.scroller)
    ScrollView mScroller;
    @InjectView(R.id.rlRoot)
    RelativeLayout rlRoot;

    private JsonEx json;

    @Inject
    Application application;
    @Inject
    LocalFileManager localFileManager;
    @Inject
    Picasso picasso;
    @Inject TrackerHelper trackerHelper;

    private DragDropHandler dragDropHandler;
    private Goal selectedItem;
    private View selectedView;

    @Inject
    ViewAnimator viewAnimator;
    private boolean isEntered;
    ;
    private boolean timeout;
    private boolean nextOnClosed;
    private int itemHeight;
    private int minHeight;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drag_drop, container, false);
    }

    private ImageView newGoalImageView(int cellwidth, String imagePath) {
        ImageView image = new ImageView(getActivity());
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        final FrameLayout.LayoutParams layoutParams;//
        if (isTablet) {
            layoutParams = new FrameLayout.LayoutParams(cellwidth, ViewGroup.LayoutParams.MATCH_PARENT);
        } else {
            layoutParams = new FrameLayout.LayoutParams(cellwidth, ViewGroup.LayoutParams.MATCH_PARENT);
        }

        layoutParams.gravity = Gravity.CENTER;
        image.setLayoutParams(layoutParams);
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        File f = getLocalFile(imagePath);
        if (f != null && f.exists() && f.isFile()) {
            picasso.load(f).into(image);
        }
        return image;
    }

    @Inject
    UiHelper uiHelper;

    private TextView newGoalTextView(String text, int cellwidth) {
        TextView lbl = new TextView(getActivity());
        lbl.setTextColor(Color.WHITE);
//        if (System.currentTimeMillis() % 2 == 0) {
//            CompatibleHelper.setBackgroundDrawable(lbl, uiHelper.createRoundedDrawable(Color.parseColor("#E19933"), 6.0f));
//        } else {
//            CompatibleHelper.setBackgroundDrawable(lbl, uiHelper.createRoundedDrawable(Color.parseColor("#4891FF"), 6.0f));
//        }
        Util.setTypeface(lbl, getString(R.string.font_frabk));
        lbl.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
        lbl.setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(cellwidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        lbl.setLayoutParams(layoutParams);
        text = TextUtils.isEmpty(text) ? "" : Helper.getStringWithReplacements(text);
        lbl.setText(text);
        return lbl;
    }

    private ImageView newGoalImage(String text, int cellwidth) {
        final int padding = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.padding_newgoal));// : 15;
        ImageView lbl = new ImageView(getActivity());
//        lbl.setTextColor(Color.WHITE);
        if (random.nextInt() % 2 == 0) {
            CompatibleHelper.setBackgroundDrawable(lbl, uiHelper.createRoundedDrawable(Color.parseColor("#E19933"), 6.0f));
        } else {
            CompatibleHelper.setBackgroundDrawable(lbl, uiHelper.createRoundedDrawable(Color.parseColor("#4891FF"), 6.0f));
        }
//        Util.setTypeface(lbl, getString(R.string.font_frabk));
//        lbl.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
//        lbl.setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(cellwidth, minHeight);
        layoutParams.gravity = Gravity.CENTER;
        lbl.setLayoutParams(layoutParams);

        return lbl;
    }

    private ImageView newItemImageView(String imagePath) {
        ImageView image = new ImageView(getActivity());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        image.setLayoutParams(layoutParams);
        return image;
    }

    private TextView newItemTextView(String text) {
        TextView lbl = new TextView(getActivity());
        lbl.setTextColor(Color.BLACK);
        CompatibleHelper.setBackgroundDrawable(lbl, uiHelper.createRoundedDrawable(Color.parseColor("#FFCC99"), 6.0f));
        Util.setTypeface(lbl, getString(R.string.font_myriad_pro_regular));
        lbl.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
        int padding = deviceInfo.getDimensionPixel(4);
        lbl.setPadding(padding, padding, padding, padding);
        lbl.setGravity(Gravity.CENTER);
        lbl.setLayoutParams(new ViewGroup.LayoutParams(0, itemHeight));
        text = TextUtils.isEmpty(text) ? "" : Helper.getStringWithReplacements(text);
        lbl.setText(text);
        return lbl;
    }

    View droppedView;

    private void setupGoalView(final View goalView) {
        goalView.setOnDragListener(
                new View.OnDragListener() {
                    public boolean onDrag(View v, DragEvent event) {
                        final int action = event.getAction();
                        switch (action) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                isEntered = false;
                                droppedView = null;
                                return true;
                            case DragEvent.ACTION_DRAG_ENDED:
                                Timber.d("ACTION_DRAG_ENDED %s", droppedView == v);
                                if (droppedView == null) {
                                    selectedView.setVisibility(View.VISIBLE);
                                } else if (droppedView == v) {
                                    if (isEntered) {
                                        postMainThread(
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        submitAnswer((Goal) droppedView.getTag(), selectedItem);
                                                    }
                                                }
                                        );
                                    } else {
                                        Timber.d("VISIBLE");
                                        selectedView.setVisibility(View.VISIBLE);
                                    }
                                }
                                return true;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                Timber.d("ACTION_DRAG_ENTERED");
                                isEntered = true;
                                droppedView = v;
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                if (droppedView == v) {
                                    isEntered = false;
                                    droppedView = null;
                                }
                                Timber.d("ACTION_DRAG_EXITED");
                                break;
                        }
                        return false;
                    }
                }
        );
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        mScroller.setOnDragListener(
                new View.OnDragListener() {
                    @Override
                    public boolean onDrag(View v, DragEvent event) {
                        final int action = event.getAction();
                        switch (action) {
                            case DragEvent.ACTION_DRAG_STARTED:
                                return true;
                            case DragEvent.ACTION_DRAG_ENDED:
                                Timber.d("mScroller: ACTION_DRAG_ENDED %s", droppedView == v);
                                return true;
                            case DragEvent.ACTION_DRAG_ENTERED:
                                Timber.d("mScroller: ACTION_DRAG_ENTERED");
//                        mScroller.smoothScrollTo(0, llGoals.getBottom());
                                break;
                            case DragEvent.ACTION_DRAG_EXITED:
                                Timber.d("mScroller: ACTION_DRAG_EXITED");
                                break;
                            case DragEvent.ACTION_DRAG_LOCATION:
                                float y = event.getY();
                                if (selectedView.getHeight() / 2 + y >= mScroller.getHeight()) {
                                    mScroller.smoothScrollTo(0, llGoals.getBottom());
                                }
                                break;
                        }
                        return false;
                    }
                }
        );
        final Blink blink = blinkHandler.getByBlinkIndex(blinkIndex);
        json = blink.loadJsonToObject();
        dragDropHandler = new DragDropHandler(blink);

        if (!TextUtils.isEmpty(dragDropHandler.getImage())) {
            final File courseFile = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.full);
//            picasso.load(new File(courseFile, dragDropHandler.getImage())).into(ivImage);
        }
        tvTitle.setText(Helper.getStringWithReplacements(dragDropHandler.getPrompt()));
        tvTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        Util.setTypeface(tvTitle, getString(R.string.font_myriad_pro_regular));
        ViewObservableHelper.globalLayoutFrom(llGoals)
                .subscribe(
                        new Action1<View>() {
                            @Override
                            public void call(View view) {
                                initViews();
                            }
                        }
                );

        trackerHelper.dragNDropLoad();
    }

    @Override
    protected int getTimer() {
        return dragDropHandler == null ? 0 : dragDropHandler.getTimer();
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.drad_and_drop_help);
    }

    @Override
    protected String getTitle() {
        return "";
    }

    //    @InjectView(R.id.content_vw)
//    FrameLayout content_vw;
    @Inject
    DeviceInfo deviceInfo;
    DragDrop data;

    private int calculateItemHeight() {
        int count = data.goals.size();
        int padding = 15;
        int rows = 0;

        if (count < 3) {
            rows = 1;
            padding = 30;// : 15;
        } else if (count < 7) {
            rows = 2;
            padding = 25;// : 8;
        } else if (count < 13) {
            rows = 3;
            padding = 20;// : 3;
        }
        int cols = 1;
        count = data.items.size();
        if (count < 2) {
            cols = 1;
        } else if (count < 5) {
            cols = 2;
        } else if (count < 10) {
            cols = 3;
        }
        rows += count / cols + (count % cols != 0 ? 1 : 0);

        return (getView().getHeight() - tvTitle.getBottom()) / rows;
    }

    private void initViews() {
        data = json == null ? null : json.dragdrop;
        if (data == null) return;
        final List<Goal> goals = data.goals;
        int padding = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.padding_dragdrop));// : 15;
        minHeight = (mScroller.getHeight() - 3 * padding) / 3;
        int width = llGoals.getWidth() / goals.size();
        int cellwidth;

        final int maxWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.max_width_dragdrop));// : 100;
        int maxLargeWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.max_large_width_dragdrop));// : 200;
        if (width - padding > maxLargeWidth) {
            cellwidth = maxLargeWidth;
        } else if (width - padding > maxWidth) {
            cellwidth = maxWidth;
        } else {
            cellwidth = width - padding;
        }

        int count = goals == null ? 0 : goals.size();
        final List<Goal> items = data.items;
        Collections.shuffle(items);
        int cols = 4;
        count = items.size();
        if (count < 2) {
            cols = 1;
        } else if (count < 5) {
            cols = 2;
        } else if (count < 10) {
            cols = 3;
        }
//
        llItems.removeAllViews();
        int llItemsWidth = llItems.getWidth();
        width = llItemsWidth / cols;
        cellwidth = width - padding;
        if (cellwidth > maxWidth && cols > 1) cellwidth = maxWidth;//Max width
        if (cellwidth > maxLargeWidth)
            cellwidth = maxLargeWidth;//Max width for single column layout
        final String longestItem = getLongestItems(data.items);
        itemHeight = uiHelper.measureTextViewHeightForText(getActivity(), longestItem, getResources().getDimensionPixelSize(R.dimen.question_font_size), cellwidth) + deviceInfo.getDimensionPixel(8);
        if (itemHeight < minHeight) {
            itemHeight = minHeight;
        } else {
            minHeight = itemHeight;
        }
        llGoals.removeAllViews();
        llGoals.setPadding(0, padding, 0, 0);
        LinearLayout rowView = null;
        for (int i = 0; i < goals.size(); i++) {
            if (i % 2 == 0) {
                rowView = newRowView();
//                if (getResources().getBoolean(R.bool.is_full_hd)) {
//                    rowView.setMinimumHeight(deviceInfo.getDimensionPixel(120));
//                }
                llGoals.addView(rowView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                final LinearLayout.LayoutParams layoutParams = ((LinearLayout.LayoutParams) rowView.getLayoutParams());
                layoutParams.weight = 1;
                if (i > 0) {
                    layoutParams.topMargin = padding;
                }
            }
            final Goal goal = goals.get(i);
            goal.goal = i;
            final View goalView;
            if (goal.isImage) {
                goalView = newGoalImageView(cellwidth, goal.image);
                rowView.addView(goalView);
                goalView.setTag(goal);
                setupGoalView(goalView);
            } else {
                Timber.e("text: %s, goal: %d", goal.text, goal.goal);
                FrameLayout frameLayout = new FrameLayout(getActivity());

                View view = newGoalTextView(goal.text, cellwidth);
                view.setMinimumHeight(minHeight);
                View viewimagetemp = newGoalImage(goal.text, cellwidth);
//                viewimagetemp.setTag(goal);
                frameLayout.addView(viewimagetemp);
                ((FrameLayout.LayoutParams) viewimagetemp.getLayoutParams()).gravity = Gravity.CENTER;
//                rowView.addView(viewimagetemp);
                frameLayout.setTag(goal);
//                rowView.addView(view);
                frameLayout.addView(view);
                ((FrameLayout.LayoutParams) view.getLayoutParams()).gravity = Gravity.CENTER;
                rowView.addView(frameLayout, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                goalView = frameLayout;
                setupGoalView(goalView);
            }
            final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) goalView.getLayoutParams();
            if (rowView.getChildCount() > 1) {
                layoutParams.leftMargin = padding;
            }
        }

        int parentPadding = (llItemsWidth - cellwidth * cols - padding * (cols + 1)) / 2;
        int col = 0;
        LinearLayout llRow = null;
        llItems.setPadding(parentPadding, 0, parentPadding, padding);

        for (int i = 0; i < items.size(); i++) {
            if (i % cols == 0) {
                llRow = newRowView();
                llItems.addView(llRow);
                final LinearLayout.LayoutParams layoutParams = ((LinearLayout.LayoutParams) llRow.getLayoutParams());
                layoutParams.weight = 1;
                if (i > 0) {
                    layoutParams.topMargin = padding;
                }
            }
            final Goal goal = items.get(i);
            final View view;
            if (goal.isImage) {
                view = newItemImageView(goal.image);
            } else {
                view = newItemTextView(goal.text);
            }
            col++;
            if (col == cols) {
                col = 0;
            }
            view.setTag(goal);
            llRow.addView(view);
            final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
            if (llRow.getChildCount() > 1) {
                layoutParams.leftMargin = padding;
            }

            layoutParams.width = goal.isImage ? (int) (cellwidth * 0.8f) : cellwidth;
            configureDragDrop(view, goal);
            final boolean onleft = (col <= (cols / 2));
            view.setVisibility(View.INVISIBLE);
            final long delay = Constant.ANIMATION_DURATION * i / 10;

            if (goal.isImage) {
                final int finalCellwidth = (int) (cellwidth * 0.8f);
                final Subscription subscription = mImageLoader.size(getLocalPath(goal.image))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<int[]>() {
                                    @Override
                                    public void call(int[] size) {
                                        final int finalHeight = finalCellwidth * size[1] / size[0];
                                        File f = getLocalFile(goal.image);
                                        if (f != null && f.exists() && f.isFile()) {
                                            picasso.load(f).resize(finalCellwidth, finalHeight).into(
                                                    ((ImageView) view), new Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            animEnter(view, onleft, delay);
                                                        }

                                                        @Override
                                                        public void onError() {

                                                        }
                                                    }
                                            );
                                        }
                                    }
                                }
                        );
                takeCareSubscription(subscription);
            } else {
                ViewObservableHelper.globalLayoutFrom(view)
                        .subscribe(
                                new Action1<View>() {
                                    @Override
                                    public void call(final View view) {
                                        animEnter(view, onleft, delay);
                                    }
                                }
                        );
            }

        }

    }

    @Inject
    ImageLoader mImageLoader;

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void animEnter(final View view, final boolean onleft, long delay) {
        mHandler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        Timber.d("onleft: %s", onleft);
                        if (onleft) {
                            viewAnimator.enterLeft(view, Constant.ANIMATION_DURATION);
                        } else
                            viewAnimator.enterRight(view, Constant.ANIMATION_DURATION);
                    }
                }, delay
        );
    }

    private String getLongestItems(List<Goal> items) {
        if (items != null && items.size() > 0) {
            String longestItem = "";
            for (Goal item : items) {
                final int length = item == null || item.text == null ? 0 : item.text.length();
                if (length > longestItem.length()) {
                    longestItem = item.text;
                }
            }
            return longestItem;
        }
        return null;
    }

    private LinearLayout newRowView() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return linearLayout;
    }

    private void startDelay(final Animator animator, int index) {
        postMainThread(
                new Runnable() {
                    @Override
                    public void run() {
                        Timber.d("animator.start();");
                        animator.start();
                    }
                }, Constant.ANIMATION_DURATION * index / 10
        );
    }

    private void configureDragDrop(final View view, final Goal goal) {
//        view.setOnTouchListener(new View.OnTouchListener() {
//            // The ‘active pointer’ is the one currently moving our object.
//            private int mActivePointerId = INVALID_POINTER_ID;
//            private float startX = 0;
//            private float startY = 0;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent ev) {
//
//                final int action = MotionEventCompat.getActionMasked(ev);
//
//                switch (action) {
//                    case MotionEvent.ACTION_DOWN: {
//                        final int pointerIndex = MotionEventCompat.getActionIndex(ev);
//                        startX = MotionEventCompat.getX(ev, pointerIndex);
//                        startY = MotionEventCompat.getY(ev, pointerIndex);
//                        // Save the ID of this pointer (for dragging)
//                        mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
//                        break;
//                    }
//
//                    case MotionEvent.ACTION_MOVE: {
//                        // Find the index of the active pointer and fetch its position
//                        final int pointerIndex =
//                                MotionEventCompat.findPointerIndex(ev, mActivePointerId);
//
//                        final float x = MotionEventCompat.getX(ev, pointerIndex);
//                        final float y = MotionEventCompat.getY(ev, pointerIndex);
//
//                        // Calculate the distance moved
//                        final float dx = x - startX;
//                        final float dy = y - startY;
//
//                        if (dx > 50 || dy > 50) {
//                            startDrag(view, goal);
//                            return true;
//                        }
//
//                        break;
//                    }
//
//                    case MotionEvent.ACTION_UP: {
//                        mActivePointerId = INVALID_POINTER_ID;
//                        break;
//                    }
//
//                    case MotionEvent.ACTION_CANCEL: {
//                        mActivePointerId = INVALID_POINTER_ID;
//                        break;
//                    }
//
//                    case MotionEvent.ACTION_POINTER_UP: {
//
//                        final int pointerIndex = MotionEventCompat.getActionIndex(ev);
//                        final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);
//
//                        if (pointerId == mActivePointerId) {
//                            // This was our active pointer going up. Choose a new
//                            // active pointer and adjust accordingly.
//                            final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
//                            mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
//                        }
//                        break;
//                    }
//                }
//                return true;
//            }
//        });

        ViewObservableHelper.longClick(view).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        Timber.d("onDrag");
                        selectedItem = goal;
                        view.setVisibility(View.INVISIBLE);
                        selectedView = view;
                        view.startDrag(null, new View.DragShadowBuilder(view), null, 0);
                    }
                }
        );
    }

    private void startDrag(View view, final Goal goal) {
        selectedItem = goal;
        view.setVisibility(View.INVISIBLE);
        selectedView = view;
        view.startDrag(null, new View.DragShadowBuilder(view), null, 0);
    }

    private void submitAnswer(Goal dropped, Goal answer) {
        Timber.d("submitAnswer ==> dropped{text:%s, goal:%d}, answer{text:%s, goal:%d}", dropped.text, dropped.goal, answer.text, answer.goal);
        if (dragDropHandler.answer(answer.goal == dropped.goal + 1)) {
            playRawSound(R.raw.correct);
            final String feedback = answer == null || answer.feedback == null || answer.feedback.length < 1 ? null : answer.feedback[0];
            if (!TextUtils.isEmpty(feedback)) {
                showHelp(feedback);
            } else if (dragDropHandler.isCompleted()) {
                blinkComplete();
            }
        } else {
            playRawSound(R.raw.incorrect);
            selectedView.setVisibility(View.VISIBLE);
            if (dragDropHandler.getIncorrectCount() >= 3) {
                blinkComplete();
            } else {
                final String feedback = answer == null || answer.feedback == null || answer.feedback.length < 2 ? null : answer.feedback[1];
                if (!TextUtils.isEmpty(feedback)) {
                    showHelp(answer.feedback[1]);
                } else if (dragDropHandler.isCompleted()) {
                    blinkComplete();
                }
            }
        }
    }

    private void showHelp(String msg) {
        busDriver.pauseTimer();
        if (TextUtils.isEmpty(msg)) {
            busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        } else {
            busDriver.showHelp(msg);
        }
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                } else if (nextOnClosed) {
                    busDriver.notifyBlinkCompleted();
                } else if (dragDropHandler.isCompleted() || timeout) {
                    blinkComplete();
                } else {
                    busDriver.startTimer();
                }
                break;
            case TimeOut:
                timedOut();
                break;
        }
    }

    void timedOut() {
        int score = json.score;
        int gems = json.gems;
//        if (user_score > 0) blinkHandler.updateScore(score, blinkIndex, course.id, gems);
//        busDriver.updateScore(blinkHandler);
        showHelp(getString(R.string.time_out_message));
        timeout = true;
    }

    private void blinkComplete() {
        busDriver.pauseTimer();
        int score = json.score;
        int gems = json.gems;
        int user_score;
        boolean feedbackShown = false;
        int incorrectCount = dragDropHandler.getIncorrectCount();
        Timber.d("blinkComplete => score: %d, gems: %d, incorrectCount: %d", score, gems, incorrectCount);
        String str;
        if (incorrectCount <= 1) {
            user_score = (incorrectCount == 0) ? score : (int) (score * 0.75);
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.correct);
            str = json.feedback == null || json.feedback.length == 0 ? null : json.feedback[0];
            if (str != null) {
                showHelp(str);
                feedbackShown = true;
            } else if (gems > 0) {
                busDriver.updateScore(blinkHandler);
            }
        } else {
            user_score = (incorrectCount == 2) ? score / 2 : (int) (score * 0.25);
            if (incorrectCount > 3) user_score = 0;
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.incorrect);
            str = json.feedback == null || json.feedback.length < 2 ? null : json.feedback[1];
            if (str != null) {
                showHelp(str);
                feedbackShown = true;
            }
        }

        if (feedbackShown) {
            nextOnClosed = true;
        } else {
            busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        }
    }


}
