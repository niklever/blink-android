package com.blinktrainingsystem.blink.module.coursedetail;

/**
 * Created by TALE on 9/10/2014.
 */
public interface CourseDetailUseCase {

    void backToCourses();

    void trySample(long courseId);

    void buyForCredit(String courseTitle, long courseId, String credit);

    void buyForCash(String courseTitle, float cash);

    void cancelTransaction();
}
