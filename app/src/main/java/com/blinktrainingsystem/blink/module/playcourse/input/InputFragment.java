package com.blinktrainingsystem.blink.module.playcourse.input;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.Font;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.parser.UserDataParser;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Input;
import com.blinktrainingsystem.blink.data.model.pojo.InputBoolean;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.net.WorkerService;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.handler.InputHandler;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ImageLoader;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.google.common.base.Splitter;
import com.squareup.picasso.Picasso;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.InjectView;
import hugo.weaving.DebugLog;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class InputFragment extends BaseBurstFragment {


    @Inject
    Picasso picasso;
    @InjectView(R.id.tick_img)
    ImageView tickImg;
    @InjectView(R.id.input_tv)
    EditText inputTv;
    @InjectView(R.id.placeholder_lbl)
    TextView placeholderLbl;

    @InjectView(R.id.tvPrompt)
    TextView tvPrompt;

    @InjectView(R.id.words_vw)
    LinearLayout wordsVw;
    @InjectView(R.id.content_sv)
    LinearLayout contentSv;
    @InjectView(R.id.flTrueFalse)
    View flTrueFalse;
    @InjectView(R.id.slider_txt)
    TextView sliderTxt;
    @InjectView(R.id.sliderMin_lbl)
    TextView sliderMinLbl;
    @InjectView(R.id.sliderValue_lbl)
    TextView sliderValueLbl;
    @InjectView(R.id.sliderMax_lbl)
    TextView sliderMaxLbl;
    @InjectView(R.id.slider_sld)
    SeekBar sliderSld;
    @InjectView(R.id.slider_vw)
    RelativeLayout sliderVw;
    @InjectView(R.id.flSliderImages)
    FrameLayout flSliderImages;
    @InjectView(R.id.datePkr)
    DatePicker datePkr;
    @InjectView(R.id.flDatePicker)
    View flDatePicker;
    @InjectView(R.id.btFalse)
    Button false_btn;
    @InjectView(R.id.btTrue)
    Button true_btn;
    @InjectView(R.id.flSentences)
    FlowLayout flSentences;
    @InjectView(R.id.scroller)
    ScrollView scroller;

    @Inject TrackerHelper trackerHelper;

    private InputHandler inputHandler;
    private ArrayList<Integer> cols = new ArrayList<Integer>(4);
    private Input input;
    private Font font;
    ;

    int inputY;
    ArrayList<EditText> words;
    ArrayList<JSONObject> numbers;
    String bgCol = "#FFCC99";
    String storedData;
    boolean timed;
    Button userBtn;
    boolean isCorrectAnswer;
    ImageView sliderImageLeft;
    ImageView sliderImageRight;
    TextView leftTextView;
    TextView rightTextView;
    private ViewGroup parent;
    private int wordBoxesContainer_H;

    final CompositeSubscription compositeSubscription = new CompositeSubscription();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parent = (ViewGroup) inflater.inflate(R.layout.fragment_input, container, false);
        return parent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        trackerHelper.inputLoad();

        final Subscription subscription = ViewObservableHelper.keyboardVisibilityChanged(parent).subscribe(
                new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        Timber.d("Keyboard visibility changed: %s", integer == View.VISIBLE);
                        if (integer == View.VISIBLE) {
                            scroller.scrollTo(0, inputTv.getTop());
                        }
                    }
                }
        );
        compositeSubscription.add(subscription);
        init();
        addPrompt();
        webServices.getUserData(course.id, localSharedPreferences.userId().getOr(0l), blink.guid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Observer<UserDataParser>() {
                            @Override
                            public void onCompleted() {


                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(UserDataParser userDataParser) {
                                if (input == null) initInput();

                                if (userDataParser.success && input != null) {
                                    // TODO: handle get data success

                                    storedData = userDataParser.data;
                                    switch (input.type) {
                                        case 1://text
                                            inputTv.setText(Helper.getStringWithReplacements(storedData));
//                                    placeholderLbl.setVisibility(View.INVISIBLE);
                                            break;
                                        case 2://words
                                            if (words != null) populateWords();
                                            break;
                                        case 3://numbers
                                            if (numbers != null) populateNumbers();
                                            break;
                                        case 4://Date
                                            break;
                                        case 5://True false
                                            break;
                                        case 6://Slider
                                            populateSlider();
                                            break;
                                        case 7://Slider with text
                                            populateSliderWithText();
                                            break;
                                    }
                                }
                            }
                        }
                );

    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
    }

    private void initInput() {
        Json json = blink.loadJsonToObject();
        input = json.input;
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case DoneClick:
            case NextClick:
                Helper.hideSoftKeyboard(getActivity().getCurrentFocus());
                nextPressed();
                break;
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                }
                busDriver.notifyBlinkCompleted();
                break;
        }
    }

    private void nextPressed() {
        if (input != null) {

            int type = input.type;//[[input objectForKey:@"type"] intValue];
//
            String strFeedback = null;
//            NSArray *arrFeedback    =   [json objectForKey:@"feedback"];
//            if(!arrFeedback){
//                arrFeedback =   [NSArray arrayWithObjects:@"", nil];
//            }

            JsonEx json = inputHandler.getJsonObject();
            int score = json.score;//[[json objectForKey:@"score"] intValue];
            int gems = json.gems;//[[json objectForKey:@"gems"] intValue];
            final String[] arrFeedback = json.feedback;

            switch (type) {
                case 1://text
                case 2://words
                    strFeedback = arrFeedback == null || arrFeedback.length == 0 ? "" : arrFeedback[0];
                    break;
                case 3://numbers

                    try {
                        boolean allCorrect = true;

                        for (JSONObject dict : numbers) {
                            int value = dict.getInt("value");//[[dict objectForKey:@"value"] intValue];
                            EditText txtAnswer = (EditText) dict.get("numberText");//[dict objectForKey:@ "numberText"];
                            final String answerString = txtAnswer.getText().toString();
                            if (!TextUtils.isEmpty(answerString)) {
                                int answer = TextUtils.isDigitsOnly(answerString) ? Integer.parseInt(answerString) : 0;//[txtAnswer.text intValue];
                                if (answer != value) {
                                    allCorrect = false;
                                    break;
                                }
                            } else {
                                allCorrect = false;
                                break;
                            }
                        }


                        if (allCorrect) {
                            strFeedback = arrFeedback == null || arrFeedback.length == 0 ? "" : arrFeedback[0];// [arrFeedback firstObject];
                            if (score > 0) {
                                blinkHandler.updateScore(score, blinkIndex, course, gems);
                            }//blkNavControllerRef updateScore:score total:score andGem1:gems totalGem:gems];
                            //                            if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
                            //                        [blkNavControllerRef playSfx:@"Correct"];
                            playRawSound(R.raw.correct);
                        } else {
                            strFeedback = arrFeedback == null || arrFeedback.length < 2 ? "" : arrFeedback[1];//  [arrFeedback lastObject];
                            //                        [blkNavControllerRef playSfx:@"Incorrect"];
                            playRawSound(R.raw.incorrect);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case 4: {//Date

                    if (arrFeedback != null && arrFeedback.length > 0) {
                        final int year = datePkr.getYear();
                        final int month = datePkr.getMonth() + 1;
                        final int dayOfMonth = datePkr.getDayOfMonth();
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String dateStr = new SimpleDateFormat("MM/dd/yyyy").format(calendar.getTime());
//                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                        [dateFormat setDateFormat:@"MM/dd/yyyy"];
//                        NSString *dateStr = [dateFormat stringFromDate:[_date_pkr date]];

//                        input.date
//                        NSDictionary *dateDict  =   [input objectForKey:@"date"];
                        String value = input.date.value;//[dateDict objectForKey:@"value"];


                        if (dateStr.equals(value)) {
                            strFeedback = arrFeedback == null || arrFeedback.length < 1 ? "" : arrFeedback[0];//  [arrFeedback firstObject];
                            if (score > 0)
                                blinkHandler.updateScore(score, blinkIndex, course, gems);// [blkNavControllerRef updateScore:score total:score andGem1:gems totalGem:gems];
//                                if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
                            playRawSound(R.raw.correct);//                             [blkNavControllerRef playSfx:@"Correct"];
                        } else {
                            strFeedback = arrFeedback == null || arrFeedback.length < 2 ? "" : arrFeedback[1];//  [arrFeedback firstObject];
//                            strFeedback =   [arrFeedback lastObject];
                            playRawSound(R.raw.incorrect);//                             [blkNavControllerRef playSfx:@"Correct"];
//                            [blkNavControllerRef playSfx:@"Incorrect"];
                        }
                    }

                    break;
                }
                case 5://True false
                    if (isCorrectAnswer) {
                        strFeedback = arrFeedback == null || arrFeedback.length < 1 ? "" : arrFeedback[0];//[arrFeedback firstObject] : [arrFeedback lastObject];
                        if (score > 0)
                            blinkHandler.updateScore(score, blinkIndex, course, gems);// [blkNavControllerRef updateScore:score total:score andGem1:gems totalGem:gems];
//                                if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
                    } else {
                        strFeedback = arrFeedback == null || arrFeedback.length < 2 ? "" : arrFeedback[1];//[arrFeedback firstObject] : [arrFeedback lastObject];
                    }
                    break;

                case 7://Slider with text
                case 6://Slider
                {
                    int ideal = input.ideal;//[[input objectForKey:@"ideal"] intValue];
                    final int value = sliderSld.getProgress();
                    if (value == ideal) {
                        strFeedback = arrFeedback == null || arrFeedback.length < 1 ? "" : arrFeedback[0];// [arrFeedback firstObject];
                    } else {
                        strFeedback = arrFeedback == null || arrFeedback.length < 2 ? "" : arrFeedback[1];// [arrFeedback firstObject];
                    }
                }
                break;
            }
            if (TextUtils.isEmpty(strFeedback)) {
//                [feedback_vc show:strFeedback withDelegate:self action:@selector(closeFeedback)];
                busDriver.closeHelp();
                busDriver.notifyBlinkCompleted();
            } else {
                busDriver.showHelp(strFeedback);
//                [self closeFeedback];
//                closeFeedback;
            }
        } else {
            busDriver.notifyBlinkCompleted();
        }
    }

    private void fadeSliderLeftRightViews() {
        float scaleLeft = 1;
        float scaleRight = 1;
        int progress = sliderSld.getProgress();
        if (progress > 50) {
            scaleRight = (float) progress / 100;
            scaleLeft = (float) (25 + ((100 - progress) / 2)) / 100;
        } else {
            scaleRight = (float) (25 + (progress / 2)) / 100;
            scaleLeft = (float) (100 - progress) / 100;
        }

        if (input.useImages) {
            Timber.d("scaleLeft: %f", scaleLeft);
            if (sliderImageLeft != null) {
                sliderImageLeft.setScaleX(scaleLeft);
                sliderImageLeft.setScaleY(scaleLeft);
            }

            Timber.d("scaleRight: %f", scaleRight);
            if (sliderImageRight != null) {
                sliderImageRight.setScaleX(scaleRight);
                sliderImageRight.setScaleY(scaleRight);
            }

        } else {
            Timber.d("scaleLeft: %f", scaleLeft);
            if (leftTextView != null) {
                leftTextView.setScaleX(scaleLeft);
                leftTextView.setScaleY(scaleLeft);
            }
            Timber.d("scaleRight: %f", scaleRight);
            if (rightTextView != null) {
                rightTextView.setScaleX(scaleRight);
                rightTextView.setScaleY(scaleRight);
            }

        }

    }

    private void init() {
        cols.add(Color.parseColor("#FFCC00"));
        cols.add(Color.parseColor("#FF9E00"));
        cols.add(Color.parseColor("#FF3333"));
        cols.add(Color.parseColor("#00CC00"));


//        contentSv.setVisibility(View.INVISIBLE);
//        sliderTxt.setVisibility(View.INVISIBLE);
//        sliderVw.setVisibility(View.INVISIBLE);
//        datePkr.setVisibility(View.INVISIBLE);

        font = new Font();
        font.name = getString(R.string.font_myriad_pro_regular);
        font.size = 18;
    }

    private void addPrompt() {
        final Blink blink = blinkHandler.getByBlinkIndex(blinkIndex);
        inputHandler = new InputHandler(blink);
        input = inputHandler.getData();
        if (input != null && input.prompt != null) {
            String prompTitle = input.prompt;
            tvPrompt.setText(Helper.getStringWithReplacements(prompTitle));
        }
        tvPrompt.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        Util.setTypeface(tvPrompt, font.name);
        /* get prompt */

        measure().subscribe(
                new Action1<Integer>() {
                    @Override
                    public void call(Integer type) {
                        createUi(type);
                    }
                }
        );


    }

    private void createUi(int type) {
        Timber.d("Input => createUi: %d", type);
        switch (type) {
            case 1://Text
                slideInTextBox();
                break;
            case 2://Words
                createWordBoxes();
                break;
            case 3://Number
                createNumberInput();
                break;
            case 4://Date
                createDateInput();
                break;
            case 5://True false
                createTrueFalse();
                break;
            case 6://Slider
                createSlider();
                inputTv.setVisibility(View.GONE);
                break;
            case 7://Slider with text
                createSliderWithText();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (compositeSubscription != null) {
            compositeSubscription.unsubscribe();
        }
        Helper.hideSoftKeyboard(getActivity().getCurrentFocus());
    }

    private Observable<Integer> measure() {
        return ViewObservableHelper.globalLayoutFrom(contentSv)
                .doOnNext(
                        new Action1<View>() {
                            @Override
                            public void call(View view) {
                                int maxHeight = contentSv.getHeight() - deviceInfo.getDimensionPixel(60);
                                inputY = tvPrompt.getHeight() + deviceInfo.getDimensionPixel(40);
                                wordBoxesContainer_H = maxHeight - inputY;
                                placeholderLbl.setTranslationY(inputY);
                                if (input != null && !TextUtils.isEmpty(input.sound)) {
                                    playSound(input.sound, false);
                                }
                            }
                        }
                )
                .map(
                        new Func1<View, Integer>() {
                            @Override
                            public Integer call(View view) {
                                if (input != null) {
                                    return input.type;
                                }
                                return 0;
                            }
                        }
                );
    }

    private void updateUserData() {
        if (input != null) {
            int index = 0;
            String data = "";
            switch (input.type) {
                case 1://text
                    data = inputTv.getText().toString();
                    break;
                case 2://words
                    data = "";
                    for (EditText txt : words) {
                        if (index == words.size() - 1) {
                            data += txt.getText().toString();
                        } else {
                            data += txt.getText().toString() + ",";
                        }
                    }
                    break;
                case 3://numbers
                    try {
                        data = "";
                        for (JSONObject dict : numbers) {
                            EditText txt = (EditText) dict.get("numberText");
                            if (index == numbers.size() - 1) {
                                data += txt.getText().toString();
                            } else {
                                data += txt.getText().toString() + ",";
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 4://Date

                    break;
                case 5://True false
                    break;
                case 6://Slider
                    data = String.valueOf(sliderSld.getProgress());
                    break;
                case 7://Slider with text
                    data = String.valueOf(sliderSld.getProgress()) + "," + inputTv.getText().toString();
                    Timber.d("Data: %s", data);
                    break;
            }

            WorkerService.intent(getActivity()).updateUserData(data, course.id, blink.guid).start();
        }

    }

    @Inject
    ImageLoader imageLoader;

    private void createSlider() {

        sliderVw.setVisibility(View.VISIBLE);

        /* TODO LOAD FONT */
//        sliderMaxLbl;
//        sliderMinLbl;
//        sliderValueLbl;

        boolean isNumber = input.isNumber;
        int type = input.type;

        if (isNumber) {

            ((LinearLayout.LayoutParams) sliderVw.getLayoutParams()).weight = 1;
            ViewObservableHelper.globalLayoutFrom(sliderVw)
                    .subscribe(
                            new Action1<View>() {
                                @Override
                                public void call(View view) {
                                    final int margin = (sliderVw.getHeight() - sliderSld.getHeight() - sliderValueLbl.getHeight() - flSliderImages.getHeight()) / 2;
                                    ((RelativeLayout.LayoutParams) flSliderImages.getLayoutParams()).topMargin = margin;
                                }
                            }
                    );

            flSliderImages.setVisibility(View.INVISIBLE);

            final double max = Double.parseDouble(input.max);
            final double min = Double.parseDouble(input.min);
            sliderSld.setMax(100);
            sliderMinLbl.setText(String.valueOf(min));
            sliderMinLbl.setVisibility(View.VISIBLE);
            sliderMaxLbl.setText(String.valueOf(max));
            sliderMaxLbl.setVisibility(View.VISIBLE);
            sliderValueLbl.setVisibility(View.VISIBLE);
            sliderMinLbl.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            Util.setTypeface(sliderMinLbl, getString(R.string.font_myriad_pro_regular));
            sliderMaxLbl.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            Util.setTypeface(sliderMaxLbl, getString(R.string.font_myriad_pro_regular));
            sliderValueLbl.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            Util.setTypeface(sliderValueLbl, getString(R.string.font_myriad_pro_regular));
            if (type == 6) {
                double val = (storedData != null && storedData.length() > 0) ? Double.parseDouble(storedData) : (max - min) / 2 + min;
                sliderSld.setProgress((int) ((val * 100) / (max - min)));
                sliderValueLbl.setText(String.valueOf(val));

                if (input.interger) {
                    sliderValueLbl.setText(String.valueOf(sliderSld.getProgress()));
                    sliderMinLbl.setText(input.min);
                    sliderMaxLbl.setText(input.max);
                } else {
                    sliderValueLbl.setText(String.valueOf(sliderSld.getProgress()));
                    sliderMinLbl.setText(input.min);
                    sliderMaxLbl.setText(input.max);

//                    _sliderValue_lbl.text = [NSString stringWithFormat:@"%4.2f", _slider_sld.value];
//                    _sliderMin_lbl.text = [NSString stringWithFormat:@"%4.2f", min];
//                    _sliderMax_lbl.text = [NSString stringWithFormat:@"%4.2f", max];
                }
                inputTv.setVisibility(View.GONE);
            }
            sliderSld.setOnSeekBarChangeListener(
                    new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            int value = (int) getValue(min, max, (float) progress / 100);
                            sliderValueLbl.setText(String.valueOf(value));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    }
            );


        } else {

            sliderSld.setVisibility(View.VISIBLE);

            sliderMinLbl.setVisibility(View.GONE);
            sliderMaxLbl.setVisibility(View.GONE);

//            _slider_sld.minimumValue = 1.0f;
            sliderSld.setMax(100);

            if (type == 6) {
                double val = (storedData != null && storedData.length() > 0) ? Double.parseDouble(storedData) : 50;
                sliderSld.setProgress((int) val);
            }

            final int imageMaxHeight = (int) (getResources().getInteger(R.integer.h_param_input) / deviceInfo.density);

//            inputTv.getLayoutParams().height = contentSv.getHeight() - 60 - imageMaxHeight - tvPrompt.getBottom() - sliderSld.getHeight() - deviceInfo.getDimensionPixel(32);

            flSliderImages.getLayoutParams().height = imageMaxHeight;

            if (input.useImages) {

                sliderImageLeft = new ImageView(getActivity());
                sliderImageLeft.setScaleType(ImageView.ScaleType.FIT_CENTER);
                flSliderImages.addView(sliderImageLeft, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                ((FrameLayout.LayoutParams) sliderImageLeft.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;
                final Subscription imageLeftSubscription = AndroidObservable.bindFragment(this, imageLoader.load(getLocalFile(input.left)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Observer<Bitmap>() {
                                    @Override
                                    public void onCompleted() {
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onNext(Bitmap bitmap) {
                                        sliderImageLeft.setImageBitmap(bitmap);
                                        int imageHeight = bitmap.getHeight();
                                        int imageWidth = bitmap.getWidth();

                                        if (imageHeight > imageMaxHeight) {
                                            double scale = (double) imageMaxHeight / imageHeight;
                                            sliderImageLeft.getLayoutParams().width = (int) (imageWidth * scale);
                                            sliderImageLeft.getLayoutParams().height = (int) (imageHeight * scale);
                                        } else if (imageWidth > 350) {
                                            double scale = (double) 350 / imageWidth;
                                            sliderImageLeft.getLayoutParams().width = (int) (imageWidth * scale);
                                            sliderImageLeft.getLayoutParams().height = (int) (imageHeight * scale);
                                        }
                                    }
                                }
                        );

                compositeSubscription.add(imageLeftSubscription);

//              frame = _slider_vw.frame;
//                CGRect frameb = image_vw.frame;   CGRect

                sliderImageRight = new ImageView(getActivity());
                sliderImageRight.setScaleType(ImageView.ScaleType.FIT_CENTER);
                final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;

                flSliderImages.addView(sliderImageRight, layoutParams);
                final Subscription imageRightSubscription = AndroidObservable.bindFragment(this, imageLoader.load(getLocalFile(input.right)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Observer<Bitmap>() {
                                    @Override
                                    public void onCompleted() {
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onNext(Bitmap bitmap) {
                                        sliderImageRight.setImageBitmap(bitmap);
                                        int imageHeight = bitmap.getHeight();
                                        int imageWidth = bitmap.getWidth();
                                        if (imageHeight > imageMaxHeight) {
                                            double scale = (double) imageMaxHeight / imageHeight;
                                            sliderImageRight.getLayoutParams().width = (int) (imageWidth * scale);
                                            sliderImageRight.getLayoutParams().height = (int) (imageHeight * scale);
                                        } else if (imageWidth > 350) {
                                            double scale = (double) 350 / imageWidth;
                                            sliderImageRight.getLayoutParams().width = (int) (imageWidth * scale);
                                            sliderImageRight.getLayoutParams().height = (int) (imageHeight * scale);
                                        }
                                    }
                                }
                        );

                compositeSubscription.add(imageRightSubscription);

            } else {
                flSliderImages.getLayoutParams().height = (int) (getResources().getInteger(R.integer.h_paramtext_input));
                String str = Helper.getStringWithReplacements(input.left);

                int height = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.h_lefttext_input));
                int width = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.w_lefttext_input));
                int spacing = deviceInfo.getDimensionPixel(30);
                final int padding = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.padding_leftrighttext_input));

//                CGRect frameb = CGRectMake(frame.origin.x, top, width, height);
                int backgroundColor = Color.parseColor("#AAD7E2");
                leftTextView = new TextView(getActivity());
                leftTextView.setBackgroundColor(backgroundColor);
                leftTextView.setTextColor(Color.BLACK);

                leftTextView.setPadding(padding, padding, padding, padding);
                leftTextView.setGravity(Gravity.CENTER);
                leftTextView.setText(str);
                leftTextView.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
                leftTextView.setMinimumHeight(height);
                Util.setTypeface(leftTextView, getString(R.string.font_myriad_pro_regular));
                flSliderImages.addView(leftTextView, new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));

                str = Helper.getStringWithReplacements(input.right);
                backgroundColor = Color.parseColor("#FFCC99");
                rightTextView = new TextView(getActivity());
                rightTextView.setBackgroundColor(backgroundColor);
                rightTextView.setTextColor(Color.BLACK);
                rightTextView.setGravity(Gravity.CENTER);
                rightTextView.setText(str);
                rightTextView.setPadding(padding, padding, padding, padding);
                rightTextView.setMinimumHeight(height);
                final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.RIGHT;
                rightTextView.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
                Util.setTypeface(rightTextView, getString(R.string.font_myriad_pro_regular));
                flSliderImages.addView(rightTextView, layoutParams);
                flSliderImages.setPadding(0, getResources().getDimensionPixelOffset(R.dimen.padding_top_leftrighttext_input), 0, 0);
                if (inputTv != null) {
                    inputTv.setMinHeight(getResources().getDimensionPixelOffset(R.dimen.minheight_leftrighttext_input_tv));
                }
            }

            sliderSld.setOnSeekBarChangeListener(
                    new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (!input.isNumber) fadeSliderLeftRightViews();
                            busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    }
            );
            fadeSliderLeftRightViews();
        }
    }


    private double getValue(double min, double max, float percent) {
        return min + (max - min) * percent;
    }

    @Override
    public void onStop() {
        super.onStop();
        updateUserData();
    }

    @Inject
    ViewAnimator viewAnimator;

    private void slideInTextBox() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, "Next");
        final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) inputTv.getLayoutParams();
        layoutParams.weight = 1;
        layoutParams.topMargin = deviceInfo.getDimensionPixel(30);
        inputTv.requestLayout();
        inputTv.setHint("Enter your response");

        ViewObservableHelper.globalLayoutFrom(inputTv)
                .subscribe(
                        new Action1<View>() {
                            @Override
                            public void call(View view) {
                                viewAnimator.bottomUp(inputTv, 500, null);
                            }
                        }
                );
    }

    private void createWordBoxes() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, "Next");
        //input must exist for this function to be called - it is only called from addPrompt
        int count = input.count;
        int cellHeight = deviceInfo.getDimensionPixel(48);
        int padding = deviceInfo.getDimensionPixel(15);

        int wordHeight = (int) (Math.ceil((count / 2.0)) * (cellHeight + padding));
        if (wordHeight > wordBoxesContainer_H) {
            wordsVw.getLayoutParams().height = wordHeight;
        }

        inputTv.setVisibility(View.GONE);

        int maxWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.w_input_wordboxes));

//        ArrayList<Integer> cols = new ArrayList<Integer>();
//        cols.add(0);
//        cols.add(wordsVw.getWidth() - maxWidth);
//        cols.add((wordsVw.getWidth() - maxWidth) / 2);

        int frameHeight = wordBoxesContainer_H;

        words = new ArrayList<EditText>();
//        CGRect wordFrame = CGRectMake(0, (frame.size.height-wordHeight)/2, maxWidth, cellHeight);
//        int originX = 0;
        int originY = (frameHeight - wordHeight) / 2;
        inputY += originY;

//        UIFont *font    =  [[BlkFontsHelper sharedInstance] getFontInformationOfView:[self class] objectName:@"font_btn"];

        FrameLayout wordRow = null;
        for (int i = 0; i < count; i++) {
            if (i % 2 == 0) {
                wordRow = newWordRow();
                wordsVw.addView(wordRow);
            }

            final EditText wordBox = newWordBox(maxWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
            wordRow.addView(wordBox);
            words.add(wordBox);
            if (wordRow.getChildCount() == 2) {
                ((FrameLayout.LayoutParams) wordBox.getLayoutParams()).gravity = Gravity.RIGHT;
            } else if (i == count - 1) {
                ((FrameLayout.LayoutParams) wordBox.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
            }
//
//            int left = (i % 2 == 0) ? cols.get(1) : cols.get(0);
//            if (i == (count - 1) && !(i % 2 == 0)) left = cols.get(2);
//
//            originX = left;
//            wordsVw.addView(editText, new ViewGroup.LayoutParams(maxWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
//            editText.setTranslationX(originX);
//            editText.setTranslationY(originY);
//            if (i % 2 == 0) originY += (cellHeight + padding);
//            words.add(editText);
        }

        populateWords();

        viewAnimator.bottomUp(wordsVw, 500, null);
//        wordsVw.setPadding(0,0,20,0);

    }

    private EditText newWordBox(int width, int height) {
        EditText editText = new EditText(getActivity());
        editText.setBackgroundColor(Color.parseColor(bgCol));
        Util.setTypeface(editText, getString(R.string.font_myriad_pro_regular));
        editText.setGravity(Gravity.CENTER);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        Util.setTypeface(editText, getString(R.string.font_frabk));
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        editText.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        editText.setEllipsize(TextUtils.TruncateAt.END);
        editText.setSingleLine(true);
        return editText;
    }

    @Inject
    DeviceInfo deviceInfo;

    private FrameLayout newWordRow() {
        final int padding = deviceInfo.getDimensionPixel(15);
        FrameLayout frameLayout = new FrameLayout(getActivity());
        frameLayout.setPadding(0, 0, 0, padding);
        return frameLayout;
    }

    private void addWordInputToRow(FrameLayout wordRow, View inputView, int gravity) {
        wordRow.addView(inputView);
        ((FrameLayout.LayoutParams) inputView.getLayoutParams()).gravity = gravity;
    }

    private void createNumberInput() {
        try {
            flSentences.setVisibility(View.VISIBLE);
            //input must exist for this function to be called - it is only called from addPrompt
            String str = Helper.getStringWithReplacements(input.text);

//        UIFont * myFont = [[BlkFontsHelper sharedInstance] getFontInformationOfView:[self class] objectName:@"font_btn"];
//        CGSize s = [str sizeWithFont:myFont constrainedToSize:CGSizeMake(content_sv.frame.size.width, MAXFLOAT) lineBreakMode:UILineBreakModeWordWrap];
        /* TODO get font */

            numbers = new ArrayList<JSONObject>();

            final List<String> tokens = Splitter.on("##").omitEmptyStrings().splitToList(str);

            for (int i = 0; i < tokens.size(); i++) {
                String token = tokens.get(i);
                if (TextUtils.isDigitsOnly(token)) {
                    final EditText spaceView = newWordBox(deviceInfo.getDimensionPixel(200), FlowLayout.LayoutParams.WRAP_CONTENT);
                    final int num = Integer.parseInt(token);
                    spaceView.setInputType(InputType.TYPE_CLASS_NUMBER);
                    spaceView.setTag(num);
                    final FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(deviceInfo.getDimensionPixel(200), FlowLayout.LayoutParams.WRAP_CONTENT);
                    params.gravity = Gravity.CENTER_VERTICAL;
                    spaceView.setLayoutParams(params);
                    flSentences.addView(spaceView);
                    spaceView.setVisibility(View.INVISIBLE);
                    JSONObject item = new JSONObject();
                    item.put("value", num);
                    item.put("numberText", spaceView);
                    numbers.add(item);
                } else {
                    addSentence(token, i);
                }
            }
            inputTv.setVisibility(View.GONE);
//            String str1 = "";
//            int charLength = 17;
//
//            for (int i = 0; i < tokens.size(); i += 2) {
//                String token = tokens.get(i);
//                int pos = str1.length() + token.length();
//                if ((i + 1) < tokens.size()) {
//                    str1 += token + "_________________ ";
//                    int num = Integer.parseInt(tokens.get(i + 1));
//
//                    JSONObject item = new JSONObject();
//                    item.put("value", num);
//                    item.put("pos", pos);
//                    numbers.add(item);
//
//                } else {
//                    str1 += token;
//                }
//            }

//            NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:str1];
//            NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
//            [paragrahStyle setLineSpacing:10];
//
//            [str2 addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [str1 length])];
//            [str2 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:26] range:NSMakeRange(0, str1.length)];

//            UIColor *gray = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.0];

//            for(NSMutableDictionary *dict in numbers){
//                int pos = [[dict objectForKey:@"pos"] intValue];
//                [str2 addAttribute:NSForegroundColorAttributeName value:gray range:NSMakeRange(pos+1, charLength)];
//            }

//            int width = contentSv.getWidth();
//            TextView txt = new TextView(getActivity());
//            /* TODO set font */
//            txt.setText(str1);
//            contentSv.addView(txt);
//            contentSv.setTranslationX(0);
//            contentSv.setTranslationY(originY);
//
//            for (JSONObject dict : numbers) {
//                /*TODO add edit text view into the right position*/
//
////                int pos = dict.getInt("pos");
////                txt.selectedRange = NSMakeRange(pos + 1, charLength);
////                UITextRange * selectionRange = [txt selectedTextRange];
////                NSString *str3 = [txt.text substringWithRange:txt.selectedRange];
////                CGRect rect1 = [txt caretRectForPosition:selectionRange.start];
////                CGRect rect2 = [txt caretRectForPosition:selectionRange.end];
////                CGRect frm = CGRectUnion(rect1, rect2);
////                frm.origin.x += org.x;
////                frm.origin.y += org.y - 2;
////                frm.size.height = 45;
////                //NSLog(@"BlkInputVC createNumberInput %d %@ start:%@ end:%@ frame:%@", pos, str3, NSStringFromCGRect(rect1), NSStringFromCGRect(rect2), NSStringFromCGRect(frm));
////                [dict setObject:NSStringFromCGRect(frame) forKey:@"frame"];
////                UITextView *num_txt = [[UITextView alloc] initWithFrame:frm];
////                num_txt.backgroundColor = bgCol;
////                num_txt.font = myFont;
////                num_txt.hidden = YES;
////                num_txt.keyboardType = UIKeyboardTypeNumberPad;
////                num_txt.delegate = self;
////                [content_sv addSubview:num_txt];
////                [dict setObject:num_txt forKey:@"numberText"];
//            }
//
            populateNumbers();

            Observable.timer(500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Action1<Long>() {
                                @Override
                                public void call(Long aLong) {
                                    for (JSONObject dict : numbers) {
                                        try {
                                            EditText num_txt = null;
                                            num_txt = (EditText) dict.get("numberText");
                                            if (num_txt != null)
                                                viewAnimator.enterFadeIn(num_txt, 500, null);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    busDriver.setFooterMenu(getString(R.string.txt_back), null, "Done");

                                }
                            }
                    );

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addSentence(String token, int index) {
        if (TextUtils.isEmpty(token)) {
            return;
        }

        if (token.startsWith(" ")) {
            addWord(" ", index);
        }

        final List<String> words = Splitter.on(" ").omitEmptyStrings().splitToList(token);
        if (words != null && words.size() > 0) {
            final int size = words.size();
            for (int i = 0; i < size; i++) {
                final String word = words.get(i) + " ";
                addWord(word, index);
            }
        }
        if (token.length() > 1 && token.endsWith(" ")) {
            addWord(" ", index);
        }
    }

    public TextView newTextView() {
        TextView textView = new TextView(getActivity());
        final FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER_VERTICAL;
        textView.setLayoutParams(lp);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        return textView;
    }


    private void addWord(String word, int i) {
        final TextView textView = newTextView();
        textView.setText(word);
        final FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_VERTICAL;
        textView.setLayoutParams(params);
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_bold));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        flSentences.addView(textView);
        textView.setVisibility(View.INVISIBLE);
        postMainThread(
                new Runnable() {
                    @Override
                    public void run() {
                        viewAnimator.enterFadeIn(textView, 500, null);
                    }
                }, i * 500
        );
    }

    private void createDateInput() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, "Done");
        flDatePicker.setVisibility(View.VISIBLE);
        inputTv.setVisibility(View.GONE);
        datePkr.setCalendarViewShown(false);
    }

    @Inject
    UiHelper uiHelper;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void createTrueFalse() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, "Next");
        try {
            inputTv.setVisibility(View.GONE);
            flTrueFalse.setVisibility(View.VISIBLE);
//            int originY = 400 - tvPrompt.getHeight() - deviceInfo.getDimensionPixel(40);

//            flTrueFalse.setPadding(0, originY, 0, 0);

            String str;
//            JSONObject dict = input.booleans.get(0);
            if (input.booleans == null || input.booleans.size() == 0) {
                return;
            }

            final InputBoolean inputBoolean = input.booleans.get(0);
//            UIFont * myFont = [[BlkFontsHelper sharedInstance] getFontInformationOfView:[self class] objectName:@"font_btn"];
            /* TODO load font */

            Util.setTypeface(false_btn, getString(R.string.font_frabk));
            Util.setTypeface(true_btn, getString(R.string.font_frabk));
            false_btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            true_btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

            final Drawable drawable = uiHelper.createRoundedDrawable(cols.get(0), 6.0f);
            CompatibleHelper.setBackgroundDrawable(true_btn, drawable);
            true_btn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            trueFalsePress(true_btn);
                        }
                    }
            );

            CompatibleHelper.setBackgroundDrawable(false_btn, drawable);
            false_btn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            trueFalsePress(false_btn);
                        }
                    }
            );
            str = Helper.getStringWithReplacements(inputBoolean.correct);
            true_btn.setText(str);
            str = Helper.getStringWithReplacements(inputBoolean.wrong);
            false_btn.setText(str);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void trueFalsePress(Button btnPress) {
        if (timed) {
            bus.post(Event.PauseTimer);
        }
        CompatibleHelper.setBackgroundDrawable(btnPress, uiHelper.createRoundedDrawable(cols.get(1), 6.0f));
        userBtn = btnPress;
        false_btn.setEnabled(false);
        true_btn.setEnabled(false);

        Observable.timer(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                showTrueFalseAnswer();
                            }
                        }
                );

    }

    private void showTrueFalseAnswer() {
        try {
            if (input != null) {
                if (input.booleans != null && input.booleans.size() > 0) {
                    InputBoolean booleans = input.booleans.get(0);
                    if (booleans != null) {
                        boolean val = booleans.value;
                        if ((val && userBtn == true_btn) || (!val && userBtn == false_btn)) {
                            CompatibleHelper.setBackgroundDrawable(userBtn, uiHelper.createRoundedDrawable(cols.get(3), 6.0f));
                            playRawSound(R.raw.correct);
                            isCorrectAnswer = true;
                        } else {
                            CompatibleHelper.setBackgroundDrawable(userBtn, uiHelper.createRoundedDrawable(cols.get(2), 6.0f));
                            playRawSound(R.raw.incorrect);
                            isCorrectAnswer = false;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Inject
    WebServices webServices;


    private void createSliderWithText() {


        createSlider();
//        sliderTxt.setVisibility(View.VISIBLE);

        double min, max;
        if (input.isNumber) {
            if (input.interger) {
                min = Integer.parseInt(input.min);
                max = Integer.parseInt(input.max);
            } else {
                min = Double.parseDouble(input.min);
                max = Double.parseDouble(input.max);
            }
        } else {
            min = 1;
            max = 100;
        }

        double val;
        if (storedData != null && storedData.length() > 0) {
            String[] tokens = storedData.split(",");
            val = Double.parseDouble(tokens[0]);
            sliderTxt.setText(tokens[1]);
        } else {
            val = ((max - min) / 2 + min);
            sliderTxt.setText("");
        }
        sliderSld.setProgress((int) val);
        fadeSliderLeftRightViews();
        sliderTxt.setText(String.valueOf(sliderSld.getProgress()));
    }

    private void populateWords() {
        if (words == null || storedData == null) return;
        String[] tokens = storedData.split(",");
        for (int index = 0; index < tokens.length; index++) {
            String token = tokens[index];
            if (index < words.size()) {
                EditText txt = words.get(index);
                if (txt != null) txt.setText(Helper.getStringWithReplacements(token));
            }
        }
    }

    private void populateNumbers() {
        try {
            if (numbers == null || storedData == null) return;
            String[] tokens = storedData.split(",");
            for (int index = 0; index < tokens.length; index++) {
                String token = tokens[index];
                if (index < numbers.size()) {
                    JSONObject dict = numbers.get(index);
                    EditText txt = (EditText) dict.get("numberText");

                    if (txt != null) txt.setText(token);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateSlider() {
        if (storedData == null) return;
        sliderValueLbl.setText(storedData);
        double val = Double.parseDouble(storedData);
        double min = Double.parseDouble(input.min);
        double max = Double.parseDouble(input.max);
        sliderSld.setProgress((int) ((val - min) / (max - min)));
        fadeSliderLeftRightViews();
    }

    private void populateSliderWithText() {
        if (storedData == null) return;
        String[] tokens = storedData.split(",");
        if (tokens.length < 2) return;
        inputTv.setText(tokens[1]);
        double val = Double.parseDouble(tokens[0]);
        sliderSld.setMax(100);
        sliderSld.setProgress((int) val);
        sliderTxt.setText(tokens[1]);
        fadeSliderLeftRightViews();
    }


    @Override
    public void onResume() {
        super.onResume();
        busDriver = BusDriver.withBus(bus);
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Override
    protected String getTitle() {
        return "";
    }



    /*
    * final File courseFile = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.full);
        if (!TextUtils.isEmpty(input.left)) {
            picasso.load(new File(courseFile, input.left)).into(_IvLeft);
        }
        if (!TextUtils.isEmpty(input.right)) {
            picasso.load(new File(courseFile, input.right)).into(_IvRight);
        }
        _TvTitle.setText(Helper.getStringWithReplacements(inputHandler.getPrompt()));

        _VSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private boolean isInteger;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float scaleLeft = 1;
                float scaleRight = 1;
                if (progress > 50) {
                    scaleLeft = (float) progress / 100;
                    scaleRight = (float) (25 + ((100 - progress) / 2)) / 100;
                } else {
                    scaleLeft = (float) (25 + (progress / 2)) / 100;
                    scaleRight = (float) (100 - progress) / 100;
                }
                Timber.d("scaleLeft: %f", scaleLeft);
                _IvLeft.setScaleX(scaleLeft);
                _IvLeft.setScaleY(scaleLeft);
                Timber.d("scaleRight: %f", scaleRight);
                _IvRight.setScaleX(scaleRight);
                _IvRight.setScaleY(scaleRight);


//                if (isInteger) {
//                    _TvSliderValue.setText(String.format("%d", progress));
//                } else {
//                    _TvSliderValue.setText(String.format("%4.2f", progress));
//                }

                if (!input.isNumber) fadeSliderLeftRightViews();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        _VSeekBar.setProgress(50);
    *
    * */

}
