package com.blinktrainingsystem.blink.module.playcourse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.adapter.BurstGridAdapter;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.util.BusDriver;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;

/**
 * Created by TALE on 9/19/2014.
 */
public class UnitsGridFragment extends PlayCourseFragment {

    @InjectView(R.id.tvTitle)
    TextView mTvTitle;
    @InjectView(R.id.gridView)
    GridView mGridView;
    @Inject
    BurstGridAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_grid_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        blinkHandler.setEnableActivityMode(false);
        final Integer unlockedUnits = localSharedPreferences.unitUnlock(course.id).getOr(0);
        adapter.unlockIndex = unlockedUnits;
        mGridView.setAdapter(adapter);
        mGridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position <= unlockedUnits) {
                            Blink blink = (Blink) parent.getItemAtPosition(position);
                            onItemSelected(blink);
                        }
                    }
                }
        );
        adapter.changeDataSet(getBlinkList());
        mTvTitle.setText(getTitle());

        ((PlayCourseActivity) getActivity()).stopLongSound();
    }

    public void onItemSelected(Blink blink) {
        bus.post(blink);
    }

    protected String getTitle() {
        return getString(R.string.txt_units);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusDriver.withBus(bus)
                .hideHeaderMenuProgress()
                .hideTimer()
                .setFooterMenu(null, null, null);
    }

    protected List<Blink> getBlinkList() {
        return blinkHandler.getAllUnit();
    }
}
