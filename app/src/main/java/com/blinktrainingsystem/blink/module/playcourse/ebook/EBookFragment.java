package com.blinktrainingsystem.blink.module.playcourse.ebook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.MuPDFPageAdapter;
import com.artifex.mupdfdemo.MuPDFReaderView;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import java.io.File;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class EBookFragment extends BaseBurstFragment {

    Json json;
    @InjectView(R.id.rlContentView)
    RelativeLayout _RlContentView;

    @Inject TrackerHelper trackerHelper;

    @Override public void onResume() {
        super.onResume();
        trackerHelper.ebookLoad();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ebook, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        json = blink == null ? null : blink.loadJsonToObject();
        String ebook = json == null ? null : json.ebook;

        /* load ebook file */
        if (!TextUtils.isEmpty(ebook)) {
            final File localFile = getLocalFile(ebook);
            Timber.d("PDF file: %s", localFile.getAbsolutePath());
            final MuPDFCore core = openFile(ebook);
            final MuPDFReaderView muPDFReaderView = new MuPDFReaderView(getActivity()) {
                @Override
                protected void onMoveToChild(int i) {
                    if (core == null)
                        return;
                    super.onMoveToChild(i);
                }

            };
            _RlContentView.addView(muPDFReaderView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            muPDFReaderView.setAdapter(new MuPDFPageAdapter(getActivity(), core));
        }
    }

    private MuPDFCore openFile(String fileName) {
        try {
            return new MuPDFCore(getActivity(), getLocalPath(fileName));
            // New file: drop the old outline data
        } catch (Exception e) {
            Timber.e(e, "Error open MuPDFCore");
        }
        return null;
    }


    @Override
    protected int getTimer() {
        return 0;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.ebook_help);
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
    }
}
