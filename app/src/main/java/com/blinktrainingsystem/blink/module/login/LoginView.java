package com.blinktrainingsystem.blink.module.login;

/**
 * Created by TALE on 9/8/2014.
 */
public interface LoginView {
    void showProgress();

    void hideProgress();

    void showError(String error);

}
