package com.blinktrainingsystem.blink.module.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.activity.BaseActivity;
import com.blinktrainingsystem.blink.common.dialog.ProgressDialogFragment;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.module.coursedetail.CourseDetailFragment;
import com.blinktrainingsystem.blink.module.courses.CoursesFragment;
import com.blinktrainingsystem.blink.module.login.LoginFragment;
import com.blinktrainingsystem.blink.module.playcourse.PlayCourseActivity;
import com.blinktrainingsystem.blink.module.register.RegisterFragment;
import com.blinktrainingsystem.blink.util.ScreenPresenter;

import com.facebook.FacebookSdk;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by TALE on 9/5/2014.
 */
public class MainActivity extends BaseActivity implements MainView {
    final String TAG = "MainActivity";

    @Inject
    ScreenPresenter screenPresenter;

    @Inject
    MainPresenter mainPresenter;

    //public CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        //callbackManager = CallbackManager.Factory.create();

        /*LoginManager.getInstance().registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    String info = "User ID: "
                            + loginResult.getAccessToken().getUserId()
                            + "\n" +
                            "Auth Token: "
                            + loginResult.getAccessToken().getToken();
                    Log.d(TAG, info);
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "User cancelled");
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.d(TAG, "There was an error " + exception.toString());
                }
            });*/

        int footer_h = getResources().getDimensionPixelSize(R.dimen.footer_h);
        Timber.d("footer_h expected is 96 and be set in xml is %d but the real size is %d", 64, footer_h);
        mainPresenter.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_single_page;
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = new ArrayList<Object>();
        modules.add(new MainModule(this));
        return modules;
    }

    @Override
    public void showLogin() {
        screenPresenter.showScreen(new LoginFragment(), false, 0);
    }

    @Override
    public void showGuestRegister(String courseTitle, long courseId) {
        RegisterFragment registerFragment = new RegisterFragment();
        registerFragment.setReturnCourse(courseTitle, courseId);
        screenPresenter.showScreen(registerFragment, true, 1);
    }

    @Override
    public void showRegister() {
        screenPresenter.showScreen(new RegisterFragment(), true, 1);
    }

    @Override
    public void backToPrevious() {
        onBackPressed();
    }

    @Override
    public void showCourses() {
        screenPresenter.showScreen(new CoursesFragment(), false, 2);
    }

    @Override
    public void showCourseDetail(Course course) {
        CourseDetailFragment fragment = CourseDetailFragment.newInstance(course);
        screenPresenter.showScreen(fragment, true, 3);
    }

    public void showProgress(boolean show) {
        if (show) {
            final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance("Loading...");
            progressDialogFragment.show(getSupportFragmentManager(), "loading");
        } else {
            final DialogFragment dialogFragment = (DialogFragment) getSupportFragmentManager().findFragmentByTag("loading");
            if (dialogFragment != null) {
                dialogFragment.dismiss();
            }
        }
    }

    @Override
    public void showPlayCourse(final Course course) {
        PlayCourseActivity.intent(MainActivity.this).course(course).start();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof CourseDetailFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
            return;
        }
        /*if (fragment instanceof LoginFragment) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            return;
        }*/
        super.onActivityResult(requestCode, resultCode, data);
    }
}
