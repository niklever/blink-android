package com.blinktrainingsystem.blink.module.playcourse.statementrotator;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Answer;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.Rotator;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.Footer3ButtonFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;

import javax.inject.Inject;

import butterknife.InjectView;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;

/**
 * Created by TALE on 9/30/2014.
 */
public class StatementRotatorFragment extends BaseBurstFragment {

    JsonEx json;
    boolean timed;
    boolean isShowTimeOut;
    boolean isComplete;
    Rotator data;
    int selectedAnser;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    @InjectView(R.id.tvTitle)
    TextView lblPrompt;

    @InjectView(R.id.npRotator)
    WheelView npRotator;
    ;
    private boolean isShowHelp;

    @Inject TrackerHelper trackerHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statement_rotator, container, false);
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.rotatorLoad();
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        initRotator();
        loadData();
    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    protected void showFooterMenu() {
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_done));
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.statement_rotator_help);
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                } else if (isShowTimeOut) {
                    bus.post(Event.BlinkCompleted);
                } else if (isComplete) {
                    bus.post(Event.BlinkCompleted);
                }
                break;

            case TimeOut: {
                /* time out */
                timeOut();
                break;
            }

            case DoneClick: {
                blinkComplete();
                break;
            }
        }
    }

    private void showHelp(String help) {
        bus.post(Event.ShowHelp.setExtra(help));
        busDriver.pauseTimer();
    }

    private void initRotator() {
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json.rotator;
    }

    private void loadData() {
        if (data == null) {
            return;
        }

        lblPrompt.setText(Helper.getStringWithReplacements(data.prompt));
        if (!TextUtils.isEmpty(data.sound)) {
            playSound(data.sound, false);
        }

        /* load data to rotator */
        String[] dataSource = new String[data.statements.size()];
        for (int i = 0; i < data.statements.size(); i++) {
            Answer answer = data.statements.get(i);
            dataSource[i] = Helper.getStringWithReplacements(answer.text);
        }
        selectedAnser = 0;
        final StringArrayAdapter viewAdapter = new StringArrayAdapter(getActivity(), dataSource, -1);
        viewAdapter.setTextColor(Color.BLACK);
        npRotator.setViewAdapter(viewAdapter);
        npRotator.addChangingListener(
                new OnWheelChangedListener() {
                    @Override
                    public void onChanged(WheelView wheel, int oldValue, int newValue) {
                        selectedAnser = newValue;
                    }
                }
        );
    }

    /**
     * Adapter for string based wheel. Highlights the current value.
     */
    private static class StringArrayAdapter extends ArrayWheelAdapter<String> {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         */
        public StringArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
        }

        @Override
        protected void configureTextView(TextView textView) {
            if (currentItem == currentValue) {
                textView.setTextColor(0xFF0000F0);
            }
            textView.setGravity(Gravity.CENTER);
            textView.setLines(1);
            Util.setTypeface(textView, context.getString(R.string.font_myriad_pro_regular));
            final float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
            textView.setTextSize(context.getResources().getDimension(R.dimen.text_rotator) / scaledDensity);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private void timeOut() {
        isShowTimeOut = true;
        showHelp(getString(R.string.time_out_message));
    }

    private void blinkComplete() {

        bus.post(new Footer3ButtonFragment.FooterMenuButton(getString(R.string.txt_back), null, null));

        Answer answer = data.statements.get(selectedAnser);

        int score = json.score;
        int gems = json.gems;
        isComplete = true;
        if (answer.correct) {
            /* user answer correct */
            if (!blinkHandler.updateScore(score, blinkIndex, course, gems)) {
                playRawSound(R.raw.points_lost);
            }
            busDriver.updateScore(blinkHandler);
            playRawSound(R.raw.correct);
            showHelp(json.feedback == null ? "" : json.feedback[0]);
        } else {
            /* user answer incorrect */
            playRawSound(R.raw.incorrect);
            showHelp(json.feedback == null ? "" : json.feedback[1]);
        }

    }
}
