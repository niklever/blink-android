package com.blinktrainingsystem.blink.module.login;

import org.json.JSONObject;

/**
 * Created by TALE on 9/8/2014.
 */
public interface LoginUseCase {
    void fbLogin(JSONObject json);

    void login(String email, String password);

    void forgotPassword(String email);

    void requestRegister();

    void cancelLoginTask();

    void enterAsGuest();
}
