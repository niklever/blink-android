package com.blinktrainingsystem.blink.module.login;

import org.json.JSONObject;

import android.app.Application;
import android.text.TextUtils;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.pojo.Credit;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.Task;
import com.blinktrainingsystem.blink.task.TaskManager;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.TrackerHelper;

/**
 * Created by TALE on 9/8/2014.
 */
public class LoginPresenter implements LoginUseCase {

    final String ERROR_NETWORK;
    final String ERROR_EMAIL_EMPTY;
    final String ERROR_PASSWORD_EMPTY;

    final LoginView loginView;

    final WebServices webServices;

    final MainPresenter mainPresenter;

    final TaskManager taskManager;

    final DeviceInfo deviceInfo;

    final Application application;

    final LocalSharedPreferences localSharedPreferences;

    private final TrackerHelper trackerHelper;

    private int taskId;

    public LoginPresenter(LoginView loginView, MainPresenter mainPresenter, Application application, WebServices webServices, TaskManager taskManager, DeviceInfo deviceInfo, LocalSharedPreferences localSharedPreferences, TrackerHelper trackerHelper) {
        this.loginView = loginView;
        this.mainPresenter = mainPresenter;
        this.application = application;
        this.webServices = webServices;
        this.taskManager = taskManager;
        this.deviceInfo = deviceInfo;
        this.localSharedPreferences = localSharedPreferences;
        this.trackerHelper = trackerHelper;
        ERROR_NETWORK = application.getResources().getString(R.string.error_network_disconnected);
        ERROR_EMAIL_EMPTY = application.getResources().getString(R.string.error_empty, "email");
        ERROR_PASSWORD_EMPTY = application.getResources().getString(R.string.error_empty, "password");
    }

    @Override
    public void enterAsGuest(){
        localSharedPreferences.admin().put(false);
        localSharedPreferences.userId().put(0l);
        localSharedPreferences.usr().put("guest");
        localSharedPreferences.pwd().put("");
        mainPresenter.onLoginSuccess();
    }

    @Override
    public void login(final String email, final String password) {
        if (TextUtils.isEmpty(email)) {
            loginView.showError(ERROR_EMAIL_EMPTY);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            loginView.showError(ERROR_PASSWORD_EMPTY);
            return;
        }

        if (!verifyNetwork()) {
            return;
        }
        loginView.showProgress();
        Task<BaseParser> task = new Task<BaseParser>(taskManager) {
            @Override
            protected BaseParser doInBackground(Object... params) {
                BaseParser baseParser = webServices.login(email, password);
                if (baseParser != null && baseParser.success) {
                    Credit credits = webServices.getCredits(baseParser.userId);
                    if (credits.success) {
                        localSharedPreferences.credits().put(credits.credits);
                        localSharedPreferences.usr().put(email);
                        localSharedPreferences.pwd().put(password);
                        localSharedPreferences.admin().put(baseParser.debug);
                        Static.admin = baseParser.debug;
                        return baseParser;
                    } else {
                        return null;
                    }
                }
                return baseParser;
            }

            @Override
            protected void onPostExecute(BaseParser baseParser) {
                super.onPostExecute(baseParser);
                if (baseParser == null) {
                    loginView.showError(Constant.ERROR_FAIL_TO_CONNECT_SERVER);
                } else if (!baseParser.success) {
                    loginView.showError(baseParser.msg);
                } else {
                    localSharedPreferences.userId().put(baseParser.userId);
                    mainPresenter.onLoginSuccess();
                    trackerHelper.loginSucceed(email, baseParser.userId);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                loginView.hideProgress();
            }
        };
        taskId = taskManager.enqueue(task);
    }


    @Override
    public void fbLogin(final JSONObject json) {
        if (!verifyNetwork()) {
            return;
        }
        loginView.showProgress();
        Task<BaseParser> task = new Task<BaseParser>(taskManager) {
            @Override
            protected BaseParser doInBackground(Object... params) {
                String id = json.optString("id","");
                String name = json.optString("name","");
                String email = json.optString("email","");
                String gender = json.optString("gender","");
                String locale = json.optString("locale","");
                BaseParser baseParser = webServices.fbLogin(id,email,name,locale,gender,true);
                if (baseParser != null && baseParser.success) {
                    Credit credits = webServices.getCredits(baseParser.userId);
                    if (credits.success) {
                        localSharedPreferences.credits().put(credits.credits);
                        localSharedPreferences.usr().put(json.optString("email", ""));
                        localSharedPreferences.pwd().put("");
                        localSharedPreferences.gender().put(json.optString("gender", ""));
                        localSharedPreferences.locale().put(json.optString("locale", ""));
                        localSharedPreferences.admin().put(baseParser.debug);
                        Static.admin = baseParser.debug;
                        return baseParser;
                    } else {
                        return null;
                    }
                }
                return baseParser;
            }

            @Override
            protected void onPostExecute(BaseParser baseParser) {
                super.onPostExecute(baseParser);
                if (baseParser == null) {
                    loginView.showError(Constant.ERROR_FAIL_TO_CONNECT_SERVER);
                } else if (!baseParser.success) {
                    loginView.showError(baseParser.msg);
                } else {
                    localSharedPreferences.userId().put(baseParser.userId);
                    mainPresenter.onLoginSuccess();
                    String email = json.optString("email","");
                    trackerHelper.loginSucceed(email, baseParser.userId);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                loginView.hideProgress();
            }
        };
        taskId = taskManager.enqueue(task);
    }

    @Override
    public void forgotPassword(final String email) {
        if (TextUtils.isEmpty(email)) {
            loginView.showError(ERROR_EMAIL_EMPTY);
            return;
        }
        if (!verifyNetwork()) {
            return;
        }
        loginView.showProgress();
        Task<BaseParser> task = new Task<BaseParser>(taskManager) {
            @Override
            protected BaseParser doInBackground(Object... params) {
                return webServices.forgottenPassword(email);
            }

            @Override
            protected void onPostExecute(BaseParser baseParser) {
                super.onPostExecute(baseParser);
                if (baseParser == null) {
                    loginView.showError("Failed to connect to server");
                } else {
                    loginView.showError(baseParser.msg);
                }
            }

            @Override
            protected void onFinished() {
                super.onFinished();
                loginView.hideProgress();
            }
        };
        taskId = taskManager.enqueue(task);
    }

    @Override
    public void requestRegister() {
        mainPresenter.onRegisterRequest();
    }

    @Override
    public void cancelLoginTask() {
        taskManager.cancel(taskId);
    }

    private boolean verifyNetwork() {
        if (deviceInfo.isNetworkConnected()) {
            return true;
        } else {
            loginView.showError(ERROR_NETWORK);
            return false;
        }
    }

}
