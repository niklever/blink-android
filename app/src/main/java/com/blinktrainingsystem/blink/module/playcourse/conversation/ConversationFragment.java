package com.blinktrainingsystem.blink.module.playcourse.conversation;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Conversation;
import com.blinktrainingsystem.blink.data.model.pojo.JsonConversation;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ImageLoader;
import com.blinktrainingsystem.blink.util.MusicPlayer;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class ConversationFragment extends BaseBurstFragment {

    JsonConversation json;
    List<Conversation> conversation;
    int playingSection;
    boolean isComplete;


    @InjectView(R.id.tvTitle)
    TextView txvTranscript;

    @InjectView(R.id.svText)
    ScrollView scrollView;

    @InjectView(R.id.ivCenter)
    ImageView imvContent;

    @InjectView(R.id.ivConOff)
    ImageView imvConOff;

    @InjectView(R.id.ivPause)
    ImageView imvPause;

    @Inject
    BlinkApplication application;

    @Inject TrackerHelper trackerHelper;

    MusicPlayer musicPlayer;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        musicPlayer = new MusicPlayer(getActivity().getApplication());
        initConversation();
        scrollView.setVisibility(View.INVISIBLE);
        loadData();
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        trackerHelper.conversationLoad();

        if (isPaused) {
            conPause();
        }
        isOnPaused = false;
    }

    boolean isOnPaused;

    @Override
    public void onPause() {
        super.onPause();
        if (!isPaused) {
            conPause();
        }
        isOnPaused = true;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @OnClick(R.id.ivConOff)
    public void conOff(View view) {
        if (scrollView.getVisibility() == View.VISIBLE) {
            scrollView.setVisibility(View.INVISIBLE);
            imvConOff.setImageResource(R.drawable.con_on_btn);
        } else {
            scrollView.setVisibility(View.VISIBLE);
            imvConOff.setImageResource(R.drawable.con_off_btn);
        }
        updateImagePosition();
    }


    boolean isPaused;

    @OnClick(R.id.ivPause)
    public void conPause() {
        Timber.d("onPause");
        if (isPaused) {
            resumeMusic();
            imvPause.setImageResource(R.drawable.con_pause_btn);
        } else {
            pauseMusic();
            imvPause.setImageResource(R.drawable.con_play_btn);
        }
        isPaused = !isPaused;
    }

    private void pauseMusic() {
        if (musicPlayer != null) {
            musicPlayer.pause();
        }
    }

    private void resumeMusic() {
        if (musicPlayer != null) {
            musicPlayer.resume();
        }
    }


    @OnClick(R.id.ivConBack)
    public void onBack() {
        if (playingSection > 0) {
            if (musicPlayer != null) {
                musicPlayer.stop();
            }
            playingSection--;
            initSection(playingSection, false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (musicPlayer != null) {
            musicPlayer.release();
        }
    }

    private void initConversation() {
        json = blink == null ? null : (JsonConversation) blink.loadJsonToObject();
        conversation = json == null ? null : json.conversation;
    }

    private void loadData() {
        if (conversation != null) {
            playingSection = 0;
            initSection(playingSection, false);
        }

    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.conversation_help);
    }

    private void showHelp(String help) {
        bus.post(Event.ShowHelp.setExtra(help));
    }

    private void blinkComplete() {
        isComplete = true;
        if (!TextUtils.isEmpty(json.feedback)) {
            showHelp(Helper.getStringWithReplacements(json.feedback));
        } else {
            busDriver.notifyBlinkCompleted();
        }
    }

    @Inject
    ImageLoader imageLoader;

    @Inject
    Picasso picasso;

    @Override public void onStop() {
        super.onStop();
        picasso.cancelRequest(imvContent);
    }

    private void initSection(final int index, boolean silent) {
        Timber.d("initSession => index: %d", index);
        if (index < 0) return;
        if (index >= conversation.size()) {
            blinkComplete();
            return;
        }

        Conversation item = conversation.get(index);

        if (item == null) {
            initSection(++playingSection, false);
            return;
        }
//        final Subscription subscription = AndroidObservable.bindFragment(this, imageLoader.load(getLocalFile(item.image)))
//                .cache()
//                .subscribeOn(Schedulers.newThread())
//                .subscribe(new Observer<Bitmap>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(Bitmap bitmap) {
//                        int maxHeight = imvContent.getBottom() - getResources().getDimensionPixelSize(R.dimen.double_padding);
//                        imvContent.setImageBitmap(bitmap);
//                        final int width = bitmap.getWidth();
//                        final int height = bitmap.getHeight();
//                        if (height < maxHeight) {
//                            float scale = (float) maxHeight / height;
//                            imvContent.setScaleX(scale);
//                            imvContent.setScaleY(scale);
//                            imvContent.setTranslationX((width * scale - width) / 2);
//                            imvContent.setTranslationY(-(height * scale - height) / 2);
//                        }
//                    }
//                });
//        takeCareSubscription(subscription);

        picasso.load(getLocalFile(item.image)).into(
                imvContent, new Callback() {
                    @Override public void onSuccess() {
                        if (isOnPaused) {
                            return;
                        }
                        Timber.d("onSuccess => %s, translationY: %f", scrollView.getVisibility() != View.VISIBLE, imvContent.getTranslationX());
                        if (scrollView.getVisibility() != View.VISIBLE && imvContent.getTranslationY() == 0) {
                            postMainThread(
                                    new Runnable() {
                                        @Override public void run() {
                                            final int halfTranscriptHeight = txvTranscript.getHeight() / 2;
                                            imvContent.animate().translationYBy(halfTranscriptHeight);
                                            Timber.d("animate to center");
                                        }
                                    }
                            );
                        }
                    }

                    @Override public void onError() {

                    }
                }
        );

        if (txvTranscript != null && item != null && item.text != null) {
            txvTranscript.setText(Helper.getStringWithReplacements(item.text));
        }

        if (!isHelp) {
            if (!TextUtils.isEmpty(item.sound)) {
                Timber.d("playRawSound");
                if (musicPlayer == null) {
                    musicPlayer = new MusicPlayer(getActivity().getApplication());
                }
                musicPlayer.play(
                        getLocalPath(item.sound), new MusicPlayer.Callback() {
                            @Override
                            public void onCompleted() {
                                playingSection++;
                                initSection(playingSection, false);
                            }
                        }, new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                if (isPaused) {
                                    pauseMusic();
                                }
                            }
                        }, false
                );

            }
        }

    }

    private void updateImagePosition() {
        final int halfTranscriptHeight = txvTranscript.getHeight() / 2;
        if (scrollView.getVisibility() == View.VISIBLE) {
            imvContent.animate().translationYBy(-halfTranscriptHeight);
        } else {
            imvContent.animate().translationYBy(halfTranscriptHeight);
        }
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                    if (conversation != null) {
                        playingSection = 0;
                        initSection(playingSection, true);
                    }
                } else if (isComplete) {
                    busDriver.notifyBlinkCompleted();
                }
                break;
        }
    }

}
