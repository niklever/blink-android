package com.blinktrainingsystem.blink.module.playcourse.handler;

import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.DragDrop;
import com.blinktrainingsystem.blink.data.model.pojo.Goal;
import com.blinktrainingsystem.blink.data.model.pojo.Json;

import java.util.List;

import timber.log.Timber;

/**
 * Created by TALE on 9/23/2014.
 */
public class DragDropHandler {

    private final Blink blink;
    private final DragDrop dragdrop;
    private Json jsonObject;
    private int rightAnsers;
    private int incorrectCount = Integer.MAX_VALUE;

    public DragDropHandler(Blink blink) throws IllegalArgumentException {
        if (blink.type != BlinkType.KBlinkDragAndDrop) {
            throw new IllegalArgumentException("Wrong type, this expect a KBlinkDragAndDrop type");
        }
        this.blink = blink;
        jsonObject = blink.loadJsonToObject();
        if (jsonObject != null) {
            dragdrop = jsonObject.dragdrop;
        } else {
            dragdrop = null;
        }

        rightAnsers = calculateRightAnswer();
    }

    private int calculateRightAnswer() {
        if (dragdrop == null || dragdrop.goals == null || dragdrop.goals.size() == 0) {
            return 0;
        }
        int result = 0;
        if (dragdrop.goals == null || dragdrop.goals.size() == 0 || dragdrop.items == null || dragdrop.items.size() == 0) {
            return 0;
        }
        for (int i = 0; i < dragdrop.goals.size(); i++) {
            for (Goal item : dragdrop.items) {
                if (item.goal == i + 1) {
                    result++;
                }
            }
        }
//        for (Goal item : dragdrop.items) {
//            if (item.goal == 1) {
//                result++;
//            }
//        }
        return result;
    }

    public List<Goal> getData() {
        return dragdrop == null ? null : dragdrop.items;
    }

    public String getImage() {
        if (dragdrop == null || dragdrop.goals == null || dragdrop.goals.size() == 0) {
            return null;
        }

        for (Goal goal : dragdrop.goals) {
            if (goal.isImage) {
                return goal.image;
            }
        }
        return null;
    }

    public String getPrompt() {
        return dragdrop == null ? "" : dragdrop.prompt;
    }

    public boolean answer(boolean answer) {
        if (answer) {
            rightAnsers--;
            return true;
        }
        if (incorrectCount == Integer.MAX_VALUE) {
            incorrectCount = 1;
        } else {
            incorrectCount++;
        }
        return false;
    }

    public int getIncorrectCount() {
        return rightAnsers == 0 && incorrectCount == Integer.MAX_VALUE ? 0 : incorrectCount;
    }

    public boolean isCompleted() {
        final boolean isCompleted = rightAnsers == 0 || (incorrectCount != Integer.MAX_VALUE && incorrectCount >= 3);
        Timber.d("rightAnswers: %d, incorrect: %d: isCompleted: %s", rightAnsers, incorrectCount, isCompleted);
        return isCompleted;
    }

    public int getTimer() {
        return dragdrop == null ? 0 : dragdrop.timer;
    }

    public String getFeedback() {
        return dragdrop == null ? "" : dragdrop.layout;
    }
}
