package com.blinktrainingsystem.blink.module.playcourse;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.dialog.ProgressDialogFragment;
import com.blinktrainingsystem.blink.common.fragment.DaggerFragment;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.parser.CourseHelper;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.MusicPlayer;
import com.google.common.collect.Lists;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by TALE on 9/17/2014.
 */
public class PlayCourseFragment extends DaggerFragment {
    private static final String KEY_INDEX = "key_index";
    private static final String KEY_COURSE = "key_course";

    protected int blinkIndex;
    protected Course course;

    @Inject
    protected LocalFileManager localFileManager;
    @Inject
    protected LocalSharedPreferences localSharedPreferences;

    @Inject
    protected CourseHelper courseHelper;
    @Inject
    protected BlinkHandler blinkHandler;
    @Inject
    protected EventBus bus;

    protected Blink blink;
    private File courseFile;

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
        if (isSocialShown()) {
            BusDriver.withBus(bus)
                    .showHeaderMenuSocial();
        } else {
            BusDriver.withBus(bus).hideHeaderMenuSocial();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    public void onEvent(Event event) {

    }

    protected boolean isSocialShown() {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
        ((RelativeLayout.LayoutParams) view.getLayoutParams()).leftMargin = getLeftMargin();
        blinkIndex = getArguments() != null ? getArguments().getInt(KEY_INDEX) : 0;
        course = getArguments() != null ? (Course) getArguments().getParcelable(KEY_COURSE) : null;

        blink = blinkHandler.getByBlinkIndex(blinkIndex);
        courseFile = localFileManager.getCourseFile(course.id, LocalFileManager.Folder.full);
    }

    public void playRawSound(int soundId) {
        ((PlayCourseActivity) getActivity()).playRawSound(soundId);
    }

    public void playSound(String soundName, boolean loop) {
        if (soundName != null) {
            ((PlayCourseActivity) getActivity()).playSound(getLocalPath(soundName), loop);
        }

    }

    public void playLongSound(String soundName, final MusicPlayer.Callback callback, boolean loop) {
        if (!TextUtils.isEmpty(soundName)) {
            ((PlayCourseActivity) getActivity()).playLongSound(getLocalPath(soundName), callback, loop);
        }

    }

    public void onAllowSkipChanged() {

    }

    protected int getLeftMargin() {
        return getResources().getDimensionPixelSize(R.dimen.score_bg_w);
    }

    public void setArguments(int index, Course course) {
        Bundle args = getArguments();
        if (args == null) {
            args = new Bundle();
        }
        args.putParcelable(KEY_COURSE, course);
        args.putInt(KEY_INDEX, index);
        setArguments(args);
    }

    protected File getLocalFile(String fileName) {
        return new File(courseFile.getAbsolutePath(), fileName);
    }

    protected String getLocalPath(String fileName) {
        return getLocalFile(fileName).getAbsolutePath();
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
//        modules.add(new PlayCourseModule(courseHelper));
        return modules;
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     *
     * @param view to extract each widget injected in the fragment.
     */
    private void injectViews(final View view) {
        ButterKnife.inject(this, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    public void showProgress(boolean show) {
        if (show) {
            final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance("Loading...");
            progressDialogFragment.show(getFragmentManager(), "loading");
        } else {
            final DialogFragment dialogFragment = (DialogFragment) getFragmentManager().findFragmentByTag("loading");
            if (dialogFragment != null) {
                dialogFragment.dismiss();
            }
        }
    }
}
