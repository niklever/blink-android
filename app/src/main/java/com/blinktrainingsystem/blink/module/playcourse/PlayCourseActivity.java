package com.blinktrainingsystem.blink.module.playcourse;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.activity.BaseActivity;
import com.blinktrainingsystem.blink.common.view.HelpView;
import com.blinktrainingsystem.blink.common.view.MenuView;
import com.blinktrainingsystem.blink.common.view.ScoreBoardView;
import com.blinktrainingsystem.blink.common.view.StopWatchView;
import com.blinktrainingsystem.blink.data.Static;
import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.module.coursedetail.CourseDetailFragment;
import com.blinktrainingsystem.blink.module.playcourse.buildstatement.BuildAStatementFragment;
import com.blinktrainingsystem.blink.module.playcourse.catchgame.CatchGameFragment;
import com.blinktrainingsystem.blink.module.playcourse.conversation.ConversationFragment;
import com.blinktrainingsystem.blink.module.playcourse.draganddrop.DragAndDropFragment;
import com.blinktrainingsystem.blink.module.playcourse.ebook.EBookFragment;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.module.playcourse.input.InputFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.Footer3ButtonFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.HeaderMenuFragment;
import com.blinktrainingsystem.blink.module.playcourse.menu.MenuBackTitleFragment;
import com.blinktrainingsystem.blink.module.playcourse.multiple.MultipleFragment;
import com.blinktrainingsystem.blink.module.playcourse.orderitem.OrderItemFragment;
import com.blinktrainingsystem.blink.module.playcourse.presentation.PresentationFragment;
import com.blinktrainingsystem.blink.module.playcourse.question.QuestionFragment;
import com.blinktrainingsystem.blink.module.playcourse.simon.SimonFragment;
import com.blinktrainingsystem.blink.module.playcourse.statementrotator.StatementRotatorFragment;
import com.blinktrainingsystem.blink.module.playcourse.thisorthat.ThisOrThatFragment;
import com.blinktrainingsystem.blink.module.playcourse.video.VideoFragment;
import com.blinktrainingsystem.blink.module.playcourse.wordfill.WordFillFragment;
import com.blinktrainingsystem.blink.module.playcourse.wordheat.WordHeatFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.MusicPlayer;
import com.blinktrainingsystem.blink.util.ScreenPresenter;
import com.blinktrainingsystem.blink.util.SoundManager;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Created by TALE on 9/5/2014.
 */
public class PlayCourseActivity extends BaseActivity implements Footer3ButtonFragment.FooterButtonMenuClickListener, HeaderMenuFragment.OnClickListener {

    private static final String KEY_COURSE = "key_course";
    private static final String TAG_HEADER_MENU = "header_menu";
    private static final String TAG_FOOTER_MENU = "footer_menu";
    private static final String TAG_FOOTER_MENU_3 = "footer_menu_3";
    private static final String TAG_BACK_TITLE_MENU = "header_back_title_menu";

    @InjectView(R.id.mvMenu)
    MenuView mvMenu;

    @InjectView(R.id.llMenu)
    View llMenu;

    @InjectView(R.id.flStopWatch)
    View flStopWatch;

    @InjectView(R.id.tvCourseTitle)
    TextView tvCourseTitle;
    @InjectView((R.id.vScoreBoard))
    ScoreBoardView vScoreBoard;
    @Optional
    @InjectView(R.id.rlMessage)
    RelativeLayout rlMessage;
    @InjectView(R.id.helper)
    HelpView helpView;
    @InjectView(R.id.vStopWatch)
    StopWatchView _StopWatch;
    @Optional
    @InjectView(R.id.tvScore)
    TextView _TvScore;
    @Optional
    @InjectView(R.id.pbScore)
    ProgressBar _PbScore;
    @Optional
    @InjectView(R.id.llScoreBg)
    View llScoreBg;
    @InjectView(R.id.rootView)
    View rootView;
    @InjectView(R.id.flOverlay)
    View overlayView;

    private Course course;

    @Inject
    ViewAnimator viewAnimator;
    @Inject
    LocalFileManager localFileManager;
    @Inject
    ScreenPresenter screenPresenter;
    @Inject
    LocalSharedPreferences localSharedPreferences;

    @Inject
    EventBus bus;

    @Inject UiHelper uiHelper;

    @Inject
    BlinkHandler blinkHandler;

    @Inject
    BlinkApplication application;

    SoundManager soundManager;

    @Inject
    MusicPlayer musicPlayer;
    private boolean isMenuShow;
    private int blinkIndexId;
    private Handler mHandle = new Handler(Looper.getMainLooper());
    private boolean enableActivity;
    public Animator stopWatchExit;

    public static class IntentBuilder {
        private final Context context;
        private final Intent intent;

        public IntentBuilder(Context context) {
            this.context = context;
            intent = new Intent(context, PlayCourseActivity.class);
        }

        public IntentBuilder course(Course course) {
            intent.putExtra(KEY_COURSE, course);
            return this;
        }

        public void start() {
            context.startActivity(intent);
        }
    }

    public static IntentBuilder intent(Context context) {
        return new IntentBuilder(context);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.unregister(this);
        bus.register(this);
        _StopWatch.setBus(bus);
        helpView.unregisterBus();
        helpView.registerBus(bus);
        Timber.d("onResume");
        resumeSound();
        overlayView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Helper.hideSoftKeyboard(getCurrentFocus());
                    }
                }
        );
        ViewObservableHelper.keyboardVisibilityChanged(rootView)
                .subscribe(
                        new Action1<Integer>() {
                            @Override
                            public void call(Integer visibility) {
                                Timber.d("Keyboard visibility changed: %s", visibility == View.VISIBLE);
                                overlayView.setVisibility(visibility);
                            }
                        }
                );
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
        bus.unregister(this);
        helpView.unregisterBus();
        _StopWatch.setBus(null);
        pauseSound();
    }

    public void pauseSound() {
        if (musicPlayer != null) {
            musicPlayer.pause();
        }
        if (soundManager != null) {
            soundManager.pausePlaying();
        }
    }

    public void resumeSound() {
        if (musicPlayer != null) {
            musicPlayer.resume();
        }
        if (soundManager != null) {
            soundManager.resumePlaying();
        }
    }

    public void stopLongSound() {
        if (musicPlayer != null) {
            musicPlayer.stop();
        }
    }

    public void stopRawSound() {
        if (soundManager != null) {
            soundManager.stopPlaying();
        }
    }

    public void playRawSound(int resId) {
        playRawSound(resId, false);
    }

    public void playRawSound(int resId, boolean loop) {
        if (soundManager == null) {
            soundManager = new SoundManager(getApplication());
        }
        soundManager.loadAndPlay(resId, loop);
    }

    public void playSound(String path) {
        playSound(path, false);
    }

    public void playSound(String path, boolean loop) {
        if (soundManager == null) {
            soundManager = new SoundManager(getApplication());
        }
        soundManager.loadAndPlay(path, loop);
    }

    public void playLongSound(String path, MusicPlayer.Callback callback, boolean loop) {
        if (musicPlayer == null) {
            musicPlayer = new MusicPlayer(getApplication());
        }
        musicPlayer.play(path, callback, loop);
    }

    public void onEvent(BlinkHandler blinkHandler) {
        final long userScoreInBurst = blinkHandler.getUserScoreInBurst();
        final int targetToUnlock = blinkHandler.getTargetToUnlock(blinkIndexId);
        final long userGemsInBurst = blinkHandler.getUserGemsInBurst(blinkIndexId, course.id);
        final long userScore = blinkHandler.getUserScore(course.id);
        _TvScore.setText(String.valueOf(userScore));
        vScoreBoard.setMaxGems(blinkHandler.getMaxGemsInThisBurst(blinkIndexId));
        Timber.d("updateScore => userScoreInBurst: %d, targetToUnlock: %d, userGemsInBurst: %d, userScore: %d", userScoreInBurst, targetToUnlock, userGemsInBurst, userScore);
        vScoreBoard.setScore(userScoreInBurst, targetToUnlock, (int) userGemsInBurst);
        if (userScoreInBurst > targetToUnlock) {
            playRawSound(R.raw.gentleflyby);
        }
        _PbScore.setMax(targetToUnlock == 0 ? 1 : targetToUnlock);
        _PbScore.setProgress((int) userScoreInBurst);
    }

    public void onEvent(Blink blink) {
        int indexOf = blinkHandler.translateIndex(blink.index);
        showScreen(indexOf);
    }

    @Inject TrackerHelper trackerHelper;

    public void onEvent(Event event) {
        switch (event) {
            case ShowHelp:
                trackerHelper.feedBackLoad();
                ivHelp.setEnabled(false);
                showHelp((String) event.getExtra());
                break;
            case CloseHelp:
                closeHelp();
                break;
            case HelpClosed:
                ivHelp.setEnabled(true);
                break;
            case BlinkCompleted:
                openNextScreen();
                break;
            case ShowTimer:
                final Boolean show = (Boolean) event.getExtra();
                if (show) {
                    if (stopWatchExit != null && stopWatchExit.isRunning()) {
                        mHandle.postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        viewAnimator.enterLeft(_StopWatch, Constant.ANIMATION_DURATION);
                                    }
                                }, 300
                        );
                    } else {
                        viewAnimator.enterLeft(_StopWatch, Constant.ANIMATION_DURATION);
                    }
                } else {
                    stopWatchExit = viewAnimator.exitLeft(_StopWatch, 200);
                    _StopWatch.pause();
                }
                break;
            case SetTimer:
                final Integer timer = (Integer) event.getExtra();
                _StopWatch.configure(timer);
                if (_StopWatch._TvProgress.getWidth() == 0) {
                    ViewObservableHelper.globalLayoutFrom(_StopWatch._TvProgress).subscribe(
                            new Action1<View>() {
                                @Override public void call(View view) {
                                    uiHelper.adjustFontSize(((TextView) view), view.getWidth() * 2 / 3);
                                }
                            }
                    );
                } else {
                    uiHelper.adjustFontSize(_StopWatch._TvProgress, _StopWatch._TvProgress.getWidth() * 2 / 3);
                }
                break;
            case StartTimer:
                Timber.d("StartTimer");
                _StopWatch.start();
                break;
            case AddTimer:
                _StopWatch.addTime((Integer) event.getExtra());
                break;
            case PauseTimer:
                _StopWatch.pause();
                break;
            case ViewCourseDetail:
                showBackTitleMenu();
                BusDriver.withBus(bus).setFooterMenu(null, null, null);
                screenPresenter.showScreen(CourseDetailFragment.newInstance(course), false, ++blinkIndexId);
                break;
            case Back:
                openPreviousScreen();
                break;
            case HeaderMenuStepClick:
                showScreen(blinkIndexId + ((Integer) event.getExtra()));
                break;
            case AllowSkipChange:
                Timber.d("AllowSkipChange");
                final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (fragment instanceof PlayCourseFragment) {
                    PlayCourseFragment baseBurstFragment = (PlayCourseFragment) fragment;
                    baseBurstFragment.onAllowSkipChanged();
                }
                break;
            case GainGem:
                openScoreBar();
                playRawSound(R.raw.gem);
                break;

            case UnlockActivity:
                mvMenu.setUnlockActivity(true);
                break;
            case OpenDossier:
                showDossierMenuScreen();
                break;
        }
    }

    public void returnFromCourseDetail() {
        openPreviousScreen();
    }

    private void closeHelp() {
        helpView.closeHelper();
    }

    private void showHelp(String message) {
        Timber.d("showHelp with message %s", message);
        helpView.showHelp(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int footer_h = getResources().getDimensionPixelSize(R.dimen.footer_h);
        Timber.d("footer_h expected is 96 and be set in xml is %d but the real size is %d", 64, footer_h);

        mvMenu.unitClickObservable.subscribe(
                new Action1<TextView>() {
                    @Override
                    public void call(TextView textView) {
                        showUnitMenuScreen();
                    }
                }
        );
        mvMenu.returnToCourseClickObservable.subscribe(
                new Action1<TextView>() {
                    @Override
                    public void call(TextView textView) {
                        disableGAMode();
                        if (Static.dossier) {
                            blinkIndexId = localSharedPreferences.blinkIndex(localSharedPreferences.userId().getOr(0l), course.id, course.folderName).getOr(blinkIndexId);
                            Static.dossier = false;
                        }
                        showScreen(blinkIndexId);
                    }
                }
        );
        mvMenu.dossiersClickObservable.subscribe(
                new Action1<TextView>() {
                    @Override
                    public void call(TextView textView) {
                        showDossierMenuScreen();
                    }
                }
        );
        mvMenu.activitiesClickObservable.subscribe(
                new Action1<TextView>() {
                    @Override
                    public void call(TextView textView) {
                        showActivities();
                    }
                }
        );
        mvMenu.quickLinkClickObservable.subscribe(
                new Action1<TextView>() {
                    @Override
                    public void call(TextView textView) {
                        final Integer index = localSharedPreferences.quickLinkIndex(course.id).getOr(-1);
                        if (index >= 0) {
                            showScreen(index);
                        }
                    }
                }
        );

        bus.register(this);

        blinkIndexId = localSharedPreferences.blinkIndex(localSharedPreferences.userId().getOr(0l), course.id, course.folderName).getOr(0);
        Timber.d("onCreate => blinkIndex: %d", blinkIndexId);

        _TvScore.setText(String.valueOf(blinkHandler.getUserScore(course.id)));

        final String unlockActivityIds = localSharedPreferences.unlockActivityIds(localSharedPreferences.userId().getOr(0l), course).getOr(null);
        mvMenu.setUnlockActivity(!TextUtils.isEmpty(unlockActivityIds));
        final Integer index = localSharedPreferences.quickLinkIndex(course.id).getOr(-1);
        if (index >= 0) {
            final String title = localSharedPreferences.quickLinkTitle(course.id).getOr(null);
            if (!TextUtils.isEmpty(title)) {
                mvMenu.setQuickLink(title);
                mvMenu.setEnableQuickLink(true);
            }
        }
        vScoreBoard.setBus(bus);
        helpView.registerBus(bus);
        initSound();
    }

    private void initSound() {
        soundManager = new SoundManager(getApplication());
        helpView.setSoundManager(soundManager);
        _StopWatch.setSoundManager(soundManager);
        Observable.just(soundManager)
                .map(
                        new Func1<SoundManager, Boolean>() {
                            @Override
                            public Boolean call(SoundManager soundManager) {
                                loadSound(soundManager);
                                return true;
                            }
                        }
                )
                .subscribe(
                        new Observer<Boolean>() {
                            @Override
                            public void onCompleted() {
                                showScreen(blinkIndexId);
                                blinkHandler.startCourse(course.id);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Boolean aBoolean) {

                            }
                        }
                );

    }

    private void loadSound(SoundManager soundManager) {
        soundManager.load(R.raw.correct);
        soundManager.load(R.raw.dragloop);
        soundManager.load(R.raw.gem);
        soundManager.load(R.raw.gentleflyby);
        soundManager.load(R.raw.incorrect);
        soundManager.load(R.raw.next_page);
        soundManager.load(R.raw.orderitemswhoops);
        soundManager.load(R.raw.points_gained);
        soundManager.load(R.raw.points_lost);
        soundManager.load(R.raw.previous_page);
        soundManager.load(R.raw.slider_down);
        soundManager.load(R.raw.statementrotator);
        soundManager.load(R.raw.suction_place);
        soundManager.load(R.raw.suction_remove);
        soundManager.load(R.raw.that);
        soundManager.load(R.raw.this_);
        soundManager.load(R.raw.timer_loop);
        soundManager.load(R.raw.timer_start);
        soundManager.load(R.raw.timer_stop);
        soundManager.load(R.raw.window_in);
        soundManager.load(R.raw.window_out);
        soundManager.load(R.raw.wordheatloop);
        soundManager.load(R.raw.wordheatminusone);
        soundManager.load(R.raw.wordheatminustwo);
        soundManager.load(R.raw.wordheatplusone);
        soundManager.load(R.raw.wordheatplustwo);
        soundManager.load(R.raw.wordheatzero);
    }

    public void showActivities() {
        enableActivity = true;
        blinkHandler.setEnableActivityMode(enableActivity);
        tvCourseTitle.setText(course.title);
        mvMenu._LlReturnToCourse.setVisibility(View.VISIBLE);
        closeMenu();
        closeScoreBar();
        ActivityGridFragment activityGrid = new ActivityGridFragment();
        activityGrid.setArguments(blinkIndexId, course);
        screenPresenter.showScreen(activityGrid, true, 100);
        ButterKnife.inject(this);
    }

    public void showScreen(int index) {
        Timber.d("Blink Index %d", index);
        if (index < 0) {
            return;
        }
        if (enableActivity) {
            index = blinkHandler.getGAIndex(course, index);
            if (index == -1) {
                showActivities();
                return;
            } else if (index == -2) {
                finish();
                return;
            }
        }

        if (blinkIndexId > index) {
            playRawSound(R.raw.previous_page);
        } else {
            playRawSound(R.raw.next_page);
            final Integer highestIndex = localSharedPreferences.highestIndex(course.id).getOr(0);
            if (index > highestIndex && index < blinkHandler.getMaxIndex() && !enableActivity) {
                localSharedPreferences.highestIndex(course.id).put(index);
            }
        }

        blinkIndexId = index;

        llScoreBg.setEnabled(true);
        tvCourseTitle.setText("");
        viewAnimator.exitLeft(_StopWatch, Constant.ANIMATION_DURATION);
        mvMenu._LlReturnToCourse.setVisibility(View.GONE);

        Blink blink = enableActivity ? blinkHandler.getGA(blinkIndexId) : blinkHandler.get(blinkIndexId);
        if (blink != null) {
            if (blink.type == BlinkType.KBlinkTypeSpecificContentLearningUnitEnd) {
                blink = blinkHandler.get(++blinkIndexId);
            } else if (blink.type == BlinkType.KBlinkBurstStart) {
                Json json = blink.loadJsonToObject();
                boolean quickLink = json == null ? false : json.quicklink;
                if (quickLink) {
                    localSharedPreferences.quickLinkIndex(course.id).put(blinkIndexId);
                }
                localSharedPreferences.quickLinkTitle(course.id).put(blink.summary);
                mvMenu.setQuickLink(blink.summary);
                mvMenu.setEnableQuickLink(quickLink);
            }
        }

        PlayCourseFragment fragment = null;
        if (blink != null) {
            if (blink.type == BlinkType.KBlinkLearningUnitStart && Static.dossier) {
                fragment = new DossierFragment();
                mvMenu._LlReturnToCourse.setVisibility(View.VISIBLE);
            } else {
                Static.dossier = false;
                fragment = getFragmentForType(blink.type);
            }
        } else if (LocalFileManager.Folder.lite.toString().equals(course.folderName)) {
            fragment = new LiteProgressFragment();
        } else {
            fragment = new BurstEndFragment();
        }

        if (fragment != null) {
            vScoreBoard.setGemsInThisBurst(blinkHandler.getUserGemsInBurst(blinkIndexId, course.id));

            showScreen(fragment, course);
            if (!enableActivity && blinkIndexId < blinkHandler.getMaxIndex() && !Static.dossier) {
                localSharedPreferences.blinkIndex(localSharedPreferences.userId().getOr(0l), course.id, course.folderName).put(blinkIndexId);
            }
            onEvent(blinkHandler);
        }
    }

    private PlayCourseFragment getFragmentForType(int type) {
        PlayCourseFragment fragment;
        Timber.d("showScreen for Type %d", type);
        switch (type) {
            case BlinkType.KBlinkLearningUnitStart:
                tvCourseTitle.setText(course.title);
                closeScoreBar();
                llScoreBg.setEnabled(false);
                fragment = new UnitStartFragment();
                break;
            case BlinkType.KBlinkBurstStart:
                tvCourseTitle.setText(blinkHandler.getUnitTitle(blinkIndexId));
                fragment = new BurstStartFragment();
                mHandle.postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                openScoreBar();
                            }
                        }, 600
                );
                break;
            case BlinkType.KBlinkTypeSpecificContentBurstEnd:
            case BlinkType.KBlinkTypeSpecificContentLearningUnitEnd:
                fragment = new BurstEndFragment();
                break;
            case BlinkType.KBlinkSimon:
                fragment = new SimonFragment();
                break;
            case BlinkType.KBlinkThisOrThat:
                fragment = new ThisOrThatFragment();
                break;
            case BlinkType.KBlinkDragAndDrop:
                fragment = new DragAndDropFragment();
                break;
            case BlinkType.KBlinkInput:
                fragment = new InputFragment();
                break;
            case BlinkType.KBlinkEBook:
                fragment = new EBookFragment();
                break;
            case BlinkType.KBlinkPresentation:
                fragment = new PresentationFragment();
                break;
            case BlinkType.KBlinkBuildAStatement:
                fragment = new BuildAStatementFragment();
                break;
            case BlinkType.KBlinkWordFill:
                fragment = new WordFillFragment();
                break;
            case BlinkType.KBlinkOrderItems:
                fragment = new OrderItemFragment();
                break;
            case BlinkType.KBlinkQuestion:
                fragment = new QuestionFragment();
                break;
            case BlinkType.KBlinkStatementRotator:
                fragment = new StatementRotatorFragment();
                break;
            case BlinkType.KBlinkWordheat:
                fragment = new WordHeatFragment();
                break;
            case BlinkType.KBlinkVideo:
                fragment = new VideoFragment();
                break;
            case BlinkType.KBlinkCatchGame:
                fragment = new CatchGameFragment();
                break;
            case BlinkType.KBlinkConversation:
                fragment = new ConversationFragment();
                break;
            case BlinkType.KBlinkMultiple:
                fragment = new MultipleFragment();
                break;
            default:
                return null;
        }
        return fragment;
    }

    private void showScreen(PlayCourseFragment fragment, Course course) {
        closeMenu();
        closeScoreBar();
        fragment.setArguments(blinkIndexId, course);
        screenPresenter.showScreen(fragment, false, blinkIndexId);
    }

    private void showUnitMenuScreen() {
        disableGAMode();
        closeMenu();
        mvMenu._LlReturnToCourse.setVisibility(View.VISIBLE);
        tvCourseTitle.setText(course.title);
        UnitsGridFragment unitsGridFragment = new UnitsGridFragment();
        unitsGridFragment.setArguments(blinkIndexId, course);
        screenPresenter.showScreen(unitsGridFragment, true, 100);
    }

    private void disableGAMode() {
        enableActivity = false;
        blinkHandler.setEnableActivityMode(enableActivity);
        blinkIndexId = localSharedPreferences.blinkIndex(localSharedPreferences.userId().getOr(0l), course.id, course.folderName).getOr(0);
    }

    private void showDossierMenuScreen() {
        disableGAMode();
        closeMenu();
        mvMenu._LlReturnToCourse.setVisibility(View.VISIBLE);
        tvCourseTitle.setText(course.title);
        DossierGridFragment dossierFragment = new DossierGridFragment();
        dossierFragment.setArguments(blinkIndexId, course);
        screenPresenter.showScreen(dossierFragment, true, 100);
    }

    public void onEvent(HeaderMenuFragment.MenuConfig menuConfig) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(TAG_HEADER_MENU) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.flHeader, HeaderMenuFragment.newInstance(menuConfig), TAG_HEADER_MENU)
                    .commit();
        }
    }

    public void showBackTitleMenu() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(TAG_BACK_TITLE_MENU) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.flHeader, MenuBackTitleFragment.newInstance(course.title), TAG_BACK_TITLE_MENU)
                    .commit();
        }
    }

    public void onEvent(Footer3ButtonFragment.FooterMenuButton footerMenuButton) {
        showFooterMenu(footerMenuButton.textLeft, footerMenuButton.textMiddle, footerMenuButton.textRight, footerMenuButton.type, footerMenuButton.disableRightButton);
    }

    public void showFooterMenu(String textLeft, String textMiddle, String textRight, int type, boolean disableRightButton) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(TAG_FOOTER_MENU_3) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.flFooter, Footer3ButtonFragment.newInstance(textLeft, textMiddle, textRight, type, disableRightButton), TAG_FOOTER_MENU_3)
                    .commit();
        }
    }

    public void showBurstGridScreen(Course course) {
        tvCourseTitle.setText(course.title);
        mvMenu._LlReturnToCourse.setVisibility(View.VISIBLE);
        closeMenu();
        closeScoreBar();
        BurstGridFragment burstGridFragment = new BurstGridFragment();
        burstGridFragment.setArguments(blinkIndexId, course);
        screenPresenter.showScreen(burstGridFragment, true, 100);
        ButterKnife.inject(this);
    }

    @Override
    public void onLeftClick(CharSequence text) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof UnitStartFragment) {
            // Current is course intro fragment.
            showBurstGridScreen(course);
        } else if (Objects.equal(text, getString(R.string.txt_back))) {
            openPreviousScreen();
        } else if (Objects.equal(text, getString(R.string.txt_replay_burst))) {
            showScreen(blinkHandler.getBurstStartIndex(blinkIndexId));
        }
    }


    @Override
    public void onMiddleClick(CharSequence text) {
        if (Objects.equal(text, getString(R.string.txt_bursts_menu))) {
            showBurstGridScreen(course);
        }
    }

    private void openPreviousScreen() {
        playRawSound(R.raw.previous_page);
        showScreen(blinkIndexId - 1);
    }

    @Override
    public void onRightClick(CharSequence text) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (Objects.equal(text, getString(R.string.txt_next))) {
            if (fragment instanceof InputFragment) {
                if (bus != null) {
                    bus.post(Event.NextClick);
                }
            } else {
                openNextScreen();
            }
        } else if (Objects.equal(text, getString(R.string.txt_start))
                || Objects.equal(text, getString(R.string.txt_next_burst))) {
            openNextScreen();
        } else if (Objects.equal(text, getString(R.string.txt_done))) {
            if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof BurstGridFragment) {
                showScreen(blinkIndexId);
            } else {
                bus.post(Event.DoneClick);
            }
        }
    }

    private void openNextScreen() {
        playRawSound(R.raw.next_page);
        showScreen(blinkIndexId + 1);
    }

    @Optional
    @OnClick(R.id.btCloseMenu)
    public void closeMenu() {
        if (isMenuShow) {
            viewAnimator.exitLeft(llMenu, Constant.ANIMATION_DURATION);
            isMenuShow = false;
        }
    }

    private boolean isScoreBoardShow;

    @Optional
    @OnClick(R.id.llScoreBg)
    public void toggleScoreBoard() {
        if (isScoreBoardShow) {
            closeScoreBar();
        } else {
            openScoreBar();
        }
    }

    void openScoreBar() {
        closeMenu();
        viewAnimator.enterLeft(vScoreBoard, Constant.ANIMATION_DURATION);
        isScoreBoardShow = true;
    }

    void closeScoreBar() {
        viewAnimator.exitLeft(vScoreBoard, Constant.ANIMATION_DURATION);
        isScoreBoardShow = false;
    }

    @Override
    public void onMenuClick() {
        openMenu();
    }

    public void openMenu() {
        if (!isMenuShow) {
            if (isScoreBoardShow) {
                toggleScoreBoard();
            }
            viewAnimator.enterLeft(llMenu, Constant.ANIMATION_DURATION);
            isMenuShow = true;
        }
    }

    @InjectView(R.id.ivHelp)
    View ivHelp;

    @OnClick(R.id.ivHelp)
    public void helpClick() {
        Timber.d("helpClick");
        bus.post(Event.HelpRequest);
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_play_course;
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = Lists.newArrayList();
        course = getIntent().getParcelableExtra(KEY_COURSE);
        if (course != null) {
            modules.add(new PlayCourseModule(course));
        }
        return modules;
    }

    @Optional
    @OnClick(R.id.tvMyCourse)
    @Override
    public void finish() {
        super.finish();
        Static.openMyCourseOnBack = true;
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundManager.release();
        musicPlayer.release();
    }

    @Override
    public void onBackPressed() {
        // To do disable back press.
//        super.onBackPressed();
    }

    public static final String SOCIAL_NETWORK_TAG = "SocialIntegration.SOCIAL_NETWORK_TAG";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
