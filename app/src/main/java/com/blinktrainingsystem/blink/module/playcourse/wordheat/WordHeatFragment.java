package com.blinktrainingsystem.blink.module.playcourse.wordheat;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.Word;
import com.blinktrainingsystem.blink.data.model.pojo.WordHeat;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class WordHeatFragment extends BaseBurstFragment {

    @InjectView(R.id.tvTitle)
    TextView _TvTitle;
    @InjectView(R.id.wordheatLayout)
    View wordheatLayout;
    @InjectView(R.id.key_bar_img)
    FrameLayout keyBarImg;

    @InjectView(R.id.tv12)
    TextView tv12;
    @InjectView(R.id.tv7)
    TextView tv7;
    @InjectView(R.id.tv6)
    TextView tv6;
    @InjectView(R.id.tv5)
    TextView tv5;
    @InjectView(R.id.tv1)
    TextView tv1;
    @InjectView(R.id.tv2)
    TextView tv2;
    @InjectView(R.id.tv13)
    TextView tv13;
    @InjectView(R.id.tv10)
    TextView tv10;
    @InjectView(R.id.tv4)
    TextView tv4;
    @InjectView(R.id.tv3)
    TextView tv3;
    @InjectView(R.id.tv9)
    TextView tv9;
    @InjectView(R.id.tv8)
    TextView tv8;
    @InjectView(R.id.tv11)
    TextView tv11;
    @InjectView(R.id.tv14)
    TextView tv14;

    private WordHeat data;
    private String prompt;
    private List<Word> words;
    private int[] cols;
    public JsonEx json;
    public boolean isTablet;
    private List<Map<String, Object>> btns;

    @Inject
    ViewAnimator viewAnimator;
    @Inject TrackerHelper trackerHelper;

    private List<View> key_bar;
    private List<Animator> glow_btns;
    private boolean nextOnClosed;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_word_heat, container, false);
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.wordHeatLoad();
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        initWordheat();
        if (data == null) {
            return;
        }

        ViewObservableHelper.globalLayoutFrom(wordheatLayout).subscribe(
                new Action1<View>() {
                    @Override
                    public void call(View view) {
                        viewDidLoad();
                    }
                }
        );
    }

    private boolean initWordheat() {
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json.wordheat;
        if (data != null) {
            words = data.words;
            prompt = data.prompt;//[data objectForKey:@ "prompt"];
        }
        int col1 = Color.parseColor("#E25E5E");//[UIColor colorWithRed:226.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1.0];
        int col2 = Color.parseColor("#FF9933");//[UIColor colorWithRed:255.0/255.0 green:153.0/255.0 blue:51.0/255.0 alpha:1.0];
        int col3 = Color.parseColor("#FFCC00");//[UIColor colorWithRed:255.0/255.0 green:204.0/255.0 blue:0.0/255.0 alpha:1.0];
        int col4 = Color.parseColor("#51CC66");//[UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:102.0/255.0 alpha:1.0];
        int col5 = Color.parseColor("#66CCFF");//[UIColor colorWithRed:102.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1.0];
        cols = new int[]{col1, col2, col3, col4, col5};//[[NSArray alloc] initWithObjects:col1, col2, col3, col4, col5, null];
        return (data != null);
    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.word_heat_help);
    }

    @Inject
    UiHelper uiHelper;

    LinearLayout newRow() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);
        return linearLayout;
    }

    int baseY = 0;
    int line = 1;

    void viewDidLoad() {
        // Do any additional setup after loading the view from its nib.
//        _TvTitle.setLines(1);//.numberOfLines = 0;
        Util.setTypeface(_TvTitle, getString(R.string.font_myriad_pro_regular));
        _TvTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
        String stringWithReplacements = Helper.getStringWithReplacements(prompt);
        _TvTitle.setText(stringWithReplacements);//prompt_lbl.attributedText = [blkNavControllerRef getStringWithLineSpacing:prompt];
//        [prompt_lbl sizeToFit];
        playPromptSound();

        String nib;
//        if(IPAD){
//            nib =   @"BlkFeedbackVC";
//        }
//        else{
//            nib =   ([appDelegate isIphone4]) ? @"BlkFeedbackVC~iPhone4" : @"BlkFeedbackVC~iPhone";
//        }
//        feedback_vc = [[BlkFeedbackVC alloc] initWithNibName:nib bundle:null];
//        feedback_vc.startTimerOnNext = true;
//        feedback_vc.nextBlinkOnNext = false;
//        [self.view addSubview:feedback_vc.view];
//        feedback_vc.view.hidden = true;
//
//        CGRect frame = prompt_lbl.frame;
        isTablet = getResources().getBoolean(R.bool.is_tablet);
        int sizeWidth = 0;//contentView.getWidth();
        int sizeHeight = 0;//contentView.getHeight();

        //Frame is now the content size
        int rows = (int) Math.ceil((double) words.size() / 2.0);
        int col = 0;
        int offset = 0;
        int left = getResources().getDimensionPixelSize(R.dimen.wordfill_left);
        final int padding = getResources().getDimensionPixelSize(R.dimen.wordfill_padding);
        int size = getResources().getDimensionPixelSize(R.dimen.wordfill_size);
        int width = getResources().getDimensionPixelSize(R.dimen.wordfill_width);
        int top = (sizeHeight - rows * (size + padding)) / 2;

        if (top < padding) {
            top = padding;
        }
//    UIFont *font = [UIFont fontWithName:@"Myriad Pro" size:60.0];
//        UIFont *font = [[BlkFontsHelper sharedInstance] getFontInformationOfView:[self class] objectName:@"btn_font"];
        btns = new ArrayList<Map<String, Object>>();//[[NSMutableArray alloc] init];
//        LinearLayout row = newRow();
//        row.setPadding(left, 0, 0, 0);
//        final FlowLayout flowLayout = newFlowLayout();
//        final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.gravity = Gravity.CENTER;
//        contentView.addView(flowLayout, layoutParams);
        int index = 0;
        boolean useBg = false;

        for (Word dict : words) {
            String word = dict.word;//"];
            int layer = dict.layer;//"] intValue];
//            CGRect rect = CGRectMake(left, top, width, size);
//            CGRect frame = CGRectMake(-width, top, width, size);
//            FrameLayout.LayoutParams rect = (FrameLayout.LayoutParams) flowLayout.getLayoutParams();
//            rect.topMargin = top;
            final TextView btn = getTextView(index);
            btn.setVisibility(View.INVISIBLE);
            Util.setTypeface(btn, getString(R.string.font_myriad_pro_regular));
//            btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            final Drawable bg;
            if (useBg) {
                bg = uiHelper.createRoundedDrawable(cols[0], 6.0f);
                CompatibleHelper.setBackgroundDrawable(btn, bg);//.backgroundColor =[cols objectAtIndex:0];
                btn.setTextColor(Color.WHITE);//.titleLabel.textColor =[UIColor whiteColor];
            } else {
//                btn.setBackgroundColor(Color.TRANSPARENT);//.backgroundColor =[UIColor clearColor];
                btn.setTextColor(cols[0]);//[btn setTitleColor:[cols objectAtIndex:0]forState:
//                UIControlStateNormal];
            }
            ((View) btn.getParent()).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            wordPressed(btn);
                        }
                    }
            );
//            [btn addTarget:self action:@selector(wordPressed:)forControlEvents:
//            UIControlEventTouchUpInside];
            btn.setTag(index);
            btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, isTablet ? 45 : 20);//.font = font;
//            [self.view addSubview:btn];
//            final FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            final FrameLayout frame = new FrameLayout(getActivity());
//            frame.addView(btn, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//            ((FrameLayout.LayoutParams) btn.getLayoutParams()).gravity = Gravity.CENTER;
//            flowLayout.addView(frame, params);
            ViewObservableHelper.globalLayoutFrom(((ViewGroup) btn.getParent()))
                    .subscribe(
                            new Action1<View>() {
                                @Override
                                public void call(View view) {
                                    ViewGroup.LayoutParams params = view.getLayoutParams();
//                                    Timber.d("view.getTop: %d, baseY: %d", view.getTop(), baseY);
//                                    if (view.getTop() >= baseY) {
//                                        if (baseY > 0) {
//                                            params.leftMargin = line % 2 == 0 ? 0 : padding * 4;
//                                            line++;
//                                            Timber.d("first character: %s", btn.getText());
//                                        }
//                                        baseY = view.getBottom();
//                                    }
                                    params.width = view.getWidth();
                                    params.height = view.getHeight();
                                    view.requestLayout();
                                }
                            }
                    );
//            if (index > 0) {
//                params.leftMargin = padding * 4;
//            }
//            params.topMargin = index % 2 == 0 ? padding * 2 : 0;
            final String wordText = Helper.getStringWithReplacements(word);
            Timber.d("index: %d, text: %s", index, wordText);
            btn.setText(wordText);
//            [btn setTitle:[blkNavControllerRef getStringWithReplacements:word]forState:
//            UIControlStateNormal];
//            [btn.layer setCornerRadius:6.0f];
//            [btn sizeToFit];
            //rect = btn.frame;
//            int centreX = left + width / 2;
//            int centreY = top + size / 2;
//            rect = btn.frame;
//            rect = CGRectMake(centre.x - rect.size.width / 2, centre.y - rect.size.height / 2, rect.size.width, rect.size.height);
//            btn.frame = rect;

            Map<String, Object> dict1 = new HashMap<String, Object>();
            dict1.put("btn", btn);
            dict1.put("target-layer", layer);
            dict1.put("current-layer", 0);
            dict1.put("shrink", true);
//            NSMutableDictionary * dict1 =[NSMutableDictionary dictionaryWithObjectsAndKeys:btn,@
//            "btn",[NSNumber numberWithInt:layer],@ "target-layer",[NSNumber numberWithInt:0],@
//            "current-layer",[NSValue valueWithCGPoint:centre],@ "centre",[NSNumber numberWithBool:
//            true],@ "shrink", null];
            btns.add(dict1);// addObject:dict1];
//            col++;
//            if (col > 1) {
//                offset++;
//                top += (size + padding);
//                if (offset == 3) offset = 0;
//                col = 0;
//                row = newRow();
//                contentView.addView(row, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                int extra = isTablet ? 75 : 25;
//                left = 171 + (width + padding) * col + extra * offset;
//                row.setPadding(left, 0, 0, 0);
//            }
            postMainThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            viewAnimator.enterLeftTop(btn, 700);
                        }
                    }, 200 * index
            );
//            [UIView animateWithDuration:0.7
//            delay:
//            ((double) index * 0.2)
//            options:
//            UIViewAnimationOptionCurveEaseInOut
//            animations:^{
//                btn.frame = rect;
//            }
//            completion:^( boolean finished){
//            }];

            index++;
        }

//        _keybar_img.hidden = true;
//        keyBarImg.setVisibility(View.INVISIBLE);
//        frame = _keybar_img.frame;
        sizeWidth = keyBarImg.getWidth() + 10;
        sizeHeight = keyBarImg.getHeight() / 5;
        key_bar = new ArrayList<View>();//[[NSMutableArray alloc]init];
        index = 0;

        for (int color : cols) {
//            UIView * vw =[[UIView alloc]initWithFrame:
            TextView view = new TextView(getActivity());//
            final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(sizeWidth, sizeHeight);
            view.setLayoutParams(params);
//            frame];
            final Drawable roundedDrawable = uiHelper.createRoundedDrawable(color, 6.0f);
            CompatibleHelper.setBackgroundDrawable(view, roundedDrawable);
//            vw.backgroundColor =cols objectAtIndex:index];
//            [vw.layer setCornerRadius:6.0];
            key_bar.add(view);// addObject:vw];
            keyBarImg.addView(view);
            params.topMargin = index * sizeHeight;
//            contentView.add
//                    [self.view; addSubview:vw];
//            frame.origin.y += (frame.size.height + 0);
            index++;
        }
    }

    private TextView getTextView(int index) {
        switch (index) {
            case 0:
                return tv1;
            case 1:
                return tv2;
            case 2:
                return tv3;
            case 3:
                return tv4;
            case 4:
                return tv5;
            case 5:
                return tv6;
            case 6:
                return tv7;
            case 7:
                return tv8;
            case 8:
                return tv9;
            case 9:
                return tv10;
            case 10:
                return tv11;
            case 11:
                return tv12;
            case 12:
                return tv13;
            case 13:
                return tv14;
            default:
                return tv1;
        }
    }

    private FlowLayout newFlowLayout() {
        FlowLayout flowLayout = new FlowLayout(getActivity());
        flowLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return flowLayout;
    }


    void playPromptSound() {
        String sound = data == null ? null : data.sound;//[data objectForKey:@"sound"];
        if (!TextUtils.isEmpty(sound)) {
            playSound(sound, false);
//            playLongSound(sound, null, false);
        }
    }

    @Override
    protected void showFooterMenu() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_done));
    }

//    void viewDidDisappear
//    :(boolean)animated
//
//    {
//        [blkNavControllerRef stopLoopingSfx];
//        if (loop_tmr != null)[loop_tmr invalidate];
//    }

    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                } else if (nextOnClosed) {
                    notifyBurstCompleted();
                }
                break;
            case DoneClick:
            case TimeOut:
                blinkComplete();
                break;
        }
    }

//    void showHelp
//    :(boolean)force
//
//    {
//
//        //String str = @"Touch the words to move through five levels from front to back. Order them according to the prompt.";
//        String str =[[BlkHelpTextHelper sharedInstance]getHelpText:
//    self ordered:false];
//        if (force ||[blkNavControllerRef checkAllowShowingHelp:blink]){
//        [feedback_vc show:str];
//    }
//    }


    void wordPressed(TextView sender) {
        TextView btn = sender;
        if (glow_btns != null && glow_btns.size() > 0) {
//        for (TextView btn in glow_btns)[btn stopGlowing];
            for (Animator animator : glow_btns) {
                if (animator != null && animator.isRunning()) {
                    animator.end();
                }
            }
        }
        glow_btns = new ArrayList<Animator>();//[[NSMutableArray alloc]init];
        int index = (Integer) btn.getTag();
        final Map<String, Object> dict = btns.get(index);
//        NSMutableDictionary * dict =[btns objectAtIndex:index];
        int layer = (Integer) dict.get("current-layer");
//        CGPoint centre =dict.get("centre");
        boolean shrink = (Boolean) dict.get("shrink");
        Timber.d("layer: %d, shrink: %s", layer, shrink);
        if (shrink) {
            layer++;
            if (layer == 5) {
                layer = 3;
                dict.put("shrink", false);
//                [dict setObject:[NSNumber numberWithBool:false]forKey:
//                @ "shrink"];
                //[blkNavControllerRef playSfx:@"Suction_Remove"];
                playRawSound(R.raw.wordheatplustwo);//"];
                Timber.d("Play sound wordheatplustwo");

            } else {
//                switch (layer) {
//                    case 4:
//                        playRawSound(R.raw.wordheatminusone);//[blkNavControllerRef playSfx:@ "WordHeatMinusOne"];
//                        break;
//                    case 3:
                playRawSound(R.raw.wordheatminustwo);//[blkNavControllerRef playSfx:@ "WordHeatMinusTwo"];
//                        break;
//                    default:
//                        //[blkNavControllerRef playSfx:@"Suction_Place"];
//                        break;
//                }
            }
        } else {
            layer--;
            if (layer < 0) {
                layer = 1;
//                [dict setObject:[NSNumber numberWithBool:true]forKey:
//                @ "shrink"];
                dict.put("shrink", true);//
                //[blkNavControllerRef playSfx:@"Suction_Place"];
                playRawSound(R.raw.wordheatminustwo);//[blkNavControllerRef playSfx:@ "WordHeatMinusTwo"];

            } else {
                //[blkNavControllerRef playSfx:@"Suction_Remove"];
//                switch (layer) {
//                    case 0:
//                        playRawSound(R.raw.wordheatzero);//"];
//                        break;
//                    case 1:
//                        playRawSound(R.raw.wordheatplusone);//"];
//                        break;
//                    case 2:
                playRawSound(R.raw.wordheatplustwo);//"];
                Timber.d("Play sound wordheatplustwo");
//                        break;
//                    default:
//                        //[blkNavControllerRef playSfx:@"Suction_Place"];
//                        break;
//                }

            }
        }

        int width = (int) (315 - ((315 - 134) / 5.0) * layer);
        int height = (int) (63 - ((63 - 27) / 5.0) * layer);
//    UIFont *font = [UIFont fontWithName:@"Myriad Pro" size:60.0 - 40.0*(layer/5.0)];
//        UIFont * font =[[BlkFontsHelper sharedInstance]getFontInformationOfView:[self class]
//        objectName:
//        @ "title_font"];
        int size;
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        if (isTablet) {
            float scale = 0.64f * (5 - layer) / 5.0f + 0.36f;
            size = (int) (45 * scale);
        } else {
            float scale = 0.4f * (5 - layer) / 5.0f + 0.6f;
            size = (int) (20 * scale);
        }
        btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);//.titleLabel.font = font;
//        CGRect frame = CGRectMake(centre.x - width / 2, centre.y - height / 2, width, height);
        boolean useBg = false;

        if (useBg) {
//            btn.titleLabel.textColor =[UIColor colorWithRed:1.0 green:
//            1.0 blue:
//            1.0 alpha:
//            1.0 - ((double) layer / 5.0)];
//            btn.backgroundColor =[cols objectAtIndex:layer];
        } else {
//            [btn setTitleColor:[cols objectAtIndex:layer]forState:
//            UIControlStateNormal];
            btn.setTextColor(cols[layer]);
        }
////        float scaleX = (float) width / btn.getWidth();
//        float scaleY = (float) height / btn.getHeight();
//        btn.setScaleX(scaleY);
//        btn.setScaleY(scaleY);
        dict.put("current-layer", layer);
        Timber.d("wordPressed => word: %s, current-layer: %d", btn.getText(), layer);
//        Timber.d("scaleX: %f, scaleY: %f, layer: %d", scaleY, scaleY, layer);
        int col = cols[layer];// objectAtIndex:layer];
        int red = (col >> 16) & 0xFF;
        int green = (col >> 8) & 0xFF;
        int blue = (col >> 0) & 0xFF;
        red *= 1.7;
        green *= 1.7;
        blue *= 1.7;
        Timber.d("red: %d, green: %d, blue: %d", red, green, blue);
        final int maxColVal = 255;
        if (red > maxColVal) red = maxColVal;
        if (green > maxColVal) green = maxColVal;
        if (blue > maxColVal) blue = maxColVal;
        col = Color.rgb(red, green, blue);
        for (Map<String, Object> dict1 : btns) {
            TextView btn1 = (TextView) dict.get("btn");//[dict objectForKey:@ "btn"];
            int current_layer = (Integer) dict1.get("current-layer");//[[dict objectForKey:@ "current-layer"]intValue];
            if (btn1 != null && (current_layer == layer)) {
//                [btn1 startGlowingWithColor:col intensity:0.8];
//                final Animator animator = viewAnimator.glowTextColor(btn, col, ValueAnimator.INFINITE, 0, null);
                final Animator animator = viewAnimator.blink(btn, ValueAnimator.INFINITE, 0, null);
                glow_btns.add(animator);
//                [glow_btns addObject:btn1];
            }
        }

        final View key = key_bar.get(layer);
        final Animator animator = viewAnimator.blink(key, ValueAnimator.INFINITE, 0, null);
//        [key startGlowingWithColor:col intensity:0.8];
        glow_btns.add(animator);
//                [glow_btns; addObject:key];

//        [blkNavControllerRef stopLoopingSfx];
//        if (loop_tmr != null)[loop_tmr invalidate];
//        loop_tmr =[NSTimer scheduledTimerWithTimeInterval:1 target:
//    self selector:@selector(startLoopSound)userInfo:null repeats:
//    false];

        startLoopSound();

//        busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
    }

    void startLoopSound() {
//        playRawSound(R.raw.wordheatloop);
    }

    void timedOut() {
//        String str = [json objectForKey:@ "score"];
        int score = json.score;//(str == null) ? 0 :[str intValue];
//        str =[json objectForKey:@ "gems"];
        int gems = json.gems;//(str == null) ? 0 :[str intValue];
//        if (score != 0)[blkNavControllerRef updateScore:0 total:
//    score andGem1:0 totalGem:
//    gems];
//    if (gems!=0) [blkNavControllerRef updateGems:0 total:gems];

        if (btns != null) {
            for (Map<String, Object> dict : btns) {
                final TextView TextView = (TextView) dict.get("btn");
                TextView.setEnabled(false);
            }
//            for (NSDictionary * dict in btns){
//                TextView btn =[dict objectForKey:@ "btn"];
//                btn.enabled = false;
//            }
        }

//        str = TIME_OUT_MESSAGE;
//        feedback_vc.startTimerOnNext = false;
//        feedback_vc.nextBlinkOnNext = true;
//        [feedback_vc show:str];
        busDriver.showHelp(getString(R.string.time_out_message));
    }

    void blinkComplete() {
        busDriver.pauseTimer();
        final String[] feedback = json.feedback;
//        NSArray * feedback = feedback[json objectForKey:
//        @ "feedback"];
//        String str =[json objectForKey:@ "score"];
        int score = json.score;//(str == null) ? 0 :[str intValue];
//        str =[json objectForKey:@ "gems"];
        int gems = json.gems;//(str == null) ? 0 :[str intValue];
        boolean feedbackShown = false;
        int incorrectCount = 0;

        if (glow_btns != null) {
            for (Animator animator : glow_btns) {
                if (animator != null && animator.isRunning()) {
                    animator.end();
                }
            }
        }

        for (Map<String, Object> dict : btns) {
            TextView btn = (TextView) dict.get("btn");//];
            int target_layer = (Integer) dict.get("target-layer");//]intValue]);//;
            int current_layer = (Integer) dict.get("current-layer");//]intValue]+1;);//
            Timber.d("text: %s, target-layer: %d, current-layer: %d", btn.getText(), target_layer, current_layer);
            if (target_layer != 0 && target_layer != current_layer + 1) {
                incorrectCount++;
                btn.setAlpha(0.2f);
            }
            btn.setEnabled(false);//.enabled = false;
        }

        int user_score = (int) ((double) ((4.0 - (double) incorrectCount) / 4.0) * (double) score);
        if (user_score < 0) user_score = 0;

        String str;
        if (incorrectCount <= 1) {
            if (score != 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
//        if (gems!=0) [blkNavControllerRef updateGems:gems total:gems];
//            [blkNavControllerRef playSfx:@ "Correct"];
            playRawSound(R.raw.correct);
            str = feedback == null || feedback.length < 1 ? "" : feedback[0];//[feedback objectAtIndex:0];
            if (!TextUtils.isEmpty(str)) {
                busDriver.showHelp(str);
                feedbackShown = true;
            }
//            [blkNavControllerRef playSfx:@ "Correct"];
            playRawSound(R.raw.correct);
        } else {
            if (score != 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
//        if (gems!=0) [blkNavControllerRef updateGems:0 total:gems];
//            [blkNavControllerRef playSfx:@ "Incorrect"];
            playRawSound(R.raw.incorrect);
            str = feedback == null || feedback.length < 2 ? "" : feedback[1];//[feedback objectAtIndex:0];
            if (!TextUtils.isEmpty(str)) {
                feedbackShown = true;
//                [feedback_vc show:str];
                busDriver.showHelp(str);
            }
//            [blkNavControllerRef playSfx:@ "Incorrect"];
            playRawSound(R.raw.incorrect);
        }
//        feedback_vc.startTimerOnNext = false;
//        feedback_vc.nextBlinkOnNext = true;
        if (!feedbackShown) {
            busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        } else {
            nextOnClosed = true;
        }
    }
}
