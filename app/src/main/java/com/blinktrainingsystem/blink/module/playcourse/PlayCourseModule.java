package com.blinktrainingsystem.blink.module.playcourse;

import com.blinktrainingsystem.blink.data.local.LocalFileManager;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.parser.CourseHelper;
import com.blinktrainingsystem.blink.data.model.pojo.Course;
import com.blinktrainingsystem.blink.module.playcourse.buildstatement.BuildAStatementFragment;
import com.blinktrainingsystem.blink.module.playcourse.catchgame.CatchGameFragment;
import com.blinktrainingsystem.blink.module.playcourse.conversation.ConversationFragment;
import com.blinktrainingsystem.blink.module.playcourse.draganddrop.DragAndDropFragment;
import com.blinktrainingsystem.blink.module.playcourse.ebook.EBookFragment;
import com.blinktrainingsystem.blink.module.playcourse.handler.BlinkHandler;
import com.blinktrainingsystem.blink.module.playcourse.input.InputFragment;
import com.blinktrainingsystem.blink.module.playcourse.multiple.MultipleFragment;
import com.blinktrainingsystem.blink.module.playcourse.orderitem.OrderItemFragment;
import com.blinktrainingsystem.blink.module.playcourse.presentation.PresentationFragment;
import com.blinktrainingsystem.blink.module.playcourse.question.QuestionFragment;
import com.blinktrainingsystem.blink.module.playcourse.simon.SimonFragment;
import com.blinktrainingsystem.blink.module.playcourse.statementrotator.StatementRotatorFragment;
import com.blinktrainingsystem.blink.module.playcourse.thisorthat.ThisOrThatFragment;
import com.blinktrainingsystem.blink.module.playcourse.video.VideoFragment;
import com.blinktrainingsystem.blink.module.playcourse.wordfill.WordFillFragment;
import com.blinktrainingsystem.blink.module.playcourse.wordheat.WordHeatFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/17/2014.
 */
@Module(
        complete = false,
        library = true,
        injects = {
                PlayCourseActivity.class,
                PlayCourseFragment.class,
                UnitStartFragment.class,
                BurstGridFragment.class,
                ActivityGridFragment.class,
                BurstStartFragment.class,
                BurstEndFragment.class,
                UnitsGridFragment.class,
                DossierFragment.class,
                DossierGridFragment.class,
                SimonFragment.class,
                ThisOrThatFragment.class,
                DragAndDropFragment.class,
                InputFragment.class,
                EBookFragment.class,
                PresentationFragment.class,
                BuildAStatementFragment.class,
                WordFillFragment.class,
                OrderItemFragment.class,
                QuestionFragment.class,
                StatementRotatorFragment.class,
                WordHeatFragment.class,
                VideoFragment.class,
                CatchGameFragment.class,
                ConversationFragment.class,
                MultipleFragment.class,
                LiteProgressFragment.class,
        }
)
public class PlayCourseModule {
    private final Course course;

    public PlayCourseModule(Course course) {
        this.course = course;
    }

    @Provides
    @Singleton
    public CourseHelper provideCourseHelper(LocalFileManager localFileManager) {
        return new CourseHelper(localFileManager, course.id, LocalFileManager.Folder.get(course.folderName));
    }

    @Provides
    @Singleton
    public BlinkHandler provideBlinkHandler(LocalFileManager localFileManager, LocalSharedPreferences localSharedPreferences) {
        return new BlinkHandler(provideCourseHelper(localFileManager), localSharedPreferences);
    }
}
