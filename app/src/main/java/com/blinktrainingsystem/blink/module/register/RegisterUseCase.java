package com.blinktrainingsystem.blink.module.register;

import android.support.annotation.Nullable;

import org.json.JSONObject;

/**
 * Created by TALE on 9/10/2014.
 */
public interface RegisterUseCase {

    void submitRegisterForm(@Nullable String firstName, @Nullable String lastName, String email, String password);

    void cancelRegisterTask();

    void backToLogin();

    void fbLogin(final JSONObject json);

}
