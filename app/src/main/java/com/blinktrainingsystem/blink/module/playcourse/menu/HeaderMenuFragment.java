package com.blinktrainingsystem.blink.module.playcourse.menu;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.Constant;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by TALE on 9/24/2014.
 */
public class HeaderMenuFragment extends MenuBaseFragment {

    private static final String KEY_PROGRESS_VISIBLE = "progress_visible";
    private static final String KEY_PROGRESS_STEP = "progress_step";
    private static final String KEY_PROGRESS_TOTAL_STEP = "progress_total_step";
    private static final String KEY_TYPE = "type";
    private static final String KEY_FB_VISIBLE = "fbVisible";
    private static final String KEY_TW_VISIBLE = "twVisible";

    @InjectView(R.id.rlProgressStep)
    RelativeLayout rlProgressStep;

    @Inject
    ViewAnimator viewAnimator;
    @InjectView(R.id.llDots)
    LinearLayout _LlDots;
    @InjectView(R.id.ivTw)
    ImageView _IvTw;
    @InjectView(R.id.ivFb)
    ImageView _IvFb;
    @InjectView(R.id.swAllowSkip)
    Switch _SwAllowSkip;
    @InjectView(R.id.llAllowSkip)
    LinearLayout _LlAllowSkip;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    private int dotSize;
    private int lineHeight;


    private OnClickListener onClickListener;

    public static interface OnClickListener {
        void onMenuClick();
    }

    public static class MenuConfig {
        public static final int PROGRESS = 1;
        public static final int SOCIAL = 2;

        public boolean progressVisible;
        public int step;
        public int totalStep;

        // OPTIONAL
        public int type = PROGRESS;
        public boolean fbVisible;
        public boolean twVisible;


        public MenuConfig(boolean progressVisible, int totalStep, int step) {
            this.progressVisible = progressVisible;
            this.totalStep = totalStep;
            this.step = step;
        }

        public MenuConfig(boolean fbVisible, boolean twVisible) {
            this.type = SOCIAL;
            this.fbVisible = fbVisible;
            this.twVisible = twVisible;
        }
    }

    public static HeaderMenuFragment newInstance(MenuConfig menuConfig) {
        final HeaderMenuFragment fragment = new HeaderMenuFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_PROGRESS_VISIBLE, menuConfig.progressVisible);
        args.putBoolean(KEY_FB_VISIBLE, menuConfig.fbVisible);
        args.putBoolean(KEY_TW_VISIBLE, menuConfig.twVisible);
        args.putInt(KEY_TYPE, menuConfig.type);
        args.putInt(KEY_PROGRESS_STEP, menuConfig.step);
        args.putInt(KEY_PROGRESS_TOTAL_STEP, menuConfig.totalStep);
        fragment.setArguments(args);
        return fragment;
    }

    public void onEvent(final MenuConfig menuConfig) {
        Timber.d("showHeaderMenu => show:%s, total step: %d, step:%d, type:%d, fbVisible:%s, twVisible:%s", menuConfig.progressVisible, menuConfig.totalStep, menuConfig.step, menuConfig.type, menuConfig.fbVisible, menuConfig.twVisible);
        switch (menuConfig.type) {
            case MenuConfig.PROGRESS:
                if (menuConfig.progressVisible) {
                    _LlDots.setVisibility(View.VISIBLE);
                    if (_LlDots.getWidth() == 0) {
                        ViewObservableHelper.globalLayoutFrom(_LlDots).subscribe(
                                new Action1<View>() {
                                    @Override
                                    public void call(View view) {
                                        currentStep = menuConfig.step;
                                        createStepViews(menuConfig.totalStep);
                                        blinkStep(currentStep);
                                    }
                                }
                        );
                    } else {
                        currentStep = menuConfig.step;
                        createStepViews(menuConfig.totalStep);
                        blinkStep(currentStep);
                    }
                    _IvFb.setVisibility(View.GONE);
                    _IvTw.setVisibility(View.GONE);
                } else {
                    _LlDots.setVisibility(View.GONE);
                }
                break;
            case MenuConfig.SOCIAL:
                _IvFb.setVisibility(menuConfig.fbVisible ? View.VISIBLE : View.GONE);
                _IvTw.setVisibility(menuConfig.twVisible ? View.VISIBLE : View.GONE);
                _LlDots.setVisibility(View.GONE);
                break;
        }

    }

    @OnClick(R.id.ivFb)
    public void shareFbClick() {
        BusDriver.withBus(bus).shareFb();
    }

    @OnClick(R.id.ivTw)
    public void shareTwClick() {
        BusDriver.withBus(bus).shareTw();
//        SimpleAlertDialogFragment.newInstance("Coming Soon!").show(getFragmentManager(), "ComingSoon");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnClickListener) {
            onClickListener = (OnClickListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.header_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (localSharedPreferences.admin().getOr(false)) {
            _LlAllowSkip.setVisibility(View.VISIBLE);
            boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
            if (!isTablet) {
                _SwAllowSkip.setScaleX(0.7f);
                _SwAllowSkip.setScaleY(0.7f);
            }
            _SwAllowSkip.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Timber.d("onCheckedChanged:%s", isChecked);
                            localSharedPreferences.allowSkip().put(isChecked);
                            BusDriver.withBus(bus).allowSkipChange();
                        }
                    }
            );
            _SwAllowSkip.setChecked(localSharedPreferences.allowSkip().getOr(false));
        }

        final Bundle args = getArguments();
        final int step = args.getInt(KEY_PROGRESS_STEP, 0);
        final int totalStep = args.getInt(KEY_PROGRESS_TOTAL_STEP, 0);
        final int type = args.getInt(KEY_TYPE, MenuConfig.PROGRESS);
        final boolean fbVisible = args.getBoolean(KEY_FB_VISIBLE, false);
        final boolean twVisible = args.getBoolean(KEY_TW_VISIBLE, false);
        final boolean visible = args.getBoolean(KEY_PROGRESS_VISIBLE, false);
        switch (type) {
            case MenuConfig.PROGRESS:
                onEvent(new MenuConfig(visible, totalStep, step));
                break;
            case MenuConfig.SOCIAL:
                onEvent(new MenuConfig(fbVisible, twVisible));
                break;
        }
    }

    @OnClick(R.id.btMenu)
    public void onMenuClick() {
        if (onClickListener != null) {
            onClickListener.onMenuClick();
        }
    }

    private View newDot() {
        ImageView dotView = new ImageView(getActivity());
        dotView.setLayoutParams(new ViewGroup.LayoutParams(dotSize, dotSize));
        dotView.setImageResource(R.drawable.dot_normal);
        return dotView;
    }

    int currentStep = 0;

    private void blinkStep(int step) {
        currentStep = step;
        int index = (step - 1) * 2;
        if (index < _LlDots.getChildCount()) {
            ImageView stepView = (ImageView) _LlDots.getChildAt(index);
            stepView.setImageResource(R.drawable.dot_selected);
            viewAnimator.blink(stepView, Constant.ANIMATION_DURATION);
        }
    }

    private View newLine() {
        View line = new View(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, lineHeight);
        lp.weight = 1;
        lp.gravity = Gravity.CENTER;
        line.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        line.setLayoutParams(lp);
        return line;
    }

    private void createStepViews(int amount) {
        if (amount <= 0) {
            return;
        }
        dotSize = getResources().getDimensionPixelSize(R.dimen.dot_size);
        lineHeight = getResources().getDimensionPixelSize(R.dimen.progress_h);
        // Clear views.
        _LlDots.removeAllViews();
        for (int i = 0; i < amount; i++) {
            if (i > 0) {
                View line = newLine();
                _LlDots.addView(line);
            }
            ImageView dotView = (ImageView) newDot();
            _LlDots.addView(dotView);
            final int step = i + 1;
            if (i < currentStep) {
                dotView.setImageResource(R.drawable.dot_over);
                dotView.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (step >= currentStep) {
                                    return;
                                }
                                final int offset = step - currentStep;
                                blinkStep(step);
                                BusDriver.withBus(bus).headerMenuStepClick(offset);
                            }
                        }
                );
            } else {
                dotView.setOnClickListener(null);
            }

        }
    }
}
