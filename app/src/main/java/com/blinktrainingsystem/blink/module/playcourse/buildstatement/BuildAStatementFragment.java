package com.blinktrainingsystem.blink.module.playcourse.buildstatement;

import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.data.model.pojo.Statement;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.ResourcesHelper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.google.common.base.Splitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class BuildAStatementFragment extends BaseBurstFragment {

    int inCorrectCount;
    int currentSentencesIdx;
    int currentWordIdx;
    int wordCount;
    int correctCount;
    String guid;
    JsonEx json;
    Statement data;
    boolean ordered;
    ArrayList<Integer> cols;
    ArrayList<TextView> statement_btns;
    int wrongCount;

    boolean isShowTimeOut;
    boolean isComplete;

    @InjectView(R.id.tvTitle)
    TextView lblPrompt;

    @InjectView(R.id.contentView)
    LinearLayout contentView;

    @Inject
    AssetManager assetManager;

    @Inject
    UiHelper shape;

    @Inject
    DeviceInfo deviceInfo;

    @Inject
    ResourcesHelper resourcesHelper;

    @Inject UiHelper uiHelper;

    @Inject TrackerHelper trackerHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_build_statement, container, false);
    }

    @Override public void onResume() {
        super.onResume();
        trackerHelper.buildStatementLoad();
    }

    @Override
    protected void startBurst() {
        super.startBurst();

        if (localSharedPreferences.isHelpShown(course.id, blink.type).getOr(false)) {
            init();
        }

        if (json != null && data != null) {
            initStatement();
            addPrompt();

            ViewObservableHelper.globalLayoutFrom(getView()).subscribe(
                    new Action1<View>() {
                        @Override
                        public void call(View view) {
                            initStatmentButton();
                        }
                    }
            );
        }
    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @Override
    protected String getHelpMessage() {
        init();
        String str;
        if (ordered) {
            str = getString(R.string.build_statment_ordered_help);
        } else {
            str = getString(R.string.build_statment_help);
        }
        return str;
    }

    private void init() {
        guid = blink == null ? null : blink.guid;
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        data = json == null ? null : json.statement;
        ordered = data.ordered;
    }

    private void showHelp(String help) {
        busDriver.showHelp(help);
    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isShowTimeOut) {
                    busDriver.notifyBlinkCompleted();
                } else if (isComplete) {
                    busDriver.notifyBlinkCompleted();
                }
                break;

            case TimeOut: {
                /* time out */
                timeOut();
            }
        }
    }

    private void initStatement() {
        inCorrectCount = 0;
        currentSentencesIdx = 0;
        currentWordIdx = 0;
        wordCount = 0;

        int color1 = Color.parseColor("#256ED7");
        int color2 = Color.parseColor("#B95F8A");
        int color3 = Color.parseColor("#13D452");
        int color4 = Color.parseColor("#00BEEE");
        int color5 = Color.parseColor("#6E6ECC");
        int color6 = Color.parseColor("#6E6E24");
        int color7 = Color.parseColor("#6E6E33");

        cols = new ArrayList<Integer>();
        cols.add(color1);
        cols.add(color2);
        cols.add(color3);
        cols.add(color4);
        cols.add(color5);
        cols.add(color6);
        cols.add(color7);
    }

    private void addPrompt() {
        if (data != null) {
            String prompt = data.prompt;
            prompt = Helper.getStringWithReplacements(prompt);
            lblPrompt.setText(prompt);
            boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
            Util.setTypeface(lblPrompt, getString(R.string.font_myriad_pro_regular));
            lblPrompt.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
            String sound = data.sound;
            playSound(sound, false);
        }
    }

    private LinearLayout newRow() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return linearLayout;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void initStatmentButton() {

        try {

            statement_btns = new ArrayList<TextView>();
            ArrayList<JSONObject> s_data = new ArrayList<JSONObject>();

            int index = 1;
            if (data != null) {
                String wrong = data.wrong;
                Splitter splitter = Splitter.on("|");
                List<String> words = splitter.splitToList(wrong);

                wrongCount = words.size();

                for (String item : words) {

                    int num = 0;
                    JSONObject dict = new JSONObject();
                    dict.put("string", item);
                    dict.put("tag", num);

                    s_data.add(dict);
                }

            /* Build an array of the words */
                for (int i = 0; i < data.statements.length; i++) {
                    String str = data.statements[i];
                    words = splitter.splitToList(str);
                    int wordIdx = 0;

                    for (String item : words) {
                        int num = ((wordIdx << 8) & 0xFF00) + (index & 0xFF);

                        JSONObject dict = new JSONObject();
                        dict.put("string", item);
                        dict.put("tag", num);
                        s_data.add(dict);
                        wordIdx++;
                    }

                    index++;
                }


            /* jumble array */
                int count = s_data.size();

                Collections.shuffle(s_data);

                int boxWidth = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.build_statment_box_width));
                int boxHeight = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.build_statment_box_height));
                int marginLeft = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.build_statment_margin_left));
                int spacing = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.build_statment_spacing));

                int row = (int) Math.ceil((double) count / 3.0);
                int top = (getView().getHeight() - lblPrompt.getHeight() - row * spacing) / 2;


                LinearLayout rowView = null;
                for (int i = 0; i < s_data.size(); i++) {

                    if (i % 3 == 0) {
                        rowView = newRow();
                        if (i > 0) {
                            ((LinearLayout.LayoutParams) rowView.getLayoutParams()).topMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.marginleft_20));
                        }
                        contentView.addView(rowView);
                    }

                    JSONObject dict = s_data.get(i);

                    final TextView btn = new TextView(getActivity());
                    Util.setTypeface(btn, getString(R.string.font_myriad_pro_regular));
                    final int maxTextSize = getResources().getDimensionPixelSize(R.dimen.answer_font_size);
                    btn.setTextSize(maxTextSize);
                    btn.setTextColor(Color.WHITE);
                    btn.setSingleLine(true);
                    btn.setGravity(Gravity.CENTER);
                    btn.setEllipsize(TextUtils.TruncateAt.END);
                    Drawable drawable = shape.createRoundedDrawable(cols.get(0), 6.0f);
                    CompatibleHelper.setBackgroundDrawable(btn, drawable);
                    final int tag = dict.getInt("tag");
                    Timber.d("tag: %d", tag);
                    btn.setTag(tag);

                    String str = dict.getString("string");
                    str = Helper.getStringWithReplacements(str);

                    btn.setText(str);

                    final float textWidth = btn.getPaint().measureText(str);
                    if (textWidth > boxWidth) {
                        uiHelper.adjustFontSize(btn, boxWidth);
                    }
                    btn.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    wordPress(btn);
                                }
                            }
                    );

                    Timber.d("Top: %d", top);
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(boxWidth, boxHeight);
                    if (i % 3 != 0) {
                        layoutParams.leftMargin = deviceInfo.getDimensionPixel(getResources().getInteger(R.integer.marginleft_20));
                    }
                    rowView.addView(btn, layoutParams);

                    statement_btns.add(btn);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void wordPress(TextView button) {
        int sentenceIdx = (Integer) button.getTag() & 0xFF;
        int wordIdx = ((Integer) button.getTag() & 0xFF00) >> 8;

        if (currentSentencesIdx == 0) {
            /* New sentences */
            if (ordered) {
                currentWordIdx = wordIdx;
                if (currentWordIdx == 0 && sentenceIdx != 0) {
                    currentSentencesIdx = sentenceIdx;
                    correctPress(sentenceIdx, (Integer) button.getTag(), button);
                } else {
                    //Whoops wrong
                    wrongPress();
                }
            } else {
                if (sentenceIdx != 0) {
                    correctPress(sentenceIdx, sentenceIdx, button);
                } else {
                    //Whoops wrong
                    wrongPress();
                }
            }
        } else {
            if (ordered) {
                if (sentenceIdx == currentSentencesIdx && (wordIdx == currentWordIdx)) {
                    correctWordCount(button, sentenceIdx);
                } else {
                    //Whoops wrong
                    wrongWordCount();
                }
            } else {
                if (sentenceIdx == currentSentencesIdx) {
                    correctWordCount(button, sentenceIdx);
                } else {
                    //Whoops wrong
                    wrongWordCount();
                }

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void correctPress(int sentenceIdx, int tag, TextView button) {
        currentSentencesIdx = sentenceIdx;
        CompatibleHelper.setBackgroundDrawable(button, shape.createRoundedDrawable(cols.get(tag), 6.0f));
        currentWordIdx++;
        wordCount = 1;

        playRawSound(R.raw.suction_place);
    }

    private void wrongPress() {
        inCorrectCount++;

        playRawSound(R.raw.incorrect);

        if (inCorrectCount >= 3) {
            blinkComplete();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void correctWordCount(TextView btn, int sentenceIdx) {
        CompatibleHelper.setBackgroundDrawable(btn, shape.createRoundedDrawable(cols.get(sentenceIdx), 6.0f));
        wordCount++;
        currentWordIdx++;
        int count = 0;
        for (TextView button : statement_btns) {
            if (((Integer) button.getTag() & 0xFF) == currentSentencesIdx) count++;
        }
        if (wordCount == count) {
            for (TextView button : statement_btns) {
                if (((Integer) button.getTag() & 0xFF) == currentSentencesIdx) {
                    button.setEnabled(false);
                }
            }
            playRawSound(R.raw.correct);
            currentSentencesIdx = 0;
            correctCount += count;
            if ((correctCount + wrongCount) == statement_btns.size()) {
                blinkComplete();
            }
        } else {
            playRawSound(R.raw.suction_place);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void wrongWordCount() {
        inCorrectCount++;
        for (TextView btn : statement_btns) {
            if (((Integer) btn.getTag() & 0xFF) == currentSentencesIdx) {
                Drawable drawable = shape.createRoundedDrawable(cols.get(0), 6.0f);
                CompatibleHelper.setBackgroundDrawable(btn, drawable);
            }
        }
        playRawSound(R.raw.incorrect);
        currentSentencesIdx = 0;
        if (inCorrectCount >= 3) {
            blinkComplete();
        }
    }

    private void timeOut() {
        if (statement_btns != null) {
            for (TextView btn : statement_btns) {
                btn.setEnabled(false);
            }
        }

        isShowTimeOut = true;
        String str = "Sorry, you're out of time.";
        showHelp(str);
    }

    private void blinkComplete() {
        busDriver.pauseTimer();

        int score = json.score;
        int gems = json.gems;
        isComplete = true;

        String feedback = "";

        for (int i = 0; i < statement_btns.size(); i++) {
            TextView btn = statement_btns.get(i);
            btn.setEnabled(false);
        }

        int user_score = (int) ((double) ((4.0 - (double) inCorrectCount) / 4.0) * (double) score);
        if (user_score < 0) user_score = 0;

        if (inCorrectCount <= 1) {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.correct);
            if (json.feedback.length > 0) {
                feedback = json.feedback[0];
            }
        } else {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }

            playRawSound(R.raw.incorrect);
            if (json.feedback.length > 1) {
                feedback = json.feedback[1];
            }
        }

        if (feedback.length() > 0) {
            showHelp(feedback);
        } else {
            busDriver.notifyBlinkCompleted();
        }
    }

}
