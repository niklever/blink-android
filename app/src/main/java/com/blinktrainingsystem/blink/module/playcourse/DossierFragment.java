package com.blinktrainingsystem.blink.module.playcourse;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.parser.BaseParser;
import com.blinktrainingsystem.blink.data.model.parser.DossierParser;
import com.blinktrainingsystem.blink.data.model.pojo.Dossier;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.net.WebServicesWrapper;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.google.common.base.Splitter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by TALE on 11/3/2014.
 */
public class DossierFragment extends PlayCourseFragment {
    @InjectView(R.id.tvTab)
    TextView _TvTab;
    @InjectView(R.id.llDossier)
    LinearLayout _LlDossier;
    @Inject
    DeviceInfo deviceInfo;
    @Inject TrackerHelper trackerHelper;

    private List<Map<String, Object>> textViews;

    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dossiers, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Util.setTypeface(_TvTab, getString(R.string.font_myriad_pro_regular));

        _TvTab.getLayoutParams().width = deviceInfo.screen_W * 2 / 7;
        _TvTab.setTextColor(getResources().getColor(R.color.text_dossier_tab));
        _TvTab.setText(Helper.getStringWithReplacements(blink.summary));
        initUi();
        initWithDossier();
    }

    //    - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//    {
//        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//        if (self) {
//            // Custom initialization
//        }
//        return self;
//    }
//


    @Override
    public void onResume() {
        super.onResume();
        trackerHelper.dossierLoad();
        BusDriver.withBus(bus).setFooterMenu(null, null, getString(R.string.txt_done));
    }

    @Inject
    WebServices webServices;

    public boolean initWithDossier() {
        final Integer highestIndex = localSharedPreferences.highestIndex(course.id).getOr(0);
        final List<Dossier> dossiers = blinkHandler.getDossiers(highestIndex, blinkIndex);
        if (dossiers == null || dossiers.size() == 0) {
            return false;
        }

        List<String> guids = new ArrayList<String>();
        for (Dossier dossier : dossiers) {
            if (!TextUtils.isEmpty(dossier.guid)) {
                guids.add(dossier.guid);
            }
        }
        showProgress(true);
        final Subscription subscription = AndroidObservable.bindFragment(this, ((WebServicesWrapper) webServices).getDossier(course.id, localSharedPreferences.userId().getOr(0l), guids))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Observer<DossierParser>() {
                            @Override
                            public void onCompleted() {
                                showProgress(false);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(DossierParser dossierParser) {
                                try {
                                    webServiceOnComplete(dossierParser);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                );

        takeCareSubscription(subscription);
        return true;
    }

    private TextView newTextView1() {
        TextView textView = new TextView(getActivity());
        textView.setTextColor(Color.WHITE);
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = (int) (deviceInfo.density * 10);
        textView.setLayoutParams(layoutParams);
        return textView;
    }

    private EditText newTextView2() {
        EditText textView = new EditText(getActivity());
        textView.setTextColor(Color.BLACK);
        Util.setTypeface(textView, getString(R.string.font_myriad_pro_regular));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, deviceInfo.getDimensionPixel(80));
        layoutParams.topMargin = (int) (deviceInfo.density * 10);
        textView.setLayoutParams(layoutParams);
        textView.setBackgroundColor(Color.WHITE);
        textView.setGravity(Gravity.TOP);
        return textView;
    }

    private LinearLayout newRow() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);
        return linearLayout;
    }

    //
    void initUi() {
        textViews = new ArrayList<Map<String, Object>>();
        SeekBar sld = null;
        EditText txt = null;
        Map<String, Object> dict = null;

        final Integer highestIndex = localSharedPreferences.highestIndex(course.id).getOr(0);
        final List<Dossier> dossiers = blinkHandler.getDossiers(highestIndex, blinkIndex);
        if (dossiers == null || dossiers.size() == 0) {
            TextView textView = newTextView1();
            textView.setText("No user data.");
            _LlDossier.addView(textView);
        } else {

            for (Dossier item : dossiers) {
                final String label = item.label;
                final TextView textView = newTextView1();
                textView.setText(Helper.getStringWithReplacements(label));
                _LlDossier.addView(textView);
                int type = item.type;//[[item objectForKey:@"type"] intValue];
                if (type >= 6) {
                    //We need to add a slider
                    sld = newSlider();
                    sld.setMax(item.max);
//                sld.minimumValue = [[item objectForKey:@"min"] intValue];
//                sld.maximumValue = [[item objectForKey:@"max"] intValue];
                    sld.setTag(textViews.size());
                    sld.setOnSeekBarChangeListener(
                            new SeekBar.OnSeekBarChangeListener() {
                                @Override
                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                    if (fromUser) {
                                        final Map<String, Object> dict = textViews.get(((Integer) seekBar.getTag()));
                                        dict.put("modified", true);
                                    }
                                }

                                @Override
                                public void onStartTrackingTouch(SeekBar seekBar) {
                                }

                                @Override
                                public void onStopTrackingTouch(SeekBar seekBar) {

                                }
                            }
                    );
                    _LlDossier.addView(sld);
                }
                if (type == 1 || type == 7) {
                    txt = newTextView2();
                    txt.setTag(textViews.size());
                    _LlDossier.addView(txt);
                }
                List<TextView> words = null;
                if (type == 2) {
                    int wordCount = item.count;// objectForKey:@"count"] intValue];
                    words = new ArrayList<TextView>();
                    int col = 0;
                    LinearLayout linearLayout = newRow();
                    _LlDossier.addView(linearLayout);
                    for (int i = 0; i < wordCount; i++) {
                        txt = newTextView2();
                        final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) txt.getLayoutParams();
                        layoutParams.width = 0;
                        layoutParams.weight = 1;
                        txt.setTag(textViews.size());// count];
                        linearLayout.addView(txt);
                        col++;
                        if (col >= 3) {
                            linearLayout = newRow();
                            _LlDossier.addView(linearLayout);
                        }
                        words.add(txt);
                    }
                }
                switch (type) {
                    case 1://Text
//                    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:type], @"type", txt, @"txt", [item objectForKey:@"guid"], @"guid", [NSNumber numberWithBool:NO], @"modified", null];
                        dict = new HashMap<String, Object>();
                        dict.put("type", type);
                        dict.put("txt", txt);
                        dict.put("guid", item.guid);
                        dict.put("modified", false);
                        break;
                    case 2://Words
//                    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:type], @"type", words, @"words", [item objectForKey:@"guid"], @"guid", [NSNumber numberWithBool:NO], @"modified", null];
                        dict = new HashMap<String, Object>();
                        dict.put("type", type);
                        dict.put("word", words);
                        dict.put("guid", item.guid);
                        dict.put("modified", false);
                        break;
                    case 6://Slider
//                    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:type], @"type", sld, @"slider", [item objectForKey:@"guid"], @"guid", [NSNumber numberWithBool:NO], @"modified", null];
                        dict = new HashMap<String, Object>();
                        dict.put("type", type);
                        dict.put("slider", sld);
                        dict.put("guid", item.guid);
                        dict.put("modified", false);
                        break;
                    case 7://Slider with text
//                    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:type], @"type", txt, @"txt", sld, @"slider", [item objectForKey:@"guid"], @"guid", [NSNumber numberWithBool:NO], @"modified", null];
                        dict = new HashMap<String, Object>();
                        dict.put("type", type);
                        dict.put("txt", txt);
                        dict.put("slider", sld);
                        dict.put("guid", item.guid);
                        dict.put("modified", false);
                        break;
                }
                if (txt != null) {
                    final EditText finalTxt = txt;
                    txt.addTextChangedListener(
                            new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    final Map<String, Object> dict = textViews.get(((Integer) finalTxt.getTag()));
                                    dict.put("modified", true);
                                }
                            }
                    );
                }
                textViews.add(dict);// addObject:dict];
            }
        }
    }

    public void onEvent(Event event) {
        switch (event) {
            case DoneClick:
                nextPressed();
                break;
        }
    }

    public void nextPressed() {

        if (textViews == null || textViews.size() == 0) {
            BusDriver.withBus(bus).openDossier();
            return;
        }
        //Need to save user data here
//        NSMutableArray *data = [[NSMutableArray alloc] init];
        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        SeekBar sld;
        EditText txt;
        Map<String, String> dct;
        List<EditText> words;
        StringBuilder str = null;

        for (Map<String, Object> dict : textViews) {
            final Boolean modified = (Boolean) dict.get("modified");
            if (modified) {
                int type = (Integer) dict.get("type");//[[dict objectForKey:@"type"] intValue];
                switch (type) {
                    case 1:
                        txt = (EditText) dict.get("txt");//[dict objectForKey:@"txt"];
                        dct = new HashMap<String, String>();
                        dct.put("text", txt.getText().toString());
                        dct.put("guid", ((String) dict.get("guid")));
                        data.add(dct);
//                        dct = [NSDictionary dictionaryWithObjectsAndKeys:txt.text, @"text", [dict objectForKey:@"guid"], @"guid", null];
//                        [data addObject:dct];
                        break;
                    case 2:
                        words = (List<EditText>) dict.get("words");//[dict objectForKey:@"words"];
                        if (words != null && words.size() > 0) {
                            for (EditText word : words) {
                                if (str == null) {
                                    str = new StringBuilder(word.getText().toString());//[word.text mutableCopy];
                                } else {
                                    str.append(String.format(",%s", word.getText().toString()));
//                                    [str appendString:[NSString stringWithFormat:@",%@", word.text]];
                                }
                            }
//                            for (EditText word in words){
//                                if (str == null){
//                                    str = [word.text mutableCopy];
//                                }else{
//                                    [str appendString:[NSString stringWithFormat:@",%@", word.text]];
//                                }
//                        }

                        }
//                    dct = [NSDictionary dictionaryWithObjectsAndKeys:str, @"text", [dict objectForKey:@"guid"], @"guid", null];
//                    [data addObject:dct];
                        dct = new HashMap<String, String>();
                        dct.put("text", str.toString());
                        dct.put("guid", ((String) dict.get("guid")));
                        data.add(dct);
                        break;
                    case 6:
                        sld = (SeekBar) dict.get("slider");//[dict objectForKey:@"slider"];
                        dct = new HashMap<String, String>();
                        dct.put("text", String.valueOf(sld.getProgress())); // TODO convert to float
                        dct.put("guid", ((String) dict.get("guid")));
                        data.add(dct);
//                        dct = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%4.2f", sld.value], @"text", [dict objectForKey:@"guid"], @"guid", null];
//                        [data addObject:dct];
                        break;
                    case 7:
                        txt = (EditText) dict.get("txt");//[dict objectForKey:@"txt"];
//                        sld = [dict objectForKey:@"slider"];
                        sld = (SeekBar) dict.get("slider");//[dict objectForKey:@"slider"];
                        dct = new HashMap<String, String>();
                        dct.put("text", String.format("%f, %s", (float) sld.getProgress(), txt.getText().toString())); // TODO convert to float
                        dct.put("guid", ((String) dict.get("guid")));
                        data.add(dct);
//                        dct = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%4.2f,%@", sld.value, txt.text], @"text", [dict objectForKey:@"guid"], @"guid", null];
//                        [data addObject:dct];
                        break;
                }
            }
        }

        if (data.size() > 0) {
            showProgress(true);
            AndroidObservable.bindFragment(this, ((WebServicesWrapper) webServices).saveDossier(data, course.id, localSharedPreferences.userId().getOr(0l)))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Observer<BaseParser>() {
                                @Override
                                public void onCompleted() {
                                    showProgress(false);
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Toast.makeText(getActivity(), "Save dossier data error!", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onNext(BaseParser baseParser) {
                                    BusDriver.withBus(bus).openDossier();
                                }
                            }
                    );
        } else {
            BusDriver.withBus(bus).openDossier();
        }
//        blkNavControllerRef.webservice saveDossier:data courseId:[blkNavControllerRef getCourseId]];
//
//        [blkNavControllerRef popViewControllerAnimated:YES];
    }
//
//    - (void)didReceiveMemoryWarning
//    {
//        [super didReceiveMemoryWarning];
//        // Dispose of any resources that can be recreated.
//    }
//

    private SeekBar newSlider() {
        SeekBar seekBar = new SeekBar(getActivity());
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = deviceInfo.getDimensionPixel(10);
        seekBar.setLayoutParams(layoutParams);
        return seekBar;
    }

    void webServiceOnComplete(DossierParser dossierParser) throws JSONException {
        if (dossierParser == null) {
            Timber.d("webServiceOnComplete => Result is null");
            return;
        }
        Timber.d("webServiceOnComplete => DossierParser {success:%s, data.size(): %d", dossierParser.success, dossierParser.data == null ? 0 : dossierParser.data.size());

        boolean success = dossierParser.success;

        if (success) {
            final List<Dossier> items = dossierParser.data;
//            NSArray *items = [dict objectForKey:@"data"];
            int index = 0;
            TextView txt;
            SeekBar sld;
            float val = 0;// [str floatValue];
            List<String> tokens;
            if (textViews != null && textViews.size() > 0 && items != null && items.size() > 0) {
                for (Dossier dict : items) {
                    if (index >= textViews.size()) {
                        break;
                    }
                    final String str = dict.data;
                    Timber.d("Data:%s", dict.data);
//                    NSString *str = [dict objectForKey:@"data"];
                    if (!TextUtils.isEmpty(str)) {
//                        NSDictionary *dct = [textViews objectAtIndex:index];
                        final Map<String, Object> dct = textViews.get(index);
                        List<TextView> words;
                        int type = ((Integer) dct.get("type"));//[[dct objectForKey:@"type"] intValue];
                        int subindex = 0;
                        switch (type) {
                            case 1://Text
                                txt = (TextView) dct.get("txt");//[dct objectForKey:@"txt"];
                                txt.setText(Helper.getStringWithReplacements(str));//.text = [blkNavControllerRef getStringWithReplacements:str];
                                break;
                            case 2://Words
                                words = (List<TextView>) dct.get("words");//[dct objectForKey:@"words"];
                                if (words != null && words.size() > 0) {
                                    tokens = Splitter.on(",").omitEmptyStrings().splitToList(str);//[str componentsSeparatedByString:@","];
                                    for (TextView word : words) {
                                        if (subindex >= words.size() || subindex > tokens.size() - 1) {
                                            break;
                                        }
                                        word.setText(Helper.getStringWithReplacements(tokens.get(subindex)));//.text = [blkNavControllerRef getStringWithReplacements:[tokens objectAtIndex:subindex]];
                                        subindex++;
                                    }
                                }
                                break;
                            case 6://Slider
                                sld = (SeekBar) dct.get("slider");// objectForKey:@"slider"];
                                try {
                                    val = Float.parseFloat(str);
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
//                                NSLog(@"Slider value set to %4.2f (%4.2f>%4.2f)", val, sld.minimumValue, sld.maximumValue);
//                                Timber.d("Slider value set to %4.2f (%4.2f>%4.2f)", val, (float) sld.getMax());
//                                sld.value = val;
                                // TODO: Check sld min,max to set the right value here.
                                sld.setProgress((int) val);
                                break;
                            case 7://Text
                                tokens = Splitter.on(",").omitEmptyStrings().splitToList(str);//[str componentsSeparatedByString:@","];
                                sld = (SeekBar) dct.get("slider");//[dct objectForKey:@"slider"];
                                try {
                                    val = Float.parseFloat(tokens != null && tokens.size() > 0 ? tokens.get(0) : "");
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                                sld.setProgress((int) val);
//                                sld.value = [[tokens objectAtIndex:0] floatValue];
                                txt = (TextView) dct.get("txt");//[dct objectForKey:@"txt"];
                                txt.setText(Helper.getStringWithReplacements(tokens != null && tokens.size() > 1 ? tokens.get(1) : ""));//.text = [blkNavControllerRef getStringWithReplacements:[tokens objectAtIndex:1]];
                                break;
                        }
                    }
                    index++;
                    if (index >= textViews.size()) break;
                }
            }
        }
    }
//
//    - (void)textViewDidBeginEditing:(EditText )textView
//    {
//        NSLog(@"textViewDidBeginEditing");
//        textRect = textView.frame;
//    }
//
//    - (void)textViewDidChange:(EditText )txtView
//    {
//        NSMutableDictionary *dict = [textViews objectAtIndex:txtView.tag];
//        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"modified"];
//    }
//
//    - (void)textViewDidEndEditing:(EditText )txtView
//    {
//        NSLog(@"textViewDidEndEditing");
//    }
//
//    - (IBAction)sliderValueChanged:(SeekBar *)sld {
//        NSMutableDictionary *dict = [textViews objectAtIndex:sld.tag];
//        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"modified"];
//        //NSLog(@"slider value = %f", sld.value);
//    }
//
//// Call this method somewhere in your view controller setup code.
//    - (void)registerForKeyboardNotifications {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:null];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:null];
//    }
//
//    - (void)hideKeyboard:(UITapGestureRecognizer *) sender
//    {
//        [self.view endEditing:YES];
//    }
//
//// Called when the UIKeyboardDidShowNotification is sent.
//    - (void)keyboardWasShown:(NSNotification*)aNotification {
//        NSLog(@"Keyboard is active.");
//        CGRect frame = orgFrame;
//        frame.size.height = 360;
//        _content_svw.frame = frame;
//        [_content_svw scrollRectToVisible:textRect animated:YES];
//        _tag_img.frame = tagFrame;
//    }
//
//// Called when the UIKeyboardWillHideNotification is sent
//    - (void)keyboardWillBeHidden:(NSNotification*)aNotification {
//        NSLog(@"Keyboard is hidden");
//        _tag_img.frame = tagFrame;
//
////    [UIView animateWithDuration: 0.5
////                          delay: 0
////                        options: UIViewAnimationOptionCurveEaseInOut
////                     animations:^{
////                         _content_svw.frame = orgFrame;
////                         _content_svw.contentOffset = CGPointMake(0,0);
////                     }
////                     completion: ^(BOOL finished) {
////                         _tag_img.frame = tagFrame;
////                     }];
//
//        [UIView animateWithDuration: 0.5
//        animations:^{
//            _content_svw.frame = orgFrame;
//            _content_svw.contentOffset = CGPointMake(0,0);
//        }
//        completion: ^(BOOL finished) {
//            _tag_img.frame = tagFrame;
//        }];
//
//    }
}
