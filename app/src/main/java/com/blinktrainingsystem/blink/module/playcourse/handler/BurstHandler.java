package com.blinktrainingsystem.blink.module.playcourse.handler;

import com.blinktrainingsystem.blink.data.model.BlinkType;
import com.blinktrainingsystem.blink.data.model.pojo.Blink;
import com.blinktrainingsystem.blink.data.model.pojo.Json;

import java.util.List;

import timber.log.Timber;

/**
 * Created by TALE on 9/23/2014.
 */
public class BurstHandler {
    private int pointLeftInBurst;
    private int score;
    private int unlockscore;
    protected final int burstIndex;
    private int gems;

    public BurstHandler(int burstIndex, List<Blink> blinkList) {
        this.burstIndex = burstIndex;
        int index = getStartBurstIndex(burstIndex, blinkList);
        pointLeftInBurst = 0;
        gems = 0;
        Blink blink = blinkList.get(index);
        Json json;
        unlockscore = 0;
        while (blink.type != BlinkType.KBlinkTypeSpecificContentBurstEnd) {
            json = blink.loadJsonToObject();
            pointLeftInBurst += json == null ? 0 : json.score;
            gems += json == null ? 0 : json.gems;
            unlockscore += json == null ? 0 : json.unlockscore;
            index++;
            if (index >= blinkList.size()) {
                break;
            }
            blink = blinkList.get(index);
        }
        Timber.d("BurstHandler constructed");
    }

    private int getStartBurstIndex(int burstIndex, List<Blink> blinkList) {
        if (blinkList != null && blinkList.size() > 0 && burstIndex >= 0 && burstIndex < blinkList.size()) {
            int index = burstIndex;
            Blink blink = blinkList.get(burstIndex);
            while (blink.type != BlinkType.KBlinkBurstStart) {
                index--;
                if (index < 0) {
                    break;
                }
                blink = blinkList.get(index);
            }
            return index;
        }
        return 0;
    }

    public int getPointLeftInBurst() {
        return pointLeftInBurst;
    }

    public int getTargetToUnlockNextBurst() {
        return unlockscore;
    }

    public int getScore() {
        return this.score;
    }

    public void submitResult(boolean result, int score) {
        if (result) {
            this.score += score;
        }
        pointLeftInBurst -= score;
    }

    public int getGems() {
        return gems;
    }
}
