package com.blinktrainingsystem.blink.module.playcourse.question;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.Answer;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.data.model.pojo.Question;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.CompatibleHelper;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.UiHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.blinktrainingsystem.blink.util.ViewObservableHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.InjectView;
import rx.Observer;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class QuestionFragment extends BaseBurstFragment {


    Json json;
    Question data;
    boolean correct;
    TextView selected_btn;
    ArrayList<TextView> answer_btns;
    boolean timed;
    boolean isShowTimeOut;
    boolean isComplete;

    @InjectView(R.id.tvTitle)
    TextView lblQuestion;

    @InjectView(R.id.imvQuestion)
    ImageView imvQuestion;

//    @InjectView(R.id.btn1)
//    Button btn1;
//
//    @InjectView(R.id.btn2)
//    Button btn2;
//
//    @InjectView(R.id.btn3)
//    Button btn3;
//
//    @InjectView(R.id.btn4)
//    Button btn4;
//
//    @InjectView(R.id.btn5)
//    Button btn5;
//
//    @InjectView(R.id.btn6)
//    Button btn6;

    @InjectView(R.id.llButton)
    LinearLayout llButton;

    @Inject
    LocalSharedPreferences localSharedPreferences;

    @Inject
    UiHelper shapeHelper;

    @Inject TrackerHelper trackerHelper;

    Handler mHandler = new Handler(Looper.getMainLooper());
    private int buttonWidth = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        trackerHelper.questionLoad();

        initQuestion();

        showImage(
                new Callback() {
                    @Override
                    public void onSuccess() {
                        onComplete();
                    }

                    @Override
                    public void onError() {
                        onComplete();
                        imvQuestion.setVisibility(View.GONE);
                    }

                    private void onComplete() {
                        Timber.d("showImage => onCompleted");
                        if (data != null) {
                            Util.setTypeface(lblQuestion, getString(R.string.font_myriad_pro_regular));
                            lblQuestion.setText(Helper.getStringWithReplacements(data.text));
                            lblQuestion.setTextSize(getResources().getDimensionPixelSize(R.dimen.question_font_size));
                            ViewObservableHelper.globalLayoutFrom(lblQuestion).subscribe(
                                    new Observer<View>() {
                                        @Override
                                        public void onCompleted() {
                                            Timber.d("globalLayout=> onCompleted");

                                            final int height = getView().getHeight();
                                            final int lblQuestionBottom = lblQuestion.getBottom();
                                            final int buttonHeight = height - lblQuestionBottom;

                                            llButton.setMinimumHeight(buttonHeight);
                                            buttonWidth = (int) (getView().getWidth() * 0.7);
                                            loadData();
                                        }

                                        @Override
                                        public void onError(Throwable throwable) {

                                        }

                                        @Override
                                        public void onNext(View view) {

                                        }
                                    }
                            );
                        }
                    }
                }
        );

        isShowTimeOut = false;

        super.showFooterMenu();
        isComplete = false;

    }

    @Override
    protected int getTimer() {
        return data == null ? 0 : data.timer;
    }

    @Override
    protected void showFooterMenu() {
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, isAllowSkip ? getString(R.string.txt_next) : null);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//    @OnClick({R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6})
    public void onButtonClick(View view) {

        for (View button : answer_btns) {
            button.setEnabled(false);
        }

        bus.post(Event.PauseTimer);
        selected_btn = (TextView) view;
        Answer answer = data.answers.get((Integer) selected_btn.getTag());
        correct = answer.correct;
        CompatibleHelper.setBackgroundDrawable(selected_btn, shapeHelper.createRoundedDrawable(Color.parseColor("#FF9E00"), 6.0f));
        if (timed) bus.post(Event.PauseTimer);

        int delay = 1;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        showAnswer();
                    }
                }, (long) (delay * 1000)
        );
    }

    private void showAnswer() {
        if (correct) {
            CompatibleHelper.setBackgroundDrawable(selected_btn, shapeHelper.createRoundedDrawable(Color.parseColor("#00CC00"), 6.0f));
        } else {
            CompatibleHelper.setBackgroundDrawable(selected_btn, shapeHelper.createRoundedDrawable(Color.parseColor("#FF3333"), 6.0f));
        }

        blinkComplete();
    }

    private void initQuestion() {
        json = blink.loadJsonToObject();
        data = json == null ? null : json.question;
    }

    @Inject
    Picasso picasso;

    private void showImage(Callback callback) {
        if (!TextUtils.isEmpty(json.image)) {
            picasso.load(getLocalFile(json.image)).into(imvQuestion, callback);
        } else {
            if (callback != null) {
                callback.onError();
            }
        }
    }

    @Inject
    ViewAnimator viewAnimator;

    @Override
    protected String getHelpMessage() {
        return getString(R.string.question_help);
    }

    private void loadData() {
        /* play prompt sound */
        if (data != null && data.sound != null && data.sound.length() > 0) {
            playSound(data.sound, false);
        }

        answer_btns = new ArrayList<TextView>();
//        answer_btns.add(btn1);
//        answer_btns.add(btn2);
//        answer_btns.add(btn3);
//        answer_btns.add(btn4);
//        answer_btns.add(btn5);
//        answer_btns.add(btn6);
//
//        hiddenAllButtons();

        int index = 0;
        for (Answer answer : data.answers) {

            final TextView btn = newButton();
            llButton.addView(btn);
            answer_btns.add(btn);
            Util.setTypeface(btn, getString(R.string.font_myriad_pro_regular));
            CompatibleHelper.setBackgroundDrawable(btn, shapeHelper.createRoundedDrawable(getResources().getColor(R.color.yellow), 6.0f));
            btn.setTextColor(Color.BLACK);
            btn.setText(Helper.getStringWithReplacements(answer.text));
            btn.setTag(index);
            btn.setTextSize(getResources().getDimensionPixelSize(R.dimen.answer_font_size));
            btn.setVisibility(View.INVISIBLE);
            mHandler.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            viewAnimator.bottomUp(btn, 300, null);
                        }
                    }, index * 200
            );

            index++;
        }


    }

    @Inject
    DeviceInfo deviceInfo;

    private TextView newButton() {
        boolean isTablet = getResources().getBoolean(R.bool.is_tablet);
        TextView button = new TextView(getActivity());
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(buttonWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        button.setLayoutParams(params);
        button.setGravity(Gravity.CENTER);
        final int padding = deviceInfo.getDimensionPixel(8);
        if (llButton.getChildCount() > 0) {
            params.topMargin = padding;
        }
        button.setPadding(padding, padding, padding, padding);

        button.setTextColor(Color.BLACK);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onButtonClick(v);
                    }
                }
        );
        return button;
    }

    private void hiddenAllButtons() {
        for (TextView button : answer_btns) {
            button.setVisibility(View.GONE);
        }
    }

    private void showHelp(String help) {
        bus.post(Event.ShowHelp.setExtra(help));
    }

    private void timeOut() {
        Timber.d("timeOut");
        int score = json.score;
        int gems = json.gems;

        for (View btn : answer_btns) {
            btn.setEnabled(false);
        }

        isShowTimeOut = true;
        showHelp(getString(R.string.time_out_message));
    }

    private void blinkComplete() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        isComplete = true;
        bus.post(Event.PauseTimer);
        String feedback = "";
        if (selected_btn != null) {
            if (data.answers != null) {
                Answer answer = data.answers.get((Integer) selected_btn.getTag());
                if (answer != null) {
                    feedback = answer.feedback;
                }
            }
        }

        int user_score = json.score;
        int gems = json.gems;
        if (correct) {
            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    BusDriver.withBus(bus).updateScore(blinkHandler);
                }
            }

            playRawSound(R.raw.correct);
            if (feedback != null && feedback.length() > 0) {
                showHelp(feedback);
            }
        } else {
            playRawSound(R.raw.incorrect);
            if (feedback != null && feedback.length() > 0) {
                showHelp(feedback);
            }
        }

    }

    public void onEvent(Event event) {
        super.onEvent(event);
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                }
                if (isShowTimeOut) {
                    bus.post(Event.BlinkCompleted);
                } else if (isComplete) {
                    bus.post(Event.BlinkCompleted);
                }
                break;

            case TimeOut: {
                /* time out */
                timeOut();
                break;
            }
        }
    }

}
