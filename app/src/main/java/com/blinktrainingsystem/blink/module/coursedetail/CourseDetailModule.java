package com.blinktrainingsystem.blink.module.coursedetail;

import android.app.Application;

import com.blinktrainingsystem.blink.data.local.LocalSharedPreferences;
import com.blinktrainingsystem.blink.data.net.WebServices;
import com.blinktrainingsystem.blink.data.repository.CoursesRepo;
import com.blinktrainingsystem.blink.module.main.MainPresenter;
import com.blinktrainingsystem.blink.task.TaskManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TALE on 9/8/2014.
 */
@Module(
        complete = false,
        library = true,
        injects = CourseDetailFragment.class
)
public class CourseDetailModule {
    private final CourseDetailView courseDetailView;

    public CourseDetailModule(CourseDetailView courseDetailView) {
        this.courseDetailView = courseDetailView;
    }

    @Provides
    @Singleton CourseDetailPresenter provideCourseDetailPresenter(Application application, MainPresenter mainPresenter, WebServices webServices, TaskManager taskManager, LocalSharedPreferences localSharedPreferences, CoursesRepo coursesRepo) {
        return new CourseDetailPresenter(application, courseDetailView, mainPresenter, webServices, taskManager, localSharedPreferences, coursesRepo);
    }

}
