package com.blinktrainingsystem.blink.module.playcourse.simon;

import android.animation.AnimatorListenerAdapter;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.adapter.StringGridAdapter;
import com.blinktrainingsystem.blink.data.model.parser.CourseHelper;
import com.blinktrainingsystem.blink.module.playcourse.handler.SimonHandler;
import com.blinktrainingsystem.blink.util.MusicPlayer;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.List;

import timber.log.Timber;

/**
 * Created by TALE on 9/24/2014.
 */
public class SimonPresenter {
    public static final int FACE_DURATION = 400;
    public static final int BLINK_DURATION = 500;
    public static final int DISPLAY_DURATION = 500;
    private static final int BUFFER_DURATION = 500;
    private final TextView tvGuide;
    private final View layoutGuide;
    private final GridView gridView;

    private final ViewAnimator viewAnimator;
    private final SimonHandler mSimonHandler;
    private final SimonFragment mSimonFragment;

    private final Handler handler;
    private final StringGridAdapter adapter;
    private final List<String> phrases;
    private final HashMap<String, String> soundMap;
    private int step;
    private int countStep;
    private boolean result;
    private int incorrectCount = 0;
    public boolean isPaused;

    public int getIncorrectCount() {
        return incorrectCount;
    }

    static class ChangeBGTask implements Runnable {

        final int color;
        final View target;

        ChangeBGTask(int color, View target) {
            this.color = color;
            this.target = target;
        }

        @Override
        public void run() {
            target.setBackgroundColor(color);
        }
    }

    public static interface Callback {
        void onCompleted();
    }

    public SimonPresenter(final SimonFragment simonFragment, final Callback callback, TextView tvGuide, View layoutGuide, final GridView gridView, final CourseHelper courseHelper, ViewAnimator viewAnimator, SimonHandler simonHandler, final Handler handler) {
        mSimonFragment = simonFragment;
        this.tvGuide = tvGuide;
        this.layoutGuide = layoutGuide;
        this.gridView = gridView;
        gridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                        final String soundId = soundMap.get(parent.getItemAtPosition(position));
                        if (result) {
                            final String rightAnswer = phrases.get(countStep);
                            result = Objects.equal(parent.getItemAtPosition(position), rightAnswer);
                        }
                        if (!result) {
                            gridView.setEnabled(false);
                            mSimonFragment.playLongSound(
                                    soundId, new MusicPlayer.Callback() {
                                        @Override public void onCompleted() {
                                            simonFragment.playRawSound(R.raw.incorrect);
                                            showTryAgain();
                                        }
                                    }, false
                            );
                        } else {
                            countStep++;
                            Timber.d("countStep: %d", countStep);
                            if (countStep == step) {
                                gridView.setEnabled(false);
                                MusicPlayer.Callback soundCallback = new MusicPlayer.Callback() {
                                    @Override
                                    public void onCompleted() {
                                        Timber.d("Check result " + result);
                                        gridView.setEnabled(false);
                                        if (result) {
                                            step++;
                                            if ((step - 1) < phrases.size()) {
                                                showWatch();
                                            } else {
                                                callback.onCompleted();
                                            }
                                        }
                                    }
                                };
                                if (soundId != null) {
                                    mSimonFragment.playLongSound(soundId, soundCallback, false);
                                } else {
                                    soundCallback.onCompleted();
                                }


                            } else {
                                if (soundId != null) {
                                    simonFragment.playSound(soundId, false);
                                }
                            }
                        }

                    }
                }
        );
        this.adapter = ((StringGridAdapter) gridView.getAdapter());
        this.viewAnimator = viewAnimator;
        this.mSimonHandler = simonHandler;
        this.phrases = Lists.newArrayList(mSimonHandler.getPhrases());
        this.soundMap = Maps.newHashMap();
        final String[] sounds = simonHandler.getSounds();
        final List<String> data = simonHandler.getData();
        if (data != null && data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                final String key = data.get(i);
                if (i < sounds.length) {
                    final String soundFile = sounds[i];
                    if (soundFile == null) {
                        continue;
                    }
                    soundMap.put(key, soundFile);
                }
            }
        }
        this.handler = handler;
    }

    private void showTryAgain() {
        tvGuide.setText("Whoops.\nStart Again.");
        viewAnimator.enterFadeIn(layoutGuide, FACE_DURATION, null);
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        start();
                    }
                }, DISPLAY_DURATION + FACE_DURATION + BUFFER_DURATION
        );
    }

    public void start() {
        gridView.setEnabled(false);
        step = 1;
        result = true;
        isPaused = false;
        showWatch();
    }

    public void pause() {
        isPaused = true;
    }

    public void showWatch() {
        tvGuide.setText(R.string.txt_watch);
        viewAnimator.enterFadeIn(
                layoutGuide, FACE_DURATION, new AnimatorListenerAdapter() {
                }
        );
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        viewAnimator.exitFadeOut(
                                layoutGuide, FACE_DURATION, new AnimatorListenerAdapter() {
                                }
                        );
                        blinkItem(0);
                    }
                }, DISPLAY_DURATION + FACE_DURATION + BUFFER_DURATION
        );
    }

    public void showRepeat() {
        tvGuide.setText(R.string.txt_repeat);
        viewAnimator.enterFadeIn(
                layoutGuide, FACE_DURATION, new AnimatorListenerAdapter() {
                }
        );
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        viewAnimator.exitFadeOut(
                                layoutGuide, FACE_DURATION, new AnimatorListenerAdapter() {
                                }
                        );
                        result = true;
                        countStep = 0;
                        gridView.setEnabled(true);
                    }
                }, DISPLAY_DURATION + FACE_DURATION + BUFFER_DURATION
        );
    }

    public void blinkItem(final int index) {
        if (isPaused) {
            return;
        }
        Timber.d("blinkItem => index: %d, step: %d", index, step);
        if (index >= step) {
            showRepeat();
            return;
        }
        final String item = phrases.get(index);
        final View view = findViewForText(item);
        view.setBackgroundResource(R.drawable.rounded_yellow_button);
        final String soundId = soundMap.get(item);
        if (soundId != null) {
            mSimonFragment.playLongSound(
                    soundId, new MusicPlayer.Callback() {
                        @Override
                        public void onCompleted() {
                            blinkItem(index + 1);
                        }
                    }, false
            );
        } else {
            blinkItem(index + 1);
        }
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        view.setBackgroundResource(R.drawable.rounded_orange_yellow);
                    }
                }, BLINK_DURATION
        );
    }

    private View findViewForText(String text) {
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            if (Objects.equal(text, adapter.getItem(i))) {
                return gridView.getChildAt(i);
            }
        }
        return null;
    }

}
