package com.blinktrainingsystem.blink.module.playcourse.simon;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.BlinkApplication;
import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.adapter.StringGridAdapter;
import com.blinktrainingsystem.blink.data.model.event.Event;
import com.blinktrainingsystem.blink.data.model.pojo.JsonEx;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.module.playcourse.PlayCourseActivity;
import com.blinktrainingsystem.blink.module.playcourse.handler.SimonHandler;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by TALE on 9/23/2014.
 */
public class SimonFragment extends BaseBurstFragment {

    @InjectView(R.id.tvTitle)
    TextView tvTitle;
    @InjectView(R.id.gridView)
    GridView gridView;
    @InjectView(R.id.tvStepGuider)
    TextView mTvStepGuider;
    @InjectView(R.id.flStepGuider)
    FrameLayout mFlStepGuider;

    @Inject
    BlinkApplication application;
    @Inject
    ViewAnimator viewAnimator;
    @Inject TrackerHelper trackerHelper;

    JsonEx json;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private SimonHandler simonHandler;
    ;
    private SimonPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simon, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        StringGridAdapter adapter = new StringGridAdapter(LayoutInflater.from(application), R.layout.item_simon);
        gridView.setAdapter(adapter);
        initData();
        if (simonHandler != null && simonHandler.getData() != null && simonHandler.getData().size() > 0) {
            final String summary = simonHandler.getSummary();
            Timber.d("startBurst => simon's summary: %s", summary);
            tvTitle.setText(Helper.getStringWithReplacements(summary));
            List<String> data = Lists.newArrayList(simonHandler.getData());
            Collections.shuffle(data);
            adapter.changeDataSet(data);

            presenter = new SimonPresenter(
                    this, new SimonPresenter.Callback() {
                @Override
                public void onCompleted() {
                    blinkComplete();
                }
            }, mTvStepGuider, mFlStepGuider, gridView, courseHelper, viewAnimator, simonHandler, mHandler
            );
            if (!isHelp && presenter != null) {
                presenter.start();
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        trackerHelper.simonLoad();

        if (presenter != null && presenter.isPaused) {
            presenter.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.pause();
        }
    }

    @Override
    protected void showFooterMenu() {
        busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
    }

    private void initData() {
        json = blink == null ? null : (JsonEx) blink.loadJsonToObject();
        simonHandler = json == null ? null : new SimonHandler(blinkHandler.get(blinkIndex));
    }


    public void onEvent(Event event) {
        switch (event) {
            case HelpClosed:
                if (isHelp) {
                    isHelp = false;
                    if (simonHandler != null) {
                        presenter.start();
                    }
                } else {
                    busDriver.notifyBlinkCompleted();
                }
                break;
        }
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        ((PlayCourseActivity) getActivity()).stopLongSound();
    }

    @Override
    protected String getHelpMessage() {
        return getString(R.string.simon_help);
    }

    public void blinkComplete() {
        int score = json.score;
        int gems = json.gems;
        final int incorrectCount = presenter.getIncorrectCount();
        int user_score = (int) ((double) ((4.0 - (double) incorrectCount) / 4.0) * (double) score);
        if (user_score < 0) user_score = 0;

        if (incorrectCount <= 1) {
            if (!blinkHandler.updateScore(user_score, blinkIndex, course, gems)) {
                playRawSound(R.raw.points_lost);
            } else {
                busDriver.updateScore(blinkHandler);
            }
            playRawSound(R.raw.correct);
            busDriver.showHelp(json.feedback[0]);
        } else {

            if (user_score > 0) {
                if (!blinkHandler.updateScore(user_score, blinkIndex, course, 0)) {
                    playRawSound(R.raw.points_lost);
                } else {
                    busDriver.updateScore(blinkHandler);
                }
            }
            playRawSound(R.raw.incorrect);
            busDriver.showHelp(json.feedback[1]);
        }
    }

}
