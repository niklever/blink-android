package com.blinktrainingsystem.blink.module.playcourse.presentation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.blinktrainingsystem.blink.R;
import com.blinktrainingsystem.blink.common.view.anytextview.Util;
import com.blinktrainingsystem.blink.data.model.pojo.Json;
import com.blinktrainingsystem.blink.data.model.pojo.Presentation;
import com.blinktrainingsystem.blink.module.playcourse.BaseBurstFragment;
import com.blinktrainingsystem.blink.util.BusDriver;
import com.blinktrainingsystem.blink.util.DeviceInfo;
import com.blinktrainingsystem.blink.util.Helper;
import com.blinktrainingsystem.blink.util.TrackerHelper;
import com.blinktrainingsystem.blink.util.ViewAnimator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import timber.log.Timber;

/**
 * Created by TALE on 9/30/2014.
 */
public class PresentationFragment extends BaseBurstFragment {

    private final AnimatorListenerAdapter animatorListener = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            addTitle();
        }
    };
    public final Handler handler = new Handler(Looper.getMainLooper());
    Json json;
    int index = 0;

    @InjectView(R.id.scroller)
    ScrollView scroller;
    @InjectView(R.id.contentView)
    LinearLayout contentView;
    @InjectView(R.id.dummy)
    View dummy;

    @Inject
    AssetManager assetManager;

    @Inject
    ViewAnimator viewAnimator;

    @Inject
    Picasso picasso;

    @Inject TrackerHelper trackerHelper;

    private Animator animator;
    ;
    private List<Presentation> presentation;
    private boolean scrolled;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_presentation, container, false);
    }

    @Override
    protected void startBurst() {
        super.startBurst();
        json = blink.loadJsonToObject();
        presentation = json == null ? null : json.presentation;
        if (json != null && presentation != null) {
            showImage();
            addTitle();
        }
        trackerHelper.presentationLoad();
    }

    @Override
    protected int getTimer() {
        return 0;
    }

    @Override
    protected void showFooterMenu() {
        super.showFooterMenu();
        BusDriver.withBus(bus).setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
    }

    public int firstItemTop;

    private void showImage() {
        final String image = json.image;
        if (!TextUtils.isEmpty(image)) {

            File imgFile = getLocalFile(image);

            if (imgFile.exists()) {

                final ImageView imageView = new ImageView(getActivity());
                final LinearLayout.LayoutParams ivLp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                ivLp.bottomMargin = 10;
                contentView.addView(imageView, ivLp);
                picasso.load(imgFile).into(
                        imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                Timber.d("onSuccess=> loaded image for %s", image);
//                        imageView.getLayoutParams().width = imageView.getWidth() / 2;
                                firstItemTop = imageView.getLayoutParams().height;
                            }

                            @Override
                            public void onError() {
                                Timber.e("onError=> load image for %s", image);
                            }
                        }
                );
            }
        }
    }

    List<Runnable> runnables = new ArrayList<Runnable>();

    @Override
    public void onPause() {
        super.onPause();
        if (animator != null) {
            Timber.d("onPause=> cancel animator");
            animator.removeAllListeners();
            animator.cancel();
        }
        for (Runnable runnable : runnables) {
            handler.removeCallbacks(runnable);
        }
    }

    @Inject
    DeviceInfo deviceInfo;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void addTitle() {
        Timber.d("addTitle=> index:%d", index);

        if (index < presentation.size()) {
            final Presentation item = presentation.get(index);
            String title = Helper.getStringWithReplacements(item.text);
            final int inAnimation = item.in;
            int alignment = item.alignment;
            int size = item.size;
            int font = item.font;

            int color1 = Color.BLACK;
            int color2 = Color.parseColor("#256ED7");
            int color3 = Color.parseColor("#FF9500");

            ArrayList<Integer> arrColor = new ArrayList<Integer>();
            arrColor.add(color1);
            arrColor.add(color2);
            arrColor.add(color3);

            ArrayList<String> arrFont = new ArrayList<String>();
            arrFont.add(getString(R.string.font_myriad_pro_regular));
            arrFont.add(getString(R.string.font_myriad_pro_bold));
            arrFont.add(getString(R.string.font_myriad_pro_italic));

            ArrayList<Integer> arrFontSize = new ArrayList<Integer>();
            arrFontSize.add(getResources().getInteger(R.integer.medium_presentation_size));
            arrFontSize.add(getResources().getInteger(R.integer.small_presentation_size));
            arrFontSize.add(getResources().getInteger(R.integer.large_presentation_size));


            String fontName = arrFont.get(font);
            int fontSize = arrFontSize.get(size);

            final TextView textView = new TextView(getActivity());
            textView.setLineSpacing(deviceInfo.getDimensionPixel(5), 1);
            textView.setText(title);
            if (item.color == null) {
                textView.setTextColor(arrColor.get(0));
            } else if (item.color.startsWith("#")) {
                textView.setTextColor(Color.parseColor(item.color));
            } else {
                textView.setTextColor(arrColor.get(Integer.parseInt(item.color)));
            }
            switch (alignment) {
                case 0:
                    textView.setGravity(Gravity.LEFT);
                    break;

                case 1:
                    textView.setGravity(Gravity.CENTER);
                    break;

                case 2:
                    // TODO: justify
                    textView.setGravity(Gravity.CENTER);
                    break;
            }


            Timber.d("fontSize: %d, fontName: %s", fontSize, fontName);
            Util.setTypeface(textView, fontName);
            textView.setTextSize(fontSize);
            contentView.addView(textView);
            textView.setVisibility(View.INVISIBLE);
            ((LinearLayout.LayoutParams) textView.getLayoutParams()).topMargin = deviceInfo.getDimensionPixel(16);

//            switch (inAnimation) {
//                case 0://Do nothing
//                    break;
//
//                case 1: //Fade
//                    textView.setAlpha(0);
//                    break;
//
//                case 2: //Slide right
//                    textView.setTranslationX(-1024);
//                    break;
//
//                case 3: //Slide left
//                    textView.setTranslationX(1024);
//                    break;
//
//                case 4: //Slide up
//                    textView.setTranslationY(768);
//                    break;
//
//                case 5: //Slide down
//                    textView.setTranslationY(-768);
//                    break;
//            }

            double delay;
            switch (item.time) {
                case 0:
                    delay = 0;
                    break;

                case 1:
                    delay = 0.25;
                    break;

                case 2:
                    delay = 0.5;
                    break;

                case 3:
                    delay = 1;
                    break;

                case 4:
                    delay = 2;
                    break;

                case 5:
                    delay = 3;
                    break;

                case 6:
                    delay = 4;
                    break;

                case 7:
                    delay = 5;
                    break;

                default:
                    delay = 10;
            }

            final int textViewIndex = contentView.indexOfChild(textView);
            Timber.d("title: %s, animation: %d, time: %f", title, inAnimation, delay);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    /* play sound */
                    final View view = contentView.getChildAt(textViewIndex);
                    final int top = view.getTop();
                    int scrollBy = 0;
                    final int padding = deviceInfo.getDimensionPixel(10);
                    if (!scrolled) {
                        if (top < dummy.getHeight()) {
                            if (view.getBottom() > dummy.getHeight()) {
                                scrollBy = view.getBottom() - dummy.getHeight() + padding;
                            }
                        } else {
                            scrollBy = top - dummy.getHeight() + view.getHeight() + padding;
                        }
                    } else {
                        scrollBy = view.getHeight() + padding;
                    }

                    Timber.d("view's height: %d, scrollBy: %d, timeStamp: %d", view.getHeight(), scrollBy, System.currentTimeMillis());
                    if (scrollBy > 0) {
                        scroller.scrollBy(0, scrollBy);
                        scrolled = true;
                    }

                    if (!TextUtils.isEmpty(item.sound)) {
                        Timber.d("Play sound %s", item.sound);
                        playSound(item.sound, false);
                    }

                    switch (inAnimation) {
                        case 0://Do nothing
                        case 1: //Fade
                            animator = viewAnimator.enterFadeIn(textView, 700, animatorListener);
                            break;

                        case 2: //Slide right
                            animator = viewAnimator.enterRight(textView, 700, animatorListener);
                            break;

                        case 3: //Slide left
                            animator = viewAnimator.enterLeft(textView, 700, animatorListener);
                            break;

                        case 4: //Slide up
                            animator = viewAnimator.enterBottom(textView, 700, animatorListener);
                            break;

                        case 5: //Slide down
                            animator = viewAnimator.enterTop(textView, 700, animatorListener);
                            break;
                    }
                }
            };
            handler.postDelayed(runnable, (long) (delay * 1000));
            runnables.add(runnable);
            index++;
        } else {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    scroller.scrollTo(0, 0);
                }
            };
            handler.postDelayed(runnable, 2000);
            runnables.add(runnable);
            busDriver.setFooterMenu(getString(R.string.txt_back), null, getString(R.string.txt_next));
        }
    }


}
